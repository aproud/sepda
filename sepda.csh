#!/bin/csh -f
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#	CIntEx v1.0
#
#	Written by Adam J. Proud (2016)
#	Contributors: D.E.C.K. Mackenzie, Z.A.M. Zielinski, B.J.H. Sheppard, J.K. Pearson
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#	The following code sets up and runs a CIntEx job as follows:
#
#	sepda input.inp output.out	OR
#	sepda input.inp
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Ensure that the user has specified an input file name
if ( $#argv < 1 ) then
	echo "User must specify the name of the input file."
	echo "Proper usage: sepda input.inp output.out"
	exit -1
endif

# Ensure that the user has used the proper input arguments i.e. input output
if ( $#argv > 2 ) then
	echo "User has supplied too many command line arguments."
	echo "Proper usage: sepda input.inp output.out"
	exit -1
endif

# Ensure that the specified input file exists
if ( ! -e ${1}) then
	echo "The input file does not exist. Your current working directory is: "
	echo `pwd`
	echo "Please ensure that this is the correct file path/name and try again."
	exit -1
endif

# Set infile to input.inp
set infile = $1

# Look for the line specifying the basis set

set udbs='0'			# To determine whether the name of the input file has to be passed to basisreader_abc.py

if ( `grep -i "BASIS" $infile | wc -l` == 1 ) then
	set basisset=`grep -i "basis" $infile | awk '{print $2}' | tr a-z A-Z`
	if ( $basisset == "UD") then
		echo "User has specified a user-defined basis set"
		echo 'but has not defined a basis set in the $basis section.'
		exit -1	
	endif
	if ( ! -e $SEPDABASIS/${basisset}.bas ) then
		echo "Basis set does not exist. Please ensure that"
		echo "you have entered a valid basis set and try again."
		exit -1
	endif
else if ( `grep -i "BASIS" $infile | wc -l` == 0 ) then
	echo "User has not specified a basis set. Please revise the"
	echo 'input file and try again.'
	exit -1	
else if ( `grep -i "BASIS" $infile | wc -l` == 2 ) then
	set basisset=`grep -i "basis" $infile | head -1 | awk '{print $2}' | tr a-z A-Z`
	if ( $basisset != "UD" ) then
		echo "User has not specified a user-defined basis set"
		echo "but has included a $basis section or has multiple."
		echo "lines defining a basis set. Please ensure the"
		echo "accuracy of the input file and try again."
		exit -1
	endif
	set udbs = '1'
else
	echo "User either has multiple lines indicating the basis set OR."
	echo 'has an incorrect $basis section. Please revise the input'
	echo "file and try again."
	exit -1
endif

# Look for the line specifying the type of localized orbitals


if ( `grep -i "mo_type" $infile | wc -l` == 0 ) then
	set mo="LMO"
	if ( `grep -i "wfn_file" $infile | wc -l` == 0 ) then
		echo "User has not provided the WFN file name."
       		echo 'Please add "WFN_FILE      filename" to $init section of input file'
       		exit -1
	else if ( ` grep -i "wfn_file" $infile | wc -l` == 1 ) then
       		set wfnSource=`grep -i "wfn_file" $infile | awk '{print $2}'`
       		if ( ! -e ${wfnSource} ) then
           		echo "The wfn file does not exist. Your current working directory is: "
           		echo `pwd`
           		echo "Please ensure that this is the correct file path/name and try again."
           		exit -1
       		endif

		python $SEPDABIN/x_wfn_coef.py $wfnSource $infile		# Generate the .wfn and .coef files from the GAMESS .dat file
		set rc = $?
		if ( $rc != 0) then
			echo "Failed to find .wfn file and/or LMO coefficients within WFN_FILE."
			echo "Please ensure that this is the proper file and try again."
			exit -1
		endif

		set wfnfile=`echo $infile | awk -F. '{print $1".s.tmp"}'`
		sed -i".bak" '/^\s*$/d' $wfnfile
		rm $wfnfile".bak"

		if ( `grep -i "coef_file" $infile | wc -l` == 0 ) then
			if ( $udbs == '1' ) then
				python $SEPDABIN/basisreader_lmo.py $basisset $wfnfile $infile
				set rc = $?
			else
				python $SEPDABIN/basisreader_lmo.py $basisset $wfnfile
				set rc = $?
			endif

			if ( $rc != 0 ) then
				echo "Formation of coefficient matrix failed. Please ensure that the basis"
				echo "set indicated in the input file matches the one used for the original"
				echo "calculation."
				exit -1
			endif

		else if ( ` grep -i "coef_file" $infile | wc -l` == 1 ) then
        		set coefSource=`grep -i "coef_file" $infile | awk '{print $2}'`
        		if ( $coefSource != $wfnSource ) then
            			echo "The user has specified both a wfn_file and a coefficient file for a"
				echo "calculation on LMOs and the file names do not match. For LMOs, this"
				echo "information should be contained within the same file."
            			echo "Please ensure the accuracy of the input file and try again."
            			exit -1
        		endif
			if ( $udbs == '1' ) then
				python $SEPDABIN/basisreader_lmo.py $basisset $wfnfile $infile
				set rc = $?
			else
				python $SEPDABIN/basisreader_lmo.py $basisset $wfnfile
				set rc = $?
			endif

			if ( $rc != 0 ) then
        		echo "Formation of coefficient matrix failed. Please ensure that the basis"
				echo "set indicated in the input file matches the one used for the original"
				echo "calculation."
        		exit -1
			endif
		else
        		echo "User has multiple lines specifying the name of the coefficient file."
        		echo "Please revise input file and try again."
        		exit -1
		endif
			
	else
    		echo "User has multiple lines specifying the WFN file."
    		echo "Please revise input file and try again."
    		exit -1
	endif

else if ( `grep -i "mo_type" $infile | wc -l` == 1 ) then
	set mo=`grep -i "mo_type" $infile | awk '{print $2}' | tr a-z A-Z`
	if ( $mo == LMO ) then							# IF calculation is for LMOs
		if ( `grep -i "wfn_file" $infile | wc -l` == 0 ) then
			echo "User has not provided the WFN file name."
        		echo 'Please add "WFN_FILE      filename" to $init section of input file'
        		exit -1
		else if ( ` grep -i "wfn_file" $infile | wc -l` == 1 ) then
        		set wfnSource=`grep -i "wfn_file" $infile | awk '{print $2}'`
        		if ( ! -e ${wfnSource} ) then
            			echo "The wfn file does not exist. Your current working directory is: "
            			echo `pwd`
            			echo "Please ensure that this is the correct file path/name and try again."
            			exit -1
        		endif

			python $SEPDABIN/x_wfn_coef.py $wfnSource $infile		# Generate the .wfn and .coef files from the GAMESS .dat file
			set rc = $?
			if ( $rc != 0) then
				echo "Failed to find .wfn file and/or LMO coefficients within WFN_FILE."
				echo "Please ensure that this is the proper file and try again."
				exit -1
			endif

			set wfnfile=`echo $infile | awk -F. '{print $1".s.tmp"}'`
			sed -i".bak" '/^\s*$/d' $wfnfile
			rm $wfnfile".bak"

			if ( `grep -i "coef_file" $infile | wc -l` == 0 ) then
				if ( $udbs == '1' ) then
					python $SEPDABIN/basisreader_lmo.py $basisset $wfnfile $infile
					set rc = $?
				else
					python $SEPDABIN/basisreader_lmo.py $basisset $wfnfile
					set rc = $?
				endif

				if ( $rc != 0 ) then
        			echo "Formation of coefficient matrix failed. Please ensure that the basis"
					echo "set indicated in the input file matches the one used for the original"
					echo "calculation."
        				exit -1
				endif

			else if ( ` grep -i "coef_file" $infile | wc -l` == 1 ) then
				set coefSource=`grep -i "coef_file" $infile | awk '{print $2}'`
        			if ( $coefSource != $wfnSource ) then
                			echo "The user has specified both a wfn_file and a coefficient file for a"
					echo "calculation on LMOs and the file names do not match. For LMOs, this"
					echo "information should be contained within the same file."
                			echo "Please ensure the accuracy of the input file and try again."
                			exit -1
        			endif

				if ( $udbs == '1' ) then
					python $SEPDABIN/basisreader_lmo.py $basisset $wfnfile $infile
					set rc = $?
				else
					python $SEPDABIN/basisreader_lmo.py $basisset $wfnfile
					set rc = $?
				endif

				if ( $rc != 0 ) then
        				echo "Formation of coefficient matrix failed. Please ensure that the basis"
					echo "set indicated in the input file matches the one used for the original"
					echo "calculation."
        				exit -1
				endif
			else
        			echo "User has multiple lines specifying the name of the coefficient file."
        			echo "Please revise input file and try again."
        			exit -1
			endif
			
		else
        		echo "User has multiple lines specifying the WFN file."
        		echo "Please revise input file and try again."
        		exit -1
		endif


	else if ( $mo == NBO ) then						# IF calculation is for NBOs
        	if ( `grep -i "wfn_file" $infile | wc -l` == 0 ) then
            		echo "User has not provided the WFN file name."
            		echo 'Please add "WFN_FILE      filename" to $init section of input file'
            		exit -1
        	else if ( ` grep -i "wfn_file" $infile | wc -l` == 1 ) then
            		set wfnSource=`grep -i "wfn_file" $infile | awk '{print $2}'`
            		if ( ! -e ${wfnSource} ) then
                		echo "The wfn file does not exist. Your current working directory is: "
                		echo `pwd`
                		echo "Please ensure that this is the correct file path/name and try again."
                		exit -1
            		endif

			python $SEPDABIN/x_wfn_NBO.py $wfnSource $infile
			set rc = $?
			if ( $rc != 0 ) then
				echo "Unable to find .wfn file contents within WFN_FILE. Please check"
				echo "to see if this is the proper file that contains the .wfn file"
				echo "and try again."
				exit -1
			endif
			set wfnfile=`echo $infile | awk -F. '{print $1".s.tmp"}'`
        	else
            		echo "User has multiple lines specifying the WFN file."
            		echo "Please revise input file and try again."
            		exit -1
        	endif

		if ( `grep -i "coef_file" $infile | wc -l` == 0 ) then
			echo "User has not provided the file name for the file containing the NBO coefficients."
			echo 'Please add "COEFS         filename" to $init section of input file'
     	  		exit -1
		else if ( ` grep -i "coef_file" $infile | wc -l` == 1 ) then
        		set nbofile=`grep -i "coef_file" $infile | awk '{print $2}'`
        		if ( ! -e ${nbofile} ) then
            			echo "The coefficient file does not exist. Your current working directory is: "
            			echo `pwd`
            			echo "Please ensure that this is the correct file path/name and try again."
            			exit -1
        		endif

			if ( $udbs == '1' ) then
				python $SEPDABIN/basisreader_nbo.py $basisset $nbofile $wfnfile $infile
				set rc = $?
			else
				python $SEPDABIN/basisreader_nbo.py $basisset $nbofile $wfnfile
				set rc = $?
			endif

			if ( $rc != 0 ) then
        			echo "Formation of coefficient matrix failed. Please ensure that the basis"
				echo "set indicated in the input file matches the one used for the original"
				echo "calculation."
        			exit -1
			endif
		else
        		echo "User has multiple lines specifying the name of the coefficient file."
        		echo "Please revise input file and try again."
        		exit -1
		endif

	else if ( $mo == CMO ) then						# IF calculation is for CMOs
        	if ( `grep -i "wfn_file" $infile | wc -l` == 0 ) then
            		echo "User has not provided the WFN file name."
            		echo 'Please add "WFN_FILE      filename" to $init section of input file'
            		exit -1
        	else if ( `grep -i "wfn_file" $infile | wc -l` == 1 ) then
            		set wfnSource=`grep -i "wfn_file" $infile | awk '{print $2}'`
            		if ( ! -e ${wfnSource} ) then
                		echo "The wfn file does not exist. Your current working directory is: "
                		echo `pwd`
                		echo "Please ensure that this is the correct file path/name and try again."
                		exit -1
            		endif
			
            		python $SEPDABIN/x_coef_CMO.py $wfnSource $infile        # Generate the .wfn and .coef files from the GAMESS .dat file
			set rc = $?
			if ( $rc != 0) then
				echo "Unable to find CMO coefficients within WFN_FILE. Please ensure"
				echo "that the .wfn file is accurate and try again."
				exit -1
			endif

            		set wfnfile=`echo $infile | awk -F. '{print $1".s.tmp"}'`
			sed -i".bak" '/^\s*$/d' $wfnfile
            		rm $wfnfile".bak"

            		if ( `grep -i "coef_file" $infile | wc -l` == 0 ) then
				if ( $udbs == '1' ) then
                    			python $SEPDABIN/basisreader_cmo.py $basisset $wfnfile $infile
                    			set rc = $?
				else
                    			python $SEPDABIN/basisreader_cmo.py $basisset $wfnfile
                    			set rc = $?
				endif
                    
                		if ( $rc != 0 ) then
                    			echo "Formation of coefficient matrix failed. Please ensure that the basis"
                    			echo "set indicated in the input file matches the one used for the original"
                    			echo "calculation."
                    			exit -1
                		endif
            		else if ( ` grep -i "coef_file" $infile | wc -l` == 1 ) then
                		set coefSource=`grep -i "coef_file" $infile | awk '{print $2}'`
                		if ( $coefSource != $wfnSource ) then
                    			echo "The user has specified both a wfn_file and a coefficient file for a"
                    			echo "calculation on CMOs and the file names do not match. For CMOs, this"
                    			echo "information should all be contained in the .wfn file."
                    			echo "Please ensure the accuracy of the input file and try again."
                    			exit -1
                		endif

				if ( $udbs == '1' ) then
                   			python $SEPDABIN/basisreader_cmo.py $basisset $wfnfile $infile
                   			set rc = $?
				else
                   			python $SEPDABIN/basisreader_cmo.py $basisset $wfnfile
                   			set rc = $?
				endif
            
                		if ( $rc != 0 ) then
                    			echo "Formation of coefficient matrix failed. Please ensure that the basis"
                    			echo "set indicated in the input file matches the one used for the original"
                    			echo "calculation."
                    			exit -1
                		endif
        		else
            			echo "User has multiple lines specifying the name of the coefficient file."
            			echo "Please revise input file and try again."
            			exit -1
        		endif

        	else
            		echo "User has multiple lines specifying the WFN file."
            		echo "Please revise input file and try again."
            		exit -1
        	endif

	else									# IF Local does not equal LMO, NBO, or CMO
		echo "User has specified an invalid mo_type. Only valid options are LMO, NBO, and CMO."
		echo "Please revise input file and try again."
		exit -1
	endif
else
	echo "User has multiple lines specifying the type of molecular orbital."
	echo "Please revise input file and try again."
	exit -1
endif


# Look for the line specifying the type of pair distribution to calculate. If it is not supplied, the default
# calculation is set to pdtype=1 which is a position intracule.

if ( `grep -i "pd_type" $infile | wc -l` == 0 ) then
	set pdtype=1
#	echo "Position intracule to be calculated."
else if ( `grep -i "pd_type" $infile | wc -l` == 1 ) then
	if ( `grep -i "pd_type" $infile | awk '{print $2}'` == 1 ) then
		set pdtype=`grep -i "pd_type" $infile | awk '{print $2}'`
#		echo "Position intracule to be calculated."
	else if ( `grep -i "pd_type" $infile | awk '{print $2}'` == 2 ) then
		set pdtype=`grep -i "pd_type" $infile | awk '{print $2}'`
#		echo "Momentum intracule to be calculated."
	else if ( `grep -i "pd_type" $infile | awk '{print $2}'` == 3 ) then
		set pdtype=`grep -i "pd_type" $infile | awk '{print $2}'`
#		echo "Posmom intracule to be calculated."
	else if ( `grep -i "pd_type" $infile | awk '{print $2}'` == 4 ) then
		set pdtype=`grep -i "pd_type" $infile | awk '{print $2}'`
#		echo "Position extracule to be calculated."
	else if ( `grep -i "pd_type" $infile | awk '{print $2}'` == 5 ) then
		set pdtype=`grep -i "pd_type" $infile | awk '{print $2}'`
#		echo "Momentum extracule to be calculated."
	else if ( `grep -i "pd_type" $infile | awk '{print $2}'` == 6 ) then
		set pdtype=`grep -i "pd_type" $infile | awk '{print $2}'`
#		echo "Posmom extracule to be calculated."
	else if ( `grep -i "pd_type" $infile | awk '{print $2}'` == 7 ) then
		set pdtype=`grep -i "pd_type" $infile | awk '{print $2}'`
#		echo "Multi-dimensional position extracule to be calculated."
	else
		echo "Invalid input for pd_type. Please revise input file and try again."
		exit -1
	endif
else
	echo "User has multiple lines specifying what type of pair disribution to calculate."
	echo "Please revise input file and try again."
	exit -1
endif

# Look for the line specifying the number of the molecular orbital to calculate.

if ( `grep -i "mo_num" $infile | wc -l` == 0 ) then
	echo "User has not specified a molecular orbital number for the calculation."
	echo 'Please revise input file and try again.'
	exit -1
else if ( ` grep -i "mo_num" $infile | wc -l` == 1 ) then
	set MOnum=`grep -i "mo_num" $infile | awk '{print $2}'`
else
	echo "User has multiple lines specifying molecular orbital number."
	echo "Please revise input file and try again."
	exit -1
endif


# Look for the line specifying the number of points. If none, specify the default for the given pdtype.

if ( `grep -i "npts" $infile | wc -l` == 0 ) then
	if ( $pdtype == 1 ) then
		set npts = 250
	else if ( $pdtype == 2 ) then
		set npts = 350
	else if ( $pdtype == 3 ) then
		set npts = 500
	else if ( $pdtype == 4 ) then
		set npts = 300
	else if ( $pdtype == 5 ) then
		set npts = 400
	else if ( $pdtype == 6 ) then
		set npts = 600
	else if ( $pdtype == 7 ) then
		set npts = 150
	else
		echo "Should never have reached this point. FIRST"
		exit -1
	endif

else if ( ` grep -i "npts" $infile | wc -l` == 1 ) then
	set npts=`grep -i "npts" $infile | awk '{print $2}'`
else
	echo "User has multiple lines specifying the number of grid points to evaluate."
	echo "Please revise input file and try again."
	exit -1
endif


# Look for the line specifying the scale factor, R. If none, specify the default for the given pdtype.

if ( `grep -i "scale" $infile | wc -l` == 0 ) then
	if ( $pdtype == 1 || $pdtype == 4 ) then
		set scale = 4.0
	else if ( $pdtype == 2 || $pdtype == 5 ) then
		set scale = 5.0
	else if ( $pdtype == 3 || $pdtype == 6 ) then
		set scale = 7.5
	else if ( $pdtype == 7 ) then
		set scale = 4.0
	else
		echo "Should never have reached this point. MID"
		exit -1
	endif

else if ( ` grep -i "scale" $infile | wc -l` == 1 ) then
	set scale=`grep -i "scale" $infile | awk '{print $2}'`
else
	echo "User has multiple lines specifying the Mura-Knowles scale factor."
	echo "Please revise input file and try again."
	exit -1
endif

# Determine whether output file was specified. If not set to input.out, otherwise output.out.
if ( $#argv == 1 ) then
	set outname=`echo $infile | awk -F. '{print $1}'`
	if ( $pdtype == 1 ) then
		set outfile="ipos_"$outname"_MO"$MOnum".dat"
	else if ( $pdtype == 2 ) then
		set outfile="imom_"$outname"_MO"$MOnum".dat"
	else if ( $pdtype == 3 ) then
		set outfile="ipm_"$outname"_MO"$MOnum".dat"
	else if ( $pdtype == 4 ) then
		set outfile="epos_"$outname"_MO"$MOnum".dat"
	else if ( $pdtype == 5 ) then
		set outfile="emom_"$outname"_MO"$MOnum".dat"
	else if ( $pdtype == 6 ) then
		set outfile="epm_"$outname"_MO"$MOnum".dat"
	else if ( $pdtype == 7 ) then
		set outfile="evpos_"$outname"_MO"$MOnum".dat"
	endif
else
	set outfile = $2	
endif


set tempFile1=`echo $infile | awk -F. '{print $1".s.tmp"}'`
set tempFile2=`echo $infile | awk -F. '{print $1".s.tmp2"}'`

# Run the appropriate program based on the pd_type that was previously specified
if ( $pdtype == 1 ) then
	$SEPDABIN/ipos.out $wfnfile $outfile $mo $MOnum $scale $npts
	cat $outfile > $tempFile2
	echo "----------------------------------------------------------------------------------------------------" > $outfile
	echo "!" >> $outfile
	echo "!       Please cite the following:" >> $outfile
	echo "!" >> $outfile
	echo "!       Hollett, J.W.; Gill, P.M.W. J. Chem. Phys. 2011, 13, 2972-2978." >> $outfile
	echo "!       Proud, A.J.; Sheppard, B.J.H.; Pearson, J.K. J. Am. Chem. Soc. 2018, 140, 219-228." >> $outfile
	echo "!" >> $outfile
	echo "----------------------------------------------------------------------------------------------------" >> $outfile
	echo "" >> $outfile
	echo "       u                     P(u)" >> $outfile
	echo "" >> $outfile
	cat $tempFile2 >> $outfile
	rm $tempFile2
else if ( $pdtype == 2 ) then
	$SEPDABIN/imom.out $wfnfile $outfile $mo $MOnum $scale $npts
	cat $outfile > $tempFile2
	echo "----------------------------------------------------------------------------------------------------" > $outfile
	echo "!" >> $outfile
	echo "!       Please cite the following:" >> $outfile
	echo "!" >> $outfile
	echo "!       Hollett, J.W.; Gill, P.M.W. J. Chem. Phys. 2011, 13, 2972-2978." >> $outfile
	echo "!       Proud, A.J.; Sheppard, B.J.H.; Pearson, J.K. J. Am. Chem. Soc. 2018, 140, 219-228." >> $outfile
	echo "!" >> $outfile
	echo "----------------------------------------------------------------------------------------------------" >> $outfile
	echo "" >> $outfile
	echo "       v                     M(v)" >> $outfile
	echo "" >> $outfile
	cat $tempFile2 >> $outfile
	rm $tempFile2
else if ( $pdtype == 3 ) then
	$SEPDABIN/ipm.out $wfnfile $outfile $mo $MOnum $scale $npts
	cat $outfile > $tempFile2
	echo "----------------------------------------------------------------------------------------------------" > $outfile
	echo "!" >> $outfile
	echo "!       Please cite the following:" >> $outfile
	echo "!" >> $outfile
	echo "!       Hollett, J.W.; Gill, P.M.W. J. Chem. Phys. 2011, 13, 2972-2978." >> $outfile
	echo "!       Proud, A.J.; Sheppard, B.J.H.; Pearson, J.K. J. Am. Chem. Soc. 2018, 140, 219-228." >> $outfile
	echo "!" >> $outfile
	echo "----------------------------------------------------------------------------------------------------" >> $outfile
	echo "" >> $outfile
	echo "       k                     F(k)" >> $outfile
	echo "" >> $outfile
	cat $tempFile2 >> $outfile
	rm $tempFile2
else if ( $pdtype == 4 ) then
	$SEPDABIN/epos.out $wfnfile $outfile $mo $MOnum $scale $npts
	cat $outfile > $tempFile2
	echo "----------------------------------------------------------------------------------------------------" > $outfile
	echo "!" >> $outfile
	echo "!       Please cite the following:" >> $outfile
	echo "!" >> $outfile
	echo "!       Hollett, J.W.; Gill, P.M.W. J. Chem. Phys. 2011, 13, 2972-2978." >> $outfile
	echo "!       Proud, A.J.; Sheppard, B.J.H.; Pearson, J.K. J. Am. Chem. Soc. 2018, 140, 219-228." >> $outfile
	echo "!" >> $outfile
	echo "----------------------------------------------------------------------------------------------------" >> $outfile
	echo "" >> $outfile
	echo "       R                     E(R)" >> $outfile
	echo "" >> $outfile
	cat $tempFile2 >> $outfile
	rm $tempFile2
else if ( $pdtype == 5 ) then
	$SEPDABIN/emom.out $wfnfile $outfile $mo $MOnum $scale $npts
	cat $outfile > $tempFile2
	echo "----------------------------------------------------------------------------------------------------" > $outfile
	echo "!" >> $outfile
	echo "!       Please cite the following:" >> $outfile
	echo "!" >> $outfile
	echo "!       Hollett, J.W.; Gill, P.M.W. J. Chem. Phys. 2011, 13, 2972-2978." >> $outfile
	echo "!       Proud, A.J.; Sheppard, B.J.H.; Pearson, J.K. J. Am. Chem. Soc. 2018, 140, 219-228." >> $outfile
	echo "!" >> $outfile
	echo "----------------------------------------------------------------------------------------------------" >> $outfile
	echo "" >> $outfile
	echo "       P                     E(P)" >> $outfile
	echo "" >> $outfile
	cat $tempFile2 >> $outfile
	rm $tempFile2
#else if ( $pdtype == 6 ) then
#	$SEPDABIN/epm.out $wfnfile $outfile $mo $MOnum $scale $npts
#	cat $outfile > temp
#	echo "----------------------------------------------------------------------------------------------------" > $outfile
#	echo "!" >> $outfile
#	echo "!       Please cite the following:" >> $outfile
#	echo "!" >> $outfile
#	echo "!       Hollett, J.W.; Gill, P.M.W. J. Chem. Phys. 2011, 13, 2972-2978." >> $outfile
#	echo "!       Proud, A.J.; Sheppard, B.J.H.; Pearson, J.K. J. Am. Chem. Soc. 2018, 140, 219-228." >> $outfile
#	echo "!" >> $outfile
#	echo "----------------------------------------------------------------------------------------------------" >> $outfile
#	echo "" >> $outfile
#	echo "       k                     F(k)" >> $outfile
#	echo "" >> $outfile
#	cat temp >> $outfile
#	rm temp
else if ( $pdtype == 7 ) then
	$SEPDABIN/evpos.out $wfnfile $outfile $mo $MOnum $scale $npts
	cat $outfile > $tempFile2
	echo "----------------------------------------------------------------------------------------------------" > $outfile
	echo "!" >> $outfile
	echo "!       Please cite the following:" >> $outfile
	echo "!" >> $outfile
	echo "!       Thakkar, A.J. Moore, N.J. Int. J. Quantum Chem. 1981, 20, 393-400." >> $outfile
	echo "!       Proud, A.J.; Sheppard, B.J.H.; Pearson, J.K. J. Am. Chem. Soc. 2018, 140, 219-228." >> $outfile
	echo "!" >> $outfile
	echo "----------------------------------------------------------------------------------------------------" >> $outfile
	echo "" >> $outfile
	echo "       Rx                Ry                Rz               E(R)" >> $outfile
	echo "" >> $outfile
	cat $tempFile2 >> $outfile
	rm $tempFile2
else
	echo "Should never have reached this point. LAST"
	exit -1	
endif

rm $tempFile1

exit(0)
