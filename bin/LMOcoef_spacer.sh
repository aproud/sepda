sed -i".bak" 's/-/ -/g' $1
sed -i".bak" 's/E -/E-/g' $1
sed -i".bak" 's/ /  /g' $1
sed -i".bak" 's/  -/ -/g' $1
sed -i".bak" 's/^ //g' $1
rm *.bak
