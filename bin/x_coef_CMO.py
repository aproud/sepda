#!/usr/local/bin/python

# Usual header declarations
import re
import os
import sys
from optparse import OptionParser       # we want to use options
parser=OptionParser()

# to pass the molecule name w/o extension : we don't need this anymore
parser.add_option("-n", "--ligand", dest="lig", help="Name of the molecule or atom",metavar="FILE")

(options,args)=parser.parse_args()
lig=options.lig

###########################################################
# The main function : Get WFN and localized coefficients 
# from GAMESS data file
###########################################################

DIR='.'

file = open(sys.argv[1],'r')
inlines = file.readlines()
file.close()
outfile = []
outtext = []
(lig,extension)=os.path.splitext(sys.argv[2])

# Get WFN File
count=0
for line in inlines:
  if (line.find('GAUSSIAN     '))>-1:
    for x in range (-1, 1000000):
	line = inlines[x+count]
	if (line.find('MO  1   '))>-1:
	    break
	else:
	    outtext.append(line)

  count=count+1
name = lig

count=0
for line in inlines:
  if (line.find('MO  1   '))>-1:
    for x in range (1,1000000):
	line = inlines[x+count]
	if (line.find('END DATA'))>-1:
	    break
	elif (line.find('MO'))>-1:
	    count=count
	else:
	    outtext.append(line)
  count = count+1

outfile=open(name+'.s.tmp','w')
outfile.writelines(outtext)
outfile.close()


