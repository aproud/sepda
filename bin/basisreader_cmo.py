#!/usr/local/bin/python

# Usual header declarations
import re
import os
import sys
from optparse import OptionParser       # we want to use options
parser=OptionParser()

# to pass the molecule name w/o extension : we don't need this anymore
parser.add_option("-n", "--ligand", dest="lig", help="Name of the molecule or atom",metavar="FILE")

(options,args)=parser.parse_args()
lig=options.lig

##################################################################################################################
#
# 	The main function: Read in contraction schemes and contraction coefficients for arbitrary basis sets. 
#                     
##################################################################################################################

BASISdef = sys.argv[1]

DIR='.'
#for f in os.listdir(DIR):
#  if f.endswith('.out'):
#    (lig,extension)=os.path.splitext(f)
#outfile=open(BASIS+'.dat','w')

# Read the basis set file
BASIS=BASISdef.upper()
if (BASIS=="UD"):
	file = open(sys.argv[3],'r')
	inlines = file.readlines()
	file.close
	
	count=0
	startCount=0
	for line in inlines:
		if (line.find('$basis'))>-1:
			basisCount=count+1
			startCount=1
		elif (startCount==1):
			if (line.find('$end'))>-1:
				basisEnd=count
				break
			count=count+1
		else:
			count=count+1
	inlines_basis=inlines[basisCount:basisEnd+1]
else:
	basisdir=os.environ["SEPDABASIS"]
	file = open(basisdir+'/'+BASIS+'.bas','r')
	inlines = file.readlines()
	file.close()

	count=0
	for line in inlines:
		if line in ['\n','\r\n']:
			break
		else:
			count=count+1
	inlines_basis=inlines[1:count]

# Read in the .coef and .wfn files

file = open(sys.argv[2],'r')
inlines_wfn1 = file.readlines()
file.close()

types=[]
expos=[]

numPrim = 0
numNuc = 0
NAO = 0

inlines_wfn=inlines_wfn1[1:]
inlines_types=[]
inlines_expos=[]

curLine=0
for line in inlines_wfn:
	if line.split()[0]=='GAUSSIAN':
		NAO=int(line.split()[1])
		numPrim=int(line.split()[4])
		numNuc=int(line.split()[6])

inlines_atoms=inlines_wfn[ 1 : numNuc + 1]
curAtom=[]

for line in inlines_atoms:
 	curAtom.append(line.split()[0])

linesTypes = numPrim / 20
rTypes = numPrim % 20
if rTypes != 0:
	linesrTypes=1
else:
	linesrTypes=0

linesExpos = numPrim / 5
rExpos = numPrim % 5
if rExpos != 0:
	linesrExpos=1
else:
	linesrExpos=0

# Define the portion of inlines_wfn that contains the orbital types		
startTypes = numNuc + linesTypes + linesrTypes + 1
endTypes = startTypes + linesTypes + linesrTypes
inlines_types = inlines_wfn[ startTypes : endTypes ]

# Define the portion of inlines_wfn that contains the exponents
startExpos = endTypes
endExpos = startExpos + linesExpos + linesrExpos
inlines_expos = inlines_wfn[ startExpos : endExpos ]

# Store all the types in the types array    
lineCount = 1
for line in inlines_types:
      if ( linesrTypes == 0 ):
	if ( lineCount < linesTypes + linesrTypes + 1 ):
		i = 0
		while ( i < 20 ):
			types.append(int(line.split()[i+2]))
			i = i + 1 
	lineCount = lineCount + 1

      else:
	if ( lineCount < linesTypes + linesrTypes ):
		i = 0
		while ( i < 20 ):
			types.append(int(line.split()[i+2]))
			i = i + 1 
	else:
		i = 0
		while ( i < rTypes ):
			types.append(int(line.split()[i+2]))
			i = i + 1
	lineCount = lineCount + 1

# Store all of the exponents in the expos array
lineCount = 1
for line in inlines_expos:
      if ( linesrExpos == 0 ):
	if ( lineCount < linesExpos + linesrExpos + 1 ):
		i = 0
		while ( i < 5 ):
			expos.append(line.split()[i+1])
			i = i + 1 
	lineCount = lineCount + 1

      else:
	if ( lineCount < linesExpos + linesrExpos ):
		i = 0
		while ( i < 5 ):
			expos.append(line.split()[i+1])
			i = i + 1 
	else:
		i = 0
		while ( i < rExpos ):
			expos.append(line.split()[i+1])
			i = i + 1

	lineCount = lineCount + 1

# Define some variables and arrays for the allocation of the contraction coefficients
counter=1
outtext = []
#contCoefs=[]
contValue=[]
orbType=[]
inlines_contraction=[]
lmoCoef=[]
moCoefs=[]
lengthCoefs = len(curAtom)

i=0
nbf=0
while (i < lengthCoefs):
 	linesToSkip = 2
	atomFound = 0
	breakTime = 0
	atomDone = 0
	counter = 1

  	for line in inlines_basis:
    	  if line.split()[0]==curAtom[i]:
		linesToSkip=1
		atomFound=1
    	  if (linesToSkip==0 and atomFound==1):

        	if (line.find(' ZZZZ'))>-1:
                	atomFound=0
			breakTime=1
		else:
			atomDone=1

			if (line.split()[0] in ['S']):
				contraction=int(line.split()[1])
#				inlines_contraction=inlines_basis[counter:counter+contraction]
				nbf = nbf + 1
				contValue.append(contraction)
				orbType.append("S")
#				for line in inlines_contraction:
#					contCoefs.append(float(line.split()[1]))

			elif (line.split()[0] in ['P']):
				contraction=int(line.split()[1])
#				inlines_contraction=inlines_basis[counter:counter+contraction]
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				nbf = nbf + 3
				orbType.append("px")
				orbType.append("py")
				orbType.append("pz")
#				for line in inlines_contraction:
#					contCoefs.append(float(line.split()[1]))
#					contCoefs.append(float(line.split()[1]))
#					contCoefs.append(float(line.split()[1]))

			elif (line.split()[0] in ['D']):
				contraction=int(line.split()[1])
#				inlines_contraction=inlines_basis[counter:counter+contraction]
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				orbType.append("dxx")
				orbType.append("dyy")
				orbType.append("dzz")
				orbType.append("dxy")
				orbType.append("dxz")
				orbType.append("dyz")
				nbf = nbf + 6
#				for line in inlines_contraction:
#					contCoefs.append(float(line.split()[1]))
#					contCoefs.append(float(line.split()[1]))
#					contCoefs.append(float(line.split()[1]))
#					contCoefs.append(float(line.split()[1]))
#					contCoefs.append(float(line.split()[1]))
#					contCoefs.append(float(line.split()[1]))

			elif (line.split()[0] in ['SP']):
				contraction=int(line.split()[1])
#				inlines_contraction=inlines_basis[counter:counter+contraction]
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				orbType.append("S")
				orbType.append("px")
				orbType.append("py")
				orbType.append("pz")
				nbf = nbf + 1
				nbf = nbf + 3

#				for line in inlines_contraction:
#					contCoefs.append(float(line.split()[1]))

#				for line in inlines_contraction:
#					contCoefs.append(float(line.split()[2]))
#					contCoefs.append(float(line.split()[2]))
#					contCoefs.append(float(line.split()[2]))

			elif (line.split()[0] in ['SPD']):
				contraction=int(line.split()[1])
#				inlines_contraction=inlines_basis[counter:counter+contraction]
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)
				contValue.append(contraction)

				orbType.append("S")
				orbType.append("px")
				orbType.append("py")
				orbType.append("pz")
				orbType.append("dxx")
				orbType.append("dyy")
				orbType.append("dzz")
				orbType.append("dxy")
				orbType.append("dxz")
				orbType.append("dyz")

				nbf = nbf + 1
				nbf = nbf + 3
				nbf = nbf + 6

#				for line in inlines_contraction:
#					contCoefs.append(float(line.split()[1]))

#				for line in inlines_contraction:
#					contCoefs.append(float(line.split()[2]))
#					contCoefs.append(float(line.split()[2]))
#					contCoefs.append(float(line.split()[2]))

#				for line in inlines_contraction:
#					contCoefs.append(float(line.split()[3]))
#					contCoefs.append(float(line.split()[3]))
#					contCoefs.append(float(line.split()[3]))
#					contCoefs.append(float(line.split()[3]))
#					contCoefs.append(float(line.split()[3]))
#					contCoefs.append(float(line.split()[3]))

			elif (line.split()[0] in ['F','G','H','I']):
				print "This code cannot calculate integrals involving orbitals with angular momenta greater than l=2"

			else:
				counter = counter

	  counter = counter + 1

	  if (atomFound==1):
		linesToSkip = 0
	  elif (atomDone==1 and breakTime==1):
		i=i+1
		break

# Just finished the big loop. Now define variables for the allocation of nbo coefs

ncoef = numPrim

conLmoCoefs=[]
lmoRem= ncoef % 5
if ( lmoRem == 0 ):
    	lmoRows = ncoef / 5
else:
	lmoRows = ncoef / 5 + 1

# Read in the lmo coefficients that apply to contracted orbitals
inlines_lmos=inlines_wfn[ endExpos : endExpos + lmoRows*NAO + 1 ]
for line in inlines_lmos:
        i = 0
        elements=line.count(".")
	while ( i < elements ):
		conLmoCoefs.append(line.split()[i])
		i = i + 1


# Uncontract the coefficients such that they can be multiplied by the corresponding contraction coefficients
#count = 0
#j = 0
#while ( j < len(conLmoCoefs) ):
#        i = 0
#        while (i < contValue[count]):
#                lmoCoef.append(float(conLmoCoefs[j]))
#                i=i+1
#        count = count + 1
#	if (count % nbf == 0):
#		count = 0
#        j = j + 1

##########################

bfStart=[1]
i=0
while (i < nbf - 1):
        bfStart.append(bfStart[i]+contValue[i])
        i=i+1

#x = 0
#while (x < NAO ):
# 	i=0
#	while (i < nbf ):
# 	       	if (orbType[i]=="px"):
#       	         	tempLmoCoef=[]
#      	 	        j = 0
#       		        while (j < contValue[i]):
#       		               	tempLmoCoef.append(lmoCoef[x*ncoef + bfStart[i] + 0*contValue[i] - 1 + j])
#       		                tempLmoCoef.append(lmoCoef[x*ncoef + bfStart[i] + 1*contValue[i] - 1 + j])
#       		                tempLmoCoef.append(lmoCoef[x*ncoef + bfStart[i] + 2*contValue[i] - 1 + j])
#				j = j + 1
#			j = 0
#			while (j < 3 * contValue[i] ):
#                           	lmoCoef[x*ncoef + bfStart[i] - 1 + j] = tempLmoCoef[j]
#                        	j = j + 1

#        	if (orbType[i]=="dxx"):
#       	         	tempLmoCoef=[]
#      	 	        j = 0
#       		        while (j < contValue[i]):
#       		               	tempLmoCoef.append(lmoCoef[x*ncoef + bfStart[i] + 0*contValue[i] - 1 + j])
#       		                tempLmoCoef.append(lmoCoef[x*ncoef + bfStart[i] + 1*contValue[i] - 1 + j])
#       		                tempLmoCoef.append(lmoCoef[x*ncoef + bfStart[i] + 2*contValue[i] - 1 + j])
#       		               	tempLmoCoef.append(lmoCoef[x*ncoef + bfStart[i] + 3*contValue[i] - 1 + j])
#       		                tempLmoCoef.append(lmoCoef[x*ncoef + bfStart[i] + 4*contValue[i] - 1 + j])
#       		                tempLmoCoef.append(lmoCoef[x*ncoef + bfStart[i] + 5*contValue[i] - 1 + j])
#				j = j + 1
#			j = 0
#			while (j < 6 * contValue[i] ):
#                           	lmoCoef[x*ncoef + bfStart[i] - 1 + j] = tempLmoCoef[j]
#                        	j = j + 1
#        	i = i + 1
#	x = x + 1
# Calculate the final MO coefficients for the LMOs
i=0
j=0
counter=0
while (i < NAO):
        j=0
        while (j < ncoef):
                moCoefs.append(conLmoCoefs[counter])
                counter = counter + 1
                j = j + 1
        i = i + 1


# Output the MO coefficients to a .coef file to be read in by the main fortran program
#i=0
#counter=0
#while (i < NAO):
#        j=0
#        while (j<len(contCoefs)):
#        while (j < ncoef):
#                outtext.append("%15.8E"% (moCoefs[i*len(contCoefs)+j]))
#                outtext.append("%15.8E"% (moCoefs[i*ncoef+j]))
#                counter = counter + 1
#                if (j==len(contCoefs)-1 or counter%5==0):
#                if (j==ncoef-1 or counter%5==0):
#                        outtext.append("\n")
#                        counter=0
#                j=j+1
#        i = i + 1

#outfile.writelines(outtext)
#outfile.close()

# We're now going to rearrange the types and exponents from the .wfn
 # file so that it will be compatible with the main code i.e. px, py, pz in sequence    
i=0
while (i < nbf ):
        if (orbType[i]=="px"):
		tempTypes=[]
		tempExpos=[]
		j = 0
                while (j < contValue[i]):
                        tempTypes.append(2)
                        tempTypes.append(3)
                        tempTypes.append(4)
			tempExpos.append(expos[bfStart[i] + 0*contValue[i] - 1 +j])
			tempExpos.append(expos[bfStart[i] + 1*contValue[i] - 1 +j])
			tempExpos.append(expos[bfStart[i] + 2*contValue[i] - 1 +j])
                        j = j + 1
		j = 0
		while (j < 3*contValue[i]):
			types[bfStart[i] - 1 + j] = tempTypes[j]
			expos[bfStart[i] - 1 + j] = tempExpos[j]
			j = j + 1

        if (orbType[i]=="dxx"):
		tempTypes=[]
		tempExpos=[]
		j = 0
                while (j < contValue[i]):
                        tempTypes.append(5)
                        tempTypes.append(6)
                        tempTypes.append(7)
                        tempTypes.append(8)
                        tempTypes.append(9)
                        tempTypes.append(10)
			tempExpos.append(expos[bfStart[i] + 0*contValue[i] - 1 +j])
			tempExpos.append(expos[bfStart[i] + 1*contValue[i] - 1 +j])
			tempExpos.append(expos[bfStart[i] + 2*contValue[i] - 1 +j])
			tempExpos.append(expos[bfStart[i] + 3*contValue[i] - 1 +j])
			tempExpos.append(expos[bfStart[i] + 4*contValue[i] - 1 +j])
			tempExpos.append(expos[bfStart[i] + 5*contValue[i] - 1 +j])
                        j = j + 1
		j = 0
		while (j < 6*contValue[i]):
			types[bfStart[i] - 1 + j] = tempTypes[j]
			expos[bfStart[i] - 1 + j] = tempExpos[j]
			j = j + 1

        i = i + 1

# We're now going to do the same thing except for the molecular orbital coefficients
# so that it will be compatible with the main code i.e. px, py, pz in sequence

k=0
while ( k < NAO ):
  i=0
  while ( i < nbf  ):
        if (orbType[i]=="px"):
                tempCoefs=[]
                j = 0
                while (j < contValue[i]):
                        tempCoefs.append(moCoefs[k*ncoef + bfStart[i] + 0*contValue[i] - 1 +j])
                        tempCoefs.append(moCoefs[k*ncoef + bfStart[i] + 1*contValue[i] - 1 +j])
                        tempCoefs.append(moCoefs[k*ncoef + bfStart[i] + 2*contValue[i] - 1 +j])
                        j = j + 1
                j = 0
                while (j < 3*contValue[i]):
                        moCoefs[k*ncoef + bfStart[i] - 1 + j] = tempCoefs[j]
                        j = j + 1

        if (orbType[i]=="dxx"):
                tempCoefs=[]
                j = 0
                while (j < contValue[i]):
                        tempCoefs.append(moCoefs[k*ncoef + bfStart[i] + 0*contValue[i] - 1 +j])
                        tempCoefs.append(moCoefs[k*ncoef + bfStart[i] + 1*contValue[i] - 1 +j])
                        tempCoefs.append(moCoefs[k*ncoef + bfStart[i] + 2*contValue[i] - 1 +j])
                        tempCoefs.append(moCoefs[k*ncoef + bfStart[i] + 3*contValue[i] - 1 +j])
                        tempCoefs.append(moCoefs[k*ncoef + bfStart[i] + 4*contValue[i] - 1 +j])
                        tempCoefs.append(moCoefs[k*ncoef + bfStart[i] + 5*contValue[i] - 1 +j])
                        j = j + 1
                j = 0
                while (j < 6*contValue[i]):
                        moCoefs[k*ncoef + bfStart[i] - 1 + j] = tempCoefs[j]
                        j = j + 1

        i = i + 1
  k = k + 1

# Now it's time to rewrite the .wfn file with the reordered types and exponents
wfnText=[]
fT = 0
fE = 0
lineCount=0

for line in inlines_wfn1:
	if (lineCount <= startTypes ):
	    wfnText.append(line)
	    lineCount = lineCount + 1
	elif ( lineCount >= startTypes + 1 and lineCount < endTypes + 1 ):
	    if ( linesrTypes == 0 ):
		if ( fT < linesTypes + 1 ):
			wfnText.append("TYPE ASSIGNMENTS    ")
			i=0
			while(i < 20): 
				wfnText.append("%3d"% (types[20*fT + i]))
				i = i + 1
			wfnText.append('\n')
			fT = fT + 1

	    else:
		if ( fT < linesTypes ):
			wfnText.append("TYPE ASSIGNMENTS    ")
			i=0
			while(i < 20): 
				wfnText.append("%3d"% (types[20*fT + i]))
				i = i + 1
			wfnText.append('\n')
			fT = fT + 1
		if ( fT == linesTypes ):
			wfnText.append("TYPE ASSIGNMENTS    ")
			i = 0
			while (i < rTypes):
				wfnText.append("%3d"% (types[20*fT + i]))
				i = i +1
			wfnText.append('\n')
			fT = fT + 1
	    lineCount = lineCount + 1	

	elif (lineCount >= startExpos + 1 and lineCount < endExpos + 1 ):
	    if ( linesrExpos == 0 ):
		if ( fE < linesExpos + 1 ):
			wfnText.append("EXPONENTS ")
			i=0
			while(i < 5): 
				wfnText.append(" %s"% (expos[5*fE + i]))
				i = i + 1
			wfnText.append('\n')
			fE = fE + 1

	    else:
		if ( fE < linesExpos ):
			wfnText.append("EXPONENTS ")
			i=0
			while(i < 5): 
				wfnText.append(" %s"% (expos[5*fE + i]))
				i = i + 1
			wfnText.append('\n')
			fE = fE + 1
		if ( fE == linesExpos ):
			wfnText.append("EXPONENTS ")
			i = 0
			while (i < rExpos):
				wfnText.append(" %s"% (expos[5*fE + i]))
				i = i +1
			wfnText.append('\n')
			fE = fE + 1
	    lineCount = lineCount + 1

	elif ( lineCount == endExpos + 1 ):
		i = 0
		counter = 0
		while (i < NAO ):
			j=0
			while ( j < ncoef ):
				wfnText.append("%15.8E"% (float(moCoefs[i*ncoef+j])))
				counter = counter + 1
				if ( j==ncoef-1 or counter%5==0):
					wfnText.append("\n")
					counter=0
				j = j + 1
			i = i + 1
	else:
		wfnText.append(line)


outfile=open(sys.argv[2],'w')
outfile.writelines(wfnText)
outfile.close()

