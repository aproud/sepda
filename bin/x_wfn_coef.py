#!/usr/local/bin/python

# Usual header declarations
import re
import os
import sys
from optparse import OptionParser       # we want to use options
parser=OptionParser()

# to pass the molecule name w/o extension : we don't need this anymore
parser.add_option("-n", "--ligand", dest="lig", help="Name of the molecule or atom",metavar="FILE")

(options,args)=parser.parse_args()
lig=options.lig

###########################################################
# The main function : Get WFN and localized coefficients 
# from GAMESS data file
###########################################################

DIR='.'

file = open(sys.argv[1],'r')
inlines = file.readlines()
file.close()
outfile = []
outfile2 = []
outtext = []
outtext2 = []
(lig,extension)=os.path.splitext(sys.argv[2])

count = 0

# Get Localized Orbital Coefficients 
for line in inlines:
  if (line.find('BOYS LOCALIZED ORBITALS'))>-1:
    for x in range (2, 1000000):
        line = inlines[x+count]
        if (line.find('$END'))>-1:
	    break
        else:
	    line = line[5:]
	    for i in range(0, len(line), 15):
	    	outtext.append(line[i:i+15]+' ')

  elif (line.find('PIPEK-MEZEY POPULATION LOCALIZED ORBITALS'))>-1:
    for x in range (2, 1000000):
        line = inlines[x+count]
  	if (line.find('$END'))>-1:
	    break
        else:
	    line = line[5:]
	    for i in range(0, len(line), 15):
	    	outtext.append(line[i:i+15]+' ')

  elif (line.find('ENERGY LOCALIZED ORBITALS'))>-1:
    for x in range (2, 1000000):
	line = inlines[x+count]
	if (line.find('$END'))>-1:
	    break
	else:
	    line = line[5:]
	    for i in range(0, len(line), 15):
	    	outtext.append(line[i:i+15]+' ')

  count=count+1

# Get WFN File
count=0
for line in inlines:
  if (line.find('GAUSSIAN        '))>-1:
    for x in range (-1, 1000000):
	line = inlines[x+count]
	if (line.find('MO  1   '))>-1:
	    break
	else:
	    outtext2.append(line)
  count=count+1

name = lig
outfile=open(name+'.s.tmp','w')
outfile.writelines(outtext2)
outfile.writelines(outtext)
outfile.close()
