!---------------------------------------------------------------------------------------------
!	Calculate a Position Intracule (using data from a .wfn file)  
!
!		Written by A.J. Proud, B.J.H Sheppard, D.E.C.K. Mackenzie,
!		Z.A.M. Zielinski and J.K. Pearson (2016) 
!
!       Calling method:
!       ./outfile wfnFile.wfn intFile.int COEFS.coef Molecular_Orbital_#
!
!---------------------------------------------------------------------------------------------

MODULE SHELL_PAIR_DETERMINATION

	integer sSetCount, pSetCount, dSetCount
	integer curShellPairCallIndex, shellPairCallListSize
	integer shellPairListSize, curShellPairIndex
	integer shellPairSSoffset, shellPairPSoffset, shellPairPPoffset
	integer shellPairDSoffset, shellPairDPoffset, shellPairDDoffset
	integer SSshellPairCount, PSshellPairCount, PPshellPairCount
	integer DSshellPairCount, DPshellPairCount, DDshellPairCount
	integer size_ssss, size_psss, size_ppss, size_psps, size_ppps
	integer size_pppp, size_dsss, size_dpss, size_dsps, size_ddss
	integer size_dsds, size_dpps, size_dspp, size_ddps, size_dpds
	integer size_ddds, size_dppp, size_ddpp, size_dpdp, size_dddp
	integer size_dddd
	
!	Arrays for building our shell pair list, and consequently call lists
	integer, dimension(:), allocatable :: sOptions
	integer, dimension(:), allocatable :: pOptions
	integer, dimension(:), allocatable :: dOptions
	
!	Arrays for Shell Pair lists and to call positionRR.f90
	integer*8, dimension(:), allocatable :: shellPairList
	integer*8, dimension(:), allocatable :: shellPairCallList	

END MODULE



MODULE HF2PDM
! Here we will store the 2D array used for the 2 particle density matrix.

	double precision, dimension(:,:), allocatable :: pMatrixFromCoefs		! 2D array used for 2 particle density matrix

END MODULE

	program position_intracule

	USE SHELL_PAIR_DETERMINATION
	USE HF2PDM
!	USE DIMENSIONS

	implicit none
	integer i, j, ia, ib, ic, id, a, b, c, d 
	integer FileStat, MO, curLine			
	character g*8, gaussian*8, input*50, output*50, localized*50, orbital*50	! These characters are for error messages, and input/output files.
	character moType*3,chR*50, chMAXLINES*50 
!	integer lineStart, lineEnd
!	character chlineStart*50, chlineEnd*50						! lineStart and lineEnd as read from command line	
	integer NMO, oNPRIM, NNUC							! Number of molecular orbitals, primitives, and nuclei in the system

	integer, dimension(:), allocatable :: classSizeList				! Array to track class sizes (for more accurate addition) 
	integer classSizeCounter							! A counter for the array above

	double precision cor_a(3), cor_b(3), cor_c(3), cor_d(3)				! Gives the coordinates of a,b,c,d
	integer getAngMom, getGaussSum1to, getPrefactorCode, ang_a, ang_b, ang_c, ang_d	! Gives the angular momentum of a,b,c,d
	double precision PHFtotal, PHFab, PHFcd, PHFad, PHFbc, HFT, P, curClassSum	! 2 particle density matrix elements
	double precision expo_a, expo_b, expo_c, expo_d					! Exponents of centres a,b,c,d for the current call to positionRR.f90
	integer curType
	integer psInCall, dsInCall, posint_size, posint_index				! Prefactor values
	integer aLoopIndex, bLoopIndex, cLoopIndex, dLoopIndex				! Prefactor values
	integer tempa, tempb, tempc, tempd						! Prefactor values
	integer curPrefactor, verbose							! Prefactor values
	double precision, dimension(1296) :: Pos_int 					! Position integral array returned from positionRR.f90
											! It has size 1296 because dddd would need 6^4 = 1296 indices
!-----------------------------------------------------------------------
!	MEGA ARRAY OFFSETS
!-----------------------------------------------------------------------

	integer sCount,pCount,dCount							! Count the number of each type of basis function s,p,d
	double precision, dimension(:), allocatable :: wfnMegaArray			! wfnMegaArray - a megaarray for the overall wavefunction
	double precision, dimension(:), allocatable :: sArray				! sArray - an array for all of the s type atomic orbitals
	double precision, dimension(:), allocatable :: pArray				! pArray - an array for all of the p type atomic orbitals
	double precision, dimension(:), allocatable :: dArray				! dArray - an array for all of the d type atomic orbitals
	integer megaArraySize, curInt
	integer typeOffset, exponentOffset, centreOffset, coefOffset, angMomOffset	! Note that only the coefs for that one orbital will be used.
	integer NPRIM, curNewRef, sIndex, pIndex, dIndex!, zeroCount			! Values to track the removal of zeroes, and splitting of mega array.

!-----------------------------------------------------------------------
! ADDITIONAL PARAMETERS
!-----------------------------------------------------------------------

	double precision pi, XU, U, MAXLINES, R 		 	! XU, MAXLINES, R scale the output plot (U vs P(u))
	integer*8 MAXNPRIM
        parameter (pi = 3.14159265358979323846264338327950288419716939937510d0)
!	parameter (MAXLINES = 250.0d0)					! Parameters for Mura-Knowles grid
!	parameter (R = 3.00d0)						! Can set up such that IMAX and R are entered by User.
!	parameter (TOLERANCE = 1.0D-6) 					! SET COEF TOLERANCE TO 1 x 10 ^ -6
	parameter (MAXNPRIM = 10000)					! MAXNPRIM is the max # of primitives and MUST be a power of 10.
									! It's used to separate basis function #s in the shellPairCallList.
!-----------------------------------------------------------------------
! Getting the arguments from the command line.
!-----------------------------------------------------------------------

	call GETARG(1, input)						! First argument from the command line is the input .wfn file
	call GETARG(2, output)						! Second argument is the output .dat file
!	call GETARG(3, localized)					! Third argument is the LMO coef file (USE 'NA' FOR NON LOCALIZED)
	call GETARG(3, moType)
	call GETARG(4, orbital)						! Fourth argument is the orbital being calculated (0=total intracule)
	call GETARG(5, chR)						! Fifth argument is the Mura-Knowles scale factor 
	call GETARG(6, chMAXLINES)					! Sixth argument is the number of points in the Mura-Knowles grid
!	call GETARG(7, chlineStart)					! Seventh argument is the line to begin calculating the intracule
!	call GETARG(8, chlineEnd)					! Eigth argument is the line to end calculating the intracule
	read (orbital,*) MO						! Converts the string "orbital" into the integer, "MO"
	read (chR,*) R
	read (chMAXLINES,*) MAXLINES
!	read (chlineStart,*) lineStart					! Converts the string "chlineStart" into the integer "lineStart"
!	read (chlineEnd,*) lineEnd					! Converts the string "chlineEnd" into the integer "lineEnd"

!-----------------------------------------------------------------------
! Attempting to open the .wfn file for reading (unit 7)
!-----------------------------------------------------------------------

	open(unit = 7,file = input,IOSTAT=FileStat,status='old')			! Unit 7 is the .wfn file
	  if (FileStat > 0) then																
	    print *, 'Error opening input file:'												
	    print *, ' -> Please ensure the .wfn file is correct, ' 
	    print *, '    and the filename does not contain illegal characters.'
	    stop
	  endif

!----------------------------------------------------------------------------------------------------------------------------------
! Opening the output file to write intracule to (unit 8)
!----------------------------------------------------------------------------------------------------------------------------------

	open(unit = 8,file = output)							! Unit 8 is the output file that u and P(u) are written to

!----------------------------------------------------------------------------------------------------------------------------------
! Opening the localized coefficient file (unit 10)
!----------------------------------------------------------------------------------------------------------------------------------

!	open(unit = 10,file = localized,IOSTAT=FileStat,status='old')			! Unit 10 is the localized .coef file
!	  if (FileStat > 0) then
!	    print *, 'Error opening localized orbital coefficients file:'
!	    print *, ' -> Please ensure the file is correct, ' 
!	    print *, '    and the filename does not contain illegal characters.'
!	    stop
!	  endif

!----------------------------------------------------------------------------------------------------------------------------------
!	Read .wfn file
!----------------------------------------------------------------------------------------------------------------------------------

	read (unit = 7, FMT = 100) G, NMO, oNPRIM, NNUC					! Reads NMO, NPRIM, NNUC from input
	gaussian = 'GAUSSIAN'
	if (g.ne.gaussian) then								! ERROR if 'GAUSSIAN' is not on the second line of the wfn file
	  print *, 'ERROR: Invalid wfn file type (no GAUSSIAN statement)'
	  print *, '	    -> Ensure the input is a .wfn file.'
	  stop
	endif

!----------------------------------------------------------------------------------------------------------------------------------
!	ALLOCATE THE MEGA ARRAY NOW
!----------------------------------------------------------------------------------------------------------------------------------

!	The array will have 3*NNUC indices for x,y,z coordinates, plus 5*NPRIM indices for the centre, type,
!	exponent, coef and ang mom of each basis function

	megaArraySize = 3*NNUC + 4*oNPRIM + oNPRIM*NMO !
	ALLOCATE(wfnMegaArray(1:megaArraySize))

! 	Offsets to reference back to the data later
	centreOffset = 3*NNUC 					! The centres are located immediately after the coordinates (3*NNUC)
	typeOffset = centreOffset + oNPRIM 			! The types are located NPRIM spaces after the centres begin
	exponentOffset = typeOffset + oNPRIM 			! The exponents are located NPRIM spaces after the types begin
	coefOffset = exponentOffset + oNPRIM	 		! The coefs are located NPRIM spaces after the exponents begin
	angMomOffset = coefOffset + oNPRIM*NMO			! The ang moms are located NPRIM spaces after the coefs for that MO begin.

	do i = 1, 3*NNUC-2,3
	    read (7,104) wfnMegaArray(i:i+2)						! Reads the coordinates of each nucleus from .wfn
	enddo
	
	read (7, 101) (wfnMegaArray(centreOffset+i),i=1,onprim)				! Reads the centre assignments
	read (7, 101) (wfnMegaArray(typeOffset+i),i=1,onprim)				! Reads the type assignments
	read (7, 102) (wfnMegaArray(exponentOffset+i),i=1,onprim)			! Reads the exponents

	if (localized /='NA') then
	  do j = 1, NMO
!	    read (10, 103) (wfnMegaArray(i),i=coefOffset+(j-1)*onprim+1,coefOffset+j*onprim)
	    read (7, 103) (wfnMegaArray(i),i=coefOffset+(j-1)*onprim+1,coefOffset+j*onprim)
	  enddo
	endif

	do j = 1, oNPRIM
	    wfnMegaArray(angMomOffset+j) = getAngMom(wfnMegaArray(typeOffset+j))
	enddo

!	Input formats:
100 	format (/a8,11x,I4,15x,I5,16x,I4) 						! 100 is for G,NMO, oNPRIM, NNUC
											! Read 8 characters ('GAUSSIAN'), skip 11 characters
											! Read 4 digits, skip 15, read 5, skip 16, read 4

104	format (25x,f11.8,1x,f11.8,1x,f11.8)                  				! 104 is for reading in coordinates


101	format (20x,20e3.0)								! 101 is for centre and type assignments
											! Skip 20 characters [20x], then read 20 3 character long integers

102	format (10x,5E14.7)								! 102 is the specific format used to read the exponents
											! Skip 11 spaces and then read in 5 doubles of the form e14.8

103	format (5(E15.8))								! 103 is the specific format used to read in coefficients
											! Read in 5 doubles of the format e15.9
!----------------------------------------------------------------------
!	Normalize localized coefficients, if necessary
!----------------------------------------------------------------------

	if (moType/='CMO') then
	    do i = 1, onprim
		curType = nint(wfnMegaArray(typeOffset+i))
	  	if ( curType < 0 .or. curType > 10 ) then
	    	    print *, 'ERROR: The present version of the code cannot handle integrals involving f-orbitals.'
	    	    stop
	  	endif
		select case (curType)
		case (1)
	   	    do j = 0, NMO - 1
			wfnMegaArray(coefOffset+j*onPrim+i) = wfnMegaArray(coefOffset+j*onPrim+i)* &
				(((2.0d0/pi)**(3.0d0/4.0d0))*((wfnMegaArray(exponentOffset+i))**(3.0d0/4.0d0)))
	    	    enddo
		case( 2:4 )
	    	    do j = 0, NMO - 1
			wfnMegaArray(coefOffset+j*onPrim+i) = wfnMegaArray(coefOffset+j*onPrim+i)* & 
				2*(((2.0d0/pi)**(3.0d0/4.0d0))*((wfnMegaArray(exponentOffset+i))**(5.0d0/4.0d0)))
	    	    enddo
		case ( 5:7 )
	    	    do j = 0, NMO - 1 
			wfnMegaArray(coefOffset+j*onPrim+i) = wfnMegaArray(coefOffset+j*onPrim+i)* &
				4*((((2.0d0/pi)**(3.0d0/4.0d0))*((wfnMegaArray(exponentOffset+i))**(7.0d0/4.0d0)))/(dsqrt(3.0d0)))		
	    	    enddo
		case ( 8:10 )
	    	    do j = 0, NMO - 1
			wfnMegaArray(coefOffset+j*onPrim+i) = wfnMegaArray(coefOffset+j*onPrim+i)* &
				4*(((2.0d0/pi)**(3.0d0/4.0d0))*((wfnMegaArray(exponentOffset+i))**(7.0d0/4.0d0)))
	    	    enddo
		end select
	    enddo
	endif

!`````````````````````````````````````````````````````````````````````````````````````````````````````````````
! So now that the angular momenta are in the megaarray, we should scan through and count them.
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

	scount = 0
	pcount = 0
	dcount = 0

	do i = 1, oNPRIM
		curInt = nint(wfnMegaArray(angMomOffset + i))
		select case (curInt)
		case (0)						! For s-type basis functions
			scount = scount + 1
		case (1)						! For p-type basis functions
			pcount = pcount + 1
		case (2)						! For d-type basis functions
			dcount = dcount + 1
		end select	
	enddo

	if ((MOD(pCount , 3).ne.0).OR.(MOD(dCount , 6).ne.0)) then
		print *, 'The wfn file is flawed. Ensure that polarization in previous calculation was set to (6d, 10f), not (5d, 7f).'
		stop
	endif

	sSetCount = scount
	pSetCount = pcount / 3
	dSetCount = dcount / 6	

!``````````````````````````````````````````````````````````````````````````````````````````````````````````````
! 	We will also build 3 subarrays for s, p, and d angular momenta.
!	These arrays are to keep track of the basis function number for each
!	type of orbital.
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

	if (scount.gt.0) then
	    ALLOCATE(sarray(1:scount))
	endif

	if (pcount.gt.0) then
	    ALLOCATE(parray(1:pcount))
	endif

	if (dcount.gt.0) then
	    ALLOCATE(darray(1:dcount))
	endif

	sindex = 1
	pindex = 1
	dindex = 1

	do i = 1, oNPRIM
		curInt = nint(wfnMegaArray(angMomOffset + i))
		select case (curInt)
		case (0) 							! Add to s array
			sArray(sIndex) = i
			sindex = sindex + 1
		case (1)							! Add to p array
			pArray(pIndex) = i
			pindex = pindex + 1
		case (2)							! Add to d array
			dArray(dIndex) = i
			dindex = dindex + 1
		end select	
	enddo

!----------------------------------------------------------------------
!	WE WILL NOW COMPUTE THE 2D ARRAY USED FOR THE HF2PDM	
!----------------------------------------------------------------------

! First we shall allocate the array.
	if (MO.ne.0) then
	    ALLOCATE(pMatrixFromCoefs(1:oNPRIM,1:oNPRIM))
	    do i = 1, oNPRIM
		do j = 1, oNPRIM
		    pMatrixFromCoefs(i,j) = 2 * wfnMegaArray(coefOffset+(MO-1)*onPrim+i) * wfnMegaArray(coefOffset+(MO-1)*onPrim+j)
		enddo
	    enddo
	endif

!----------------------------------------------------------------------
!	MAKE THE SHELL PAIR ARRAY FOR THE SYSTEM
!----------------------------------------------------------------------

!	To do this we will first need to determine the size of this
!	array. Then we can set up a few offsets to work with it.
!	From there we can work through 6 double loops to fill it.

!	Start out by getting the sizes of the different sections of
!	the shell pair array and building the offsets.

	SSshellPairCount = getGaussSum1to(sSetCount)
	PPshellPairCount = getGaussSum1to(pSetCount)
	DDshellPairCount = getGaussSum1to(dSetCount)

	PSshellPairCount = pSetCount * sSetCount
	DSshellPairCount = dSetCount * sSetCount
	DPshellPairCount = dSetCount * pSetCount

	shellPairSSoffset = 0
	shellPairPSoffset = shellPairSSoffset + SSshellPairCount
	shellPairPPoffset = shellPairPSoffset + PSshellPairCount
	shellPairDSoffset = shellPairPPoffset + PPshellPairCount
	shellPairDPoffset = shellPairDSoffset + DSshellPaircount
	shellPairDDoffset = shellPairDPoffset + DPshellPaircount

!	Allocate shell pair list
	shellPairListSize = SSshellPairCount + PSshellPairCount + PPshellPairCount + DSshellPairCount + DPshellPairCount + DDshellPairCount
	ALLOCATE(shellPairList(1:shellPairListSize))
	curShellPairIndex = 1

!````````````````````````````````````````````````````````````````````````````````````````
!	NOW LETS FILL THE SHELLPAIR ARRAY, USING 6 LOOPS (OPTIONAL BASED ON IF)
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

! [ss]
	if (SSshellPairCount.gt.0) then
		do i = 1, sSetCount
			do j = 1, i
				shellPairList(curShellPairIndex) = MAXNPRIM * sarray(i) + sarray(j)
				curShellPairIndex = curShellPairIndex + 1
			enddo			
		enddo
	endif

! [ps]
	if (PSshellPairCount.gt.0) then
		do i = 1, pSetCount
			do j = 1, sSetCount
				shellPairList(curShellPairIndex) = MAXNPRIM * parray((3*i) - 2) + sarray(j)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

! [pp]
	if (PPshellPairCount.gt.0) then

		do i = 1, pSetCount
			do j = 1, i
				shellPairList(curShellPairIndex) = MAXNPRIM * parray((3*i)-2) + parray((3*j)-2)
				curShellPairIndex = curShellPairIndex + 1
			enddo			
		enddo
	endif

! [ds]
	if (DSshellPairCount.gt.0) then
		do i = 1, dSetCount
			do j = 1, sSetCount
				shellPairList(curShellPairIndex) = MAXNPRIM * darray((6*i)-5) + sarray(j)
				curShellPairIndex = curShellPairIndex + 1
			enddo			
		enddo
	endif

! [dp]
	if (DPshellPairCount.gt.0) then
		do i = 1, dSetCount
			do j = 1, pSetCount
				shellPairList(curShellPairIndex) = MAXNPRIM * darray((6*i)-5) + parray((3*j)-2)
				curShellPairIndex = curShellPairIndex + 1
			enddo			
		enddo
	endif

! [dd]
	if (DDshellPairCount.gt.0) then
		do i = 1, dSetCount
			do j = 1, i
				shellPairList(curShellPairIndex) = MAXNPRIM * darray((6*i)-5) + darray((6*j)-5)
				curShellPairIndex = curShellPairIndex + 1
			enddo			
		enddo
	endif

! We need to build the proper lists of shell pairs to work with.
! Since [ssss] is symmetrical between the two atoms, we include the ab <= cd rule for [abcd] shell pair generation.
! So we need to build a list of shell pairs using units from 1 to scount (i = i <= scount) 

!	The shell pair list generated here should be : {11,12,13,22,23,33}, which in turn should be used to generate:
!	{1111,1112,1113,1122,1123,1133,1212,1213,1222,1223,1233,1313,1322,1323,1333,2222,2223,2233,2323,2333,3333}
!	Two rules are applied here, a <= b and c <= d, and as mentioned previously, because the atoms are symmetrical,
!	We add the rule whereby ab (represented as a*10 + b) <= cd (represented as c*10 + d) so ab must be <= cd to avoid doubles.
!	We will now generate these lists, so that we have the proper indices to use when calling Josh's code.
!	We only have to work with S basis functions here, so first let's figure out the shell
!	It makes sense to build the total shellPairArray here and now.
!	This array will have size: 
!	gaussSum1to(sSetCount) + gaussSum1to(pSetCount) + gaussSum1to(dSetCount) 
!	+ (sSetCount * pSetCount) + (sSetCount * dSetCount) + (pSetCount * dSetCount)
!	We need to perform the following work:
!	ss, ps, pp, ds, dp, dd
!	So to build the shell call list, we will add class groups one at a time.

!----------------------------------------------------------------------
!	Calculate Position Intracule (output P(u) and u)
!----------------------------------------------------------------------

!	First build the shellPairCallList, and then work with it at the end.
!	We must allocate shellPairCallList but we need the size of shellPairCallList first:

	size_ssss = getGaussSum1to(ssshellpaircount)				! size_ssss
	size_psss = psshellpaircount * ssshellpaircount				! size_psss
	size_ppss = ppshellpaircount * ssshellpaircount				! size_ppss
	size_psps = getGaussSum1to(psshellpaircount)				! size_psps
	size_ppps = ppshellpaircount * psshellpaircount				! size_ppps
	size_pppp = getGaussSum1to(ppshellpaircount)				! size_pppp
	size_dsss = dsshellpaircount * ssshellpaircount				! size_dsss
	size_dpss = dpshellpaircount * ssshellpaircount				! size_dpss
	size_dsps = dsshellpaircount * psshellpaircount				! size_dsps
	size_ddss = ddshellpaircount * ssshellpaircount				! size_ddss
	size_dsds = getGaussSum1to(dsshellpaircount)				! size_dsds
	size_dpps = dpshellpaircount * psshellpaircount				! size_dpps
	size_dspp = dsshellpaircount * ppshellpaircount				! size_dspp
	size_ddps = ddshellpaircount * psshellpaircount				! size_ddps
	size_dpds = dpshellpaircount * dsshellpaircount				! size_dpds
	size_ddds = ddshellpaircount * dsshellpaircount				! size_ddds
	size_dppp = dpshellpaircount * ppshellpaircount				! size_dppp
	size_ddpp = ddshellpaircount * ppshellpaircount				! size_ddpp
	size_dpdp = getGaussSum1to(dpshellpaircount)				! size_dpdp
	size_dddp = ddshellpaircount * dpshellpaircount				! size_dddp
	size_dddd = getGaussSum1to(ddshellpaircount)				! size_dddd

	shellPairCallListSize = size_ssss + size_psss + size_ppss + size_psps + size_ppps + size_pppp &
					  + size_dsss + size_dpss + size_dsps + size_ddss + size_dsds &
					  + size_dpps + size_dspp + size_ddps + size_dpds + size_ddds &
					  + size_dppp + size_ddpp + size_dpdp + size_dddp + size_dddd

	allocate(shellPairCallList(1:shellPairCallListSize))
	allocate(classSizeList(1:21))					! An array to add a full class at a time to increase accuracy! 
	
	classSizeList(1) = size_ssss
	classSizeList(2) = size_psss + classSizeList(1)
	classSizeList(3) = size_ppss + classSizeList(2)
	classSizeList(4) = size_psps + classSizeList(3)
	classSizeList(5) = size_ppps + classSizeList(4)
	classSizeList(6) = size_pppp + classSizeList(5)
	classSizeList(7) = size_dsss + classSizeList(6)
	classSizeList(8) = size_dpss + classSizeList(7)
	classSizeList(9) = size_dsps + classSizeList(8)
	classSizeList(10) = size_ddss + classSizeList(9)
	classSizeList(11) = size_dsds + classSizeList(10)
	classSizeList(12) = size_dpps + classSizeList(11)
	classSizeList(13) = size_dspp + classSizeList(12)
	classSizeList(14) = size_ddps + classSizeList(13)
	classSizeList(15) = size_dpds + classSizeList(14)
	classSizeList(16) = size_ddds + classSizeList(15)
	classSizeList(17) = size_dppp + classSizeList(16)
	classSizeList(18) = size_ddpp + classSizeList(17)
	classSizeList(19) = size_dpdp + classSizeList(18)
	classSizeList(20) = size_dddp + classSizeList(19)
	classSizeList(21) = size_dddd + classSizeList(20)

	curShellPairCallIndex = 1

! [ss ss]	
	do i = 1, SSshellPairCount
		do j = 1, i
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(i) + shellPairList(j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [ps ss]
	do i = 1, PSshellPairCount
		do j = 1, SSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPSoffset+i) + shellPairList(shellPairSSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [pp ss]
	do i = 1, PPshellPairCount
		do j = 1, SSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPPoffset+i) + shellPairList(shellPairSSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [ps ps]
	do i = 1, PSshellPairCount
		do j = 1, i	
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPSoffset+i) + shellPairList(shellPairPSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [pp ps]
	do i = 1, PPshellPairCount

		do j = 1, PSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPPoffset+i) + shellPairList(shellPairPSoffset+j)

			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [pp pp]
	do i = 1, PPshellPairCount
		do j = 1, i
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPPoffset+i) + shellPairList(shellPairPPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [ds ss]
	do i = 1, DSshellPairCount
		do j = 1, SSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) + shellPairList(shellPairSSoffset+j)			
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dp ss]
	do i = 1, DPshellPairCount
		do j = 1, SSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) + shellPairList(shellPairSSoffset+j)	
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [ds ps]
	do i = 1, DSshellPairCount
		do j = 1, PSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) + shellPairList(shellPairPSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dd ss]
	do i = 1, DDshellPairCount
		do j = 1, SSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) + shellPairList(shellPairSSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [ds ds]
	do i = 1, DSshellPairCount
		do j = 1, i
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) + shellPairList(shellPairDSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dp ps]
	do i = 1, DPshellPairCount
		do j = 1, PSshellPairCount			
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) + shellPairList(shellPairPSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [ds pp]
	do i = 1, DSshellPairCount
		do j = 1, PPshellPairCount			
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) + shellPairList(shellPairPPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dd ps]
	do i = 1, DDshellPairCount
		do j = 1, PSshellPairCount			
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) + shellPairList(shellPairPSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dp ds]
	do i = 1, DPshellPairCount
		do j = 1, DSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) + shellPairList(shellPairDSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dd ds]
	do i = 1, DDshellPairCount
		do j = 1, DSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) + shellPairList(shellPairDSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dp pp]
	do i = 1, DPshellPairCount
		do j = 1, PPshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) + shellPairList(shellPairPPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dd pp]
	do i = 1, DDshellPairCount
		do j = 1, PPshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) + shellPairList(shellPairPPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dp dp]
	do i = 1, DPshellPairCount
		do j = 1, i
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) + shellPairList(shellPairDPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dd dp]
	do i = 1, DDshellPairCount
		do j = 1, DPshellPairCount	
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) + shellPairList(shellPairDPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dd dd]
	do i = 1, DDshellPairCount
		do j = 1, i
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) + shellPairList(shellPairDDoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! Start using the shellPairCallList to call positionRR.f90
	do curLine = 1, MAXLINES

! Offsets to reference back to the data later
	  classSizeCounter = 1
	  P = 0.0D0
	  curClassSum = 0.0D0
	  xu = curLine/(MAXLINES + 1)
	  u = -R * (log(1 - (xu**3)))

	  do i = 1, shellPairCallListSize		

	    a = shellPairCallList(i) / MAXNPRIM ** 3
	    b = MOD((shellPairCallList(i) / MAXNPRIM ** 2), MAXNPRIM)
	    c = MOD((shellPairCallList(i) / MAXNPRIM), MAXNPRIM)
	    d = MOD((shellPairCallList(i)), MAXNPRIM)

	    cor_a = wfnMegaArray(3*NINT(wfnMegaArray(centreOffset+a))-2:3*NINT(wfnMegaArray(centreOffset+a)))	! coor(ocent(a), 1:3)
	    cor_b = wfnMegaArray(3*NINT(wfnMegaArray(centreOffset+b))-2:3*NINT(wfnMegaArray(centreOffset+b)))	! coor(ocent(b), 1:3)
	    cor_c = wfnMegaArray(3*NINT(wfnMegaArray(centreOffset+c))-2:3*NINT(wfnMegaArray(centreOffset+c)))	! coor(ocent(c), 1:3)
	    cor_d = wfnMegaArray(3*NINT(wfnMegaArray(centreOffset+d))-2:3*NINT(wfnMegaArray(centreOffset+d)))	! coor(ocent(d), 1:3)

	    ang_a = wfnMegaArray(angMomOffset+a)
	    ang_b = wfnMegaArray(angMomOffset+b)
	    ang_c = wfnMegaArray(angMomOffset+c)
	    ang_d = wfnMegaArray(angMomOffset+d)

!	With these ang values, we can determine the number of ps and ds in this current call.
!	With that knowledge, we can determine the size of the array.

	    psInCall = 0
	    dsInCall = 0

	    posint_size = 0

	    aLoopIndex = 0
	    bLoopIndex = 0
	    cLoopIndex = 0
	    dLoopIndex = 0

	    call determinePsAndDsInCall(ang_a,ang_b,ang_c,ang_d,aLoopIndex,bLoopIndex,cLoopIndex,dLoopIndex,psInCall,dsInCall,posint_size)

!	So now that we have the 4 loop indices, we are about to call the RR subroutine

	    expo_a = wfnMegaArray(exponentOffset + a)
	    expo_b = wfnMegaArray(exponentOffset + b)
	    expo_c = wfnMegaArray(exponentOffset + c)
	    expo_d = wfnMegaArray(exponentOffset + d)

!	We have gotten the exponent values, and we are now calling the RR subroutine

	    call PositionIntegral(cor_a,cor_b,cor_c,cor_d,expo_a,expo_b,expo_c,expo_d,ang_a,ang_b,ang_c,ang_d,u,verbose,Pos_int)
	    posint_index = 1

!	We should now do a loop through the 4 loop indices to account for all of the posint
!	values returned by the recurrence relation posint array.

		curPrefactor = getPrefactorCode(a,b,c,d,MAXNPRIM)		! We have 5 possible values here, 1 through 5. 

!	They mean the following:
!		1: group 1
!		2: group 1
!		3: group 1
!	No flip required, multiply by 2**(getPrefactorCode-1) and proceed.
!		4: group 2
!		5: group 2
!	A flip is required. For 4, multiply by 2, flip and run again, also multiplying by 2
!	For 5, multiply by 4, flip and run again, also multiplying by 4.

		do ia = 0,(aLoopIndex - 1)
		    do ib = 0,(bLoopIndex - 1)
			do ic = 0,(cLoopIndex - 1)
			    do id = 0,(dLoopIndex - 1)
				if (curPrefactor.ge.1.AND.curPrefactor.le.3) then
				! Within here, we will iterate through the posint values in Josh's array,
				! all the while determining the new coefficient values to multiply by,
				! and multiplying by the prefactor for the given sequence call to 
				! Josh's code, contracting to add the integrals to our overall value.
					tempa = a + ia	
					tempb = b + ib
					tempc = c + ic
					tempd = d + id

					if (MO == 0) then					! Calculate P(u) for full system
						PHFab = PHFtotal(tempa,tempb,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						PHFcd = PHFtotal(tempc,tempd,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						PHFad = PHFtotal(tempa,tempd,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						PHFbc = PHFtotal(tempb,tempc,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						HFT = (((2*PHFab*PHFcd)-(PHFad*PHFbc))/4.0d0)

					else if ((MO > 0).and.(MO <= oNPRIM)) then			! Calculate P(u) for a single MO
						PHFab = pMatrixFromCoefs(tempa,tempb)
	  					PHFcd = pMatrixFromCoefs(tempc,tempd)
  						PHFad = pMatrixFromCoefs(tempa,tempd)
  						PHFbc = pMatrixFromCoefs(tempb,tempc)
  						HFT = ((0.5d0*PHFab*PHFcd)-(0.250d0*PHFad*PHFbc))
					else
						print *, 'ERROR: You have entered an invalid value for molecular orbital.'
						stop
					endif

					curClassSum = curClassSum + HFT * pos_int(posint_index) * (2**(curPrefactor-1))	

				elseif(curPrefactor.eq.4.or.curPrefactor.eq.5) then

				! Within here, we will iterate through the posint values from the RR array,
				! all the while determining the new coefficient values to multiply by,
				! and multiplying by the prefactor for the given sequence call to the
				! RR code, contracting to add the integrals to our overall value.

					tempa = a + ia	
					tempb = b + ib
					tempc = c + ic
					tempd = d + id

					if (MO == 0) then					! Calculate P(u) for full system
						PHFab = PHFtotal(tempa,tempb,nmo,onprim,wfnMegaArray,coefOffset,nNuc)	
  						PHFcd = PHFtotal(tempc,tempd,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						PHFad = PHFtotal(tempa,tempd,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						PHFbc = PHFtotal(tempb,tempc,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						HFT = (((2*PHFab*PHFcd)-(PHFad*PHFbc))/4.0d0)

!					else if ((MO > 0).and.(MO <= NMO)) then			! Calculate P(u) for a single MO
					else if ((MO > 0).and.(MO <= oNPRIM)) then			! Calculate P(u) for a single MO
						PHFab = pMatrixFromCoefs(tempa,tempb)
	  					PHFcd = pMatrixFromCoefs(tempc,tempd)
  						PHFad = pMatrixFromCoefs(tempa,tempd)
  						PHFbc = pMatrixFromCoefs(tempb,tempc)
  						HFT = ((0.5d0*PHFab*PHFcd)-(0.250d0*PHFad*PHFbc))
					else
						print *, 'ERROR: You have entered an invalid value for molecular orbital.'
						stop
					endif

					curClassSum = curClassSum + HFT * pos_int(posint_index) * (2**(curPrefactor-3))							
!--------------------------------------------------------------------------------------------------------------------------------------------
!
!	THE FOLLOWING IS NOT A MISTAKE!
!	WE NEED TO FLIP ONE OF THE SETS, AND GET THE NEW HFT
!	VALUE, SO WE'LL FLIP A AND B.
!
!--------------------------------------------------------------------------------------------------------------------------------------------

					tempa = b + ib
					tempb = a + ia
					tempc = c + ic
					tempd = d + id

					if (MO == 0) then					! Calculate P(u) for full system
						PHFab = PHFtotal(tempa,tempb,nmo,onprim,wfnMegaArray,coefOffset,nNuc)	
  						PHFcd = PHFtotal(tempc,tempd,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						PHFad = PHFtotal(tempa,tempd,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						PHFbc = PHFtotal(tempb,tempc,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
  						HFT = (((2*PHFab*PHFcd)-(PHFad*PHFbc))/4.0d0)
!					else if ((MO > 0).and.(MO <= NMO)) then			! Calculate P(u) for a single MO
					else if ((MO > 0).and.(MO <= oNPRIM)) then			! Calculate P(u) for a single MO
						PHFab = pMatrixFromCoefs(tempa,tempb)
	  					PHFcd = pMatrixFromCoefs(tempc,tempd)
  						PHFad = pMatrixFromCoefs(tempa,tempd)
  						PHFbc = pMatrixFromCoefs(tempb,tempc)
  						HFT = ((0.5d0*PHFab*PHFcd)-(0.250d0*PHFad*PHFbc))
					else
						print *, 'ERROR: You have entered an invalid value for molecular orbital.'
						stop
					endif
					curClassSum = curClassSum + HFT * pos_int(posint_index) * (2**(curPrefactor-3))					
				endif	! end curPrefactor loop
				posint_index = posint_index + 1
		    	    enddo ! end id loop
			enddo ! end ic loop
	    	    enddo ! end ib loop
		enddo ! end ia loop

!	If we have finished all of the sequence calls for this class, then we can work on the next class

		if (i == classSizeList(classSizeCounter)) then			
		    P = P + curClassSum
		    curClassSum = 0.0D0
		    classSizeCounter = classSizeCounter + 1
		endif
	    enddo ! end do i = 1 -> shellPairCallListSize
	    write (8,110) u, P
	enddo ! end curLine loop

110	format (2(e15.8,8x))	! To write u and P(u) to the output .dat file


!**********************************************************************************************************************************
! Program end
!**********************************************************************************************************************************

  	end		! end program

!-------------------------------------------------------------------------------
!	FUNCTIONS
!-------------------------------------------------------------------------------

!```````````````````````````````````````````````````````````````````````````````
!	getGaussSum1to(n) gives the sum from 1 to n going by increments of 1
!	using the gaussian formula. 
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

	integer function getGaussSum1to(n)
	implicit none
	integer n, output
	output = 0
	output = (n * (n + 1)) / 2
	getGaussSum1to = output
	return  
	end

!```````````````````````````````````````````````````````````````````````````````
!	ang(x) takes a type assignment, and converts it to an angular momentum
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

	integer function getAngMom(x)
        implicit none
        double precision x
	integer y
	y=NINT(x)
        if (y == 1) then 
        	getAngMom = 0 
        else if ((y >= 2).AND.(y <= 4)) then
        	getAngMom = 1 
        else if ((y >= 5).AND.(y <= 10)) then 
        	getAngMom = 2 
	else
		print *, "Invalid basis function type. Ensure that no f-orbitals are in the basis set."		
        endif   
        return  
	end

!```````````````````````````````````````````````````````````````````````````````
!	determinePrefactor(x) takes a 4 digit input (representing an integral,
!	and determines the symmetry prefactor for it.
!	x is of the form [abcd] where abcd = 1000a + 100b + 10c + d
!	and a,b,c,d represent specific basis functions.
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

	integer function getPrefactorCode(valA,valB,valC,valD,MAXNPRIM)
        implicit none
        integer valA, valB, valC, valD
        integer degreeOfSymmetry1, degreeOfSymmetry2, degreeOfSymmetry3
        integer posintPrefactor, oneAtomConsistent
		integer*8 MAXNPRIM
		degreeOfSymmetry1 = 0  ! Atom 1
		degreeOfSymmetry2 = 0  ! Atom 2
		degreeOfSymmetry3 = 0  ! Both atoms
!	Is a == b?
        if (vala == valb) then 
          degreeOfSymmetry1 = 1
        else
        	degreeOfSymmetry1 = 2
        endif

!	Is c == d         
         if( valc == vald ) then
         	degreeOfSymmetry2 = 1
         else
         	degreeOfSymmetry2 = 2
         endif

!	Is ab == cd
	if((vala*MAXNPRIM+valb) == (valc*MAXNPRIM+vald)) then
		degreeOfSymmetry3 = 1
	else
		degreeOfSymmetry3 = 2
	endif
	
	posintPrefactor = degreeOfSymmetry1 * degreeOfSymmetry2 * degreeOfSymmetry3

	if(degreeOfSymmetry1 == 1 .or. degreeOfSymmetry2 == 1) then
		oneAtomConsistent = 1
	else
		oneAtomConsistent = 0
	endif

! If oneAtomConsistent = 1, then the situation is in group 1, and falls under one of the following:
! 	aaaa, bbaa, baaa, bcaa

	if(oneAtomConsistent == 1) then
		if(posintPrefactor == 1) then
			getPrefactorCode = 1
		elseif(posintPrefactor == 2) then
			getPrefactorCode = 2
		elseif(posintPrefactor == 4) then
			getPrefactorCode = 3
		endif	! End posintPrefactor if statement
	endif	! End if oneAtomConsistent == 1

! Otherwise, the situation is in group 2, and falls under one of the following:
!	baba, baca, abcd

	if(oneAtomConsistent == 0) then
		if(posintPrefactor == 4) then
			getPrefactorCode = 4
		elseif(posintPrefactor == 8) then
			getPrefactorCode = 5
		endif ! End posintPrefactor if statement
	endif ! End if oneAtomConsistent == 0
	return
	end

!``````````````````````````````````````````````````````````````````````````````````````````````
!	PHFTotal is a function that determines the proper scaling coefficient to 
!	multiply each integral by for a total molecular intracule.
!,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

	double precision function PHFtotal(x,y,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
        implicit none
        integer x, y, i, nMO, onPrim, coefOffset, nNuc
        double precision wfnMegaArray(3*nNuc+((nMO+4)*onPrim))

        PHFtotal = 0
        do i = 1, nMO
                PHFtotal = PHFtotal + (wfnMegaArray(coefOffset + (i-1)*onPrim + x) * wfnMegaArray(coefOffset + (i-1)*oNPRIM + y))
        enddo
        PHFtotal = PHFtotal * 2

        return
        end

!``````````````````````````````````````````````````````````````````````````````````````````````
	subroutine determinePsAndDsInCall(ang_a,ang_b,ang_c,ang_d,aLI,bLI,cLI,dLI,psInCall,dsInCall,posint_size)
!``````````````````````````````````````````````````````````````````````````````````````````````

! LI here stands for Loop Index
! aLoopIndex represents the number of dimensions to consider for the ath basis function
! The same is true for bLI,cLI, and dLI for bth,cth, and dth basis function respectively
	implicit none	
	integer ang_a,ang_b,ang_c,ang_d
	integer psInCall, dsInCall
	integer aLI,bLI,cLI,dLI
	integer posint_size

! a Loop Index
	select case (ang_a)
	case(0)
		aLI = 1	
	case(1)
		aLI = 3
		psInCall = psInCall + 1
	case(2)
		aLI = 6
		dsInCall = dsInCall + 1
	end select

! b Loop Index
	select case (ang_b)
	case(0)
		bLI = 1	
	case(1)
		bLI = 3
		psInCall = psInCall + 1
	case(2)
		bLI = 6
		dsInCall = dsInCall + 1
	end select

! c Loop Index
	select case (ang_c)
	case(0)
		cLI = 1	
	case(1)
		cLI = 3
		psInCall = psInCall + 1
	case(2)
		cLI = 6
		dsInCall = dsInCall + 1
	end select

! d Loop Index
	select case (ang_d)
	case(0)
		dLI = 1	
	case(1)
		dLI = 3
		psInCall = psInCall + 1
	case(2)
		dLI = 6
		dsInCall = dsInCall + 1
	end select	
	posint_size = 3 ** psInCall * 6 ** dsInCall
	end

!------------------------------------------------------------------------------------------------------------------------------------------
