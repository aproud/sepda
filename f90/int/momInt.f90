!---------------------------------------------------------------------------------------------------------------------------------!
!																  !
!	Calculate a Momentum Intracule using data from a .wfn file, and the momentum RR code					  !
!																  !
!		Written by A.J. Proud, B.J.H. Sheppard, D.E.C.K. Mackenzie,							  !
!		Z.A.M. Zielinski, and J.K. Pearson										  !
!																  !
!       Calling method:														  !
!       ./outfile wfnFile.wfn output.dat COEFS.coef Molecular_Orbital_#								  !
!																  !
!---------------------------------------------------------------------------------------------------------------------------------!

MODULE SHELL_PAIR_DETERMINATION

	integer sSetCount, pSetCount, dSetCount
	integer shellPairListSize, shellPairCallListSize
	integer curShellPairIndex, curShellPairCallIndex
	integer shellPairSSoffset, shellPairPSoffset, shellPairPPoffset
	integer shellPairDSoffset, shellPairDPoffset, shellPairDDoffset
	integer shellPairSPoffset, shellPairSDoffset, shellPairPDoffset 					! These are new for momentum space
	integer SSshellPairCount, PSshellPairCount, PPshellPairCount
	integer DSshellPairCount, DPshellPairCount, DDshellPairCount
	integer SPshellPairCount, SDshellPairCount, PDshellPairCount						! These are new for momentum space
	integer size_ssss, size_psss, size_ppss, size_psps, size_pssp, size_ppps
	integer size_ppsp, size_pppp, size_dsss, size_dpss, size_dsps, size_dssp
	integer size_ddss, size_dsds, size_dssd, size_dpps, size_dpsp, size_dspp
	integer size_ddps, size_dpds, size_dpsd, size_ddds, size_dppp, size_ddpp
	integer size_dpdp, size_dppd, size_dddp, size_dddd, size_ddsd, size_ddpd
	integer size_spss, size_sdss, size_pdpp, size_pdss, size_sdpp, size_ddsp

!	Arrays for building our shell pair list, and consequently call lists
	integer, dimension(:), allocatable :: sOptions
	integer, dimension(:), allocatable :: pOptions
	integer, dimension(:), allocatable :: dOptions

!	Arrays for Shell Pair lists
	integer*8, dimension(:), allocatable :: shellPairList

!	Array for calls to RR code
	integer*8, dimension(:), allocatable :: shellPairCallList, shellPairPrefactor

END MODULE


MODULE HF2PDM

! Here we will store the 2D array used for the 2 particle density matrix.
	double precision, dimension(:,:), allocatable :: pMatrixFromCoefs					! 2D array used for 2 particle density matrix

END MODULE

!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
!
!	THIS IS THE START OF THE MAIN PROGRAM
!
!-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	program Momentum_Intracule
	USE SHELL_PAIR_DETERMINATION
	USE HF2PDM

	implicit none

	double precision pi, xv, v, MAXLINES, R									! xv, MAXLINES, R scale the output plot (v vs M(v))
	integer*8 MAXNPRIM
	parameter ( pi = 3.14159265358979323846264338327950288419716939937510d0)
!	parameter ( MAXLINES = 400.0D0 )
!	parameter ( R = 4.5D0 )
!	parameter (TOLERANCE = 1.0D-6) 										! SET COEF TOLERANCE TO 1 x 10 ^ -6
	parameter (MAXNPRIM = 10000)										! MAXNPRIM is the maximum number of primitives, and it MUST be a power of 10.

	integer i, j, k, ia, ib, ic, id, a, b, c, d
	integer FileStat, MO, curLine
	character g*8, gaussian*8, input*50, output*50, localized*50, orbital*50
	character moType*3, chR*50, chMAXLINES*50
!	integer lineStart, lineEnd
!	character chlineStart*50, chlineEnd*50, chopcode*50 							! These characters are for error messages, and input/output files.	

	integer NMO, p, q, oNPRIM, NNUC										! NMO: number of molecular orbitals

	integer, dimension(:), allocatable :: classSizeList							! An array to track class sizes (for more accurate addition) 
	integer classSizeCounter										! A counter for the array above

	double precision cor_a(3), cor_b(3), cor_c(3), cor_d(3)							! Gives the coordinates of a, b, c, and d.
	double precision expo_a, expo_b, expo_c, expo_d								! Gives the exponents of a, b, c, and d.
	integer getAngMom, getGaussSum1to, getPrefactorCode, ang_a, ang_b, ang_c, ang_d				! Gives the angular momentum of a, b, c, and d.
	double precision PHFtotal, PHFone, PHFab, PHFcd, PHFad, PHFbc, HFT, M, curClassSum			! Expressions necessary to calculate M (see part 2)
	double precision, dimension(1296) :: Mom_int 								! An array containing the integrals obtained from recurrence relation

	integer getClassPrefactor
	integer psInCall, dsInCall, momint_size, momint_index
	integer aLoopIndex, bLoopIndex, cLoopIndex, dLoopIndex		
	integer ta, tb, tc, td
	integer*8 getCorrectCallCodeForSymmetry, currentCallCodeForSymmetry
	integer verbose, prefactor
	integer sCount, pCount, dCount										! sCount, pCount and dCount contain the count of each type of atomic orbital
	double precision, dimension(:), allocatable :: wfnMegaArray						! WFN_MegaArray - a megaarray for the overall wavefunction
	double precision, dimension(:), allocatable :: sArray							! sArray - an array for all of the s type atomic orbitals
	double precision, dimension(:), allocatable :: pArray							! pArray - an array for all of the p type atomic orbitals
	double precision, dimension(:), allocatable :: dArray							! dArray - an array for all of the d type atomic orbitals
	integer megaArraySize, curInt, curType
	integer typeOffset, exponentOffset, centreOffset, coefOffset, angMomOffset, moOffset
	integer sindex, pindex, dindex
!	integer NPRIM, zeroCount, curNewRef, sIndex, pIndex, dIndex						! Values to track the removal of zeroes, and splitting of mega array.



! I/O Formatting
! unit 7  = input wfn file
! unit 8  = output file
! unit 10 = coef file

100 format (/a8,11x,I4,15x,I5,16x,I4) 
! 100 is the specific format for G,NMO, oNPRIM, NNUC
! Read 8 characters ('GAUSSIAN'), skip 11 characters
! read a 4 digit integer (number of molecular orbitals), skip 15 characters
! read a 5 digit integer (number of primitives before tolerance), skip 16 characters
! read a 4 digit integer (number of nuclei)

101	format (20x,20e3.0)
! 101 is the specific format used to read the centre and type assignments, where 20 ints (or less) are
! listed in a row at a time. So we skip 20 characters [20x], and then we read 20 3 character long integers

102	format (10x,5e14.7)
! 102 is the specific format used to read the exponents
! skip 11 spaces and then read in 5 doubles of the form e14.8

103	format (5(e15.8))
! 103 is the specific format used to read in coeffecients
! Read in 5 doubles of the format e15.9

104 	format (25x,f11.8,1x,f11.8,1x,f11.8)
! 104 is the specific format used to read in the atomic coordinates

!----------------------------------------------------------------------------------------------------------------------------------
! Getting the arguments from the command line.
!----------------------------------------------------------------------------------------------------------------------------------

	call GETARG(1, input)			! Argument 1 is the input WFN file
	call GETARG(2, output)			! Argument 2 is the output file the intracule is written to
!	call GETARG(3, localized)		! Argument 3 is the localized coefficient file
	call GETARG(3, moType)
	call GETARG(4, orbital)			! Argument 4 is the orbital number (0 for total molecular intracule)
	call GETARG(5, chR)
	call GETARG(6, chMAXLINES)
!	call GETARG(5, chlineStart)		! Argument 5 is the point number to begin at
!	call GETARG(6, chlineEnd)		! Argument 6 is the point number to end at
	read (orbital,*) MO			! Converts the string "orbital" into an integer, "MO"
	read (chR,*) R
	read (chMAXLINES,*) MAXLINES 
!	read (chlineStart,*) lineStart		! Converts the string "chlineStart" into an integer "lineStart"
!	read (chlineEnd,*) lineEnd		! Converts the string "chlineEnd" into an integer "lineEnd"

!----------------------------------------------------------------------------------------------------------------------------------
! Attempting to open the .wfn file (unit 7)
!----------------------------------------------------------------------------------------------------------------------------------

	open(unit = 7,file = input,IOSTAT=FileStat,status='old')
	  if (FileStat > 0) then
	    print *, 'Error opening input file:'
	    print *, ' -> Please ensure the .wfn file is correct, ' 
	    print *, '    and the filename does not contain illegal characters.'
	    stop
	  endif

!----------------------------------------------------------------------------------------------------------------------------------
! Opening the output file to write intracule to (unit 8)
!----------------------------------------------------------------------------------------------------------------------------------

	open(unit = 8, file = output)

!----------------------------------------------------------------------------------------------------------------------------------
! Opening the localized coefficient file (unit 10)
!----------------------------------------------------------------------------------------------------------------------------------

!	open(unit = 10,file = localized,IOSTAT=FileStat,status='old')
!	  if (FileStat > 0) then
!	    print *, 'Error opening localized orbital coefficients file:'
!	    print *, ' -> Please ensure the file is correct, ' 
!	    print *, '    and the filename does not contain illegal characters.'
!	    stop
!	  endif

!----------------------------------------------------------------------------------------------------------------------------------
! Reading the .wfn file
!----------------------------------------------------------------------------------------------------------------------------------

	read (unit = 7, FMT = 100) G, NMO, oNPRIM, NNUC			   ! Reads NMO, NPRIM, NNUC from input

! Possible error: if the .wfn file doesn't have 'GAUSSIAN' then the read formatting will be messed up and fail
	gaussian = 'GAUSSIAN'
	if (g.ne.gaussian) then
	  print *, 'ERROR: Invalid input file type(no GAUSSIAN statement)'
	  print *, '	   Ensure the input is a .wfn file.'
	  stop
	endif

!----------------------------------------------------------------------------------------------------------------------------------
! Allocate the mega array and define mega array offsets
!----------------------------------------------------------------------------------------------------------------------------------

	megaArraySize = 3*nNuc + 4*oNPrim + oNPRIM*NMO		! 3 coords/nuc, nPrim coefs/MO and nprim vals for centres/types/expos/angmoms
	ALLOCATE(wfnMegaArray(1:megaArraySize))

	centreOffset = 3*NNUC
	typeOffset = centreOffset + oNPRIM
	exponentOffset = typeOffset + oNPRIM
	coefOffset = exponentOffset + oNPRIM
	angMomOffset = coefOffset + oNPRIM*NMO

!----------------------------------------------------------------------------------------------------------------------------------
! Input info into mega array
!----------------------------------------------------------------------------------------------------------------------------------

! Input the coordinates
	do i = 1, 3*NNUC-2, 3
		read (7, 104) wfnMegaArray(i:(i+2))
	enddo

	read (7, 101) (wfnMegaArray(centreOffset+i),i=1,oNPRIM)		
	read (7, 101) (wfnMegaArray(typeOffset+i),i=1,oNPRIM)
	read (7, 102) (wfnMegaArray(exponentOffset+i),i=1,oNPRIM)


	if (localized /='NA') then
	    do j = 1, NMO
		read (7, 103) (wfnMegaArray(i),i=coefOffset+(j-1)*oNPRIM+1,coefOffset+j*oNPRIM)
	    enddo
	endif

! Input the angular momenta
	do j = 1, oNPRIM
		wfnMegaArray(angMomOffset+j) = getAngMom(wfnMegaArray(typeOffset + j))
	enddo

!----------------------------------------------------------------------------------------------------------------------------------
!  Normalize localized coefficients, if necessary:
!----------------------------------------------------------------------------------------------------------------------------------

        if (moType/='CMO') then
            do i = 1, onprim
                curType = nint(wfnMegaArray(typeOffset+i))
                if ( curType < 0 .or. curType > 10 ) then
                    print *, 'ERROR: The present version of the code cannot handle integrals involving f-orbitals.'
                    stop
                endif
                select case (curType)
                case (1)
                    do j = 0, NMO - 1
                        wfnMegaArray(coefOffset+j*onPrim+i) = wfnMegaArray(coefOffset+j*onPrim+i)* &
                                (((2.0d0/pi)**(3.0d0/4.0d0))*((wfnMegaArray(exponentOffset+i))**(3.0d0/4.0d0)))
                    enddo
                case( 2:4 )
                    do j = 0, NMO - 1
                        wfnMegaArray(coefOffset+j*onPrim+i) = wfnMegaArray(coefOffset+j*onPrim+i)* &
                                2*(((2.0d0/pi)**(3.0d0/4.0d0))*((wfnMegaArray(exponentOffset+i))**(5.0d0/4.0d0)))
                    enddo
                case ( 5:7 )
                    do j = 0, NMO - 1
                        wfnMegaArray(coefOffset+j*onPrim+i) = wfnMegaArray(coefOffset+j*onPrim+i)* &
                                4*((((2.0d0/pi)**(3.0d0/4.0d0))*((wfnMegaArray(exponentOffset+i))**(7.0d0/4.0d0)))/(dsqrt(3.0d0)))
                    enddo
                case ( 8:10 )
                    do j = 0, NMO - 1
                        wfnMegaArray(coefOffset+j*onPrim+i) = wfnMegaArray(coefOffset+j*onPrim+i)* &
                                4*(((2.0d0/pi)**(3.0d0/4.0d0))*((wfnMegaArray(exponentOffset+i))**(7.0d0/4.0d0)))
                    enddo
                end select
            enddo
        endif

!----------------------------------------------------------------------------------------------------------------------------------
! Scan through mega array's angular momenta and count how many s, p, and d's are in the molecule
!----------------------------------------------------------------------------------------------------------------------------------

	sCount = 0
	pCount = 0
	dCount = 0
	do i = 1, oNPRIM
		curInt = NINT(wfnMegaArray(angMomOffset+i))
		select case (curInt)
		case (0)					! For s-type basis functions
			sCount = sCount + 1
		case (1)					! For p-type basis functions
			pCount = pCount + 1
		case (2)					! For d-type basis functions
			dCount = dCount + 1
		end select
	enddo

	if((MOD(pCount, 3).ne.0).OR.(MOD(dCount, 6).ne.0)) then
                print *, 'The wfn file is flawed. Ensure that polarization in previous calculation was set to (6d, 10f), not (5d, 7f).'
		stop
	endif

        sSetCount = scount
        pSetCount = pcount / 3
        dSetCount = dcount / 6

!----------------------------------------------------------------------------------------------------------------------------------
! Make sArray, pArray, and dArray(if needed) to make shell pairs. These will hold the basis function numbers.
!----------------------------------------------------------------------------------------------------------------------------------

	if(sCount.gt.0) then
  	  allocate(sArray(1:sCount))
	endif

	if(sCount.gt.0) then
	  allocate(pArray(1:pCount))
	endif

	if (dCount.gt.0) then
	  allocate(dArray(1:dCount))
	endif

	sIndex = 1
	pIndex = 1
	dIndex = 1

	do i = 1, oNPRIM
	   curInt = nint(wfnMegaArray(angMomOffset + i))
	   select case (curInt)
	   case(0)								! Add to sArray
	      sArray(sIndex) = i
	      sIndex = sIndex + 1
	   case(1)								! Add to pArray
	      pArray(pIndex) = i
	      pIndex = pIndex + 1
	   case(2)								! Add to dArray
	      dArray(dIndex) = i
	      dIndex = dIndex +1
	   end select
	enddo

!----------------------------------------------------------------------------------------------------------------------------------
! Compute the 2D array for the 2-particle density matrix (2PDM).
!----------------------------------------------------------------------------------------------------------------------------------

	if (MO.ne.0) then								! This will make the 2PDM for the current MO being calculated
	    allocate(pMatrixFromCoefs(1:oNPRIM,1:oNPRIM))
	    do i = 1, oNPRIM
		do j = 1, oNPRIM
			pMatrixFromCoefs(i,j) = 2 * wfnMegaArray(coefOffset+(MO-1)*oNPRIM+i) * wfnMegaArray(coefOffset+(MO-1)*oNPRIM+j)
		enddo
	enddo
	endif

!----------------------------------------------------------------------------------------------------------------------------------
! Make the shell pairs using the basis function numbers. SS, PS, SP, DS, SD, PP, DP, PD, DD 
!----------------------------------------------------------------------------------------------------------------------------------


! Determine how many of each shell pair there will be and use these to allocate an array called "shellPairList"
! We only want unique pairs so for SS, PP, DD we avoid duplicates by using a Gaussian Sum
	SSshellPairCount = getGaussSum1to(sSetCount)
	PPshellPairCount = getGaussSum1to(pSetCount)
	DDshellPairCount = getGaussSum1to(dSetCount)

	PSshellPairCount = pSetCount * sSetCount
	SPshellPairCount = sSetCount * pSetCount
	DSshellPairCount = dSetCount * sSetCount
	SDshellPairCount = sSetCount * dSetCount
	DPshellPairCount = dSetCount * pSetCount
	PDshellPairCount = pSetCount * dSetCount

! Define offsets for each shell pair type in "shellPairList"
	shellPairSSoffset = 0
	shellPairPSoffset = shellPairSSoffset + SSshellPairCount
	shellPairSPoffset = shellPairPSoffset + PSshellPairCount
	shellPairPPoffset = shellPairSPoffset + SPshellPairCount
	shellPairDSoffset = shellPairPPoffset + PPshellPairCount
	shellPairSDoffset = shellPairDSoffset + DSshellPairCount
	shellPairDPoffset = shellPairSDoffset + SDshellPairCount
	shellPairPDoffset = shellPairDPoffset + DPshellPairCount
	shellPairDDoffset = shellPairPDoffset + PDshellPaircount

! Allocate shell pair list
	shellPairListSize = SSshellPairCount + PSshellPairCount + SPshellPairCount + PPshellPairCount &			
		 	  + DSshellPairCount + SDshellPairCount + DPshellPairCount + PDshellPairCount + DDshellPairCount

	ALLOCATE(shellPairList(1:shellPairListSize))
	curShellPairIndex = 1


!----------------------------------------------------------------------------------------------------------------------------------
!       NOW LETS FILL THE SHELLPAIR ARRAY, USING 9 LOOPS (OPTIONAL BASED ON IF)
!----------------------------------------------------------------------------------------------------------------------------------

![SS]
	if(SSshellPairCount.gt.0)then
		do i = 1, sSetCount
			do j = 1, i
				shellPairList(curShellPairIndex) = MAXNPRIM * sArray(i) + sArray(j)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

![PS]
	if(PSshellPairCount.gt.0)then
		do i = 1, pSetCount
			do j = 1, sSetCount
				shellPairList(curShellPairIndex) = MAXNPRIM * pArray((3*i)-2) + sArray(j)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

![SP]
	if(SPshellPairCount.gt.0)then
		do i = 1, sSetCount
			do j = 1, pSetCount
				shellPairList(curShellPairIndex) = MAXNPRIM * sArray(i) + pArray((3*j)-2)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

![PP]
	if(PPshellPairCount.gt.0)then
		do i = 1, pSetCount
			do j = 1, i
				shellPairList(curShellPairIndex) = MAXNPRIM * pArray((3*i)-2) + pArray((3*j)-2)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

![DS]
	if(DSshellPairCount.gt.0)then
		do i = 1, dSetCount
			do j = 1, sSetCount
				shellPairList(curShellPairIndex) = MAXNPRIM * dArray((6*i)-5) + sArray(j)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

![SD]
	if(SDshellPairCount.gt.0)then
		do i = 1, sSetCount
			do j = 1, dSetCount
				shellPairList(curShellPairIndex) = MAXNPRIM * sArray(i) + dArray((6*j)-5)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

![DP]
	if(DPshellPairCount.gt.0)then
		do i = 1, dSetCount
			do j = 1, pSetCOunt
				shellPairList(curShellPairIndex) = MAXNPRIM * dArray((6*i)-5) + pArray((3*j)-2)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

![PD]
	if(PDshellPairCount.gt.0)then
		do i = 1, pSetCount
			do j = 1, dSetCount
				shellPairList(curShellPairIndex) = MAXNPRIM * pArray((3*i)-2) + dArray((6*j)-5)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

![DD]
	if(DDshellPairCount.gt.0)then
		do i = 1, dSetCount
			do j = 1, i
				shellPairList(curShellPairIndex) = MAXNPRIM * dArray((6*i)-5) + dArray((6*j)-5)
				curShellPairIndex = curShellPairIndex + 1
			enddo
		enddo
	endif

!----------------------------------------------------------------------------------------------------------------------------------
! Use the shell pair list to make the shellPairCallList which will have all the shellPair pairs (ssss, psps, pssp dsps etc)
! It is important to use the shellPairList to do this to avoid double calling something.
!----------------------------------------------------------------------------------------------------------------------------------

! Determine sizes of all the classes to allocate the array later

	size_ssss = getGaussSum1to(SSshellPairCount) + getGaussSum1to(getGaussSum1to(sSetCount-1))
	size_psss = PSshellPairCount * (SSshellPairCount + getGaussSum1to(sSetCount-1))
	size_ppss = PPshellPairCount * SSshellPairCount + getGaussSum1to(pSetCount-1) * getGaussSum1to(sSetCount-1)
	size_psps = getGaussSum1to(PSshellPairCount)
	size_pssp = getGaussSum1to(PSshellPairCount)
	size_ppps = PPshellPairCount * PSshellPairCount + pSetCount*(pSetCount - 1)/2 * PSshellPairCount
	size_pppp = getGaussSum1to(PPshellPairCount) + getGaussSum1to(getGaussSum1to(pSetCount-1))
	size_dsss = DSshellPairCount * (SSshellPairCount + getGaussSum1to(sSetCount-1))
	size_dpss = DPshellPairCount * (SSshellPairCount + getGaussSum1to(sSetCount-1))
	size_dsps = DSshellPairCount * PSshellPairCount
	size_dssp = DSshellPairCount * SPshellPairCount
	size_ddss = DDshellPairCount * SSshellPairCount + getGaussSum1to(dsetCount-1) * getGaussSum1to(sSetCount-1)
	size_dsds = getGaussSum1to(DSshellPairCount)
	size_dssd = getGaussSum1to(DSshellPairCount)
	size_dpps = DPshellPairCount * PSshellPairCount
	size_dpsp = DPshellPairCount * SPshellPairCount
	size_dspp = DSshellPairCount * (PPshellPairCount + getGaussSum1to(pSetCount-1))
	size_ddps = DDshellPairCount * PSshellPairCount  + getGaussSum1to(dSetCount-1) * PSshellPairCount
	size_dpds = DPshellPairCount * DSshellPairCount
	size_dpsd = DPshellPairCount * SDshellPairCount
	size_ddds = DDshellPairCount * DSshellPairCount + getGaussSum1to(dSetCount-1) * DSshellPairCount
	size_dppp = DPshellPairCount * (PPshellPairCount + getGaussSum1to(pSetCount-1))
	size_ddpp = DDshellPairCount * PPshellPairCount + getGaussSum1to(dSetCount-1) * getGaussSum1to(pSetCount-1)
	size_dpdp = getGaussSum1to(DPshellPairCount)
	size_dppd = getGaussSum1to(DPshellPairCount)
	size_dddp = DDshellPairCount * DPshellPairCount + dSetCount*(dSetCount-1)/2 * DPshellPairCount
	size_dddd = getGaussSum1to(DDshellPairCount) + getGaussSum1to(getGaussSum1to(dSetCount-1))

	shellPairCallListSize = size_ssss + size_psss + size_ppss + size_psps + size_pssp + size_ppps &
					  + size_pppp + size_dsss + size_dpss + size_dsps + size_dssp &
					  + size_ddss + size_dsds + size_dssd + size_dpps + size_dpsp &
					  + size_dspp + size_ddps + size_dpds + size_dpsd + size_ddds &
					  + size_dppp + size_ddpp + size_dpdp + size_dppd + size_dddp + size_dddd

	allocate(shellPairCallList(1:shellPairCallListSize))
	allocate(shellPairPrefactor(1:shellPairCallListSize))

	allocate(classSizeList(1:27))						! Add a full class at a time to increase efficiency

	classSizeList(1) = size_ssss
	classSizeList(2) = size_psss + classSizeList(1)
	classSizeList(3) = size_ppss + classSizeList(2)
	classSizeList(4) = size_psps + classSizeList(3)
	classSizeList(5) = size_pssp + classSizeList(4)
	classSizeList(6) = size_ppps + classSizeList(5)
	classSizeList(7) = size_pppp + classSizeList(6) 
	classSizeList(8) = size_dsss + classSizeList(7)
	classSizeList(9) = size_dpss + classSizeList(8)
	classSizeList(10) = size_dsps + classSizeList(9)
	classSizeList(11) = size_dssp + classSizeList(10)
	classSizeList(12) = size_ddss + classSizeList(11)
	classSizeList(13) = size_dsds + classSizeList(12)
	classSizeList(14) = size_dssd + classSizeList(13)
	classSizeList(15) = size_dpps + classSizeList(14)
	classSizeList(16) = size_dpsp + classSizeList(15)
	classSizeList(17) = size_dspp + classSizeList(16)
	classSizeList(18) = size_ddps + classSizeList(17)
	classSizeList(19) = size_dpds + classSizeList(18)
	classSizeList(20) = size_dpsd + classSizeList(19)
	classSizeList(21) = size_ddds + classSizeList(20)
	classSizeList(22) = size_dppp + classSizeList(21)
	classSizeList(23) = size_ddpp + classSizeList(22)
	classSizeList(24) = size_dpdp + classSizeList(23)
	classSizeList(25) = size_dppd + classSizeList(24)
	classSizeList(26) = size_dddp + classSizeList(25)
	classSizeList(27) = size_dddd + classSizeList(26)

! Start building shellPairCallList by inputing the classes

! [ssss]
	curShellPairCallIndex = 1	
	do i = 1, SSshellPairCount
		do j = 1, i
		    a = shellPairList(shellPairSSoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairSSoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairSSoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairSSoffset+j),MAXNPRIM)
		    if ((a.ne.b).and.(c.ne.d)) then
		        shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairSSoffset+i) &
		        + shellPairList(shellPairSSoffset+j)			
		        curShellPairCallIndex = curShellPairCallIndex + 1
		        shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairSSoffset+i) + d*MAXNPRIM + c
		        curShellPairCallIndex = curShellPairCallIndex + 1
		    else			
		        shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairSSoffset+i) &
		        + shellPairList(shellPairSSoffset+j)			
		        curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [psss]
	do i = 1, PSshellPairCount
		do j = 1, SSshellPairCount
		    a = shellPairList(shellPairPSoffset+i)/MAXNPRIM
                    b = MOD(shellPairList(shellPairPSoffset+i),MAXNPRIM)
                    c = shellPairList(shellPairSSoffset+j)/MAXNPRIM
                    d = MOD(shellPairList(shellPairSSoffset+j),MAXNPRIM)
		    if (c.ne.d) then
			shellPairCallList(curShellPairCallIndex) = a * MAXNPRIM**3 + b * MAXNPRIM**2 + c * MAXNPRIM + d
                        curShellPairCallIndex = curShellPairCallIndex + 1
                        shellPairCallList(curShellPairCallIndex) = a * MAXNPRIM**3 + b * MAXNPRIM**2 + d * MAXNPRIM + c
                        curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPSoffset+i) &
			+ shellPairList(shellPairSSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [ppss]
	do i = 1, PPshellPairCount
		do j = 1, SSshellPairCount
		    a = shellPairList(shellPairPPoffset+i)/MAXNPRIM
                    b = MOD(shellPairList(shellPairPPoffset+i),MAXNPRIM)
                    c = shellPairList(shellPairSSoffset+j)/MAXNPRIM
                    d = MOD(shellPairList(shellPairSSoffset+j),MAXNPRIM)
		    if ((a.ne.b).and.(c.ne.d)) then
                        shellPairCallList(curShellPairCallIndex) = a * MAXNPRIM**3 + b * MAXNPRIM**2 + c * MAXNPRIM + d
                        curShellPairCallIndex = curShellPairCallIndex + 1
                        shellPairCallList(curShellPairCallIndex) = a * MAXNPRIM**3 + b * MAXNPRIM**2 + d * MAXNPRIM + c
                        curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPPoffset+i) &
			+ shellPairList(shellPairSSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [psps]
	do i = 1, PSshellPairCount
		do j = 1, i
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPSoffset+i) &
			+ shellPairList(shellPairPSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [pssp]
	do i = 1, PSshellPairCount
		do j = 1, i
			a = shellPairList(shellPairPSoffset+j) / MAXNPRIM
			b = MOD(shellPairList(shellPairPSoffset+j),MAXNPRIM)
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPSoffset+i) + MAXNPRIM * b + a
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [ppps]
	do i = 1, PPshellPairCount
		do j = 1, PSshellPairCount
			a = shellPairList(shellPairPPoffset+i)/MAXNPRIM
			b = MOD(shellPairList(shellPairPPoffset+i),MAXNPRIM)
			if (a.ne.b) then
			    c = shellPairList(shellPairPSoffset+j)/MAXNPRIM
			    d = MOD(shellPairList(shellPairPSoffset+j),MAXNPRIM)
			    shellPairCallList(curShellPairCallIndex) = a * MAXNPRIM**3 + b * MAXNPRIM**2 + c * MAXNPRIM + d
			    curShellPairCallIndex = curShellPairCallIndex + 1
			    shellPairCallList(curShellPairCallIndex) = b * MAXNPRIM**3 + a * MAXNPRIM**2 + c * MAXNPRIM + d
			    curShellPairCallIndex = curShellPairCallIndex + 1
			else		
			    shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPPoffset+i) &
			    + shellPairList(shellPairPSoffset+j)
			    curShellPairCallIndex = curShellPairCallIndex + 1
			endif
		enddo
	enddo

! [pppp]
	do i = 1, PPshellPairCount
		do j = 1, i
		    a = shellPairList(shellPairPPoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairPPoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairPPoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairPPoffset+j),MAXNPRIM)
		    if((a.ne.b).and.(c.ne.d))then
			    shellPairCallList(curShellPairCallIndex) = a * MAXNPRIM**3 + b * MAXNPRIM**2 + c * MAXNPRIM + d
			    curShellPairCallIndex = curShellPairCallIndex + 1
			    shellPairCallList(curShellPairCallIndex) = a * MAXNPRIM**3 + b * MAXNPRIM**2 + d * MAXNPRIM + c
			    curShellPairCallIndex = curShellPairCallIndex + 1
		    else			
			    shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairPPoffset+i) &
			    + shellPairList(shellPairPPoffset+j)
			    curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [dsss]
	do i = 1, DSshellPairCount
		do j = 1, SSshellPairCount
		    c = shellPairList(shellPairSSoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairSSoffset+j),MAXNPRIM)
		    if(c.ne.d)then
			    shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDSoffset+i) + MAXNPRIM*c + d
			    curShellPairCallIndex = curShellPairCallIndex + 1
			    shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDSoffset+i) + MAXNPRIM*d + c
			    curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			    shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) &
			    + shellPairList(shellPairSSoffset+j)
			    curShellPairCallIndex = curShellPairCallIndex + 1
		    endif	
		enddo
	enddo

! [dpss]
	do i = 1, DPshellPairCount
		do j = 1, SSshellPairCount
		    c = shellPairList(shellPairSSoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairSSoffset+j),MAXNPRIM)
		    if(c.ne.d)then
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDPoffset+i) + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDPoffset+i) + MAXNPRIM*d + c
			curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) &
			+ shellPairList(shellPairSSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [dsps]
	do i = 1, DSshellPairCount
		do j = 1, PSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) &
			+ shellPairList(shellPairPSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dssp]
	do i = 1, DSshellPairCount
		do j = 1, SPshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) &
			+ shellPairList(shellPairSPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [ddss]
	do i = 1, DDshellPairCount
		do j = 1, SSshellPairCount
		    a = shellPairList(shellPairDDoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairDDoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairSSoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairSSoffset+j),MAXNPRIM)
		    if((a.ne.b).and.(c.ne.d))then
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDDoffset+i) + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDDoffset+i) + MAXNPRIM*d + c
			curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) &
			+ shellPairList(shellPairSSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [dsds]
	do i = 1, DSshellPairCount
		do j = 1, i
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) &
			+ shellPairList(shellPairDSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dssd]
	do i = 1, DSshellPairCount
		do j = 1, i
			a = shellPairList(shellPairDSoffset+j) / MAXNPRIM
			b = MOD(shellPairList(shellPairDSoffset+j),MAXNPRIM)
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) + MAXNPRIM * b + a
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dpps]
	do i = 1, DPshellPairCount
		do j = 1, PSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) &
			+ shellPairList(shellPairPSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dpsp]
	do i = 1, DPshellPairCount
		do j = 1, SPshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) &
			+ shellPairList(shellPairSPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dspp]
	do i = 1, DSshellPairCount
		do j = 1, PPshellPairCount
		    a = shellPairList(shellPairDSoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairDSoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairPPoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairPPoffset+j),MAXNPRIM)
		    if(c.ne.d)then
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDSoffset+i) + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDSoffset+i) + MAXNPRIM*d + c
			curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDSoffset+i) &
			+ shellPairList(shellPairPPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [ddps]
	do i = 1, DDshellPairCount
		do j = 1, PSshellPairCount
		    a = shellPairList(shellPairDDoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairDDoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairPSoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairPSoffset+j),MAXNPRIM)
		    if(a.ne.b)then
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**3*b + MAXNPRIM**2*a + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) &
			+ shellPairList(shellPairPSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [dpds]
	do i = 1, DPshellPairCount
		do j = 1, DSshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) &
			+ shellPairList(shellPairDSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dpsd]
	do i = 1, DPshellPairCount
		do j = 1, SDshellPairCount
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) &
			+ shellPairList(shellPairSDoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [ddds]
	do i = 1, DDshellPairCount
		do j = 1, DSshellPairCount
		    a = shellPairList(shellPairDDoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairDDoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairDSoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairDSoffset+j),MAXNPRIM)
		    if(a.ne.b)then
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDDoffset+i) + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**3*b + MAXNPRIM**2*a + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) &
			+ shellPairList(shellPairDSoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [dppp]
	do i = 1, DPshellPairCount
		do j = 1, PPshellPairCount
		    a = shellPairList(shellPairDPoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairDPoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairPPoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairPPoffset+j),MAXNPRIM)
		    if(c.ne.d)then
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDPoffset+i) + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDPoffset+i) + MAXNPRIM*d + c
			curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) &
			+ shellPairList(shellPairPPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [ddpp]
	do i = 1, DDshellPairCount
		do j = 1, PPshellPairCount
		    a = shellPairList(shellPairDDoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairDDoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairPPoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairPPoffset+j),MAXNPRIM)
		    if((a.ne.b).and.(c.ne.d))then
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDDoffset+i) + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDDoffset+i) + MAXNPRIM*d + c
			curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) &
			+ shellPairList(shellPairPPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [dpdp]
	do i = 1, DPshellPairCount
		do j = 1, i
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) &
			+ shellPairList(shellPairDPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dppd]
	do i = 1, DPshellPairCount
		do j = 1, i
			p = shellPairList(shellPairDPoffset+j) / MAXNPRIM
			q = MOD(shellPairList(shellPairDPoffset+j),MAXNPRIM)
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDPoffset+i) + MAXNPRIM * q + p
			curShellPairCallIndex = curShellPairCallIndex + 1
		enddo
	enddo

! [dddp]
	do i = 1, DDshellPairCount
		do j = 1, DPshellPairCount
		    a = shellPairList(shellPairDDoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairDDoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairDPoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairDPoffset+j),MAXNPRIM)
		    if(a.ne.b)then
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDDoffset+i) + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**3*b + MAXNPRIM**2*a + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) &
			+ shellPairList(shellPairDPoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! [dddd]
	do i = 1, DDshellPairCount
		do j = 1, i
		    a = shellPairList(shellPairDDoffset+i)/MAXNPRIM
		    b = MOD(shellPairList(shellPairDDoffset+i),MAXNPRIM)
		    c = shellPairList(shellPairDDoffset+j)/MAXNPRIM
		    d = MOD(shellPairList(shellPairDDoffset+j),MAXNPRIM)
		    if((a.ne.b).and.(c.ne.d))then
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDDoffset+i) + MAXNPRIM*c + d
			curShellPairCallIndex = curShellPairCallIndex + 1
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM**2*shellPairList(shellPairDDoffset+i) + MAXNPRIM*d + c
			curShellPairCallIndex = curShellPairCallIndex + 1
		    else
			shellPairCallList(curShellPairCallIndex) = MAXNPRIM ** 2 * shellPairList(shellPairDDoffset+i) &
			+ shellPairList(shellPairDDoffset+j)
			curShellPairCallIndex = curShellPairCallIndex + 1
		    endif
		enddo
	enddo

! shellPairPrefactorArray
	do i = 1, shellPairCallListSize
		shellPairPrefactor(i) = getClassPrefactor(shellPairCallList(i),MAXNPRIM)
	enddo


	do curLine = 1, MAXLINES
	classSizeCounter = 1
	M = 0.0D0
	curClassSum = 0.0D0
	xv = curLine/(MAXLINES + 1)
	v = -R * (log(1 - (xv**3)))

	do i = 1, shellPairCallListSize

	    prefactor = shellPairPrefactor(i)

	    a = shellPairCallList(i) / MAXNPRIM ** 3
	    b = MOD((shellPairCallList(i) / MAXNPRIM ** 2), MAXNPRIM)
	    c = MOD((shellPairCallList(i) / MAXNPRIM), MAXNPRIM)
	    d = MOD((shellPairCallList(i)), MAXNPRIM)

            cor_a = wfnMegaArray(3*NINT(wfnMegaArray(centreOffset+a))-2:3*NINT(wfnMegaArray(centreOffset+a))) ! coor(ocent(a), 1:3)
            cor_b = wfnMegaArray(3*NINT(wfnMegaArray(centreOffset+b))-2:3*NINT(wfnMegaArray(centreOffset+b))) ! coor(ocent(b), 1:3)
            cor_c = wfnMegaArray(3*NINT(wfnMegaArray(centreOffset+c))-2:3*NINT(wfnMegaArray(centreOffset+c))) ! coor(ocent(c), 1:3)
            cor_d = wfnMegaArray(3*NINT(wfnMegaArray(centreOffset+d))-2:3*NINT(wfnMegaArray(centreOffset+d))) ! coor(ocent(d), 1:3)

	    ang_a = wfnMegaArray(angMomOffset+a)
	    ang_b = wfnMegaArray(angMomOffset+b)
	    ang_c = wfnMegaArray(angMomOffset+c)
	    ang_d = wfnMegaArray(angMomOffset+d)

	    psInCall = 0
	    dsInCall = 0
	    momint_size = 0

	    aLoopIndex = 0
	    bLoopIndex = 0
	    cLoopIndex = 0
	    dLoopIndex = 0

	    call determinePsAndDsInCall(ang_a,ang_b,ang_c,ang_d,aLoopIndex,bLoopIndex,cLoopIndex,dLoopIndex,psInCall,dsInCall,momint_size)

	    expo_a = wfnMegaArray(exponentOffset+a)
	    expo_b = wfnMegaArray(exponentOffset+b)
	    expo_c = wfnMegaArray(exponentOffset+c)
	    expo_d = wfnMegaArray(exponentOffset+d)

	    call MomentumIntegral(cor_a,cor_b,cor_c,cor_d,expo_a,expo_b,expo_c,expo_d,ang_a,ang_b,ang_c,ang_d,v,Mom_int)

            momint_index = 1

	    do ia = 0, (aLoopIndex - 1)
		do ib = 0, (bLoopIndex - 1)
	    	    do ic = 0, (cLoopIndex - 1)
			do id = 0, (dLoopIndex - 1)
				ta = a + ia
				tb = b + ib
				tc = c + ic
				td = d + id	
						
				if (MO == 0) then								! Do calculation for the full molecule
					PHFab = PHFtotal(ta,tb,nmo,onprim,wfnMegaArray,coefOffset,nNuc)	
		  			PHFcd = PHFtotal(tc,td,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
		  			PHFad = PHFtotal(ta,td,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
		  			PHFbc = PHFtotal(tb,tc,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
		  			HFT = (((2*PHFab*PHFcd)-(PHFad*PHFbc))/4.0d0)
				else if ((MO > 0).and.(MO <= NMO)) then						! Do calculation for ONE orbital
					PHFab = pMatrixFromCoefs(ta,tb)
			  		PHFcd = pMatrixFromCoefs(tc,td)
		  			PHFad = pMatrixFromCoefs(ta,td)
		  			PHFbc = pMatrixFromCoefs(tb,tc)
		  			HFT = ((0.5d0*PHFab*PHFcd)-(0.250d0*PHFad*PHFbc))
				else
					print*, 'ERROR: You have entered an invalid value for molecular orbital.'
					stop
				endif
						
				curClassSum = curClassSum + (HFT * mom_int(momint_index) * prefactor)
				momint_index = momint_index + 1

			enddo ! id
		    enddo ! ic
		enddo ! ib
	    enddo ! ia

			
	    if(i == classSizeList(classSizeCounter))then
		    M = M + curClassSum
		    curClassSum = 0.0D0
		    classSizeCounter = classSizeCounter + 1
	    endif
	enddo ! i = 1, shellPairCallListSize

! Write v and M(v) to the output file
	write (8,109) v,M
	enddo ! curLine loop

109 	format (2(e15.8,8x))	
!**********************************************************************************************************************************
! Program end
!**********************************************************************************************************************************

	end ! end program

!----------------------------------------------------------------------------------------------------------------------------------
! ang(x) takes a type assignment, and converts it to an angular momentum
!----------------------------------------------------------------------------------------------------------------------------------

	integer function getAngMom(x)
        implicit none
        double precision x
        integer y
        y=NINT(x)
        if (y == 1) then 
        	getAngMom = 0 
        else if ((y >= 2).AND.(y <= 4)) then
        	getAngMom = 1 
        else if ((y >= 5).AND.(y <= 10)) then 
        	getAngMom = 2 
        else if (y > 10) then
                print *, "Invalid basis function type. Ensure that no f-orbitals are in the basis set."
        endif   
        return  
	end

!----------------------------------------------------------------------------------------------------------------------------------
! getGaussSum1to(n) gives the sum from 1 to n in increments of 1 using the Gaussian formula
!----------------------------------------------------------------------------------------------------------------------------------

	integer function getGaussSum1to(n)
		implicit none
		integer n, output
		output = 0
		output = (n*(n+1))/2
		getGaussSum1to = output
		return
	end

!----------------------------------------------------------------------------------------------------------------------------------
! determinePsAndDsInCall
!----------------------------------------------------------------------------------------------------------------------------------

	subroutine determinePsAndDsInCall(ang_a,ang_b,ang_c,ang_d,aLI,bLI,cLI,dLI,psInCall,dsInCall,momint_size)
	! LI here stands for Loop Index
	! aLoopIndex represents the number of dimensions to consider for the ath basis function
	! bLoopIndex represents the number of dimensions to consider for the bth basis function
	! cLoopIndex represents the number of dimensions to consider for the cth basis function
	! dLoopIndex represents the number of dimensions to consider for the dth basis function
	implicit none
	integer ang_a,ang_b,ang_c,ang_d
	integer psInCall, dsInCall
	integer aLI,bLI,cLI,dLI
	integer momint_size
	select case (ang_a)
	case(0)
		aLI = 1	
	case(1)
		aLI = 3
		psInCall = psInCall + 1
	case(2)
		aLI = 6
		dsInCall = dsInCall + 1
	end select
	select case (ang_b)
	case(0)
		bLI = 1	
	case(1)
		bLI = 3
		psInCall = psInCall + 1
	case(2)
		bLI = 6
		dsInCall = dsInCall + 1
	end select
	select case (ang_c)
	case(0)
		cLI = 1	
	case(1)
		cLI = 3
		psInCall = psInCall + 1
	case(2)
		cLI = 6
		dsInCall = dsInCall + 1
	end select
	select case (ang_d)
	case(0)
		dLI = 1	
	case(1)
		dLI = 3
		psInCall = psInCall + 1
	case(2)
		dLI = 6
		dsInCall = dsInCall + 1
	end select	
	momint_size = 3 ** psInCall * 6 ** dsInCall
	end

!----------------------------------------------------------------------------------------------------------------------------------
! PHFtotal calculates the 2 particle density matrix element to be multiplied by the momint value for MO == 0
!----------------------------------------------------------------------------------------------------------------------------------	

	double precision function PHFtotal(x,y,nmo,onprim,wfnMegaArray,coefOffset,nNuc)
	implicit none
	integer x, y, i, nMO, onPrim, coefOffset, nNuc
	double precision wfnMegaArray(3*nNuc+((nMO+4)*onPrim))

	PHFtotal = 0
	do i = 1, nMO
		PHFtotal = PHFtotal + (wfnMegaArray(coefOffset + (i-1)*oNPRIM + x) * wfnMegaArray(coefOffset + (i-1)*oNPRIM + y))
	enddo
	PHFtotal = PHFtotal * 2

	return
	end

!----------------------------------------------------------------------------------------------------------------------------------
! permutationPrinter takes ta, tb, tc,and td, and prints all possible permutations.
!----------------------------------------------------------------------------------------------------------------------------------

!----------------------------------------------------------------------------------------------------------------------------------
! getClassPrefactor
!----------------------------------------------------------------------------------------------------------------------------------

        integer function getClassPrefactor(shellPairCallListValue, MAXNPRIM)
        implicit none
        integer a, b, c, d, distinctCount, sum
        integer, dimension(:), allocatable :: distinctVals
        integer*8 shellPairCallListValue, MAXNPRIM
        a = shellPairCallListValue / MAXNPRIM ** 3
        b = MOD((shellPairCallListValue / MAXNPRIM ** 2), MAXNPRIM)
        c = MOD((shellPairCallListValue / MAXNPRIM), MAXNPRIM)
        d = MOD((shellPairCallListValue), MAXNPRIM)

!----------------------------------------------------------------------------------------------------
! MOMENTUM PRE-FACTOR EXPLANATION
!----------------------------------------------------------------------------------------------------
! The possible basis function symmetries (where the 1,2,3,4 are arbitrary basis function number labels) are:
!     Symmetries                        Pre-Factor
! 1111                                                  1
! 2111 = 1211 = 1121 = 1112             4
! 2211 = 1122                                   2
! 2121 = 1212                                   2
! 2112 = 1221                                   2
! 3211 = 2311 = 1132 = 1123             4
! 3121 = 1312 = 2131 = 1213             4
! 3112 = 1321 = 1231 = 2113             4
! 4321 = 3412 = 2143 = 1234             4
! 4312 = 3421 = 1243 = 2134             4
!
! Only the first column of the symmetries get called to Josh's code, so we then multiply by the appropriate
! pre-factor to account for the symmetries. All we need to do is an if loop. If a=b=c=d we have the first case
! and the pre-factor is simply 1. If (a=b.and.c=d.and.a.ne.c.and.b.ne.d).or.(a=c.and.b=d.and.a.ne.b.and.c.ne.d).....
! ....or.(a=d.and.b=c.and.a.ne.b.and.c.ne.d). This one is long but covers the three possible cases of having 2 different
! basis functions which need a pre-factor of only 2. Everything else can be multiplied by 4.
! So to determine a class prefactor in the momentum situation, we are going to count the number of distinct
! basis functions in a call.
! If there is one distinct basis function, the symmetry factor is automatically 1.
! In the case that there are three or more distinct basis functions, the symmetry factor is automatically 4.
! Now in the case that there are two distinct basis functions, we could have 2 or 4 as our prefactor.
!
! We will have 2 as our prefactor in the following cases: aabb, abab, abba.
! We will have 4 as our prefactor in the following cases: abbb, abaa, aaba and aaab.
! Note that in the 2 cases, if we add up the numbers in the sequence, we get a sum of 2(a+b)
! So a + a + b + b = 2a + 2b = 2(a+b) or a + b + a + b = 2(a+b) or a + b + b + a = 2(a+b)
! While in the second case we will get a sum of (a + 3b) or (3a + b)
! This can be used to our advantage. While we are looking for the count of distinct basis functions, we can also sum up the values.
! Then we can determine the symmetry prefactor either by the count of distinct basis functions, or in the case of 2, by the sum.
!
! Begin determining the number of distinct basis functions.
        sum = a + b + c + d
        allocate(distinctVals(1:4))
        distinctVals(1) = a 	! Obviously the first value is a distinct value.
        distinctCount = 1

        if(b.ne.distinctVals(1)) then
                distinctVals(2) = b
                distinctCount = distinctCount + 1
        endif


        if(distinctCount.eq.1) then
                if(c.ne.distinctVals(1)) then
                        distinctCount = 2
                        distinctVals(2) = c
                endif 		! end if c.ne.distinctVals(1)
        elseif(distinctCount.eq.2) then
                if(c.ne.distinctVals(1)) then
                        if(c.ne.distinctVals(2))then
                                distinctCount = 3
                                distinctVals(3) = c
                        endif
                endif
        endif


        if(distinctCount.eq.1) then
                if(d.ne.distinctVals(1))then
                        distinctCount = 2
                        distinctVals(2) = d
                endif
        elseif(distinctCount.eq.2)then
                if(d.ne.distinctVals(1))then
                        if(d.ne.distinctVals(2))then
                                distinctCount = 3
                                distinctVals(3) = d
                        endif
                endif
        elseif(distinctCount.eq.3)then
                if(d.ne.distinctVals(1))then
                        if(d.ne.distinctVals(2))then
                                if(d.ne.distinctVals(3))then
                                        distinctCount = 4
                                        distinctVals(4) = d
                                endif
                        endif
                endif
        endif

! At this point distinctCount = 1, 2, 3 or 4.

        if(distinctCount == 1) then
                getClassPrefactor = 1
        elseif(distinctCount .ge. 3) then
                getClassPrefactor = 4
        elseif(distinctCount .eq. 2) then
                if((2*(distinctVals(1) + distinctVals(2))) == sum) then
                        getClassPrefactor = 2
                else
                        getClassPrefactor = 4
                endif
        endif
        return
        end
