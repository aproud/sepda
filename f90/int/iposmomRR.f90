!*MODULE INTRACULE  *DECK PosMomIntegral
      subroutine PosMomIntegral(Av,Bv,Cv,Dv,a,b,c,d,A_ang,B_ang,C_ang,D_ang,x,PosMom_int)
! 
!   *****************************************************************
!   *                                                               *
!   *  Calculates Posmom integrals (in Fourier Space)               *
!   *  using 5-term RR                                              *
!   *                                                               *
!   *       --------------------- OUTPUT ---------------------      *
!   *       PosMom_int = Posmom integrals                           *
!   *       --------------------------------------------------      *
!   *                                                               * 
!   *       --------------------- INPUT ----------------------      *
!   *       Av = cartesian coordinates of centre A                  *
!   *       Bv = cartesian coordinates of centre B                  *
!   *       Cv = cartesian coordinates of centre C                  *
!   *       Dv = cartesian coordinates of centre D                  *
!   *       a = exponent alpha (associated with centre A)           *
!   *       b = exponent beta (associated with centre B)            *
!   *       c = exponent gamma (associated with centre C)           *
!   *       d = exponent delta (associated with centre D)           *
!   *       A_ang = angular momentum of basis function on A         *
!   *       B_ang = angular momentum of basis function on B         *
!   *       C_ang = angular momentum of basis function on C         *
!   *       D_ang = angular momentum of basis function on D         * 
!   *       x = value of x (argument to Posmom intracule)           *
!   *       --------------------------------------------------      *
!   *                                                               *
!   *  For details of the equations contained in this section of    *
!   *  code, see Hollett and Gill, PCCP, 2010                       *
!   *                                                               *
!   *  code written by JWH (2010)                                   *
!   *                                                               *
!   *****************************************************************
!
!---------------------------------------------------------------------
! Input/Output:
!---------------------------------------------------------------------
      implicit none
      integer :: A_ang, B_ang, C_ang, D_ang
      double precision, dimension(3) :: Av, Bv, Cv, Dv
      double precision :: a, b, c, d, x
      double precision, dimension(1296) :: PosMom_int 
!
!--------------------------------------------------------------------
! Local: 
!--------------------------------------------------------------------
!
      double precision, dimension(3) :: Pv, Qv
      double precision, dimension(3) :: RRa1, RRb1, RRc1, RRd1
      double precision :: RRa2, RRa3, RRa4, RRa5
      double precision :: RRb2, RRb3, RRb4, RRb5
      double precision :: RRc2, RRc3, RRc4, RRc5
      double precision :: RRd2, RRd3, RRd4, RRd5
      double precision :: Kx, Ka1, Ka2, Kb1, Kb2, Kc1, Kc2, Kd1, Kd2
      double precision :: sechx, tanhx, T, G
      double precision :: mu2, lambda2, eta, Sad, Psquared, Qsquared, PdotQ
      integer :: Lcode, i, j, k, l, a_i, b_i, c_i, d_i
      integer :: ij_index, ijk_index, ijkl_index, ik_index, jk_index
      integer :: d_index, dp_index, pp_index, pd_index, dd_index, dpp_index, pdp_index, ppd_index
      integer :: ddp_index, dpd_index, pdd_index, ddpp_index, dpdp_index, dppd_index, ppp_index
      integer :: pddp_index, dppp_index, dp2_index, dp3_index, dddd_index, ddd_index, dddp_index
      integer :: p1, p2, p3, p4, d1, d2, d3, d4
      integer :: Lvec_d(6,3), v_1i(3), xyz(3)
      double precision :: pi, toler, coeff, sq3
      double precision, allocatable, dimension(:) :: Int1000, Int1100, Int1010, Int0110, Int1110
      double precision, allocatable, dimension(:) :: Int0100, Int2000, Int0200, Int0020, Int2100
      double precision, allocatable, dimension(:) :: Int2010, Int1200, Int2200, Int2020, Int2210
      double precision, allocatable, dimension(:) :: Int2120, Int2110, Int1210, Int1120, Int2211
      double precision, allocatable, dimension(:) :: Int2121, Int1220, Int2002, Int1102, Int2001
      double precision, allocatable, dimension(:) :: Int1020, Int2102, Int2101, Int2220, Int2221
      double precision, allocatable, dimension(:) :: Int0210, Int1002, Int1001, Int1221, Int0002
      double precision, allocatable, dimension(:) :: Int0010, Int0001, Int0220
!
!--------------------------------------------------------------------
! Begin: 
!--------------------------------------------------------------------
!
      sq3 = dsqrt(1.0D0)
!
      pi = 3.14159265358979323846264338327950288419716939937510d0
! Set angular momentum code
      Lcode = 1000*A_ang + 100*B_ang + 10*C_ang + D_ang
! Set angular momentum vectors, for higher functions
      Lvec_d=0
      Lvec_d(1,1)=2 ! xx
      Lvec_d(2,2)=2 ! yy
      Lvec_d(3,3)=2 ! zz
      Lvec_d(4,1)=1 ! xy
      Lvec_d(4,2)=1 
      Lvec_d(5,1)=1 ! xz
      Lvec_d(5,3)=1 
      Lvec_d(6,2)=1 ! yz
      Lvec_d(6,3)=1 
! Set xyz code for p functions
      xyz(1)=1
      xyz(2)=2
      xyz(3)=3
!  
!--------------------------------------------------------------------
!  Calculate common intermediates, prefactor
!--------------------------------------------------------------------
!
      Psquared = 0.0D0
      Qsquared = 0.0D0
      PdotQ = 0.0D0
      do i=1,3
        Pv(i) = 2.0D0*a*d*(Av(i)-Dv(i))/(a+d) + 2.0D0*b*c*(Bv(i)-Cv(i))/(b+c)
        Psquared = Psquared + Pv(i)**2
        Qv(i) = (a*Av(i)+d*Dv(i))/(a+d) - (b*Bv(i)+c*Cv(i))/(b+c)
        Qsquared = Qsquared + Qv(i)**2
        PdotQ = PdotQ + Pv(i)*Qv(i)
      end do 
!
      mu2 = 0.25D0*(1.0D0/(a+d) + 1.0D0/(b+c))
      lambda2 = a*d/(a+d) + b*c/(b+c)
      eta = a/(a+d) - b/(b+c)
!
! hyperbolic functions
      sechx = 2.0D0/(exp(x) + exp(-x))
      tanhx = sechx*(exp(x) - exp(-x))/2.0D0
      Kx = 4.0D0*lambda2*mu2 + (eta + tanhx)**2
!
      Sad = 0.0D0
! calculate entire exponent, Sad*e^T
      do i=1,3
        Sad = Sad - a*d*(Av(i)-Dv(i))**2/(a+d) - b*c*(Bv(i)-Cv(i))**2/(b+c)
      end do
      Sad = Sad + (mu2*Psquared + (eta + tanhx)*PdotQ - lambda2*Qsquared)/Kx ! This is Sad and T combined before the exponential of (58)
      Sad = exp(Sad)
!      
!-------------------------------------------------------------------
! Calculate required G's
!-------------------------------------------------------------------
!
! Integral does not change upon differentiation, hence 5-terms and no m indices
! There is only one G, which is equivalent to [0000]_X
!
      G = (pi*sechx)**3*Sad/((a+d)*(b+c)*Kx)**(1.5D0)
!
!
! Calculate coefficents for RR
      Ka1 = 4.0D0*mu2*d + eta + tanhx
      Ka2 = 2.0D0*(-lambda2 + d*(eta + tanhx))
!
      Kb1 = 4.0D0*mu2*c - eta - tanhx
      Kb2 = 2.0D0*(lambda2 + c*(eta + tanhx))
!
      Kc1 = -4.0D0*mu2*b - eta - tanhx
      Kc2 = 2.0D0*(lambda2 - b*(eta + tanhx))
!
      Kd1 = -4.0D0*mu2*a + eta + tanhx
      Kd2 = 2.0D0*(-lambda2 - a*(eta + tanhx))
!
      do i=1,3 ! x,y,z 
        RRa1(i) = d*(Dv(i)-Av(i))/(a+d) + 0.5D0*(Ka1*Pv(i) + Ka2*Qv(i))/((a+d)*Kx)
        RRb1(i) = c*(Cv(i)-Bv(i))/(b+c) + 0.5D0*(Kb1*Pv(i) + Kb2*Qv(i))/((b+c)*Kx)
        RRc1(i) = b*(Bv(i)-Cv(i))/(b+c) + 0.5D0*(Kc1*Pv(i) + Kc2*Qv(i))/((b+c)*Kx)
        RRd1(i) = a*(Av(i)-Dv(i))/(a+d) + 0.5D0*(Kd1*Pv(i) + Kd2*Qv(i))/((a+d)*Kx)
      end do ! i
!
!                                                                        Corresponding Integral
! RRa
      RRa2 = 0.5D0/(a+d) + 0.5D0*(d*Ka1 + 0.5D0*Ka2)/((a+d)**2*Kx)       ! [(a-1i)bcd]
      RRa3 = 0.5D0*(c*Ka1 - 0.5D0*Ka2)/((a+d)*(b+c)*Kx)                  ! [a(b-1i)cd]
      RRa4 = 0.5D0*(-b*Ka1 - 0.5D0*Ka2)/((a+d)*(b+c)*Kx)                 ! [ab(c-1i)d]
      RRa5 = 0.5D0/(a+d) + 0.5D0*(-a*Ka1 + 0.5D0*Ka2)/((a+d)**2*Kx)      ! [abc(d-1i)]
!
! RRb
      RRb2 = 0.5D0*(d*Kb1 + 0.5D0*Kb2)/((a+d)*(b+c)*Kx)                  ! [(a-1i)bcd]
      RRb3 = 0.5D0/(b+c) + 0.5D0*(c*Kb1 -0.5D0*Kb2)/((b+c)**2*Kx)        ! [a(b-1i)cd]
      RRb4 = 0.5D0/(b+c) + 0.5D0*(-b*Kb1 - 0.5D0*Kb2)/((b+c)**2*Kx)      ! [ab(c-1i)d]
      RRb5 = 0.5D0*(-a*Kb1 + 0.5D0*Kb2)/((a+d)*(b+c)*Kx)                 ! [abc(d-1i)]
!
! RRc
      RRc2 = 0.5D0*(d*Kc1 + 0.5D0*Kc2)/((a+d)*(b+c)*Kx)                  ! [(a-1i)bcd]
      RRc3 = 0.5D0/(b+c) + 0.5D0*(c*Kc1 -0.5D0*Kc2)/((b+c)**2*Kx)        ! [a(b-1i)cd]
      RRc4 = 0.5D0/(b+c) + 0.5D0*(-b*Kc1 - 0.5D0*Kc2)/((b+c)**2*Kx)      ! [ab(c-1i)d]
      RRc5 = 0.5D0*(-a*Kc1 + 0.5D0*Kc2)/((a+d)*(b+c)*Kx)                 ! [abc(d-1i)]
!
! RRd
      RRd2 = 0.5D0/(a+d) + 0.5D0*(d*Kd1 + 0.5D0*Kd2)/((a+d)**2*Kx)       ! [(a-1i)bcd]
      RRd3 = 0.5D0*(c*Kd1 - 0.5D0*Kd2)/((a+d)*(b+c)*Kx)                  ! [a(b-1i)cd]
      RRd4 = 0.5D0*(-b*Kd1 - 0.5D0*Kd2)/((a+d)*(b+c)*Kx)                 ! [ab(c-1i)d]
      RRd5 = 0.5D0/(a+d) + 0.5D0*(-a*Kd1 + 0.5D0*Kd2)/((a+d)**2*Kx)      ! [abc(d-1i)]
!
!------------------------------------------------------------------
! [ssss]
!------------------------------------------------------------------
      if(Lcode.eq.0)then
!
        PosMom_Int(1) = G
!
        return
      end if
!
!------------------------------------------------------------------
! [psss]
!------------------------------------------------------------------
      if(Lcode.eq.1000)then
!
! Requires one step
!
! Step 1 :
! [1000]^(0) from [0000]^(1)
!
        do i=1,3 ! x,y,z 
! 2-terms required
          PosMom_Int(i) = RRa1(i)*G
        end do !
!
! print for testing
!        write(6,'(a,F16.10)')'a= ',a
!        write(6,'(a,F16.10)')'b= ',b
!        write(6,'(a,F16.10)')'c= ',c
!        write(6,'(a,F16.10)')'d= ',d
!        write(6,'(a,F16.10)')'G= ',G
!        do i=1,3
!          write(6,'(a,I1,a,F16.10)')'RRa1(',i,')= ',RRa1(i)
!        end do
!        do i=1,3
!          write(6,'(a,I1,a,F16.10)')'PosMom_Int(',i,')= ',PosMom_Int(i)
!        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [ppss]
!------------------------------------------------------------------
      if(Lcode.eq.1100)then
!
! Requires 2 steps
!
        allocate(Int1000(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [1100] from [1000] and [0000]
! b+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              PosMom_Int(ij_index) = RRb1(j)*Int1000(i) &
                                   + RRb2*G
            else ! 1-term
              PosMom_Int(ij_index) = RRb1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [psps]
!------------------------------------------------------------------
      if(Lcode.eq.1010)then
!
! Requires 2 steps
!
        allocate(Int1000(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [1010] from [1000] and [0000]
! c+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              PosMom_Int(ij_index) = RRc1(j)*Int1000(i) &
                                   + RRc2*G
            else ! 1-term 
              PosMom_Int(ij_index) = RRc1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [pssp]
!------------------------------------------------------------------
      if(Lcode.eq.1001)then
!
! Requires 2 steps
!
        allocate(Int1000(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [1001] from [1000] and [0000]
! d+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              PosMom_Int(ij_index) = RRd1(i)*Int1000(i) &
                                   + RRd2*G
            else ! 1-term 
              PosMom_Int(ij_index) = RRd1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [ppps]
!------------------------------------------------------------------
      if(Lcode.eq.1110)then
!
! Requires 4 steps
!
        allocate(Int1000(3),Int0100(3),Int1100(9))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i
!
! Step 3 :
! [1100] from [1000] and [0000]
! b+1i
!
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z 
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int1100(ij_index) = RRb1(j)*Int1000(i) &
                                + RRb2*G
            else ! 1-term
              Int1100(ij_index) = RRb1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
! Step 4 :
! [1110] from [1100], [1000], and [0100]
! c+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            do k=1,3 ! x,y,z
              ijk_index = 9*(i-1) + 3*(j-1) + k
              ij_index = 3*(i-1) + j
              if((i.eq.k).and.(j.eq.k))then ! 3-terms
                PosMom_Int(ijk_index) = RRc1(k)*Int1100(ij_index) &
                                      + RRc2*Int0100(j) + RRc3*Int1000(i)
              else if(i.eq.k)then ! 2-terms
                PosMom_Int(ijk_index) = RRc1(k)*Int1100(ij_index) &
                                      + RRc2*Int0100(j)
              else if(j.eq.k)then ! 2-terms
                PosMom_Int(ijk_index) = RRc1(k)*Int1100(ij_index) &
                                      + RRc3*Int1000(i)
              else ! 1-term
                PosMom_Int(ijk_index) = RRc1(k)*Int1100(ij_index)
              end if
            end do ! k
          end do ! j
        end do ! i
!
        return
      end if
!
!------------------------------------------------------------------
! [pppp]
!------------------------------------------------------------------
      if(Lcode.eq.1111)then
!
! Requires 7 steps
!
        allocate(Int1000(3),Int0100(3),Int1100(9))
        allocate(Int1010(9),Int0110(9),Int1110(27))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [1100] from [1000] and [0000]
! b+1i
!
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int1100(ij_index) = RRb1(j)*Int1000(i) &
                                  + RRb2*G
            else ! 1-term
              Int1100(ij_index) = RRb1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
! Step 4 :
! [1010] from [1000] and [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int1010(ij_index) = RRc1(j)*Int1000(i) &
                                + RRc2*G
            else ! 1-term
              Int1010(ij_index) = RRc1(j)*Int1000(i)
            end if
          end do ! j
        end do ! i
!
! Step 5 :
! [0110] from [0100] and [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 2-terms
              Int0110(ij_index) = RRc1(j)*Int0100(i) &
                                + RRc3*G
            else ! 2-terms
              Int0110(ij_index) = RRc1(j)*Int0100(i)
            end if
          end do ! j
        end do ! i
!
! Step 6 :
! [1110] from [1100], [0100] and [1000]
! c+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            do k=1,3 ! x,y,z
              ijk_index = 9*(i-1) + 3*(j-1) + k
              ij_index = 3*(i-1) + j
              if((i.eq.k).and.(j.eq.k))then ! 3-terms
                Int1110(Ijk_index) = RRc1(k)*Int1100(ij_index) &
                                   + RRc2*Int0100(j) + RRc3*Int1000(i)
              else if(i.eq.k)then ! 2-terms
                Int1110(Ijk_index) = RRc1(k)*Int1100(ij_index) &
                                   + RRc2*Int0100(j)
              else if(j.eq.k)then ! 2-terms
                Int1110(Ijk_index) = RRc1(k)*Int1100(ij_index) &
                                   + RRc3*Int1000(i)
              else ! 1-term
                Int1110(Ijk_index) = RRc1(k)*Int1100(ij_index)
              end if
            end do ! k
          end do ! j
        end do ! i
!
! Step 7 :
! [1111] from [1110], [0110], [1010] and [1100]
! d+1i
!
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z 
            do k=1,3 ! x,y,z 
              do l=1,3 ! x,y,z 
                ijkl_index = 27*(i-1) + 9*(j-1) + 3*(k-1) + l
                ijk_index = 9*(i-1) + 3*(j-1) + k
                ij_index = 3*(i-1) + j
                ik_index = 3*(i-1) + k
                jk_index = 3*(j-1) + k
                if((i.eq.l).and.(j.eq.l).and.(k.eq.l))then
                  PosMom_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                         + RRd2*Int0110(jk_index) + RRd3*Int1010(ik_index) &
                                         + RRd4*Int1100(ij_index)
                else if((i.eq.l).and.(j.eq.l))then
                  PosMom_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                         + RRd2*Int0110(jk_index) + RRd3*Int1010(ik_index)
                else if((i.eq.l).and.(k.eq.l))then 
                  PosMom_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                         + RRd2*Int0110(jk_index) &
                                         + RRd4*Int1100(ij_index)
                else if((j.eq.l).and.(k.eq.l))then 
                  PosMom_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                         + RRd3*Int1010(ik_index) &
                                         + RRd4*Int1100(ij_index)
                else if(i.eq.l)then 
                  PosMom_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                         + RRd2*Int0110(jk_index)
                else if(j.eq.l)then 
                  PosMom_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                         + RRd3*Int1010(ik_index)
                else if(k.eq.l)then 
                  PosMom_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index) &
                                         + RRd4*Int1100(ij_index)
                else
                  PosMom_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index)
                end if
              end do ! l
            end do ! k
          end do ! j
        end do ! l
!
        return
      end if
!
!------------------------------------------------------------------
! [dsss]
! Note: d integrals required some modification of code notation
!------------------------------------------------------------------
      if(Lcode.eq.2000)then
!
! Requires 2 steps
!
        allocate(Int1000(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          PosMom_Int(d_index) = RRa1(i)*Int1000(p1) &
                              + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            PosMom_Int(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        PosMom_Int(4:6)= sq3*PosMom_Int(4:6)
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpss]
!------------------------------------------------------------------
      if(Lcode.eq.2100)then
!
! Requires 3 steps
!
        allocate(Int1000(3),Int2000(6))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            PosMom_Int(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              PosMom_Int(dp_index) = PosMom_Int(dp_index) &
                                   + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            PosMom_Int(dp_index) = sq3*PosMom_Int(dp_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dsps]
!------------------------------------------------------------------
      if(Lcode.eq.2010)then
!
! Requires 3 steps
!
        allocate(Int1000(3),Int2000(6))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            PosMom_Int(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              PosMom_Int(dp_index) = PosMom_Int(dp_index) &
                                   + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            PosMom_Int(dp_index) = sq3*PosMom_Int(dp_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dssp]
!------------------------------------------------------------------
      if(Lcode.eq.2001)then
!
! Requires 3 steps
!
        allocate(Int1000(3),Int2000(6))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design i ne p1
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 3:
! [2001] from [2000] and [1000]
! d+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            PosMom_Int(dp_index) = RRd1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              PosMom_Int(dp_index) = PosMom_Int(dp_index) &
                                   + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            PosMom_Int(dp_index) = sq3*PosMom_Int(dp_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [ddss]
!------------------------------------------------------------------
      if(Lcode.eq.2200)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            PosMom_Int(dd_index) = RRb1(i)*Int2100(dp_index) &
                                 + RRb3*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              PosMom_Int(dd_index) = PosMom_Int(dd_index) &
                                   + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              PosMom_Int(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                PosMom_Int(dd_index) = PosMom_Int(dd_index) &
                                     + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) PosMom_Int(dd_index) = sq3*PosMom_Int(dd_index)
            if(d2.gt.3) PosMom_Int(dd_index) = sq3*PosMom_Int(dd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dsds]
!------------------------------------------------------------------
      if(Lcode.eq.2020)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1010(9))
        allocate(Int2010(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2020] from [2010], [2000] and [1010]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            PosMom_Int(dd_index) = RRc1(i)*Int2010(dp_index) &
                                 + RRc4*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              PosMom_Int(dd_index) = PosMom_Int(dd_index) &
                                   + dble(a_i)*RRc2*Int1010(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (c-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              PosMom_Int(dd_index) = RRc1(i)*Int2010(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                PosMom_Int(dd_index) = PosMom_Int(dd_index) &
                                     + dble(a_i)*RRc2*Int1010(pp_index)
              end if
            end do ! i
          end do ! p2
!
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) PosMom_Int(dd_index) = sq3*PosMom_Int(dd_index)
            if(d2.gt.3) PosMom_Int(dd_index) = sq3*PosMom_Int(dd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dssd]
!------------------------------------------------------------------
      if(Lcode.eq.2002)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1001(9))
        allocate(Int2001(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1001] from [1000] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1001(pp_index) = RRd1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1001(pp_index) = Int1001(pp_index) &
                                + RRd2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
              ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2001] from [2000] and [1000]
! d+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2001(dp_index) = RRd1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2001(dp_index) = Int2001(dp_index) &
                                + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2002] from [2001], [2000] and [1001]
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            PosMom_Int(dd_index) = RRd1(i)*Int2001(dp_index) &
                                 + RRd5*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              PosMom_Int(dd_index) = PosMom_Int(dd_index) &
                                   + dble(a_i)*RRd2*Int1001(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (d-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              PosMom_Int(dd_index) = RRd1(i)*Int2001(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                PosMom_Int(dd_index) = PosMom_Int(dd_index) &
                                     + dble(a_i)*RRd2*Int1001(pp_index)
              end if
            end do ! i
          end do ! p2
!
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) PosMom_Int(dd_index) = sq3*PosMom_Int(dd_index)
            if(d2.gt.3) PosMom_Int(dd_index) = sq3*PosMom_Int(dd_index)
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpps]
!------------------------------------------------------------------
      if(Lcode.eq.2110)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!             
              PosMom_Int(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                PosMom_Int(dpp_index) = PosMom_Int(dpp_index) &
                                      + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                PosMom_Int(dpp_index) = PosMom_Int(dpp_index) &
                                      + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              PosMom_Int(dpp_index) = sq3*PosMom_Int(dpp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpsp]
!------------------------------------------------------------------
      if(Lcode.eq.2101)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1

            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2101] from [2100], [2000] and [1100]
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!             
              PosMom_Int(dpp_index) = RRd1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                PosMom_Int(dpp_index) = PosMom_Int(dpp_index) &
                                      + dble(a_i)*RRd2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                PosMom_Int(dpp_index) = PosMom_Int(dpp_index) &
                                      + RRd3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              PosMom_Int(dpp_index) = sq3*PosMom_Int(dpp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dspp]
!------------------------------------------------------------------
      if(Lcode.eq.2011)then
!
! Requires 5 steps
!
        allocate(Int1000(3),Int2000(6),Int1010(9))
        allocate(Int2010(18))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2:
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 5:
! [2011] from [2010], [2000] and [1010]
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!             
              PosMom_Int(dpp_index) = RRd1(i)*Int2010(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                PosMom_Int(dpp_index) = PosMom_Int(dpp_index) &
                                      + dble(a_i)*RRd2*Int1010(pp_index)
              end if
              if(i.eq.p2)then
                PosMom_Int(dpp_index) = PosMom_Int(dpp_index) &
                                      + RRd4*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              PosMom_Int(dpp_index) = sq3*PosMom_Int(dpp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [ddps]
!------------------------------------------------------------------
      if(Lcode.eq.2210)then
!
! Requires 8 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int1200(18),Int2200(36))
        allocate(Int0100(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1200] from [1100], [1000] and [0100]
! b+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              PosMom_Int(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                PosMom_Int(ddp_index) = PosMom_Int(ddp_index) &
                                      + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                PosMom_Int(ddp_index) = PosMom_Int(ddp_index) &
                                      + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddp_index=0
        do d1=1,6
          do d2=1,6
            do p3=1,3
              ddp_index=ddp_index+1
              if(d1.gt.3) PosMom_Int(ddp_index) = sq3*PosMom_Int(ddp_index)
              if(d2.gt.3) PosMom_Int(ddp_index) = sq3*PosMom_Int(ddp_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpds]
!------------------------------------------------------------------
      if(Lcode.eq.2120)then
!
! Requires 8 steps
!
        allocate(Int1000(3),Int2000(6),Int1010(9))
        allocate(Int2010(18),Int1020(18),Int2020(36))
        allocate(Int0010(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 3:
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1020] from [1010], [1000] and [0010]
! c+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1020(pd_index) = RRc1(i)*Int1010(pp_index) &
                              + RRc4*Int1000(p1)
            if(i.eq.p1)then
              Int1020(pd_index) = Int1020(pd_index) &
                                + RRc2*Int0010(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int1020(pd_index) = RRc1(i)*Int1010(pp_index)
              if(i.eq.p1)then
                Int1020(pd_index) = Int1020(pd_index) &
                                  + RRc2*Int0010(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2020] from [2010], [2000] and [1010]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2020(dd_index) = RRc1(i)*Int2010(dp_index) &
                              + RRc4*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2020(dd_index) = Int2020(dd_index) &
                                + dble(a_i)*RRc2*Int1010(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2020(dd_index) = RRc1(i)*Int2010(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2020(dd_index) = Int2020(dd_index) &
                                  + dble(a_i)*RRc2*Int1010(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [2120] from [2020], [2010] and [1020]
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              PosMom_Int(dpd_index) = RRb1(i)*Int2020(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                PosMom_Int(dpd_index) = PosMom_Int(dpd_index) &
                                      + dble(a_i)*RRb2*Int1020(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                PosMom_Int(dpd_index) = PosMom_Int(dpd_index) &
                                      + dble(c_i)*RRb4*Int2010(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              if(d1.gt.3) PosMom_Int(dpd_index) = sq3*PosMom_Int(dpd_index)
              if(d3.gt.3) PosMom_Int(dpd_index) = sq3*PosMom_Int(dpd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dpsd]
!------------------------------------------------------------------
      if(Lcode.eq.2102)then
!
! Requires 8 steps
!
        allocate(Int1000(3),Int2000(6),Int1001(9))
        allocate(Int2001(18),Int1002(18),Int2002(36))
        allocate(Int0001(3))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0001] from [0000]
! d+1i
!
        do i=1,3 ! x,y,z
          Int0001(i) = RRd1(i)*G
        end do ! i 
!
! Step 3:
! [1001] from [1000] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1001(pp_index) = RRd1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1001(pp_index) = Int1001(pp_index) &
                                + RRd2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1002] from [1001], [1000] and [0001]
! c+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1002(pd_index) = RRd1(i)*Int1001(pp_index) &
                              + RRd5*Int1000(p1)
            if(i.eq.p1)then
              Int1002(pd_index) = Int1002(pd_index) &
                                + RRd2*Int0001(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int1002(pd_index) = RRd1(i)*Int1001(pp_index)
              if(i.eq.p1)then
                Int1002(pd_index) = Int1002(pd_index) &
                                  + RRd2*Int0001(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2001] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2001(dp_index) = RRd1(i)*Int2000(d1)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2001(dp_index) = Int2001(dp_index) &
                                + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2002] from [2001], [2000] and [1001]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2002(dd_index) = RRd1(i)*Int2001(dp_index) &
                              + RRd5*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2002(dd_index) = Int2002(dd_index) &
                                + dble(a_i)*RRd2*Int1001(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2002(dd_index) = RRd1(i)*Int2001(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2002(dd_index) = Int2002(dd_index) &
                                  + dble(a_i)*RRd2*Int1001(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 8:
! [2102] from [2002], [2001] and [1002]
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              d_i=Lvec_d(d2,i)
!
              PosMom_Int(dpd_index) = RRb1(i)*Int2002(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                PosMom_Int(dpd_index) = PosMom_Int(dpd_index) &
                                      + dble(a_i)*RRb2*Int1002(pd_index)
              end if
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                PosMom_Int(dpd_index) = PosMom_Int(dpd_index) &
                                      + dble(d_i)*RRb5*Int2001(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              if(d1.gt.3) PosMom_Int(dpd_index) = sq3*PosMom_Int(dpd_index)
              if(d3.gt.3) PosMom_Int(dpd_index) = sq3*PosMom_Int(dpd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [ddds]
!------------------------------------------------------------------
      if(Lcode.eq.2220)then
!
! Requires 12 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int1200(18),Int2200(36))
        allocate(Int0100(3),Int0200(6),Int1210(54))
        allocate(Int2110(54),Int2210(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 6:
! [1200] from [1100], [1000] and [0100]
! b+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
!
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 7:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 8:
! [1210] from [1200], [1100] and [0200]
! c+1i
!
        pdp_index=0
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!                
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! (b-1i)
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(p1.eq.i)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dpp_index=0
        dp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!              
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(p2.eq.i)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 10:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 11:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2210(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [2220] from [2210], [2200], [2110] and [1210]
! c+1i
!   
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              ddd_index=ddd_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              PosMom_Int(ddd_index) = RRc1(i)*Int2210(ddp_index) &
                                    + RRc4*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                PosMom_Int(ddd_index) = PosMom_Int(ddd_index) &
                                      + dble(a_i)*RRc2*Int1210(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                PosMom_Int(ddd_index) = PosMom_Int(ddd_index) &
                                      + dble(b_i)*RRc3*Int2110(dpp_index)
              end if
            end do ! p3
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                PosMom_Int(ddd_index) = RRc1(i)*Int2210(ddp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  PosMom_Int(ddd_index) = PosMom_Int(ddd_index) &
                                        + dble(a_i)*RRc2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  PosMom_Int(ddd_index) = PosMom_Int(ddd_index) &
                                        + dble(b_i)*RRc3*Int2110(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              if(d1.gt.3) PosMom_Int(ddd_index) = sq3*PosMom_Int(ddd_index)
              if(d2.gt.3) PosMom_Int(ddd_index) = sq3*PosMom_Int(ddd_index)
              if(d3.gt.3) PosMom_Int(ddd_index) = sq3*PosMom_Int(ddd_index)
            end do
          end do
        end do
! 
        return
      end if
!
!------------------------------------------------------------------
! [dppp]
!------------------------------------------------------------------
      if(Lcode.eq.2111)then
!
! Requires 9 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int2010(18),Int1110(27))
        allocate(Int0100(3))
        allocate(Int2110(54))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 5 :
! [1110] from [1100], [1000] and [0100]
! c+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!
              Int1110(ppp_index) = RRc1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc2*Int0100(p2)
              end if
              if(i.eq.p2)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc3*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 6:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
!
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 7:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 8:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dpp_index=0
        dp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!                
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(p2.eq.i)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2111] from [2110], [2100], [2010] and [1110]
! d+1i
!
        dppp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp2_index=3*(d1-1)+p2
            do p3=1,3
              dpp_index=dpp_index+1
              dp3_index=3*(d1-1)+p3
              do i=1,3
                dppp_index=dppp_index+1
!
                a_i=Lvec_d(d1,i)
!
                PosMom_Int(dppp_index) = RRd1(i)*Int2110(dpp_index)
!
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  PosMom_Int(dppp_index) = PosMom_Int(dppp_index) &
                                         + dble(a_i)*RRd2*Int1110(ppp_index)
                end if
                if(i.eq.p2)then
                  PosMom_Int(dppp_index) = PosMom_Int(dppp_index) &
                                         + RRd3*Int2010(dp3_index)
                end if
                if(i.eq.p3)then
                  PosMom_Int(dppp_index) = PosMom_Int(dppp_index) &
                                         + RRd4*Int2100(dp2_index)
                end if
              end do ! i
            end do ! p3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              do p4=1,3
                dppp_index=27*(d1-1)+9*(p2-1)+3*(p3-1)+p4
                PosMom_Int(dppp_index) = sq3*PosMom_Int(dppp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [ddpp]
!------------------------------------------------------------------
      if(Lcode.eq.2211)then
!
! Requires 12 steps
!
        allocate(Int1000(3),Int2000(6),Int1100(9))
        allocate(Int2100(18),Int1200(18),Int1210(54))
        allocate(Int0100(3),Int0200(6),Int2200(36))
        allocate(Int2110(54),Int2210(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 5 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [1200] from [1100], [1000] and [0100]
! b+1i
!
        pd_index=0 
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1        
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 7:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1
!
! Step 8:
! [1210] from [1200], [1100] and [0200]
! c+1i
!
        pdp_index=0
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!                
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! (b-1i)
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dpp_index=0
        dp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!                
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(p2.eq.i)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 10:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 11:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2210(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [2211] from [2210], [2200], [2110] and [1210]
! d+1i
!
        ddpp_index=0
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              ddp_index=ddp_index+1
              do i=1,3
                ddpp_index=ddpp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                PosMom_Int(ddpp_index) = RRd1(i)*Int2210(ddp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  PosMom_Int(ddpp_index) = PosMom_Int(ddpp_index) &
                                         + dble(a_i)*RRd2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  PosMom_Int(ddpp_index) = PosMom_Int(ddpp_index) &
                                         + dble(b_i)*RRd3*Int2110(dpp_index)
                end if
                if(i.eq.p3)then
                  PosMom_Int(ddpp_index) = PosMom_Int(ddpp_index) &
                                         + RRd4*Int2200(dd_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddpp_index=0
        do d1=1,6
          do d2=1,6
            do p3=1,3
              do p4=1,3
                ddpp_index=ddpp_index+1
                if(d1.gt.3) PosMom_Int(ddpp_index) = sq3*PosMom_Int(ddpp_index)
                if(d2.gt.3) PosMom_Int(ddpp_index) = sq3*PosMom_Int(ddpp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [dpdp]
!------------------------------------------------------------------
      if(Lcode.eq.2121)then
!
! Requires 12 steps
!
        allocate(Int1000(3),Int2000(6),Int1010(9))
        allocate(Int2010(18),Int1020(18),Int1120(54))
        allocate(Int0010(3),Int0020(6),Int2020(36))
        allocate(Int2110(54),Int2120(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0010] from [0000]
! c+1i
!
        do i=1,3 ! x,y,z
          Int0010(i) = RRc1(i)*G
        end do ! i 
!
! Step 3 :
! [0020] from [0010] and [0000]
! c+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0020(d_index) = RRc1(i)*Int0010(p1) &
                           + RRc4*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0020(d_index) = RRc1(i)*Int0010(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [1020] from [0020], [0010]
! a+1i
!
        do d1=1,6
          do i=1,3
            pd_index=6*(i-1)+d1
!
            c_i=Lvec_d(d1,i)
!
            Int1020(pd_index) = RRa1(i)*Int0020(d1)
            if(c_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1020(pd_index) = Int1020(pd_index) &
                                + dble(c_i)*RRa4*Int0010(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 7:
! [2010] from [2000], [1000]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 8:
! [1120] from [1020], [1010] and [0020]
! b+1i
!
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              ppd_index=18*(p1-1)+6*(i-1)+d2
!                
              c_i=Lvec_d(d2,i)
!
              Int1120(ppd_index) = RRb1(i)*Int1020(pd_index)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! (c-1i)
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + dble(c_i)*RRb4*Int1010(pp_index)
              end if
              if(p1.eq.i)then
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + RRb2*Int0020(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2020] from [2010], [2000] and [1010]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2020(dd_index) = RRc1(i)*Int2010(dp_index) &
                              + RRc4*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2020(dd_index) = Int2020(dd_index) &
                                + dble(a_i)*RRc2*Int1010(pp_index)
            end if
          end do ! p2
! 
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (c-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2020(dd_index) = RRc1(i)*Int2010(dp_index)
!
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2020(dd_index) = Int2020(dd_index) &
                                  + dble(a_i)*RRc2*Int1010(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 10:
! [2110] from [2010], [2000] and [1010]
! b+1i
!
        dp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=9*(d1-1)+3*(i-1)+p2
!                
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRb1(i)*Int2010(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRb2*Int1010(pp_index)
              end if
              if(p2.eq.i)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRb4*Int2000(d1)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 11:
! [2120] from [2020], [2010] and [1020]
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              Int2120(dpd_index) = RRb1(i)*Int2020(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + dble(a_i)*RRb2*Int1020(pd_index)
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + dble(c_i)*RRb4*Int2010(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [2121] from [2120], [2110], [2020] and [1120]
! d+1i
!
        dpdp_index=0
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dd_index=6*(d1-1)+d3
              dpd_index=dpd_index+1
              do i=1,3
                dpdp_index=dpdp_index+1
!
                a_i=Lvec_d(d1,i)
                c_i=Lvec_d(d3,i)
!
                PosMom_Int(dpdp_index) = RRd1(i)*Int2120(dpd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  PosMom_Int(dpdp_index) = PosMom_Int(dpdp_index) &
                                         + dble(a_i)*RRd2*Int1120(ppd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p3=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  PosMom_Int(dpdp_index) = PosMom_Int(dpdp_index) &
                                         + dble(c_i)*RRd4*Int2110(dpp_index)
                end if
                if(i.eq.p2)then
                  PosMom_Int(dpdp_index) = PosMom_Int(dpdp_index) &
                                         + RRd3*Int2020(dd_index)
                end if
              end do ! i
            end do ! d3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpdp_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              do p4=1,3
                dpdp_index=dpdp_index+1
                if(d1.gt.3) PosMom_Int(dpdp_index) = sq3*PosMom_Int(dpdp_index)
                if(d3.gt.3) PosMom_Int(dpdp_index) = sq3*PosMom_Int(dpdp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [dppd]
!------------------------------------------------------------------
      if(Lcode.eq.2112)then
!
! Requires 12 steps
!
        allocate(Int1000(3),Int2000(6),Int1001(9))
        allocate(Int2001(18),Int1002(18),Int1102(54))
        allocate(Int0001(3),Int0002(6),Int2002(36))
        allocate(Int2101(54),Int2102(108))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0001] from [0000]
! d+1i
!
        do i=1,3 ! x,y,z
          Int0001(i) = RRd1(i)*G
        end do ! i 
!
! Step 3 :
! [0002] from [0001] and [0000]
! d+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0002(d_index) = RRd1(i)*Int0001(p1) &
                           + RRd5*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0002(d_index) = RRd1(i)*Int0001(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1001] from [1000] and [0000]
! d+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1001(pp_index) = RRd1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1001(pp_index) = Int1001(pp_index) &
                                + RRd2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [1002] from [0002], [0001]
! a+1i
!
        do d1=1,6
          do i=1,3
            pd_index=6*(i-1)+d1
!
            d_i=Lvec_d(d1,i)
!
            Int1002(pd_index) = RRa1(i)*Int0002(d1)
            if(d_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1002(pd_index) = Int1002(pd_index) &
                                + dble(d_i)*RRa5*Int0001(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 7:
! [2001] from [2000], [1000]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2001(dp_index) = RRd1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2001(dp_index) = Int2001(dp_index) &
                                + dble(a_i)*RRd2*Int1000(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 8:
! [1102] from [1002], [1001] and [0002]
! b+1i
!
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              ppd_index=18*(p1-1)+6*(i-1)+d2
!                
              d_i=Lvec_d(d2,i)
!
              Int1102(ppd_index) = RRb1(i)*Int1002(pd_index)
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! (d-1i)
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1102(ppd_index) = Int1102(ppd_index) &
                                   + dble(d_i)*RRb5*Int1001(pp_index)
              end if
              if(i.eq.p1)then
                Int1102(ppd_index) = Int1102(ppd_index) &
                                   + RRb2*Int0002(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 9:
! [2002] from [2001], [2000] and [1001]
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2002(dd_index) = RRd1(i)*Int2001(dp_index) &
                              + RRd5*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2002(dd_index) = Int2002(dd_index) &
                                + dble(a_i)*RRd2*Int1001(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2002(dd_index) = RRd1(i)*Int2001(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2002(dd_index) = Int2002(dd_index) &
                                  + dble(a_i)*RRd2*Int1001(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 10:
! [2101] from [2001], [2000] and [1001]
! b+1i
!
        dp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=9*(d1-1)+3*(i-1)+p2 
!                
              a_i=Lvec_d(d1,i)
!
              Int2101(dpp_index) = RRb1(i)*Int2001(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2101(dpp_index) = Int2101(dpp_index) &
                                   + dble(a_i)*RRb2*Int1001(pp_index)
              end if
              if(i.eq.p2)then
                Int2101(dpp_index) = Int2101(dpp_index) &
                                   + RRb5*Int2000(d1)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 11:
! [2102] from [2002], [2001] and [1002]
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              d_i=Lvec_d(d2,i)
!
              Int2102(dpd_index) = RRb1(i)*Int2002(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2102(dpd_index) = Int2102(dpd_index) &
                                   + dble(a_i)*RRb2*Int1002(pd_index)
              end if
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! d-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2102(dpd_index) = Int2102(dpd_index) &
                                   + dble(d_i)*RRb5*Int2001(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 12:
! [2112] from [2102], [2101], [2002] and [1102]
! c+1i
!
        dppd_index=0
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dd_index=6*(d1-1)+d3 
              dpd_index=dpd_index+1
              do i=1,3
                dppd_index=54*(d1-1)+18*(p2-1)+6*(i-1)+d3
!
                a_i=Lvec_d(d1,i)
                d_i=Lvec_d(d3,i)
!
                PosMom_Int(dppd_index) = RRc1(i)*Int2102(dpd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  PosMom_Int(dppd_index) = PosMom_Int(dppd_index) &
                                         + dble(a_i)*RRc2*Int1102(ppd_index)
                end if
                if(d_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! d-1i
                  p3=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  PosMom_Int(dppd_index) = PosMom_Int(dppd_index) &
                                         + dble(d_i)*RRc5*Int2101(dpp_index)
                end if
                if(i.eq.p2)then
                  PosMom_Int(dppd_index) = PosMom_Int(dppd_index) &
                                         + RRc3*Int2002(dd_index)
                end if
              end do ! i
            end do ! d3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dppd_index=0
        do d1=1,6
          do p2=1,3
            do p3=1,3
              do d4=1,6
                dppd_index=dppd_index+1
                if(d1.gt.3) PosMom_Int(dppd_index) = sq3*PosMom_Int(dppd_index)
                if(d4.gt.3) PosMom_Int(dppd_index) = sq3*PosMom_Int(dppd_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [dddp]
!------------------------------------------------------------------
      if(Lcode.eq.2221)then
!
! Requires 18 steps
!
        allocate(Int1000(3), Int0100(3))
        allocate(Int0200(6), Int1100(9), Int2000(6))
        allocate(Int0210(18), Int1200(18), Int1110(27))
        allocate(Int2010(18), Int2100(18), Int1210(54))
        allocate(Int2110(54), Int2200(36), Int2210(108))
        allocate(Int1220(108), Int2120(108), Int2220(216))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 4:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [0210] from [0200], [0100]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 7:
! [1200] from [0200], [0100]
! a+1i
!
        do d1=1,6
          do i=1,3
            pd_index=6*(i-1)+d1
!
            b_i=Lvec_d(d1,i)
!
            Int1200(pd_index) = RRa1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int1200(pd_index) = Int1200(pd_index) &
                                + dble(b_i)*RRa3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 8:
! [1110] from [1100], [1000] and [0100]
! c+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!                
              Int1110(ppp_index) = RRc1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc2*Int0100(p2)
              end if
              if(i.eq.p2)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc3*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 9:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1) 
            end if
          end do ! i
        end do ! d1 
!
! Step 10:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 11:
! [1210] from [1200], [1100] and [0200]
! c+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                     + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 12:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 13:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dp_index=0
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 14:
! [1220] from [1210], [1200], [1110] and [0210]
! c+1i
!
        pdd_index=0
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do p3=1,3
              i=p3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              pdd_index=pdd_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index) &
                                 + RRc4*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + dble(b_i)*RRc3*Int1110(ppp_index)
              end if
              if(i.eq.p1)then
                dp_index=3*(d2-1)+p3 
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + RRc2*Int0210(dp_index)
              end if
            end do ! p3
!
            do p3=2,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              do i=1,p3-1
                pdd_index=pdd_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + dble(b_i)*RRc3*Int1110(ppp_index)
                end if
                if(i.eq.p1)then
                  dp_index=3*(d2-1)+p3 
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + RRc2*Int0210(dp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1 
!
! Step 15:
! [2120] from [2110], [2100], [1110] and [2010]
! c+1i
!
        dpd_index=0
        dp2_index=0
        do d1=1,6
          do p2=1,3
            dp2_index=dp2_index+1
            do p3=1,3
              i=p3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              dpd_index=dpd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2120(dpd_index) = RRc1(i)*Int2110(dpp_index) &
                                 + RRc4*Int2100(dp2_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + dble(a_i)*RRc2*Int1110(ppp_index)
              end if
              if(i.eq.p2)then
                dp3_index=3*(d1-1)+p3
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + RRc3*Int2010(dp3_index)
              end if
            end do ! p3
!
            do p3=2,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              do i=1,p3-1
                dpd_index=dpd_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2120(dpd_index) = RRc1(i)*Int2110(dpp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int2120(dpd_index) = Int2120(dpd_index) &
                                     + dble(a_i)*RRc2*Int1110(ppp_index)
                end if
                if(i.eq.p2)then
                  dp3_index=3*(d1-1)+p3
                  Int2120(dpd_index) = Int2120(dpd_index) &
                                     + RRc3*Int2010(dp3_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1 
!
! Step 16:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2210(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 17:
! [2220] from [2210], [2200], [2110], [1210]
! c+1i
!
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddd_index=ddd_index+1
              ddp_index=18*(d1-1)+3*(d2-1)+p3
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index) &
                                 + RRc4*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(a_i)*RRc2*Int1210(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(b_i)*RRc3*Int2110(dpp_index)
              end if
            end do ! p3
!
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int2220(ddd_index) = Int2220(ddd_index) &
                                     + dble(a_i)*RRc2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2220(ddd_index) = Int2220(ddd_index) &
                                     + dble(b_i)*RRc3*Int2110(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! Step 18:
! [2221] from [2220], [2210], [2120], [1220]
! d+1i
!
        ddd_index=0
        dddp_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              do i=1,3
                dddp_index=dddp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                PosMom_Int(dddp_index) = RRd1(i)*Int2220(ddd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdd_index=36*(p1-1)+6*(d2-1)+d3
                  PosMom_Int(dddp_index) = PosMom_Int(dddp_index) &
                                         + dble(a_i)*RRd2*Int1220(pdd_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpd_index=18*(d1-1)+6*(p2-1)+d3
                  PosMom_Int(dddp_index) = PosMom_Int(dddp_index) &
                                         + dble(b_i)*RRd3*Int2120(dpd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p3=dot_product(v_1i,xyz)
                  ddp_index=18*(d1-1)+3*(d2-1)+p3
                  PosMom_Int(dddp_index) = PosMom_Int(dddp_index) &
                                         + dble(c_i)*RRd4*Int2210(ddp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dddp_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              do p4=1,3
                dddp_index=dddp_index+1
                if(d1.gt.3) PosMom_Int(dddp_index) = sq3*PosMom_Int(dddp_index)
                if(d2.gt.3) PosMom_Int(dddp_index) = sq3*PosMom_Int(dddp_index)
                if(d3.gt.3) PosMom_Int(dddp_index) = sq3*PosMom_Int(dddp_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [dddd]
!------------------------------------------------------------------
      if(Lcode.eq.2222)then
!
! Requires 27 steps
!
        allocate(Int1000(3), Int0100(3))
        allocate(Int0110(9), Int1010(9), Int0200(6))
        allocate(Int1100(9), Int2000(6), Int0210(18))
        allocate(Int1110(27), Int2010(18), Int1200(18))
        allocate(Int2100(18), Int0220(36), Int1120(54))
        allocate(Int2020(36), Int1210(54), Int2110(54))
        allocate(Int2200(36), Int1220(108), Int2120(108))
        allocate(Int2210(108), Int1221(324), Int2121(324))
        allocate(Int2211(324), Int2220(216), Int2221(648))
!
! Step 1 :
! [1000] from [0000]
! a+1i
!
        do i=1,3 ! x,y,z
          Int1000(i) = RRa1(i)*G
        end do ! i 
!
! Step 2 :
! [0100] from [0000]
! b+1i
!
        do i=1,3 ! x,y,z
          Int0100(i) = RRb1(i)*G
        end do ! i 
!
! Step 3 :
! [0110] from [0100] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3
          do i=1,3
            pp_index=pp_index+1
!
            Int0110(pp_index) = RRc1(i)*Int0100(p1)
            if(i.eq.p1)then
              Int0110(pp_index) = Int0110(pp_index) &
                                + RRc3*G
            end if
          end do ! i
        end do ! p1
!
! Step 4 :
! [1010] from [1000] and [0000]
! c+1i
!
        pp_index=0
        do p1=1,3
          do i=1,3
            pp_index=pp_index+1
!
            Int1010(pp_index) = RRc1(i)*Int1000(p1)
            if(i.eq.p1)then
              Int1010(pp_index) = Int1010(pp_index) &
                                + RRc2*G
            end if
          end do ! i
        end do ! p1
!
! Step 5 :
! [0200] from [0100] and [0000]
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int0200(d_index) = RRb1(i)*Int0100(p1) &
                           + RRb3*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int0200(d_index) = RRb1(i)*Int0100(p1)
          end do ! i
        end do ! p1
!
! Step 6:
! [1100] from [1000] and [0000]
! b+1i
!
        pp_index=0
        do p1=1,3 ! x, y, z
          do i=1,3 ! x, y, z
            pp_index=pp_index+1
!
            Int1100(pp_index) = RRb1(i)*Int1000(p1)
            if(p1.eq.i)then
              Int1100(pp_index) = Int1100(pp_index) &
                                + RRb2*G
            end if
          end do ! i
        end do ! p1
!
! Step 7 :
! [2000] from [1000] and [0000]
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Int2000(d_index) = RRa1(i)*Int1000(p1) &
                           + RRa2*G
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Int2000(d_index) = RRa1(i)*Int1000(p1)
          end do ! i
        end do ! p1
!
! Step 8:
! [0210] from [0200], [0100]
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            b_i=Lvec_d(d1,i)
!
            Int0210(dp_index) = RRc1(i)*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int0210(dp_index) = Int0210(dp_index) &
                                + dble(b_i)*RRc3*Int0100(p1)
            end if
          end do ! i 
        end do ! d1
!
! Step 9:
! [1110] from [1100], [1000]
! c+1i
!
        ppp_index=0
        pp_index=0
        do p1=1,3
          do p2=1,3
            pp_index=pp_index+1
            do i=1,3
              ppp_index=ppp_index+1
!
              Int1110(ppp_index) = RRc1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc2*Int0100(p2)
              end if
              if(i.eq.p2)then
                Int1110(ppp_index) = Int1110(ppp_index) &
                                   + RRc3*Int1000(p1)
              end if
            end do ! i
          end do ! p2
        end do ! p1
!
! Step 10:
! [2010] from [2000] and [1000]
! c+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2010(dp_index) = RRc1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2010(dp_index) = Int2010(dp_index) &
                                + dble(a_i)*RRc2*Int1000(p1) 
            end if
          end do ! i
        end do ! d1 
!
! Step 11:
! [1200] from [1100], [0100] and [1000]
! b+1i
!
        pd_index=0
        do p1=1,3
          do p2=1,3
            i=p2
            pd_index=pd_index+1
            pp_index=3*(p1-1)+p2
!
            Int1200(pd_index) = RRb1(i)*Int1100(pp_index) &
                              + RRb3*Int1000(p1)
            if(i.eq.p1)then
              Int1200(pd_index) = Int1200(pd_index) &
                                + RRb2*Int0100(p2)
            end if
          end do ! p2
!
          do p2=2,3
            pp_index=3*(p1-1)+p2
            do i=1,p2-1
              pd_index=pd_index+1
!
              Int1200(pd_index) = RRb1(i)*Int1100(pp_index)
              if(i.eq.p1)then
                Int1200(pd_index) = Int1200(pd_index) &
                                  + RRb2*Int0100(p2)
              end if
            end do ! i 
          end do ! p2
        end do ! p1
!
! Step 12:
! [2100] from [2000] and [1000]
! b+1i
!
        dp_index=0 
        do d1=1,6
          do i=1,3
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2100(dp_index) = RRb1(i)*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Int2100(dp_index) = Int2100(dp_index) &
                                + dble(a_i)*RRb2*Int1000(p1)
            end if
          end do ! i
        end do ! d1 
!
! Step 13 : 
! [0220] from [0210], [0200] and [0110]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
!
            b_i=Lvec_d(d1,i)
!
            Int0220(dd_index) = RRc1(i)*Int0210(dp_index) &
                              + RRc4*Int0200(d1)
            if(b_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int0220(dd_index) = Int0220(dd_index) &
                                + dble(b_i)*RRc3*Int0110(pp_index)
            end if
          end do ! p3
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              b_i=Lvec_d(d1,i)
!
              Int0220(dd_index) = RRc1(i)*Int0210(dp_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0220(dd_index) = Int0220(dd_index) &
                                  + dble(b_i)*RRc3*Int0110(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 14:
! [1120] from [1110], [1100], [0110], [1010]
! c+1i
!
        ppd_index=0
        do p1=1,3
          do p2=1,3
            do p3=1,3
              i=p3
              ppd_index=ppd_index+1
              ppp_index=9*(p1-1)+3*(p2-1)+p3 
              pp_index=3*(p1-1)+p2
!
              Int1120(ppd_index) = RRc1(i)*Int1110(ppp_index) &
                                 + RRc4*Int1100(pp_index)
              if(i.eq.p1)then
                pp_index=3*(p2-1)+p3
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + RRc2*Int0110(pp_index)
              end if
              if(i.eq.p2)then
                pp_index=3*(p1-1)+p3
                Int1120(ppd_index) = Int1120(ppd_index) &
                                   + RRc3*Int1010(pp_index)
              end if
            end do ! p3
!
            do p3=2,3
              ppp_index=9*(p1-1)+3*(p2-1)+p3 
              do i=1,p3-1
                ppd_index=ppd_index+1
!
                Int1120(ppd_index) = RRc1(i)*Int1110(ppp_index)
                if(i.eq.p1)then
                  pp_index=3*(p2-1)+p3
                  Int1120(ppd_index) = Int1120(ppd_index) &
                                     + RRc2*Int0110(pp_index)
                end if
                if(i.eq.p2)then
                  pp_index=3*(p1-1)+p3
                  Int1120(ppd_index) = Int1120(ppd_index) &
                                     + RRc3*Int1010(pp_index)
                end if
              end do ! i
            end do ! p3
          end do ! p2
        end do ! p1
!
! Step 15:
! [2020] from [2010], [2000] and [1010]
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
!
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Int2020(dd_index) = RRc1(i)*Int2010(dp_index) &
                              + RRc4*Int2000(d1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2020(dd_index) = Int2020(dd_index) &
                                + dble(a_i)*RRc2*Int1010(pp_index)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2020(dd_index) = RRc1(i)*Int2010(dp_index)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2020(dd_index) = Int2020(dd_index) &
                                  + dble(a_i)*RRc2*Int1010(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 16:
! [1210] from [1200], [1100] and [0200]
! c+1i
!
        pd_index=0
        pdp_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do i=1,3
              pdp_index=pdp_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1210(pdp_index) = RRc1(i)*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + dble(b_i)*RRc3*Int1100(pp_index)
              end if
              if(i.eq.p1)then
                Int1210(pdp_index) = Int1210(pdp_index) &
                                   + RRc2*Int0200(d2)
              end if
            end do ! i
          end do ! d2
        end do ! p1
!
! Step 17:
! [2110] from [2100], [2000] and [1100]
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2110(dpp_index) = RRc1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + dble(a_i)*RRc2*Int1100(pp_index)
              end if
              if(i.eq.p2)then
                Int2110(dpp_index) = Int2110(dpp_index) &
                                   + RRc3*Int2000(d1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 18:
! [2200] from [2100], [2000] and [1100]
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2
            dp_index=3*(d1-1)+p2
            dd_index=dd_index+1
!
            a_i=Lvec_d(d1,i)
!
            Int2200(dd_index) = RRb1(i)*Int2100(dp_index) &
                              + RRb3*Int2000(d1)
            if(a_i.ne.0)then
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Int2200(dd_index) = Int2200(dd_index) &
                                + dble(a_i)*RRb2*Int1100(pp_index)
            end if
          end do ! p2
!
          do p2=2,3
            dp_index=3*(d1-1)+p2
            do i=1,p2-1
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index) = RRb1(i)*Int2100(dp_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index) = Int2200(dd_index) &
                                  + dble(a_i)*RRb2*Int1100(pp_index)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! Step 19:
! [1220] from [1210], [1200], [1110] and [0210]
! c+1i
!
        pdd_index=0
        pd_index=0
        do p1=1,3
          do d2=1,6
            pd_index=pd_index+1
            do p3=1,3
              i=p3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              pdd_index=pdd_index+1
!
              b_i=Lvec_d(d2,i)
!
              Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index) &
                                 + RRc4*Int1200(pd_index)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + dble(b_i)*RRc3*Int1110(ppp_index)
              end if
              if(i.eq.p1)then
                dp_index=3*(d2-1)+p3
                Int1220(pdd_index) = Int1220(pdd_index) &
                                   + RRc2*Int0210(dp_index)
              end if 
            end do ! p3
!
            do p3=2,3
              pdp_index=18*(p1-1)+3*(d2-1)+p3
              do i=1,p3-1
                pdd_index=pdd_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1220(pdd_index) = RRc1(i)*Int1210(pdp_index)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + dble(b_i)*RRc3*Int1110(ppp_index)
                end if
                if(i.eq.p1)then
                  dp_index=3*(d2-1)+p3
                  Int1220(pdd_index) = Int1220(pdd_index) &
                                     + RRc2*Int0210(dp_index)
                end if 
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1 
!
! Step 20:
! [2120] from [2110], [2100], [1110] and [2010]
! c+1i
!
        dpd_index=0
        dp2_index=0
        do d1=1,6
          do p2=1,3
            dp2_index=dp2_index+1
            do p3=1,3
              i=p3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              dpd_index=dpd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2120(dpd_index) = RRc1(i)*Int2110(dpp_index) &
                                 + RRc4*Int2100(dp2_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                ppp_index=9*(p1-1)+3*(p2-1)+p3
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + dble(a_i)*RRc2*Int1110(ppp_index)
              end if
              if(i.eq.p2)then
                dp3_index=3*(d1-1)+p3
                Int2120(dpd_index) = Int2120(dpd_index) &
                                   + RRc3*Int2010(dp3_index)
              end if
            end do ! p3
!
            do p3=2,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              do i=1,p3-1
                dpd_index=dpd_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2120(dpd_index) = RRc1(i)*Int2110(dpp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int2120(dpd_index) = Int2120(dpd_index) &
                                     + dble(a_i)*RRc2*Int1110(ppp_index)
                end if
                if(i.eq.p2)then
                  dp3_index=3*(d1-1)+p3
                  Int2120(dpd_index) = Int2120(dpd_index) &
                                     + RRc3*Int2010(dp3_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! p1 
!
! Step 21:
! [2210] from [2200], [2100] and [1200]
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2210(ddp_index) = RRc1(i)*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(a_i)*RRc2*Int1200(pd_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Int2210(ddp_index) = Int2210(ddp_index) &
                                   + dble(b_i)*RRc3*Int2100(dp_index)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! Step 22 :
! [1221] from [1220], [1210], [1120] and [0220]
! d+1i
!
        pddp_index=0
        pdd_index=0
        do p1=1,3
          do d2=1,6
            do d3=1,6
              pdd_index=pdd_index+1
              dd_index=6*(d2-1)+d3
              do i=1,3
                pddp_index=pddp_index+1
!
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                Int1221(pddp_index) = RRd1(i)*Int1220(pdd_index)
                if(i.eq.p1)then
                  Int1221(pddp_index) = Int1221(pddp_index) &
                                      + RRd2*Int0220(dd_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  Int1221(pddp_index) = Int1221(pddp_index) &
                                      + dble(b_i)*RRd3*Int1120(ppd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int1221(pddp_index) = Int1221(pddp_index) &
                                      + dble(c_i)*RRd4*Int1210(pdp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! p1
!
! Step 23 :
! [2121] from [2120], [2110], [2020] and [1120]
! d+1i
!
        dpdp_index=0
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              dd_index=6*(d1-1)+d3
              do i=1,3
                dpdp_index=dpdp_index+1
!
                a_i=Lvec_d(d1,i)
                c_i=Lvec_d(d3,i)
!
                Int2121(dpdp_index) = RRd1(i)*Int2120(dpd_index)
                if(i.eq.p2)then
                  Int2121(dpdp_index) = Int2121(dpdp_index) &
                                      + RRd3*Int2020(dd_index)
                end if
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  Int2121(dpdp_index) = Int2121(dpdp_index) &
                                      + dble(a_i)*RRd2*Int1120(ppd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2121(dpdp_index) = Int2121(dpdp_index) &
                                      + dble(c_i)*RRd4*Int2110(dpp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! p1
!
! Step 24 :
! [2211] from [2210], [2110], [2200] and [1210]
! d+1i
!
        ddpp_index=0
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              ddp_index=ddp_index+1
              do i=1,3
                ddpp_index=ddpp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2211(ddpp_index) = RRd1(i)*Int2210(ddp_index)
                if(i.eq.p3)then
                  Int2211(ddpp_index) = Int2211(ddpp_index) &
                                      + RRd4*Int2200(dd_index)
                end if
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int2211(ddpp_index) = Int2211(ddpp_index) &
                                      + dble(a_i)*RRd2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2211(ddpp_index) = Int2211(ddpp_index) &
                                      + dble(b_i)*RRd3*Int2110(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! Step 25:
! [2220] from [2210], [2200], [2110], [1210]
! c+1i
!
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddd_index=ddd_index+1
              ddp_index=18*(d1-1)+3*(d2-1)+p3
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index) &
                                 + RRc4*Int2200(dd_index)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(a_i)*RRc2*Int1210(pdp_index)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! b-1i
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                Int2220(ddd_index) = Int2220(ddd_index) &
                                   + dble(b_i)*RRc3*Int2110(dpp_index)
              end if
            end do ! p3
!
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2220(ddd_index) = RRc1(i)*Int2210(ddp_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int2220(ddd_index) = Int2220(ddd_index) &
                                     + dble(a_i)*RRc2*Int1210(pdp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2220(ddd_index) = Int2220(ddd_index) &
                                     + dble(b_i)*RRc3*Int2110(dpp_index)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! Step 26 :
! [2221] from [2220], [2210], [2120] and [1220]
! d+1i
!
        dddp_index=0
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              do i=1,3
                dddp_index=dddp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                Int2221(dddp_index) = RRd1(i)*Int2220(ddd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pdd_index=36*(p1-1)+6*(d2-1)+d3
                  Int2221(dddp_index) = Int2221(dddp_index) &
                                      + dble(a_i)*RRd2*Int1220(pdd_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpd_index=18*(d1-1)+6*(p2-1)+d3
                  Int2221(dddp_index) = Int2221(dddp_index) &
                                      + dble(b_i)*RRd3*Int2120(dpd_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  ddp_index=18*(d1-1)+3*(d2-1)+p3
                  Int2221(dddp_index) = Int2221(dddp_index) &
                                      + dble(c_i)*RRd4*Int2210(ddp_index)
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! d1
!
! Step 27 :
! [2222] from [2221], [2220], [2211], [2121] and [1221]
! d+1i
!
        dddd_index=0
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              do p4=1,3
                i=p4
                dddp_index=108*(d1-1)+18*(d2-1)+3*(d3-1)+p4
                dddd_index=dddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                PosMom_Int(dddd_index) = RRd1(i)*Int2221(dddp_index) &
                                       + RRd5*Int2220(ddd_index)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pddp_index=108*(p1-1)+18*(d2-1)+3*(d3-1)+p4
                  PosMom_Int(dddd_index) = PosMom_Int(dddd_index) &
                                         + dble(a_i)*RRd2*Int1221(pddp_index)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpdp_index=54*(d1-1)+18*(p2-1)+3*(d3-1)+p4
                  PosMom_Int(dddd_index) = PosMom_Int(dddd_index) &
                                         + dble(b_i)*RRd3*Int2121(dpdp_index)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  ddpp_index=54*(d1-1)+9*(d2-1)+3*(p3-1)+p4
                  PosMom_Int(dddd_index) = PosMom_Int(dddd_index) &
                                         + dble(c_i)*RRd4*Int2211(ddpp_index)
                end if
              end do ! p4
!
              do p4=2,3
                dddp_index=108*(d1-1)+18*(d2-1)+3*(d3-1)+p4
                do i=1,p4-1
                  dddd_index=dddd_index+1
!
                  a_i=Lvec_d(d1,i)
                  b_i=Lvec_d(d2,i)
                  c_i=Lvec_d(d3,i)
!
                  PosMom_Int(dddd_index) = RRd1(i)*Int2221(dddp_index)
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1
                    p1=dot_product(v_1i,xyz)
                    pddp_index=108*(p1-1)+18*(d2-1)+3*(d3-1)+p4
                    PosMom_Int(dddd_index) = PosMom_Int(dddd_index) &
                                           + dble(a_i)*RRd2*Int1221(pddp_index)
                  end if
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1
                    p2=dot_product(v_1i,xyz)
                    dpdp_index=54*(d1-1)+18*(p2-1)+3*(d3-1)+p4
                    PosMom_Int(dddd_index) = PosMom_Int(dddd_index) &
                                           + dble(b_i)*RRd3*Int2121(dpdp_index)
                  end if
                  if(c_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d3,1:3)
                    v_1i(i)=v_1i(i)-1
                    p3=dot_product(v_1i,xyz)
                    ddpp_index=54*(d1-1)+9*(d2-1)+3*(p3-1)+p4
                    PosMom_Int(dddd_index) = PosMom_Int(dddd_index) &
                                           + dble(c_i)*RRd4*Int2211(ddpp_index)
                  end if
                end do ! i
              end do ! p4
            end do ! d3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              do d4=1,6
                dddd_index=dddd_index+1
                if(d1.gt.3) PosMom_Int(dddd_index) = sq3*PosMom_Int(dddd_index)
                if(d2.gt.3) PosMom_Int(dddd_index) = sq3*PosMom_Int(dddd_index)
                if(d3.gt.3) PosMom_Int(dddd_index) = sq3*PosMom_Int(dddd_index)
                if(d4.gt.3) PosMom_Int(dddd_index) = sq3*PosMom_Int(dddd_index)
              end do
            end do
          end do
        end do
!
        return
      end if
!
!--------------------------------------------------------------------
! Shouldn't be able to get here, print out an error message
!--------------------------------------------------------------------
!
        write (*,*) 'Orbital angular momentum not recognised by subroutine PosMomIntegral'
        write (*,*) 'Please check that your basis set contains only s and p functions'
!
      end subroutine PosMomIntegral
