!*MODULE EXTRACULE  *DECK PositionIntegral
      subroutine PositionIntegral(Av,Bv,Cv,Dv,a,b,c,d,A_ang,B_ang,C_ang,D_ang,R,verbose,Pos_int)
! 
!   *****************************************************************
!   *                                                               *
!   *  Calculates Position extracule integrals using 8-term RR      *
!   *                                                               *
!   *       --------------------- OUTPUT ---------------------      *
!   *       Pos_int = Position integrals                            *
!   *       --------------------------------------------------      *
!   *                                                               * 
!   *       --------------------- INPUT ----------------------      *
!   *       Av = cartesian coordinates of centre A                  *
!   *       Bv = cartesian coordinates of centre B                  *
!   *       Cv = cartesian coordinates of centre C                  *
!   *       Dv = cartesian coordinates of centre D                  *
!   *       a = exponent alpha (associated with centre A)           *
!   *       b = exponent beta (associated with centre B)            *
!   *       c = exponent gamma (associated with centre C)           *
!   *       d = exponent delta (associated with centre D)           *
!   *       A_ang = angular momentum of basis function on A         *
!   *       B_ang = angular momentum of basis function on B         *
!   *       C_ang = angular momentum of basis function on C         *
!   *       D_ang = angular momentum of basis function on D         * 
!   *       R = value of R (argument to Position extracule)         *
!   *       --------------------------------------------------      *
!   *                                                               *
!   *  For details of the equations contained in this section of    *
!   *  code, see Hollett and Gill, PCCP, 2010                       *
!   *                                                               *
!   *  Code written by JWH (2010)                                   *
!   *  Adapted for E(R) by AJP (2015)				    *
!   *                                                               *
!   *****************************************************************
!
!---------------------------------------------------------------------
! Input/Output:
!---------------------------------------------------------------------
      implicit none
      integer :: A_ang, B_ang, C_ang, D_ang, verbose
      double precision, dimension(3) :: Av, Bv, Cv, Dv
      double precision :: a, b, c, d, R
      double precision, dimension(1296) :: Pos_int 
!
!--------------------------------------------------------------------
! Local: 
!--------------------------------------------------------------------
!
      double precision, dimension(3) :: Uv
      double precision, dimension(3) :: RRa1, RRa2, RRb1, RRb2, RRc1, RRc2, RRd1, RRd2
      double precision :: RRa3, RRa4, RRa5, RRa6, RRa7, RRa8
      double precision :: RRb3, RRb4, RRb5, RRb6, RRb7, RRb8
      double precision :: RRc3, RRc4, RRc5, RRc6, RRc7, RRc8
      double precision :: RRd3, RRd4, RRd5, RRd6, RRd7, RRd8
      double precision, allocatable, dimension(:) :: G, i_bessel
      double precision :: nu2, prefactor, Sab, U_length, besselarg, bargcut, bargmod
      integer :: Lcode, i, j, k, l, m, L_total, a_i, b_i, c_i, d_i
      integer :: ij_index, ijk_index, ijkl_index, ik_index, jk_index
      integer :: d_index, dp_index, pp_index, pd_index, dd_index, dpp_index, pdp_index, ppd_index
      integer :: ddp_index, dpd_index, pdd_index, ddpp_index, dpdp_index, dppd_index, ppp_index
      integer :: pddp_index, dppp_index, dp2_index, dp3_index, dddd_index, ddd_index, dddp_index
      integer :: p1, p2, p3, p4, d1, d2, d3, d4
      integer :: Lvec_d(6,3), v_1i(3), xyz(3)
      double precision :: pi, toler, coeff, sq3
      integer, allocatable :: Factorial(:), Binomial(:,:)
      double precision, allocatable, dimension(:,:) :: Int1000, Int1100, Int1010, Int0110, Int1110
      double precision, allocatable, dimension(:,:) :: Int0100, Int2000, Int0200, Int0020, Int2100
      double precision, allocatable, dimension(:,:) :: Int2010, Int1200, Int2200, Int2020, Int2210
      double precision, allocatable, dimension(:,:) :: Int2120, Int2110, Int1210, Int1120, Int2211
      double precision, allocatable, dimension(:,:) :: Int2121, Int1220, Int2002, Int1102, Int2001
      double precision, allocatable, dimension(:,:) :: Int1020, Int2102, Int2101, Int2220, Int2221
      double precision, allocatable, dimension(:,:) :: Int0210, Int1002, Int1001, Int1221, Int0002
      double precision, allocatable, dimension(:,:) :: Int0010, Int0001, Int0220
!
!--------------------------------------------------------------------
! Begin: 
!--------------------------------------------------------------------
!

		verbose = 0

		if(verbose.eq.1) then
		
				print *, "Hey there, we are in Josh's code."
				print *, ""
				print *, "The input variables are as follows:"
				print *, ""
				print *, "R value: ",R
				print *, ""
				print *, "angmom of a: ",A_ang
				print *, "angmom of b: ",B_ang
				print *, "angmom of c: ",C_ang
				print *, "angmom of d: ",D_ang
				print *, ""
				print *, "exponent of basis function a : ",a
				print *, "exponent of basis function b : ",b
				print *, "exponent of basis function c : ",c
				print *, "exponent of basis function d : ",d
				print *, ""
				print *, "X coordinate of A: ",Av(1)
				print *, "Y coordinate of A: ",Av(2)
				print *, "Z coordinate of A: ",Av(3)
				print *, ""
				print *, "X coordinate of B: ",Bv(1)
				print *, "Y coordinate of B: ",Bv(2)
				print *, "Z coordinate of B: ",Bv(3)
				print *, ""
				print *, "X coordinate of C: ",Cv(1)
				print *, "Y coordinate of C: ",Cv(2)
				print *, "Z coordinate of C: ",Cv(3)
				print *, ""
				print *, "X coordinate of D: ",Dv(1)
				print *, "Y coordinate of D: ",Dv(2)
				print *, "Z coordinate of D: ",Dv(3)
				print *, ""
	
		endif	
			
      toler = 1.0D-10
      bargcut = 550.0D0
!     sq3 = 1.0D0
      sq3 = dsqrt(3.0D0)
      sq3 = 1.0D0
!
      pi = 3.14159265358979323846264338327950288419716939937510d0
! Set angular momentum code
      Lcode = 1000*A_ang + 100*B_ang + 10*C_ang + D_ang

	if(verbose.eq.1) then
	
		print *, ""
		print *, "Lcode: ",Lcode
		print *, ""
		
	endif



! Set angular momentum vectors, for higher functions
      Lvec_d=0
      Lvec_d(1,1)=2 ! xx
      Lvec_d(2,2)=2 ! yy
      Lvec_d(3,3)=2 ! zz
      Lvec_d(4,1)=1 ! xy
      Lvec_d(4,2)=1 
      Lvec_d(5,1)=1 ! xz
      Lvec_d(5,3)=1 
      Lvec_d(6,2)=1 ! yz
      Lvec_d(6,3)=1 
! Set xyz code for p functions
      xyz(1)=1
      xyz(2)=2
      xyz(3)=3
!  
!--------------------------------------------------------------------
!  Calculate common intermediates, prefactor
!--------------------------------------------------------------------
!
      U_length = 0.0D0
      do i=1,3 
        Uv(i) = (a*Av(i)+b*Bv(i))/(a+b) + (c*Cv(i)+d*Dv(i))/(c+d)
        U_length = U_length + Uv(i)**2
      enddo
      U_length = dsqrt(U_length)
!
      nu2 = (a+b)*(c+d)/(a+b+c+d)
!
      Sab = 0.0D0
! calculate exponent first
      do i=1,3
        Sab = Sab - a*b*(Av(i)-Bv(i))**2/(a+b) - c*d*(Cv(i)-Dv(i))**2/(c+d)
      end do
      Sab = exp(Sab)
!      
!-------------------------------------------------------------------
! Calculate required G's
!-------------------------------------------------------------------
!
! Calculate total angular momentum, L_total
      L_total = A_ang + B_ang + C_ang + D_ang 
! 
      allocate(G(0:L_total))
      G(0:L_total) = 0.0D0
! Calculate binomial coefficients
! Factorial first
      allocate(Factorial(0:L_total))
      Factorial(0:L_total)=1
      do m=1,L_total
        Factorial(m) = Factorial(m-1)*m
      end do
! now binomial coefficients
      allocate(Binomial(0:L_total,0:L_total))
      do m=0,L_total
        do k=0,m
          Binomial(m,k) = Factorial(m)/(Factorial(m-k)*Factorial(k))
        end do
      end do
!
! Calculate bessel functions 
! i_bessel(k) contains the kth bessel functions divided by the argument to the kth power (i.e. i_k(x)/x^k )
      allocate(i_bessel(0:8))
      besselarg = 4.0D0*nu2*U_length*R
!
! Bessel functions are numerically unstable close to zero and therefore have to be approximated by power series
!
      if(besselarg.lt.toler)then ! bessel argument < 10^-10
        i_bessel(0) = 1.0D0
        do k=1,8
          i_bessel(k) = i_bessel(k-1)*1.0D0/(2.0D0*dble(k)+1)
        end do
! normal prefactor
        prefactor = Sab*32.0D0*pi**(2.5D0)*R**2*exp(-nu2*(4.0D0*R**2+U_length**2))/(a+b+c+d)**(1.5D0)
! 
      else if((besselarg.gt.toler).and.(besselarg.lt.0.25D0))then ! 10^-10 < bessel argument < 1/4
! Use 6th order power series for all 8 bessel functions
        i_bessel(8) = 1.0D0/34459425.0D0 + besselarg**2/1309458150.0D0 &
                    + besselarg**4/109994484600.0D0 + besselarg**6/15179238874800.0D0
        i_bessel(7) = 1.0D0/2027025.0D0 + besselarg**2/68918850.0D0 &
                    + besselarg**4/5237832600.0D0 + besselarg**6/659966907600.0D0
        i_bessel(6) = besselarg**2*i_bessel(8) + 15.0D0*i_bessel(7)
        i_bessel(5) = besselarg**2*i_bessel(7) + 13.0D0*i_bessel(6)
        i_bessel(4) = besselarg**2*i_bessel(6) + 11.0D0*i_bessel(5)
        i_bessel(3) = besselarg**2*i_bessel(5) + 9.0D0*i_bessel(4)
        i_bessel(2) = besselarg**2*i_bessel(4) + 7.0D0*i_bessel(3)
        i_bessel(1) = besselarg**2*i_bessel(3) + 5.0D0*i_bessel(2)
        i_bessel(0) = besselarg**2*i_bessel(2) + 3.0D0*i_bessel(1)
! normal prefactor
        prefactor = Sab*32.0D0*pi**(2.5D0)*R**2*exp(-nu2*(4.0D0*R**2+U_length**2))/(a+b+c+d)**(1.5D0)
!
      else if((besselarg.gt.0.25D0).and.(besselarg.lt.0.75D0))then ! 1/4 < bessel argument < 3/4
! Use 6th order power series for k=5,8
        i_bessel(8) = 1.0D0/34459425.0D0 + besselarg**2/1309458150.0D0 &
                    + besselarg**4/109994484600.0D0 + besselarg**6/15179238874800.0D0
        i_bessel(7) = 1.0D0/2027025.0D0 + besselarg**2/68918850.0D0 &
                    + besselarg**4/5237832600.0D0 + besselarg**6/659966907600.0D0
        i_bessel(6) = besselarg**2*i_bessel(8) + 15.0D0*i_bessel(7)
        i_bessel(5) = besselarg**2*i_bessel(7) + 13.0D0*i_bessel(6)
! Use straight bessel for k=0,4
        i_bessel(0) = 0.5D0*(exp(besselarg)-exp(-besselarg))/besselarg 
        i_bessel(1) = (0.5D0*(exp(besselarg)+exp(-besselarg))-i_bessel(0))/besselarg**2
        do k=2,4
          i_bessel(k) = (i_bessel(k-2)-(2.0D0*dble(k)-1.0D0)*i_bessel(k-1))/besselarg**2
        end do
! normal prefactor
        prefactor = Sab*32.0D0*pi**(2.5D0)*R**2*exp(-nu2*(4.0D0*R**2+U_length**2))/(a+b+c+d)**(1.5D0)
! 
      else if((besselarg.gt.0.75D0).and.(besselarg.lt.1.7D0))then ! 3/4 < bessel argument < 1.7
! Use 6th order power series for k=7,8
        i_bessel(8) = 1.0D0/34459425.0D0 + besselarg**2/1309458150.0D0 &
                    + besselarg**4/109994484600.0D0 + besselarg**6/15179238874800.0D0
        i_bessel(7) = 1.0D0/2027025.0D0 + besselarg**2/68918850.0D0 &
                    + besselarg**4/5237832600.0D0 + besselarg**6/659966907600.0D0
! Use straight bessel for k=0,6
        i_bessel(0) = 0.5D0*(exp(besselarg)-exp(-besselarg))/besselarg 
        i_bessel(1) = (0.5D0*(exp(besselarg)+exp(-besselarg))-i_bessel(0))/besselarg**2
        do k=2,6
          i_bessel(k) = (i_bessel(k-2)-(2.0D0*dble(k)-1.0D0)*i_bessel(k-1))/besselarg**2
        end do
! normal prefactor
        prefactor = Sab*32.0D0*pi**(2.5D0)*R**2*exp(-nu2*(4.0D0*R**2+U_length**2))/(a+b+c+d)**(1.5D0)
! 
      else if (besselarg > bargcut)then ! large bessel argument
        bargmod = besselarg -nu2*(4.0D0*R**2+U_length**2)
        i_bessel(0) = 0.5d0*exp(bargmod)/besselarg
        if(L_total.ge.1)then
          i_bessel(1) = (0.5d0*exp(bargmod) - i_bessel(0))/besselarg**2
        end if
        do k=2,L_total
          i_bessel(k) = (i_bessel(k-2)-(2.0D0*dble(k)-1.0D0)*i_bessel(k-1))/besselarg**2
        end do
! modified prefactor (no exponential)
        prefactor = Sab*32.0D0*pi**(2.5D0)*R**2/(a+b+c+d)**(1.5D0)
!
      else
        i_bessel(0) = 0.5D0*(exp(besselarg)-exp(-besselarg))/besselarg 
        i_bessel(1) = (0.5D0*(exp(besselarg)+exp(-besselarg))-i_bessel(0))/besselarg**2
        do k=2,8
          i_bessel(k) = (i_bessel(k-2)-(2.0D0*dble(k)-1.0D0)*i_bessel(k-1))/besselarg**2
        end do
! normal prefactor
        prefactor = Sab*32.0D0*pi**(2.5D0)*R**2*exp(-nu2*(4.0D0*R**2+U_length**2))/(a+b+c+d)**(1.5D0)
!
      end if ! besselarg
!
      coeff = 8.0D0*nu2*R**2
!
      do m=0,L_total
! each G_m is a m+1 sum
        do k=0,m
          G(m) = G(m) + dble(Binomial(m,k))*(-1.0D0)**(m-k)*coeff**k*i_bessel(k)
        end do ! k
        G(m) = prefactor*nu2**m*G(m)
      end do ! m
!
! Calculate coefficents for RR
      do i=1,3 ! x,y,z 
        RRa1(i) = b*(Bv(i) - Av(i))/(a+b)
        RRa2(i) = Uv(i)/(a+b)
        RRb1(i) = a*(Av(i) - Bv(i))/(a+b)
        RRb2(i) = Uv(i)/(a+b)
        RRc1(i) = d*(Dv(i) - Cv(i))/(c+d)
        RRc2(i) = Uv(i)/(c+d)
        RRd1(i) = c*(Cv(i) - Dv(i))/(c+d)
        RRd2(i) = Uv(i)/(c+d)
      end do ! i
!
!                                        Corresponding Integral
! RRa
      RRa3 = 1.0D0/(2.0D0*(a+b))         ! [(a-1i)bcd]^(m)
      RRa4 = 1.0D0/(2.0D0*(a+b)**2)      ! [(a-1i)bcd]^(m+1)
      RRa5 = RRa3                        ! [a(b-1i)cd]^(m)
      RRa6 = RRa4                        ! [a(b-1i)cd]^(m+1)
      RRa7 = 1.0D0/(2.0D0*(a+b)*(c+d))  ! [ab(c-1i)d]^(m+1)
      RRa8 = RRa7                        ! [abc(d-1i)]^(m+1)
! RRb
      RRb3 = RRa3
      RRb4 = RRa4
      RRb5 = RRa3                        ! same as RRa
      RRb6 = RRa4
      RRb7 = RRa7
      RRb8 = RRa8
! RRc
      RRc3 = RRb7                        ! [(a-1i)bcd]^(m+1)
      RRc4 = RRb8                        ! [a(b-1i)cd]^(m+1)
      RRc5 = 1.0D0/(2.0D0*(c+d))         ! [ab(c-1i)d]^(m)
      RRc6 = 1.0D0/(2.0D0*(c+d)**2)      ! [ab(c-1i)d]^(m+1)
      RRc7 = RRc5                        ! [abc(d-1i)]^(m)
      RRc8 = RRc6                        ! [abc(d-1i)]^(m+1) 
! RRd
      RRd3 = RRc3
      RRd4 = RRc4
      RRd5 = RRc5                        ! same as RRc 
      RRd6 = RRc6
      RRd7 = RRc7
      RRd8 = RRc8
!
!------------------------------------------------------------------
! [ssss]
!------------------------------------------------------------------
      if(Lcode.eq.0)then
!
!        Pos_Int(1) = prefactor*G(0)
        Pos_Int(1) = G(0)
!
        return
      end if
!
!------------------------------------------------------------------
! [psss]
!------------------------------------------------------------------
      if(Lcode.eq.1000)then
!
! Requires one step
!
! Step 1 :
! [1000]^(0) from [0000]^(1)
!
        do i=1,3 ! x,y,z 
! 2-terms required
          Pos_Int(i) = RRa1(i)*G(0) + RRa2(i)*G(1)
        end do !
!
! multiply through prefactor
!        Pos_Int(1:3) = prefactor*Pos_Int(1:3) 
!
! print for testing
!        write(6,'(a,F16.10)')'a= ',a
!        write(6,'(a,F16.10)')'b= ',b
!        write(6,'(a,F16.10)')'c= ',c
!        write(6,'(a,F16.10)')'d= ',d
!        write(6,'(a,F16.10)')'G(0)= ',G(0)
!        write(6,'(a,F16.10)')'G(1)= ',G(1)
!        do i=1,3
!          write(6,'(a,I1,a,F16.10)')'RRa1(',i,')= ',RRa1(i)
!        end do
!        do i=1,3
!          write(6,'(a,I1,a,F16.10)')'RRa2(',i,')= ',RRa2(i)
!        end do
!        do i=1,3
!          write(6,'(a,I1,a,F16.10)')'Pos_Int(',i,')= ',Pos_Int(i)
!        end do
!
        return
      end if
!
!------------------------------------------------------------------
! [ppss]
!------------------------------------------------------------------
      if(Lcode.eq.1100)then
!
! Requires 2 steps
!
  allocate(Int1000(3,0:L_total-1))
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i
        end do ! m
!
! Step 2 :
! [1100]^(0) from [1000]^(1) and [0000]^(1)
! b+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 4-terms
              Pos_Int(ij_index) = RRb1(j)*Int1000(i,0) + RRb2(j)*Int1000(i,1) &
                                + RRb3*G(0) + RRb4*G(1)
            else ! 2-terms 
              Pos_Int(ij_index) = RRb1(j)*Int1000(i,0) + RRb2(j)*Int1000(i,1)
            end if
          end do ! j
        end do ! i
!
! multiply through prefactor
!        Pos_Int(1:9) = prefactor*Pos_Int(1:9)
!
        return
      end if
!
!------------------------------------------------------------------
! [psps]
!------------------------------------------------------------------
      if(Lcode.eq.1010)then
!
! Requires 2 steps
!
  allocate(Int1000(3,0:L_total-1))
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i
        end do ! m 
!
! Step 2 :
! [1010]^(0) from [1000]^(1) and [0000]^(1)
! c+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 3-terms
              Pos_Int(ij_index) = RRc1(j)*Int1000(i,0) + RRc2(j)*Int1000(i,1) &
                                + RRc3*G(1)
            else ! 2-terms 
              Pos_Int(ij_index) = RRc1(j)*Int1000(i,0) + RRc2(j)*Int1000(i,1)
            end if
          end do ! j
        end do ! i
!
! multiply through prefactor
!        Pos_Int(1:9) = prefactor*Pos_Int(1:9)
!
        return
      end if
!
!------------------------------------------------------------------
! [pssp]
!------------------------------------------------------------------
      if(Lcode.eq.1001)then
!
! Requires 2 steps
!
  allocate(Int1000(3,0:L_total-1))
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i
        end do ! m
!
! Step 2 :
! [1001]^(0) from [1000]^(1) and [0000]^(1)
! d+1i
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z
            ij_index = 3*(i-1) + j
            if(i.eq.j)then ! 3-terms
              Pos_Int(ij_index) = RRd1(i)*Int1000(i,0) + RRd2(i)*Int1000(i,1) &
                                 + RRd3*G(1)
            else ! 2-terms 
              Pos_Int(ij_index) = RRd1(j)*Int1000(i,0) + RRd2(j)*Int1000(i,1)
            end if
          end do ! j
        end do ! i
!
! multiply through prefactor
!        Pos_Int(1:9) = prefactor*Pos_Int(1:9)
!
        return
      end if
!
!------------------------------------------------------------------
! [ppps]
!------------------------------------------------------------------
      if(Lcode.eq.1110)then
!
! Requires 4 steps
!
  allocate(Int1000(3,0:L_total-1),Int0100(3,0:L_total-2),Int1100(9,0:L_total-2))
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i
        end do ! m 
!
! Step 2 :
! [0100]^(1) from [0000]^(2)
! b+1i
!
        do m=0,L_total-2
          do i=1,3 ! x,y,z
            Int0100(i,m) = RRb1(i)*G(m) + RRb2(i)*G(m+1)
          end do ! i
        end do ! m
!
! Step 3 :
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
        do m=0,L_total-2 
          do i=1,3 ! x,y,z 
            do j=1,3 ! x,y,z 
              ij_index = 3*(i-1) + j
              if(i.eq.j)then ! 4-terms
                Int1100(ij_index,m) = RRb1(j)*Int1000(i,m) + RRb2(j)*Int1000(i,m+1) &
                                    + RRb3*G(m) + RRb4*G(m+1)
              else ! 2-terms
                Int1100(ij_index,m) = RRb1(j)*Int1000(i,m) + RRb2(j)*Int1000(i,m+1)
              end if
            end do ! j
          end do ! i
        end do ! l
!
! Step 4 :
! [1110]^(0) from [1100]^(1), [1000]^(1), and [0100]^(1)
! c+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            do k=1,3 ! x,y,z
              ijk_index = 9*(i-1) + 3*(j-1) + k
              ij_index = 3*(i-1) + j
              if((i.eq.k).and.(j.eq.k))then ! 4-terms
                Pos_Int(ijk_index) = RRc1(k)*Int1100(ij_index,0) + RRc2(k)*Int1100(ij_index,1) &
                                   + RRc3*Int0100(j,1) + RRc4*Int1000(i,1)
              else if(i.eq.k)then ! 3-terms
                Pos_Int(ijk_index) = RRc1(k)*Int1100(ij_index,0) + RRc2(k)*Int1100(ij_index,1) &
                                   + RRc3*Int0100(j,1)
              else if(j.eq.k)then ! 3-terms
                Pos_Int(ijk_index) = RRc1(k)*Int1100(ij_index,0) + RRc2(k)*Int1100(ij_index,1) &
                                   + RRc4*Int1000(i,1)
              else ! 2-terms
                Pos_Int(ijk_index) = RRc1(k)*Int1100(ij_index,0) + RRc2(k)*Int1100(ij_index,1)
              end if
            end do ! k
          end do ! j
        end do ! i
!
! multiply through prefactor
!        Pos_Int(1:27) = prefactor*Pos_Int(1:27)
!
        return
      end if
!
!------------------------------------------------------------------
! [ppsp]
!------------------------------------------------------------------
      if(Lcode.eq.1101)then
!
! Requires 4 steps
!
  allocate(Int1000(3,0:L_total-1),Int0100(3,0:L_total-2),Int1100(9,0:L_total-2))
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i
        end do ! m
!
! Step 2 :
! [0100]^(1) from [0000]^(2)
! b+1i
!
        do m=0,L_total-2
          do i=1,3 ! x,y,z
            Int0100(i,m) = RRb1(i)*G(m) + RRb2(i)*G(m+1)
          end do ! i
        end do ! m
!
! Step 3 :
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
        do l=0,L_total-2
          do i=1,3 ! x,y,z 
            do j=1,3 ! x,y,z 
              ij_index = 3*(i-1) + j
              if(i.eq.j)then ! 4-terms
                Int1100(ij_index,m) = RRb1(j)*Int1000(i,m) + RRb2(j)*Int1000(i,m+1) &
                                    + RRb3*G(m) + RRb4*G(m+1)
              else ! 2-terms
                Int1100(ij_index,m) = RRb1(j)*Int1000(i,m) + RRb2(j)*Int1000(i,m+1)
              end if
            end do ! j
          end do ! i
        end do ! m
!
! Step 4 :
! [1101]^(0) from [1100]^(0,1), [1000]^(0,1), and [0100]^(0,1)
! d+1i
!
        do i=1,3 ! x,y,z
          do j=1,3 ! x,y,z
            do k=1,3 ! x,y,z
              ijk_index = 9*(i-1) + 3*(j-1) + k
              ij_index = 3*(i-1) + j
              if((i.eq.k).and.(j.eq.k))then ! 4-terms
                Pos_Int(ijk_index) = RRd1(k)*Int1100(ij_index,0) + RRd2(k)*Int1100(ij_index,1) &
                                   + RRd3*Int0100(j,1) + RRd4*Int1000(i,1)
              else if(i.eq.k)then ! 3-terms
                Pos_Int(ijk_index) = RRd1(k)*Int1100(ij_index,0) + RRd2(k)*Int1100(ij_index,1) &
                                   + RRd3*Int0100(j,1)
              else if(j.eq.k)then ! 3-terms
                Pos_Int(ijk_index) = RRd1(k)*Int1100(ij_index,0) + RRd2(k)*Int1100(ij_index,1) &
                                   + RRd4*Int1000(i,1)
              else ! 2-terms
                Pos_Int(ijk_index) = RRd1(k)*Int1100(ij_index,0) + RRd2(k)*Int1100(ij_index,1)
              end if
            end do ! k
          end do ! j
        end do ! i
!
! multiply through prefactor
!        Pos_Int(1:27) = prefactor*Pos_Int(1:27)
!
        return
      end if
!
!------------------------------------------------------------------
! [pppp]
!------------------------------------------------------------------
      if(Lcode.eq.1111)then
!
! Requires 7 steps
!
  allocate(Int1000(3,0:L_total-1),Int0100(3,0:L_total-2),Int1100(9,0:L_total-2))
  allocate(Int1010(9,0:L_total-3),Int0110(9,0:L_total-3),Int1110(27,0:L_total-3))
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0100]^(2) from [0000]^(3) 
! b+1i
!
        do m=0,L_total-2
          do i=1,3 ! x,y,z
            Int0100(i,m) = RRb1(i)*G(m) + RRb2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3 :
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
        do m=0,L_total-2
          do i=1,3 ! x,y,z 
            do j=1,3 ! x,y,z
              ij_index = 3*(i-1) + j
              if(i.eq.j)then ! 4-terms
                Int1100(ij_index,m) = RRb1(j)*Int1000(i,m) + RRb2(j)*Int1000(i,m+1) &
                                    + RRb3*G(m) + RRb4*G(m+1)
              else ! 2-terms
                Int1100(ij_index,m) = RRb1(j)*Int1000(i,m) + RRb2(j)*Int1000(i,m+1)
              end if
            end do ! j
          end do ! i
        end do ! m
!
! Step 4 :
! [1010]^(1) from [1000]^(2) and [0000]^(2)
! c+1i
!
        do m=0,L_total-3
          do i=1,3 ! x,y,z
            do j=1,3 ! x,y,z
              ij_index = 3*(i-1) + j
              if(i.eq.j)then ! 3-terms
                Int1010(ij_index,m) = RRc1(j)*Int1000(i,m) + RRc2(j)*Int1000(i,m+1) &
                                    + RRc3*G(m+1)
              else ! 2-terms
                Int1010(ij_index,m) = RRc1(j)*Int1000(i,m) + RRc2(j)*Int1000(i,m+1)
              end if
            end do ! j
          end do ! i
        end do ! m
!
! Step 5 :
! [0110]^(1) from [0100]^(2) and [0000]^(2)
! c+1i
!
        do m=0,L_total-3
          do i=1,3 ! x,y,z
            do j=1,3 ! x,y,z
              ij_index = 3*(i-1) + j
              if(i.eq.j)then ! 3-terms
                Int0110(ij_index,m) = RRc1(j)*Int0100(i,m) + RRc2(j)*Int0100(i,m+1) &
                                    + RRc4*G(m+1)
              else ! 2-terms
                Int0110(ij_index,m) = RRc1(j)*Int0100(i,m) + RRc2(j)*Int0100(i,m+1)
              end if
            end do ! j
          end do ! i
        end do ! m 
!
! Step 6 :
! [1110]^(1) from [1100]^(2), [0100]^(2) and [1000]^(2)
! c+1i
!
        do m=0,L_total-3 
          do i=1,3 ! x,y,z
            do j=1,3 ! x,y,z
              do k=1,3 ! x,y,z
                ijk_index = 9*(i-1) + 3*(j-1) + k
                ij_index = 3*(i-1) + j
                if((i.eq.k).and.(j.eq.k))then ! 4-terms
                  Int1110(Ijk_index,m) = RRc1(k)*Int1100(ij_index,m) + RRc2(k)*Int1100(ij_index,m+1) &
                                       + RRc3*Int0100(j,m+1) + RRc4*Int1000(i,m+1)
                else if(i.eq.k)then ! 3-terms
                  Int1110(Ijk_index,m) = RRc1(k)*Int1100(ij_index,m) + RRc2(k)*Int1100(ij_index,m+1) &
                                       + RRc3*Int0100(j,m+1)
                else if(j.eq.k)then ! 3-terms
                  Int1110(Ijk_index,m) = RRc1(k)*Int1100(ij_index,m) + RRc2(k)*Int1100(ij_index,m+1) &
                                       + RRc4*Int1000(i,m+1)
                else ! 2-terms
                  Int1110(Ijk_index,m) = RRc1(k)*Int1100(ij_index,m) + RRc2(k)*Int1100(ij_index,m+1)
                end if
              end do ! k
            end do ! j
          end do ! i
        end do ! m 
!
! Step 7 :
! [1111]^(0) from [1110]^(1), [0110]^(1), [1010]^(1) and [1100]^(1)
! d+1i
!
        do i=1,3 ! x,y,z 
          do j=1,3 ! x,y,z 
            do k=1,3 ! x,y,z 
              do l=1,3 ! x,y,z 
                ijkl_index = 27*(i-1) + 9*(j-1) + 3*(k-1) + l
                ijk_index = 9*(i-1) + 3*(j-1) + k
                ij_index = 3*(i-1) + j
                ik_index = 3*(i-1) + k
                jk_index = 3*(j-1) + k
                if((i.eq.l).and.(j.eq.l).and.(k.eq.l))then
                  Pos_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index,0) + RRd2(l)*Int1110(ijk_index,1) &
                                      + RRd3*Int0110(jk_index,1) + RRd4*Int1010(ik_index,1) &
                                      + RRd5*Int1100(ij_index,0) + RRd6*Int1100(ij_index,1)
                else if((i.eq.l).and.(j.eq.l))then
                  Pos_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index,0) + RRd2(l)*Int1110(ijk_index,1) &
                                      + RRd3*Int0110(jk_index,1) + RRd4*Int1010(ik_index,1)
                else if((i.eq.l).and.(k.eq.l))then 
                  Pos_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index,0) + RRd2(l)*Int1110(ijk_index,1) &
                                      + RRd3*Int0110(jk_index,1) &
                                      + RRd5*Int1100(ij_index,0) + RRd6*Int1100(ij_index,1)
                else if((j.eq.l).and.(k.eq.l))then 
                  Pos_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index,0) + RRd2(l)*Int1110(ijk_index,1) &
                                      + RRd4*Int1010(ik_index,1) &
                                      + RRd5*Int1100(ij_index,0) + RRd6*Int1100(ij_index,1)
                else if(i.eq.l)then 
                  Pos_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index,0) + RRd2(l)*Int1110(ijk_index,1) &
                                      + RRd3*Int0110(jk_index,1)
                else if(j.eq.l)then 
                  Pos_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index,0) + RRd2(l)*Int1110(ijk_index,1) &
                                      + RRd4*Int1010(ik_index,1)
                else if(k.eq.l)then 
                  Pos_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index,0) + RRd2(l)*Int1110(ijk_index,1) &
                                      + RRd5*Int1100(ij_index,0) + RRd6*Int1100(ij_index,1)
                else
                  Pos_Int(ijkl_index) = RRd1(l)*Int1110(ijk_index,0) + RRd2(l)*Int1110(ijk_index,1)
                end if
              end do ! l
            end do ! k
          end do ! j
        end do ! l
!
! multiply through prefactor
!        Pos_Int(1:81) = prefactor*Pos_Int(1:81)
!
        return
      end if
!
!------------------------------------------------------------------
! [dsss]
! Note: d integrals required some modification of code notation
!------------------------------------------------------------------
      if(Lcode.eq.2000)then
!
! Requires 2 steps
!
        allocate(Int1000(3,0:L_total-1))
!
! Step 1 :
! [1000]^(1) from [0000]^(2)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [2000]^(0) from [1000]^(1) and [0000]^(1)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
! do first three d's
        d_index=0
        do p1=1,3 ! x,y,z
          i=p1
          d_index=d_index+1
          Pos_Int(d_index) = RRa1(i)*Int1000(p1,0) + RRa2(i)*Int1000(p1,1) &
                           + RRa3*G(0) + RRa4*G(1)
        end do ! p1
! do last three d's
        do p1=2,3 ! y,z
          do i=1,p1-1
            d_index=d_index+1
            ! by design p1 ne i
            Pos_Int(d_index) = RRa1(i)*Int1000(p1,0) + RRa2(i)*Int1000(p1,1)
          end do ! i
        end do ! p1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        Pos_Int(4:6)= sq3*Pos_Int(4:6)
! 
! multiply through prefactor
!        Pos_Int(1:6) = prefactor*Pos_Int(1:6)
!
        return
      end if
!
!------------------------------------------------------------------
! [dpss]
!------------------------------------------------------------------
      if(Lcode.eq.2100)then
!
! Requires 3 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2))
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [2000]^(1) from [1000]^(2) and [0000]^(2)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                               + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design i ne p1
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 3:
! [2100]^(0) from [2000]^(1) and [1000]^(1)
! b+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Pos_Int(dp_index) = RRb1(i)*Int2000(d1,0) + RRb2(i)*Int2000(d1,1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Pos_Int(dp_index) = Pos_Int(dp_index) &
                                + dble(a_i)*(RRb3*Int1000(p1,0) + RRb4*Int1000(p1,1))
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            Pos_Int(dp_index) = sq3*Pos_Int(dp_index)
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:18) = prefactor*Pos_Int(1:18)
!
        return
      end if
!
!------------------------------------------------------------------
! [dsps]
!------------------------------------------------------------------
      if(Lcode.eq.2010)then
!
! Requires 3 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2))
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [2000]^(1) from [1000]^(2) and [0000]^(2)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design i ne p1
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 3:
! [2010]^(0) from [2000]^(1) and [1000]^(1)
! c+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Pos_Int(dp_index) = RRc1(i)*Int2000(d1,0) + RRc2(i)*Int2000(d1,1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Pos_Int(dp_index) = Pos_Int(dp_index) &
                                + dble(a_i)*RRc3*Int1000(p1,1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            Pos_Int(dp_index) = sq3*Pos_Int(dp_index)
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:18) = prefactor*Pos_Int(1:18)
!
        return
      end if
!
!------------------------------------------------------------------
! [dssp]
!------------------------------------------------------------------
      if(Lcode.eq.2001)then
!
! Requires 3 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2))
!
! Step 1 :
! [1000]^(2) from [0000]^(3)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [2000]^(1) from [1000]^(2) and [0000]^(2)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design i ne p1
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 3:
! [2001]^(0) from [2000]^(1) and [1000]^(1)
! d+1i
!
        dp_index=0
        do d1=1,6
          do i=1,3 ! x, y, z
            dp_index=dp_index+1
!
            a_i=Lvec_d(d1,i)
!
            Pos_Int(dp_index) = RRd1(i)*Int2000(d1,0) + RRd2(i)*Int2000(d1,1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1
              p1=dot_product(v_1i,xyz)
              Pos_Int(dp_index) = Pos_Int(dp_index) &
                                + dble(a_i)*RRd3*Int1000(p1,1)
            end if
          end do ! i
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            dp_index=3*(d1-1)+p2
            Pos_Int(dp_index) = sq3*Pos_Int(dp_index)
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:18) = prefactor*Pos_Int(1:18)
!
        return
      end if
!
!------------------------------------------------------------------
! [ddss]
!------------------------------------------------------------------
      if(Lcode.eq.2200)then
!
! Requires 5 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1100(9,0:L_total-3))
        allocate(Int2100(18,0:L_total-3))
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2:
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1100(pp_index,m) = RRb1(i)*Int1000(p1,m) + RRb2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1100(pp_index,m) = Int1100(pp_index,m) &
                                    + RRb3*G(m) + RRb4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                               + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [2100]^(1) from [2000]^(2) and [1000]^(2)
! b+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2100(dp_index,m) = RRb1(i)*Int2000(d1,m) + RRb2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2100(dp_index,m) = Int2100(dp_index,m) &
                                    + dble(a_i)*(RRb3*Int1000(p1,m) + RRb4*Int1000(p1,m+1))
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 5:
! [2200]^(0) from [2100]^(1), [2000]^(1) and [1100]^(1)
! b+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Pos_Int(dd_index) = RRb1(i)*Int2100(dp_index,0) + RRb2(i)*Int2100(dp_index,1) &
                              + RRb5*Int2000(d1,0)          + RRb6*Int2000(d1,1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Pos_Int(dd_index) = Pos_Int(dd_index) &
                                + dble(a_i)*(RRb3*Int1100(pp_index,0) + RRb4*Int1100(pp_index,1))
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Pos_Int(dd_index) = RRb1(i)*Int2100(dp_index,0) + RRb2(i)*Int2100(dp_index,1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Pos_Int(dd_index) = Pos_Int(dd_index) &
                                  + dble(a_i)*(RRb3*Int1100(pp_index,0) + RRb4*Int1100(pp_index,1))
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) Pos_Int(dd_index) = sq3*Pos_Int(dd_index)
            if(d2.gt.3) Pos_Int(dd_index) = sq3*Pos_Int(dd_index)
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:36) = prefactor*Pos_Int(1:36)
!
        return
      end if
!
!------------------------------------------------------------------
! [dsds]
!------------------------------------------------------------------
      if(Lcode.eq.2020)then
!
! Requires 5 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1010(9,0:L_total-3))
        allocate(Int2010(18,0:L_total-3))
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2:
! [1010]^(1) from [1000]^(2) and [0000]^(2)
! c+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1010(pp_index,m) = RRc1(i)*Int1000(p1,m) + RRc2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1010(pp_index,m) = Int1010(pp_index,m) &
                                    + RRc3*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [2010]^(1) from [2000]^(2) and [1000]^(2)
! c+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2010(dp_index,m) = RRc1(i)*Int2000(d1,m) + RRc2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2010(dp_index,m) = Int2010(dp_index,m) &
                                    + dble(a_i)*RRc3*Int1000(p1,m+1)
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 5:
! [2020]^(0) from [2010]^(1), [2000]^(1) and [1010]^(1)
! c+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Pos_Int(dd_index) = RRc1(i)*Int2010(dp_index,0) + RRc2(i)*Int2010(dp_index,1) &
                              + RRc5*Int2000(d1,0)          + RRc6*Int2000(d1,1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Pos_Int(dd_index) = Pos_Int(dd_index) &
                                + dble(a_i)*RRc3*Int1010(pp_index,1)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (c-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Pos_Int(dd_index) = RRc1(i)*Int2010(dp_index,0) + RRc2(i)*Int2010(dp_index,1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Pos_Int(dd_index) = Pos_Int(dd_index) &
                                  + dble(a_i)*RRc3*Int1010(pp_index,1)
              end if
            end do ! i
          end do ! p2
!
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) Pos_Int(dd_index) = sq3*Pos_Int(dd_index)
            if(d2.gt.3) Pos_Int(dd_index) = sq3*Pos_Int(dd_index)
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:36) = prefactor*Pos_Int(1:36)
!
        return
      end if
!
!------------------------------------------------------------------
! [dssd]
!------------------------------------------------------------------
      if(Lcode.eq.2002)then
!
! Requires 5 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1001(9,0:L_total-3))
        allocate(Int2001(18,0:L_total-3))
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2:
! [1001]^(1) from [1000]^(2) and [0000]^(2)
! d+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1001(pp_index,m) = RRd1(i)*Int1000(p1,m) + RRd2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1001(pp_index,m) = Int1001(pp_index,m) &
                                    + RRd3*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [2001]^(1) from [2000]^(2) and [1000]^(2)
! d+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2001(dp_index,m) = RRd1(i)*Int2000(d1,m) + RRd2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2001(dp_index,m) = Int2001(dp_index,m) &
                                    + dble(a_i)*RRd3*Int1000(p1,m+1)
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 5:
! [2002]^(0) from [2001]^(1), [2000]^(1) and [1001]^(1)
! d+1i
!
        dd_index=0
        do d1=1,6
          do p2=1,3
            i=p2 ! do xx, yy, zz first
            dd_index=dd_index+1
            dp_index=3*(d1-1)+p2
! 
            a_i=Lvec_d(d1,i) ! angular momentum vector
!
            Pos_Int(dd_index) = RRd1(i)*Int2001(dp_index,0) + RRd2(i)*Int2001(dp_index,1) &
                              + RRd7*Int2000(d1,0)          + RRd8*Int2000(d1,1)
            if(a_i.ne.0)then ! (a-1i) terms
              v_1i(1:3)=Lvec_d(d1,1:3)
              v_1i(i)=v_1i(i)-1 ! a-1_i
              p1=dot_product(v_1i,xyz)
              pp_index=3*(p1-1)+p2
              Pos_Int(dd_index) = Pos_Int(dd_index) &
                                + dble(a_i)*RRd3*Int1001(pp_index,1)
            end if
          end do ! p2
!
          do p2=2,3 ! y, z
            dp_index=3*(d1-1)+p2
            do i=1,p2-1 ! x, y
              dd_index=dd_index+1
!
! i ne p2, therefore no (d-1i) terms
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Pos_Int(dd_index) = RRd1(i)*Int2001(dp_index,0) + RRd2(i)*Int2001(dp_index,1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Pos_Int(dd_index) = Pos_Int(dd_index) &
                                  + dble(a_i)*RRd3*Int1001(pp_index,1)
              end if
            end do ! i
          end do ! p2
!
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            if(d1.gt.3) Pos_Int(dd_index) = sq3*Pos_Int(dd_index)
            if(d2.gt.3) Pos_Int(dd_index) = sq3*Pos_Int(dd_index)
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:36) = prefactor*Pos_Int(1:36)
!
        return
      end if
!
!------------------------------------------------------------------
! [dpps]
!------------------------------------------------------------------
      if(Lcode.eq.2110)then
!
! Requires 5 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1100(9,0:L_total-3))
        allocate(Int2100(18,0:L_total-3))
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2:
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1100(pp_index,m) = RRb1(i)*Int1000(p1,m) + RRb2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1100(pp_index,m) = Int1100(pp_index,m) &
                                    + RRb3*G(m) + RRb4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [2100]^(1) from [2000]^(2) and [1000]^(2)
! b+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2100(dp_index,m) = RRb1(i)*Int2000(d1,m) + RRb2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2100(dp_index,m) = Int2100(dp_index,m) &
                                    + dble(a_i)*(RRb3*Int1000(p1,m) + RRb4*Int1000(p1,m+1))
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 5:
! [2110]^(0) from [2100]^(1), [2000]^(1) and [1100]^(1)
! c+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!             
              Pos_Int(dpp_index) = RRc1(i)*Int2100(dp_index,0) + RRc2(i)*Int2100(dp_index,1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Pos_Int(dpp_index) = Pos_Int(dpp_index) &
                                   + dble(a_i)*RRc3*Int1100(pp_index,1)
              end if
              if(i.eq.p2)then
                Pos_Int(dpp_index) = Pos_Int(dpp_index) &
                                   + RRc4*Int2000(d1,1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              Pos_Int(dpp_index) = sq3*Pos_Int(dpp_index)
            end do
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:54) = prefactor*Pos_Int(1:54)
!
        return
      end if
!
!------------------------------------------------------------------
! [dpsp]
!------------------------------------------------------------------
      if(Lcode.eq.2101)then
!
! Requires 5 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1100(9,0:L_total-3))
        allocate(Int2100(18,0:L_total-3))
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2:
! [1100]^(1) from [1000]^(2) and [0000]^(2)
! b+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1

              Int1100(pp_index,m) = RRb1(i)*Int1000(p1,m) + RRb2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1100(pp_index,m) = Int1100(pp_index,m) &
                                    + RRb3*G(m) + RRb4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [2100]^(1) from [2000]^(2) and [1000]^(2)
! b+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2100(dp_index,m) = RRb1(i)*Int2000(d1,m) + RRb2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2100(dp_index,m) = Int2100(dp_index,m) &
                                    + dble(a_i)*(RRb3*Int1000(p1,m) + RRb4*Int1000(p1,m+1))
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 5:
! [2101]^(0) from [2100]^(1), [2000]^(1) and [1100]^(1)
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!             
              Pos_Int(dpp_index) = RRd1(i)*Int2100(dp_index,0) + RRd2(i)*Int2100(dp_index,1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Pos_Int(dpp_index) = Pos_Int(dpp_index) &
                                   + dble(a_i)*RRd3*Int1100(pp_index,1)
              end if
              if(i.eq.p2)then
                Pos_Int(dpp_index) = Pos_Int(dpp_index) &
                                   + RRd4*Int2000(d1,1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              Pos_Int(dpp_index) = sq3*Pos_Int(dpp_index)
            end do
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:54) = prefactor*Pos_Int(1:54)
!
        return
      end if
!
!------------------------------------------------------------------
! [dspp]
!------------------------------------------------------------------
      if(Lcode.eq.2011)then
!
! Requires 5 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1010(9,0:L_total-3))
        allocate(Int2010(18,0:L_total-3))
!
! Step 1 :
! [1000]^(3) from [0000]^(4)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2:
! [1010]^(1) from [1000]^(2) and [0000]^(2)
! c+1i
!
        do m=0,L_total-3
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1010(pp_index,m) = RRc1(i)*Int1000(p1,m) + RRc2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1010(pp_index,m) = Int1010(pp_index,m) &
                                    + RRc3*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 3 :
! [2000]^(2) from [1000]^(3) and [0000]^(3)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [2010]^(1) from [2000]^(2) and [1000]^(2)
! c+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2010(dp_index,m) = RRc1(i)*Int2000(d1,m) + RRc2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2010(dp_index,m) = Int2010(dp_index,m) &
                                    + dble(a_i)*RRc3*Int1000(p1,m+1)
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 5:
! [2011]^(0) from [2010]^(1), [2000]^(1) and [1010]^(1)
! d+1i
!
        dp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp_index=dp_index+1
            do i=1,3
              dpp_index=dpp_index+1
!
              a_i=Lvec_d(d1,i)
!             
              Pos_Int(dpp_index) = RRd1(i)*Int2010(dp_index,0) + RRd2(i)*Int2010(dp_index,1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Pos_Int(dpp_index) = Pos_Int(dpp_index) &
                                   + dble(a_i)*RRd3*Int1010(pp_index,1)
              end if
              if(i.eq.p2)then
                Pos_Int(dpp_index) = Pos_Int(dpp_index) &
                                   + RRd5*Int2000(d1,0) + RRd6*Int2000(d1,1)
              end if
            end do ! i
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              dpp_index=9*(d1-1)+3*(p2-1)+p3
              Pos_Int(dpp_index) = sq3*Pos_Int(dpp_index)
            end do
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:54) = prefactor*Pos_Int(1:54)
!
        return
      end if
!
!------------------------------------------------------------------
! [ddps]
!------------------------------------------------------------------
      if(Lcode.eq.2210)then
!
! Requires 8 steps
!
        allocate(Int1000(3,0:L_total-1),Int0100(3,0:L_total-3),Int1100(9,0:L_total-3))
        allocate(Int2000(6,0:L_total-2),Int1200(18,0:L_total-4),Int2100(18,0:L_total-3)) 
        allocate(Int2200(36,0:L_total-4))
!
! Step 1:
! [1000]^(4) from [0000]^(5)
! a+1i
!
        do m=0,L_total-1
!
          do i=1,3
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do
!
       end do ! m
!
! Step 2:
! [0100]^(2) from [0000]^(3)
! b+1i
!
        do m=0,L_total-3
!
          do i=1,3
            Int0100(i,m) = RRb1(i)*G(m) + RRb2(i)*G(m+1)
          end do
!
        end do ! m
!
! Step 3: 
! [1100}^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3
            do i=1,3
              pp_index=pp_index+1
!
              Int1100(pp_index,m) = RRb1(i)*Int1000(p1,m) + RRb2(i)*Int1000(p1,m+1)
              if(i.eq.p1)then
                Int1100(pp_index,m) = Int1100(pp_index,m) &
                                    + RRb3*G(m) + RRb4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i 
!
        do m=0,L_total-2
!
          d_index=0
          do p1=1,3
            i=p1
            d_index=d_index+1
!
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                               + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
!
          do p1=2,3
            do i=1,p1-1
              d_index=d_index+1
!
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5:
! [1200]^(1) from [1100]^(2), [1000]^(2) and [0100]^(2)
! b+1i
!
        do m=0,L_total-4
!
          pd_index=0
          do p1=1,3
            do p2=1,3
              i=p2
              pd_index=pd_index+1
              pp_index=3*(p1-1)+p2
!
              Int1200(pd_index,m) = RRb1(i)*Int1100(pp_index,m) + RRb2(i)*Int1100(pp_index,m+1) &
                                  + RRb5*Int1000(p1,m) + RRb6*Int1000(p1,m+1)
              if(i.eq.p1)then
                Int1200(pd_index,m) = Int1200(pd_index,m) &
                                    + RRb3*Int0100(p2,m) + RRb4*Int0100(p2,m+1)
              end if
            end do ! p2
!
            do p2=2,3
              pp_index=3*(p1-1)+p2
              do i=1,p2-1
                pd_index=pd_index+1 
!
                Int1200(pd_index,m) = RRb1(i)*Int1100(pp_index,m) + RRb2(i)*Int1100(pp_index,m+1)
                if(i.eq.p1)then
                  Int1200(pd_index,m) = Int1200(pd_index,m) &
                                      + RRb3*Int0100(p2,m) + RRb4*Int0100(p2,m+1)
                end if
              end do ! i
            end  do ! p2
          end do ! p1
!
        end do ! m
!
! Step 6:
! [2100]^(2) from [2000]^(3) and [1000]^(3)
! b+1i
!
        do m=0,L_total-3
!
          dp_index=0
          do d1=1,6 
            do i=1,3
              dp_index=dp_index+1
!
              a_i = Lvec_d(d1,i)
!
              Int2100(dp_index,m) = RRb1(i)*Int2000(d1,m) + RRb2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int2100(dp_index,m) = Int2100(dp_index,m) &
                                    + dble(a_i)*(RRb3*Int1000(p1,m) + RRb4*Int1000(p1,m+1))
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 7:
! [2200]^(1) from [2100]^(2), [1100]^(2), and [2000]^(2)
! b+1i
!
        do m=0,L_total-4
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2
              dd_index=dd_index+1
              dp_index=3*(d1-1)+p2
!
              a_i = Lvec_d(d1,i)
!
              Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1) &
                                  + RRb5*Int2000(d1,m) + RRb6*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index,m) = Int2200(dd_index,m) &
                                    + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
              end if
            end do ! p2
!
            do p2=2,3
              dp_index=3*(d1-1)+p2
              do i=1,p2-1
                dd_index=dd_index+1
!
                a_i = Lvec_d(d1,i)
!
                Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3) = Lvec_d(d1,1:3)
                  v_1i(i) = v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2200(dd_index,m) = Int2200(dd_index,m) &
                                      + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
                end if
              end do ! i
            end do ! p2
          end do ! d1 
!
        end do ! m
!
! Step 8:
! [2210]^(0) from [2200]^(1), [2100]^(1), [1200]^(1)
! c+1i
!
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              ddp_index=ddp_index+1
!           
              a_i = Lvec_d(d1,i)
              b_i = Lvec_d(d2,i)
!
              Pos_Int(ddp_index) = RRc1(i)*Int2200(dd_index,0) + RRc2(i)*Int2200(dd_index,1)
              if(a_i.ne.0)then
                v_1i(1:3) = Lvec_d(d1,1:3)
                v_1i(i) = v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Pos_Int(ddp_index) = Pos_Int(ddp_index) &
                                   + dble(a_i)*RRc3*Int1200(pd_index,1)
              end if
              if(b_i.ne.0)then
                v_1i(1:3) = Lvec_d(d2,1:3)
                v_1i(i) = v_1i(i)-1
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Pos_Int(ddp_index) = Pos_Int(ddp_index) &
                                   + dble(b_i)*RRc4*Int2100(dp_index,1)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddp_index=0
        do d1=1,6
          do d2=1,6
            do p3=1,3
              ddp_index=ddp_index+1
              if(d1.gt.3) Pos_Int(ddp_index) = sq3*Pos_Int(ddp_index)
              if(d2.gt.3) Pos_Int(ddp_index) = sq3*Pos_Int(ddp_index)
            end do
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:108) = prefactor*Pos_Int(1:108)
!
        return
      end if
!
!------------------------------------------------------------------
! [dpds]
!------------------------------------------------------------------
      if(Lcode.eq.2120)then
!
! Requires 8 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1010(9,0:L_total-3))
        allocate(Int2010(18,0:L_total-3),Int1020(18,0:L_total-4),Int2020(36,0:L_total-4))
        allocate(Int0010(3,0:L_total-3))
!
! Step 1 :
! [1000]^(4) from [0000]^(5)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0010]^(2) from [0000]^(3)
! c+1i
!
        do m=0,L_total-3
          do i=1,3 ! x,y,z
            Int0010(i,m) = RRc1(i)*G(m) + RRc2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3:
! [1010]^(2) from [1000]^(3) and [0000]^(3)
! c+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1010(pp_index,m) = RRc1(i)*Int1000(p1,m) + RRc2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1010(pp_index,m) = Int1010(pp_index,m) &
                                    + RRc3*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4 :
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5:
! [1020]^(1) from [1010]^(2), [1000]^(2) and [0010]^(2)
! c+1i
!
        do m=0,L_total-4
!
          pd_index=0 
          do p1=1,3
            do p2=1,3
              i=p2
              pd_index=pd_index+1
              pp_index=3*(p1-1)+p2
!
              Int1020(pd_index,m) = RRc1(i)*Int1010(pp_index,m) + RRc2(i)*Int1010(pp_index,m+1) &
                                  + RRc5*Int1000(p1,m)          + RRc6*Int1000(p1,m+1)
              if(i.eq.p1)then
                Int1020(pd_index,m) = Int1020(pd_index,m) &
                                    + RRc3*Int0010(p2,m+1)
              end if
            end do ! p2
            do p2=2,3
              pp_index=3*(p1-1)+p2
              do i=1,p2-1
                pd_index=pd_index+1        
!
                Int1020(pd_index,m) = RRc1(i)*Int1010(pp_index,m) + RRc2(i)*Int1010(pp_index,m+1)
                if(i.eq.p1)then
                  Int1020(pd_index,m) = Int1020(pd_index,m) &
                                      + RRc3*Int0010(p2,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! p1
!
        end do ! m
!
! Step 6:
! [2010]^(2) from [2000]^(3) and [1000]^(3)
! c+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2010(dp_index,m) = RRc1(i)*Int2000(d1,m) + RRc2(i)*Int2000(d1,m+1)
!
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2010(dp_index,m) = Int2010(dp_index,m) &
                                    + dble(a_i)*RRc3*Int1000(p1,m+1)
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 7:
! [2020]^(1) from [2010]^(2), [2000]^(2) and [1010]^(2)
! c+1i
!
        do m=0,L_total-4
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2 ! do xx, yy, zz first
              dd_index=dd_index+1
              dp_index=3*(d1-1)+p2
! 
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2020(dd_index,m) = RRc1(i)*Int2010(dp_index,m) + RRc2(i)*Int2010(dp_index,m+1) &
                                  + RRc5*Int2000(d1,m)          + RRc6*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2020(dd_index,m) = Int2020(dd_index,m) &
                                    + dble(a_i)*RRc3*Int1010(pp_index,m+1)
              end if
            end do ! p2
!
            do p2=2,3 ! y, z
              dp_index=3*(d1-1)+p2
              do i=1,p2-1 ! x, y
                dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
                a_i=Lvec_d(d1,i) ! angular momentum vector
!
                Int2020(dd_index,m) = RRc1(i)*Int2010(dp_index,m) + RRc2(i)*Int2010(dp_index,m+1)
                if(a_i.ne.0)then ! (a-1i) terms
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1_i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2020(dd_index,m) = Int2020(dd_index,m) &
                                      + dble(a_i)*RRc3*Int1010(pp_index,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m
!
! Step 8:
! [2120]^(0) from [2020]^(1), [2010]^(1) and [1020]^(1)
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              c_i=Lvec_d(d2,i)
!
              Pos_Int(dpd_index) = RRb1(i)*Int2020(dd_index,0) + RRb2(i)*Int2020(dd_index,1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Pos_Int(dpd_index) = Pos_Int(dpd_index) &
                                   + dble(a_i)*(RRb3*Int1020(pd_index,0) + RRb4*Int1020(pd_index,1))
              end if
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Pos_Int(dpd_index) = Pos_Int(dpd_index) &
                                   + dble(c_i)*RRb7*Int2010(dp_index,1)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              if(d1.gt.3) Pos_Int(dpd_index) = sq3*Pos_Int(dpd_index)
              if(d3.gt.3) Pos_Int(dpd_index) = sq3*Pos_Int(dpd_index)
            end do
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:108) = prefactor*Pos_Int(1:108)
!
        return
      end if
!
!------------------------------------------------------------------
! [dpsd]
!------------------------------------------------------------------
      if(Lcode.eq.2102)then
!
! Requires 8 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1001(9,0:L_total-3))
        allocate(Int2001(18,0:L_total-3),Int1002(18,0:L_total-4),Int2002(36,0:L_total-4))
        allocate(Int0001(3,0:L_total-3))
!
! Step 1 :
! [1000]^(4) from [0000]^(5)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0001]^(2) from [0000]^(3)
! d+1i
!
        do m=0,L_total-3
          do i=1,3 ! x,y,z
            Int0001(i,m) = RRd1(i)*G(m) + RRd2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3:
! [1001]^(2) from [1000]^(3) and [0000]^(3)
! d+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1001(pp_index,m) = RRd1(i)*Int1000(p1,m) + RRd2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1001(pp_index,m) = Int1001(pp_index,m) &
                                    + RRd3*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4 :
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5:
! [1002]^(1) from [1001]^(2), [1000]^(2) and [0001]^(2)
! c+1i
!
        do m=0,L_total-4
!
          pd_index=0 
          do p1=1,3
            do p2=1,3
              i=p2
              pd_index=pd_index+1
              pp_index=3*(p1-1)+p2
!
              Int1002(pd_index,m) = RRd1(i)*Int1001(pp_index,m) + RRd2(i)*Int1001(pp_index,m+1) &
                                  + RRd7*Int1000(p1,m)          + RRd8*Int1000(p1,m+1)
              if(i.eq.p1)then
                Int1002(pd_index,m) = Int1002(pd_index,m) &
                                    + RRd3*Int0001(p2,m+1)
              end if
            end do ! p2
            do p2=2,3
              pp_index=3*(p1-1)+p2
              do i=1,p2-1
                pd_index=pd_index+1        
!
                Int1002(pd_index,m) = RRd1(i)*Int1001(pp_index,m) + RRd2(i)*Int1001(pp_index,m+1)
                if(i.eq.p1)then
                  Int1002(pd_index,m) = Int1002(pd_index,m) &
                                      + RRd3*Int0001(p2,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! p1
!
        end do ! m
!
! Step 6:
! [2001]^(2) from [2000]^(3) and [1000]^(3)
! c+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2001(dp_index,m) = RRd1(i)*Int2000(d1,m) + RRd2(i)*Int2000(d1,m+1)
!
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2001(dp_index,m) = Int2001(dp_index,m) &
                                    + dble(a_i)*RRd3*Int1000(p1,m+1)
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 7:
! [2002]^(1) from [2001]^(2), [2000]^(2) and [1001]^(2)
! c+1i
!
        do m=0,L_total-4
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2 ! do xx, yy, zz first
              dd_index=dd_index+1
              dp_index=3*(d1-1)+p2
! 
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2002(dd_index,m) = RRd1(i)*Int2001(dp_index,m) + RRd2(i)*Int2001(dp_index,m+1) &
                                  + RRd7*Int2000(d1,m)          + RRd8*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2002(dd_index,m) = Int2002(dd_index,m) &
                                    + dble(a_i)*RRd3*Int1001(pp_index,m+1)
              end if
            end do ! p2
!
            do p2=2,3 ! y, z
              dp_index=3*(d1-1)+p2
              do i=1,p2-1 ! x, y
                dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
                a_i=Lvec_d(d1,i) ! angular momentum vector
!
                Int2002(dd_index,m) = RRd1(i)*Int2001(dp_index,m) + RRd2(i)*Int2001(dp_index,m+1)
                if(a_i.ne.0)then ! (a-1i) terms
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1_i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2002(dd_index,m) = Int2002(dd_index,m) &
                                      + dble(a_i)*RRd3*Int1001(pp_index,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m
!
! Step 8:
! [2102]^(0) from [2002]^(1), [2001]^(1) and [1002]^(1)
! b+1i
!
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do i=1,3
              dpd_index=18*(d1-1)+6*(i-1)+d2
!
              a_i=Lvec_d(d1,i)
              d_i=Lvec_d(d2,i)
!
              Pos_Int(dpd_index) = RRb1(i)*Int2002(dd_index,0) + RRb2(i)*Int2002(dd_index,1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pd_index=6*(p1-1)+d2
                Pos_Int(dpd_index) = Pos_Int(dpd_index) &
                                   + dble(a_i)*(RRb3*Int1002(pd_index,0) + RRb4*Int1002(pd_index,1))
              end if
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1 ! c-1i
                p2=dot_product(v_1i,xyz)
                dp_index=3*(d1-1)+p2
                Pos_Int(dpd_index) = Pos_Int(dpd_index) &
                                   + dble(d_i)*RRb8*Int2001(dp_index,1)
              end if
            end do ! i
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dpd_index=dpd_index+1
              if(d1.gt.3) Pos_Int(dpd_index) = sq3*Pos_Int(dpd_index)
              if(d3.gt.3) Pos_Int(dpd_index) = sq3*Pos_Int(dpd_index)
            end do
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:108) = prefactor*Pos_Int(1:108)
!
        return
      end if
!
!------------------------------------------------------------------
! [ddds]
!------------------------------------------------------------------
      if(Lcode.eq.2220)then
!
! Requires 12 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1100(9,0:L_total-3))
        allocate(Int2100(18,0:L_total-3),Int1200(18,0:L_total-4),Int2200(36,0:L_total-4))
        allocate(Int0100(3,0:L_total-3),Int0200(6,0:L_total-4),Int1210(54,0:L_total-5))
        allocate(Int2110(54,0:L_total-5),Int2210(108,0:L_total-5))
!
! Step 1 :
! [1000]^(5) from [0000]^(6)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0100]^(3) from [0000]^(4)
! b+1i
!
        do m=0,L_total-3
          do i=1,3 ! x,y,z
            Int0100(i,m) = RRb1(i)*G(m) + RRb2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3 :
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                               + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4 :
! [0200]^(2) from [0100]^(3) and [0000]^(3)
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-4
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int0200(d_index,m) = RRb1(i)*Int0100(p1,m) + RRb2(i)*Int0100(p1,m+1) &
                                + RRb5*G(m) + RRb6*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int0200(d_index,m) = RRb1(i)*Int0100(p1,m) + RRb2(i)*Int0100(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5:
! [1100]^(3) from [1000]^(4) and [0000]^(4)
! b+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1100(pp_index,m) = RRb1(i)*Int1000(p1,m) + RRb2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1100(pp_index,m) = Int1100(pp_index,m) &
                                    + RRb3*G(m) + RRb4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 6:
! [1200]^(2) from [1100]^(3), [1000]^(3) and [0100]^(3)
! b+1i
!
        do m=0,L_total-4
!
          pd_index=0 
          do p1=1,3
            do p2=1,3
              i=p2
              pd_index=pd_index+1
              pp_index=3*(p1-1)+p2
              Int1200(pd_index,m) = RRb1(i)*Int1100(pp_index,m) + RRb2(i)*Int1100(pp_index,m+1) &
                                  + RRb5*Int1000(p1,m) + RRb6*Int1000(p1,m+1)
              if(i.eq.p1)then
                Int1200(pd_index,m) = Int1200(pd_index,m) &
                                    + RRb3*Int0100(p2,m) + RRb4*Int0100(p2,m+1)
              end if
            end do ! p2
!
            do p2=2,3
              pp_index=3*(p1-1)+p2
              do i=1,p2-1
                pd_index=pd_index+1        
!
                Int1200(pd_index,m) = RRb1(i)*Int1100(pp_index,m) + RRb2(i)*Int1100(pp_index,m+1)
                if(i.eq.p1)then
                  Int1200(pd_index,m) = Int1200(pd_index,m) &
                                      + RRb3*Int0100(p2,m) + RRb4*Int0100(p2,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! p1
!
        end do ! m
!
! Step 7:
! [2100]^(3) from [2000]^(4) and [1000]^(4)
! b+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2100(dp_index,m) = RRb1(i)*Int2000(d1,m) + RRb2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2100(dp_index,m) = Int2100(dp_index,m) &
                                    + dble(a_i)*(RRb3*Int1000(p1,m) + RRb4*Int1000(p1,m+1))
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 8:
! [1210]^(1) from [1200]^(2), [1100]^(2) and [0200]^(2)
! c+1i
!
        do m=0,L_total-5
!
          pdp_index=0
          pd_index=0
          do p1=1,3
            do d2=1,6
              pd_index=pd_index+1
              do i=1,3
                pdp_index=pdp_index+1
!                
                b_i=Lvec_d(d2,i)
!
                Int1210(pdp_index,m) = RRc1(i)*Int1200(pd_index,m) + RRc2(i)*Int1200(pd_index,m+1)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! (b-1i)
                  p2=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int1210(pdp_index,m) = Int1210(pdp_index,m) &
                                       + dble(b_i)*RRc4*Int1100(pp_index,m+1)
                end if
                if(p1.eq.i)then
                  Int1210(pdp_index,m) = Int1210(pdp_index,m) &
                                       + RRc3*Int0200(d2,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 9:
! [2110]^(1) from [2100]^(2), [2000]^(2) and [1100]^(2)
! c+1i
!
        do m=0,L_total-5
!
          dpp_index=0
          dp_index=0
          do d1=1,6
            do p2=1,3
              dp_index=dp_index+1
              do i=1,3
                dpp_index=dpp_index+1
!                
                a_i=Lvec_d(d1,i)
!
                Int2110(dpp_index,m) = RRc1(i)*Int2100(dp_index,m) + RRc2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + dble(a_i)*RRc3*Int1100(pp_index,m+1)
                end if
                if(p2.eq.i)then
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + RRc4*Int2000(d1,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 10:
! [2200]^(2) from [2100]^(3), [2000]^(3) and [1100]^(3)
! b+1i
!
        do m=0,L_total-4
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2 ! do xx, yy, zz first
              dd_index=dd_index+1
              dp_index=3*(d1-1)+p2
! 
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1) &
                                  + RRb5*Int2000(d1,m)          + RRb6*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index,m) = Int2200(dd_index,m) &
                                    + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
              end if
            end do ! p2
!
            do p2=2,3 ! y, z
              dp_index=3*(d1-1)+p2
              do i=1,p2-1 ! x, y
                dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
                a_i=Lvec_d(d1,i) ! angular momentum vector
!
                Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then ! (a-1i) terms
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1_i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2200(dd_index,m) = Int2200(dd_index,m) &
                                      + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m
!
! Step 11:
! [2210]^(1) from [2200]^(2), [2100]^(2) and [1200]^(2)
! c+1i
!
        do m=0,L_total-5
!
          ddp_index=0
          dd_index=0
          do d1=1,6
            do d2=1,6
              dd_index=dd_index+1
              do i=1,3
                ddp_index=ddp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2210(ddp_index,m) = RRc1(i)*Int2200(dd_index,m) + RRc2(i)*Int2200(dd_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pd_index=6*(p1-1)+d2
                  Int2210(ddp_index,m) = Int2210(ddp_index,m) &
                                   + dble(a_i)*RRc3*Int1200(pd_index,m+1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dp_index=3*(d1-1)+p2
                  Int2210(ddp_index,m) = Int2210(ddp_index,m) &
                                   + dble(b_i)*RRc4*Int2100(dp_index,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! d1
!
        end do ! m
!
! Step 12:
! [2220]^(0) from [2210]^(1), [2200]^(1), [2110]^(1) and [1210]^(1)
! c+1i
!   
        ddd_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              i=p3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              ddd_index=ddd_index+1
!
              a_i=Lvec_d(d1,i)
              b_i=Lvec_d(d2,i)
!
              Pos_Int(ddd_index) = RRc1(i)*Int2210(ddp_index,0) + RRc2(i)*Int2210(ddp_index,1) &
                                 + RRc5*Int2200(dd_index,0)     + RRc6*Int2200(dd_index,1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! (a-1i)
                p1=dot_product(v_1i,xyz)
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                Pos_Int(ddd_index) = Pos_Int(ddd_index) &
                                   + dble(a_i)*RRc3*Int1210(pdp_index,1)
              end if
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d2,1:3)
                v_1i(i)=v_1i(i)-1
                p2=dot_product(v_1i,xyz)
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                Pos_Int(ddd_index) = Pos_Int(ddd_index) &
                                   + dble(b_i)*RRc4*Int2110(dpp_index,1)
              end if
            end do ! p3
            do p3=2,3
              ddp_index=18*(d1-1)+3*(d2-1)+p3
              do i=1,p3-1
                ddd_index=ddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Pos_Int(ddd_index) = RRc1(i)*Int2210(ddp_index,0) + RRc2(i)*Int2210(ddp_index,1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Pos_Int(ddd_index) = Pos_Int(ddd_index) &
                                     + dble(a_i)*RRc3*Int1210(pdp_index,1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Pos_Int(ddd_index) = Pos_Int(ddd_index) &
                                   + dble(b_i)*RRc4*Int2110(dpp_index,1)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              if(d1.gt.3) Pos_Int(ddd_index) = sq3*Pos_Int(ddd_index)
              if(d2.gt.3) Pos_Int(ddd_index) = sq3*Pos_Int(ddd_index)
              if(d3.gt.3) Pos_Int(ddd_index) = sq3*Pos_Int(ddd_index)
            end do
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:216) = prefactor*Pos_Int(1:216)
!
        return
      end if
!
!------------------------------------------------------------------
! [dppp]
!------------------------------------------------------------------
      if(Lcode.eq.2111)then
!
! Requires 9 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1100(9,0:L_total-3))
        allocate(Int2100(18,0:L_total-3),Int2010(18,0:L_total-4),Int1110(27,0:L_total-4))
        allocate(Int0100(3,0:L_total-3))
        allocate(Int2110(54,0:L_total-4))
!
! Step 1 :
! [1000]^(4) from [0000]^(5)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0100]^(2) from [0000]^(3)
! b+1i
!
        do m=0,L_total-3
          do i=1,3 ! x,y,z
            Int0100(i,m) = RRb1(i)*G(m) + RRb2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3:
! [1100]^(2) from [1000]^(3) and [0000]^(3)
! b+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1100(pp_index,m) = RRb1(i)*Int1000(p1,m) + RRb2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1100(pp_index,m) = Int1100(pp_index,m) &
                                    + RRb3*G(m)             + RRb4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4 :
! [2000]^(3) from [1000]^(4) and [0000]^(4)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5 :
! [1110]^(1) from [1100]^(2), [1000]^(2) and [0100]^(2)
! c+1i
!
        do m=0,L_total-4
!
          ppp_index=0
          pp_index=0
          do p1=1,3
            do p2=1,3
              pp_index=pp_index+1
              do i=1,3
                ppp_index=ppp_index+1
!
                Int1110(ppp_index,m) = RRc1(i)*Int1100(pp_index,m) + RRc2(i)*Int1100(pp_index,m+1)
                if(i.eq.p1)then
                  Int1110(ppp_index,m) = Int1110(ppp_index,m) &
                                       + RRc3*Int0100(p2,m+1)
                end if
                if(i.eq.p2)then
                  Int1110(ppp_index,m) = Int1110(ppp_index,m) &
                                       + RRc4*Int1000(p1,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! p1
!
        end do ! m
!
! Step 6:
! [2010]^(1) from [2000]^(2) and [1000]^(2)
! c+1i
!
        do m=0,L_total-4
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2010(dp_index,m) = RRc1(i)*Int2000(d1,m) + RRc2(i)*Int2000(d1,m+1)
!
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2010(dp_index,m) = Int2010(dp_index,m) &
                                    + dble(a_i)*RRc3*Int1000(p1,m+1)
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 7:
! [2100]^(2) from [2000]^(3) and [1000]^(3)
! b+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2100(dp_index,m) = RRb1(i)*Int2000(d1,m) + RRb2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2100(dp_index,m) = Int2100(dp_index,m) &
                                    + dble(a_i)*(RRb3*Int1000(p1,m) + RRb4*Int1000(p1,m+1))
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 8:
! [2110]^(1) from [2100]^(2), [2000]^(2) and [1100]^(2)
! c+1i
!
        do m=0,L_total-4
!
          dpp_index=0
          dp_index=0
          do d1=1,6
            do p2=1,3
              dp_index=dp_index+1
              do i=1,3
                dpp_index=dpp_index+1
!                
                a_i=Lvec_d(d1,i)
!
                Int2110(dpp_index,m) = RRc1(i)*Int2100(dp_index,m) + RRc2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + dble(a_i)*RRc3*Int1100(pp_index,m+1)
                end if
                if(p2.eq.i)then
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + RRc4*Int2000(d1,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 9:
! [2111]^(0) from [2110]^(1), [2100]^(1), [2010]^(1) and [1110]^(1)
! d+1i
!
        dppp_index=0
        dpp_index=0
        do d1=1,6
          do p2=1,3
            dp2_index=3*(d1-1)+p2
            do p3=1,3
              dpp_index=dpp_index+1
              dp3_index=3*(d1-1)+p3
              do i=1,3
                dppp_index=dppp_index+1
!
                a_i=Lvec_d(d1,i)
!
                Pos_Int(dppp_index) = RRd1(i)*Int2110(dpp_index,0) + RRd2(i)*Int2110(dpp_index,1)
!
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Pos_Int(dppp_index) = Pos_Int(dppp_index) &
                                      + dble(a_i)*RRd3*Int1110(ppp_index,1)
                end if
                if(i.eq.p2)then
                  Pos_Int(dppp_index) = Pos_Int(dppp_index) &
                                      + RRd4*Int2010(dp3_index,1)
                end if
                if(i.eq.p3)then
                  Pos_Int(dppp_index) = Pos_Int(dppp_index) &
                                      + RRd5*Int2100(dp2_index,0) + RRd6*Int2100(dp2_index,1)
                end if
              end do ! i
            end do ! p3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        do d1=4,6
          do p2=1,3
            do p3=1,3
              do p4=1,3
                dppp_index=27*(d1-1)+9*(p2-1)+3*(p3-1)+p4
                Pos_Int(dppp_index) = sq3*Pos_Int(dppp_index)
              end do
            end do
          end do
        end do
! 
! multiply through prefactor
!        Pos_Int(1:162) = prefactor*Pos_Int(1:162)
!
        return
      end if
!
!------------------------------------------------------------------
! [ddpp]
!------------------------------------------------------------------
      if(Lcode.eq.2211)then
!
! Requires 12 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1100(9,0:L_total-3))
        allocate(Int2100(18,0:L_total-3),Int1200(18,0:L_total-4),Int1210(54,0:L_total-5))
        allocate(Int0100(3,0:L_total-3),Int0200(6,0:L_total-4),Int2200(36,0:L_total-4))
        allocate(Int2110(54,0:L_total-5),Int2210(108,0:L_total-5))
!
! Step 1 :
! [1000]^(5) from [0000]^(6)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0100]^(3) from [0000]^(4)
! b+1i
!
        do m=0,L_total-3
          do i=1,3 ! x,y,z
            Int0100(i,m) = RRb1(i)*G(m) + RRb2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3:
! [1100]^(3) from [1000]^(4) and [0000]^(4)
! b+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1100(pp_index,m) = RRb1(i)*Int1000(p1,m) + RRb2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1100(pp_index,m) = Int1100(pp_index,m) &
                                    + RRb3*G(m) + RRb4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4 :
! [0200]^(2) from [0100]^(3) and [0000]^(3)
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-4
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int0200(d_index,m) = RRb1(i)*Int0100(p1,m) + RRb2(i)*Int0100(p1,m+1) &
                                + RRb5*G(m) + RRb6*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int0200(d_index,m) = RRb1(i)*Int0100(p1,m) + RRb2(i)*Int0100(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5 :
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 6:
! [1200]^(2) from [1100]^(3), [1000]^(3) and [0100]^(3)
! b+1i
!
        do m=0,L_total-4
!
          pd_index=0 
          do p1=1,3
            do p2=1,3
              i=p2
              pd_index=pd_index+1
              pp_index=3*(p1-1)+p2
!
              Int1200(pd_index,m) = RRb1(i)*Int1100(pp_index,m) + RRb2(i)*Int1100(pp_index,m+1) &
                                  + RRb5*Int1000(p1,m)          + RRb6*Int1000(p1,m+1)
              if(i.eq.p1)then
                Int1200(pd_index,m) = Int1200(pd_index,m) &
                                    + RRb3*Int0100(p2,m)          + RRb4*Int0100(p2,m+1)
              end if
            end do ! p2
            do p2=2,3
              pp_index=3*(p1-1)+p2
              do i=1,p2-1
                pd_index=pd_index+1        
!
                Int1200(pd_index,m) = RRb1(i)*Int1100(pp_index,m) + RRb2(i)*Int1100(pp_index,m+1)
                if(i.eq.p1)then
                  Int1200(pd_index,m) = Int1200(pd_index,m) &
                                      + RRb3*Int0100(p2,m)          + RRb4*Int0100(p2,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! p1
!
        end do ! m
!
! Step 7:
! [2100]^(3) from [2000]^(4) and [1000]^(4)
! b+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2100(dp_index,m) = RRb1(i)*Int2000(d1,m) + RRb2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                Int2100(dp_index,m) = Int2100(dp_index,m) &
                                    + dble(a_i)*(RRb3*Int1000(p1,m) + RRb4*Int1000(p1,m+1))
              end if
            end do ! i
          end do ! d1
!
        end do ! m
!
! Step 8:
! [1210]^(1) from [1200]^(2), [1100]^(2) and [0200]^(2)
! c+1i
!
        do m=0,L_total-5
!
          pdp_index=0
          pd_index=0
          do p1=1,3
            do d2=1,6
              pd_index=pd_index+1
              do i=1,3
                pdp_index=pdp_index+1
!                
                b_i=Lvec_d(d2,i)
!
                Int1210(pdp_index,m) = RRc1(i)*Int1200(pd_index,m) + RRc2(i)*Int1200(pd_index,m+1)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! (b-1i)
                  p2=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int1210(pdp_index,m) = Int1210(pdp_index,m) &
                                       + dble(b_i)*RRc4*Int1100(pp_index,m+1)
                end if
                if(i.eq.p1)then
                  Int1210(pdp_index,m) = Int1210(pdp_index,m) &
                                       + RRc3*Int0200(d2,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 9:
! [2110]^(1) from [2100]^(2), [2000]^(2) and [1100]^(2)
! c+1i
!
        do m=0,L_total-5
!
          dpp_index=0
          dp_index=0
          do d1=1,6
            do p2=1,3
              dp_index=dp_index+1
              do i=1,3
                dpp_index=dpp_index+1
!                
                a_i=Lvec_d(d1,i)
!
                Int2110(dpp_index,m) = RRc1(i)*Int2100(dp_index,m) + RRc2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + dble(a_i)*RRc3*Int1100(pp_index,m+1)
                end if
                if(p2.eq.i)then
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + RRc4*Int2000(d1,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 10:
! [2200]^(2) from [2100]^(3), [2000]^(3) and [1100]^(3)
! b+1i
!
        do m=0,L_total-4
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2 ! do xx, yy, zz first
              dd_index=dd_index+1
              dp_index=3*(d1-1)+p2
! 
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1) &
                                  + RRb5*Int2000(d1,m)          + RRb6*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index,m) = Int2200(dd_index,m) &
                                    + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
              end if
            end do ! p2
!
            do p2=2,3 ! y, z
              dp_index=3*(d1-1)+p2
              do i=1,p2-1 ! x, y
                dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
                a_i=Lvec_d(d1,i) ! angular momentum vector
!
                Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then ! (a-1i) terms
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1_i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2200(dd_index,m) = Int2200(dd_index,m) &
                                      + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m
!
! Step 11:
! [2210]^(1) from [2200]^(2), [2100]^(2) and [1200]^(2)
! c+1i
!
        do m=0,L_total-5
!
          ddp_index=0
          dd_index=0
          do d1=1,6
            do d2=1,6
              dd_index=dd_index+1
              do i=1,3
                ddp_index=ddp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2210(ddp_index,m) = RRc1(i)*Int2200(dd_index,m) + RRc2(i)*Int2200(dd_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pd_index=6*(p1-1)+d2
                  Int2210(ddp_index,m) = Int2210(ddp_index,m) &
                                       + dble(a_i)*RRc3*Int1200(pd_index,m+1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dp_index=3*(d1-1)+p2
                  Int2210(ddp_index,m) = Int2210(ddp_index,m) &
                                       + dble(b_i)*RRc4*Int2100(dp_index,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! d1
!
        end do ! m 
!
! Step 12:
! [2211]^(0) from [2210]^(1), [2200]^(1), [2110]^(1) and [1210]^(1)
! d+1i
!
        ddpp_index=0
        ddp_index=0
        dd_index=0
        do d1=1,6
          do d2=1,6
            dd_index=dd_index+1
            do p3=1,3
              ddp_index=ddp_index+1
              do i=1,3
                ddpp_index=ddpp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Pos_Int(ddpp_index) = RRd1(i)*Int2210(ddp_index,0) + RRd2(i)*Int2210(ddp_index,1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Pos_Int(ddpp_index) = Pos_Int(ddpp_index) &
                                      + dble(a_i)*RRd3*Int1210(pdp_index,1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Pos_Int(ddpp_index) = Pos_Int(ddpp_index) &
                                      + dble(b_i)*RRd4*Int2110(dpp_index,1)
                end if
                if(i.eq.p3)then
                  Pos_Int(ddpp_index) = Pos_Int(ddpp_index) &
                                      + RRd5*Int2200(dd_index,0) + RRd6*Int2200(dd_index,1)
                end if
              end do ! i
            end do ! p3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        ddpp_index=0
        do d1=1,6
          do d2=1,6
            do p3=1,3
              do p4=1,3
                ddpp_index=ddpp_index+1
                if(d1.gt.3) Pos_Int(ddpp_index) = sq3*Pos_Int(ddpp_index)
                if(d2.gt.3) Pos_Int(ddpp_index) = sq3*Pos_Int(ddpp_index)
              end do
            end do
          end do
        end do
!
! multiply through prefactor
!        Pos_Int(1:324) = prefactor*Pos_Int(1:324)
!
        return
      end if
!
!------------------------------------------------------------------
! [dpdp]
!------------------------------------------------------------------
      if(Lcode.eq.2121)then
!
! Requires 12 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1010(9,0:L_total-3))
        allocate(Int2010(18,0:L_total-3),Int1020(18,0:L_total-4),Int1120(54,0:L_total-5))
        allocate(Int0010(3,0:L_total-2),Int0020(6,0:L_total-3),Int2020(36,0:L_total-4))
        allocate(Int2110(54,0:L_total-5),Int2120(108,0:L_total-5))
!
! Step 1 :
! [1000]^(5) from [0000]^(6)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0010]^(4) from [0000]^(5)
! c+1i
!
        do m=0,L_total-2
          do i=1,3 ! x,y,z
            Int0010(i,m) = RRc1(i)*G(m) + RRc2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3 :
! [0020]^(3) from [0010]^(4) and [0000]^(4)
! c+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-3
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int0020(d_index,m) = RRc1(i)*Int0010(p1,m) + RRc2(i)*Int0010(p1,m+1) &
                                + RRc5*G(m) + RRc6*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int0020(d_index,m) = RRc1(i)*Int0010(p1,m) + RRc2(i)*Int0010(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [1010]^(3) from [1000]^(4) and [0000]^(4)
! c+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1010(pp_index,m) = RRc1(i)*Int1000(p1,m) + RRc2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1010(pp_index,m) = Int1010(pp_index,m) &
                                    + RRc3*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5 :
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 6:
! [1020]^(2) from [0020]^(3), [0010]^(3)
! a+1i
!
        do m=0,L_total-4
!
          do d1=1,6
            do i=1,3
              pd_index=6*(i-1)+d1
!
              c_i=Lvec_d(d1,i)
!
              Int1020(pd_index,m) = RRa1(i)*Int0020(d1,m) + RRa2(i)*Int0020(d1,m+1)
              if(c_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int1020(pd_index,m) = Int1020(pd_index,m) &
                                    + dble(c_i)*RRa7*Int0010(p1,m+1)
              end if
            end do ! i 
          end do ! d1
!
        end do ! m
!
! Step 7:
! [2010]^(3) from [2000]^(4), [1000]^(4)
! c+1i
!
        do m=0,L_total-3
!
          dp_index=0
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2010(dp_index,m) = RRc1(i)*Int2000(d1,m) + RRc2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int2010(dp_index,m) = Int2010(dp_index,m) &
                                    + dble(a_i)*RRc3*Int1000(p1,m+1)
              end if
            end do ! i 
          end do ! d1
!
        end do ! m
!
! Step 8:
! [1120]^(1) from [1020]^(2), [1010]^(2) and [0020]^(2)
! b+1i
!
        do m=0,L_total-5
!
          pd_index=0
          do p1=1,3
            do d2=1,6
              pd_index=pd_index+1
              do i=1,3
                ppd_index=18*(p1-1)+6*(i-1)+d2
!                
                c_i=Lvec_d(d2,i)
!
                Int1120(ppd_index,m) = RRb1(i)*Int1020(pd_index,m) + RRb2(i)*Int1020(pd_index,m+1)
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! (c-1i)
                  p2=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int1120(ppd_index,m) = Int1120(ppd_index,m) &
                                       + dble(c_i)*RRb7*Int1010(pp_index,m+1)
                end if
                if(p1.eq.i)then
                  Int1120(ppd_index,m) = Int1120(ppd_index,m) &
                                       + RRb3*Int0020(d2,m) + RRb4*Int0020(d2,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 9:
! [2020]^(2) from [2010]^(3), [2000]^(3) and [1010]^(3)
! c+1i
!
        do m=0,L_total-4
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2 ! do xx, yy, zz first
              dd_index=dd_index+1
              dp_index=3*(d1-1)+p2
! 
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2020(dd_index,m) = RRc1(i)*Int2010(dp_index,m) + RRc2(i)*Int2010(dp_index,m+1) &
                                  + RRc5*Int2000(d1,m)          + RRc6*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2020(dd_index,m) = Int2020(dd_index,m) &
                                    + dble(a_i)*RRc3*Int1010(pp_index,m+1)
              end if
            end do ! p2
! 
            do p2=2,3 ! y, z
              dp_index=3*(d1-1)+p2
              do i=1,p2-1 ! x, y
                dd_index=dd_index+1
!
! i ne p2, therefore no (c-1i) terms
!
                a_i=Lvec_d(d1,i) ! angular momentum vector
!
                Int2020(dd_index,m) = RRc1(i)*Int2010(dp_index,m) + RRc2(i)*Int2010(dp_index,m+1)
!
                if(a_i.ne.0)then ! (a-1i) terms
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1_i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2020(dd_index,m) = Int2020(dd_index,m) &
                                      + dble(a_i)*RRc3*Int1010(pp_index,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m
!
! Step 10:
! [2110]^(1) from [2010]^(2), [2000]^(2) and [1010]^(2)
! b+1i
!
        do m=0,L_total-5
!
          dpp_index=0
          dp_index=0
          do d1=1,6
            do p2=1,3
              dp_index=dp_index+1
              do i=1,3
                dpp_index=9*(d1-1)+3*(i-1)+p2
!                
                a_i=Lvec_d(d1,i)
!
                Int2110(dpp_index,m) = RRb1(i)*Int2010(dp_index,m) + RRb2(i)*Int2010(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + dble(a_i)*(RRb3*Int1010(pp_index,m) + RRb4*Int1010(pp_index,m+1))
                end if
                if(p2.eq.i)then
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + RRb7*Int2000(d1,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 11:
! [2120]^(1) from [2020]^(2), [2010]^(2) and [1020]^(2)
! b+1i
!
        do m=0,L_total-5
!
          dd_index=0
          do d1=1,6
            do d2=1,6
              dd_index=dd_index+1
              do i=1,3
                dpd_index=18*(d1-1)+6*(i-1)+d2
!
                a_i=Lvec_d(d1,i)
                c_i=Lvec_d(d2,i)
!
                Int2120(dpd_index,m) = RRb1(i)*Int2020(dd_index,m) + RRb2(i)*Int2020(dd_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pd_index=6*(p1-1)+d2
                  Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                       + dble(a_i)*(RRb3*Int1020(pd_index,m) + RRb4*Int1020(pd_index,m+1))
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p2=dot_product(v_1i,xyz)
                  dp_index=3*(d1-1)+p2
                  Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                       + dble(c_i)*RRb7*Int2010(dp_index,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! d1
!
        end do ! m 
!
! Step 12:
! [2121]^(0) from [2120]^(1), [2110]^(1), [2020]^(1) and [1120]^(1)
! d+1i
!
        dpdp_index=0
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dd_index=6*(d1-1)+d3
              dpd_index=dpd_index+1
              do i=1,3
                dpdp_index=dpdp_index+1
!
                a_i=Lvec_d(d1,i)
                c_i=Lvec_d(d3,i)
!
                Pos_Int(dpdp_index) = RRd1(i)*Int2120(dpd_index,0) + RRd2(i)*Int2120(dpd_index,1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  Pos_Int(dpdp_index) = Pos_Int(dpdp_index) &
                                      + dble(a_i)*RRd3*Int1120(ppd_index,1)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p3=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Pos_Int(dpdp_index) = Pos_Int(dpdp_index) &
                                      + dble(c_i)*(RRd5*Int2110(dpp_index,0) + RRd6*Int2110(dpp_index,1))
                end if
                if(i.eq.p2)then
                  Pos_Int(dpdp_index) = Pos_Int(dpdp_index) &
                                      + RRd4*Int2020(dd_index,1)
                end if
              end do ! i
            end do ! d3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dpdp_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              do p4=1,3
                dpdp_index=dpdp_index+1
                if(d1.gt.3) Pos_Int(dpdp_index) = sq3*Pos_Int(dpdp_index)
                if(d3.gt.3) Pos_Int(dpdp_index) = sq3*Pos_Int(dpdp_index)
              end do
            end do
          end do
        end do
!
! multiply through prefactor
!        Pos_Int(1:324) = prefactor*Pos_Int(1:324)
!
        return
      end if
!
!------------------------------------------------------------------
! [dppd]
!------------------------------------------------------------------
      if(Lcode.eq.2112)then
!
! Requires 12 steps
!
        allocate(Int1000(3,0:L_total-1),Int2000(6,0:L_total-2),Int1001(9,0:L_total-3))
        allocate(Int2001(18,0:L_total-3),Int1002(18,0:L_total-4),Int1102(54,0:L_total-5))
        allocate(Int0001(3,0:L_total-2),Int0002(6,0:L_total-3),Int2002(36,0:L_total-4))
        allocate(Int2101(54,0:L_total-5),Int2102(108,0:L_total-5))
!
! Step 1 :
! [1000]^(5) from [0000]^(6)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0001]^(4) from [0000]^(5)
! d+1i
!
        do m=0,L_total-2
          do i=1,3 ! x,y,z
            Int0001(i,m) = RRd1(i)*G(m) + RRd2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3 :
! [0002]^(3) from [0001]^(4) and [0000]^(4)
! d+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-3
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int0002(d_index,m) = RRd1(i)*Int0001(p1,m) + RRd2(i)*Int0001(p1,m+1) &
                                + RRd7*G(m) + RRd8*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int0002(d_index,m) = RRd1(i)*Int0001(p1,m) + RRd2(i)*Int0001(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [1001]^(3) from [1000]^(4) and [0000]^(4)
! d+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1001(pp_index,m) = RRd1(i)*Int1000(p1,m) + RRd2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1001(pp_index,m) = Int1001(pp_index,m) &
                                    + RRd3*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5 :
! [2000]^(4) from [1000]^(5) and [0000]^(5)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 6:
! [1002]^(2) from [0002]^(3), [0001]^(3)
! a+1i
!
        do m=0,L_total-4
!
          do d1=1,6
            do i=1,3
              pd_index=6*(i-1)+d1
!
              d_i=Lvec_d(d1,i)
!
              Int1002(pd_index,m) = RRa1(i)*Int0002(d1,m) + RRa2(i)*Int0002(d1,m+1)
              if(d_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int1002(pd_index,m) = Int1002(pd_index,m) &
                                    + dble(d_i)*RRa8*Int0001(p1,m+1)
              end if
            end do ! i 
          end do ! d1
!
        end do ! m
!
! Step 7:
! [2001]^(3) from [2000]^(4), [1000]^(4)
! c+1i
!
        do m=0,L_total-3
!
          dp_index=0
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2001(dp_index,m) = RRd1(i)*Int2000(d1,m) + RRd2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int2001(dp_index,m) = Int2001(dp_index,m) &
                                    + dble(a_i)*RRd3*Int1000(p1,m+1)
              end if
            end do ! i 
          end do ! d1
!
        end do ! m
!
! Step 8:
! [1102]^(1) from [1002]^(2), [1001]^(2) and [0002]^(2)
! b+1i
!
        do m=0,L_total-5
!
          pd_index=0
          do p1=1,3
            do d2=1,6
              pd_index=pd_index+1
              do i=1,3
                ppd_index=18*(p1-1)+6*(i-1)+d2
!                
                d_i=Lvec_d(d2,i)
!
                Int1102(ppd_index,m) = RRb1(i)*Int1002(pd_index,m) + RRb2(i)*Int1002(pd_index,m+1)
                if(d_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! (d-1i)
                  p2=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int1102(ppd_index,m) = Int1102(ppd_index,m) &
                                       + dble(d_i)*RRb8*Int1001(pp_index,m+1)
                end if
                if(i.eq.p1)then
                  Int1102(ppd_index,m) = Int1102(ppd_index,m) &
                                       + RRb3*Int0002(d2,m) + RRb4*Int0002(d2,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 9:
! [2002]^(2) from [2001]^(3), [2000]^(3) and [1001]^(3)
! d+1i
!
        do m=0,L_total-4
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2 ! do xx, yy, zz first
              dd_index=dd_index+1
              dp_index=3*(d1-1)+p2
! 
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2002(dd_index,m) = RRd1(i)*Int2001(dp_index,m) + RRd2(i)*Int2001(dp_index,m+1) &
                                  + RRd7*Int2000(d1,m)          + RRd8*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2002(dd_index,m) = Int2002(dd_index,m) &
                                    + dble(a_i)*RRd3*Int1001(pp_index,m+1)
              end if
            end do ! p2
!
            do p2=2,3 ! y, z
              dp_index=3*(d1-1)+p2
              do i=1,p2-1 ! x, y
                dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
                a_i=Lvec_d(d1,i) ! angular momentum vector
!
                Int2002(dd_index,m) = RRd1(i)*Int2001(dp_index,m) + RRd2(i)*Int2001(dp_index,m+1)
                if(a_i.ne.0)then ! (a-1i) terms
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1_i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2002(dd_index,m) = Int2002(dd_index,m) &
                                      + dble(a_i)*RRd3*Int1001(pp_index,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m
!
! Step 10:
! [2101]^(1) from [2001]^(2), [2000]^(2) and [1001]^(2)
! b+1i
!
        do m=0,L_total-5
!
          dp_index=0
          do d1=1,6
            do p2=1,3
              dp_index=dp_index+1
              do i=1,3
                dpp_index=9*(d1-1)+3*(i-1)+p2 
!                
                a_i=Lvec_d(d1,i)
!
                Int2101(dpp_index,m) = RRb1(i)*Int2001(dp_index,m) + RRb2(i)*Int2001(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! (a-1i)
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2101(dpp_index,m) = Int2101(dpp_index,m) &
                                       + dble(a_i)*(RRb3*Int1001(pp_index,m) + RRb4*Int1001(pp_index,m+1))
                end if
                if(i.eq.p2)then
                  Int2101(dpp_index,m) = Int2101(dpp_index,m) &
                                       + RRb8*Int2000(d1,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 11:
! [2102]^(1) from [2002]^(2), [2001]^(2) and [1002]^(2)
! b+1i
!
        do m=0,L_total-5
!
          dd_index=0
          do d1=1,6
            do d2=1,6
              dd_index=dd_index+1
              do i=1,3
                dpd_index=18*(d1-1)+6*(i-1)+d2
!
                a_i=Lvec_d(d1,i)
                d_i=Lvec_d(d2,i)
!
                Int2102(dpd_index,m) = RRb1(i)*Int2002(dd_index,m) + RRb2(i)*Int2002(dd_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pd_index=6*(p1-1)+d2
                  Int2102(dpd_index,m) = Int2102(dpd_index,m) &
                                       + dble(a_i)*(RRb3*Int1002(pd_index,m) + RRb4*Int1002(pd_index,m+1))
                end if
                if(d_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! d-1i
                  p2=dot_product(v_1i,xyz)
                  dp_index=3*(d1-1)+p2
                  Int2102(dpd_index,m) = Int2102(dpd_index,m) &
                                       + dble(d_i)*RRb8*Int2001(dp_index,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! d1
!
        end do ! m 
!
! Step 12:
! [2112]^(0) from [2102]^(1), [2101]^(1), [2002]^(1) and [1102]^(1)
! c+1i
!
        dppd_index=0
        dpd_index=0
        do d1=1,6
          do p2=1,3
            do d3=1,6
              dd_index=6*(d1-1)+d3 
              dpd_index=dpd_index+1
              do i=1,3
                dppd_index=54*(d1-1)+18*(p2-1)+6*(i-1)+d3
!
                a_i=Lvec_d(d1,i)
                d_i=Lvec_d(d3,i)
!
                Pos_Int(dppd_index) = RRc1(i)*Int2102(dpd_index,0) + RRc2(i)*Int2102(dpd_index,1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppd_index=18*(p1-1)+6*(p2-1)+d3
                  Pos_Int(dppd_index) = Pos_Int(dppd_index) &
                                      + dble(a_i)*RRc3*Int1102(ppd_index,1)
                end if
                if(d_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! d-1i
                  p3=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Pos_Int(dppd_index) = Pos_Int(dppd_index) &
                                      + dble(d_i)*(RRc7*Int2101(dpp_index,0) + RRc8*Int2101(dpp_index,1))
                end if
                if(i.eq.p2)then
                  Pos_Int(dppd_index) = Pos_Int(dppd_index) &
                                      + RRc4*Int2002(dd_index,1)
                end if
              end do ! i
            end do ! d3
          end do ! p2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dppd_index=0
        do d1=1,6
          do p2=1,3
            do p3=1,3
              do d4=1,6
                dppd_index=dppd_index+1
                if(d1.gt.3) Pos_Int(dppd_index) = sq3*Pos_Int(dppd_index)
                if(d4.gt.3) Pos_Int(dppd_index) = sq3*Pos_Int(dppd_index)
              end do
            end do
          end do
        end do
!
! multiply through prefactor
!        Pos_Int(1:324) = prefactor*Pos_Int(1:324)
!
        return
      end if
!
!------------------------------------------------------------------
! [dddp]
!------------------------------------------------------------------
      if(Lcode.eq.2221)then
!
! Requires 18 steps
!
        allocate(Int1000(3,0:L_total-1), Int0100(3,0:L_total-2))
        allocate(Int0200(6,0:L_total-3), Int1100(9,0:L_total-3), Int2000(6,0:L_total-2))
        allocate(Int0210(18,0:L_total-5), Int1200(18,0:L_total-4), Int1110(27,0:L_total-5))
        allocate(Int2010(18,0:L_total-5), Int2100(18,0:L_total-3), Int1210(54,0:L_total-5))
        allocate(Int2110(54,0:L_total-5), Int2200(36,0:L_total-4), Int2210(108,0:L_total-5))
        allocate(Int1220(108,0:L_total-6), Int2120(108,0:L_total-6), Int2220(216,0:L_total-6))
!
! Step 1 :
! [1000]^(6) from [0000]^(7)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0100]^(5) from [0000]^(6)
! b+1i
!
        do m=0,L_total-2
          do i=1,3 ! x,y,z
            Int0100(i,m) = RRb1(i)*G(m) + RRb2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3 :
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-3
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int0200(d_index,m) = RRb1(i)*Int0100(p1,m) + RRb2(i)*Int0100(p1,m+1) &
                                + RRb5*G(m) + RRb6*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int0200(d_index,m) = RRb1(i)*Int0100(p1,m) + RRb2(i)*Int0100(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4:
! [1100]^(4) from [1000]^(5) and [0000]^(5)
! b+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1100(pp_index,m) = RRb1(i)*Int1000(p1,m) + RRb2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1100(pp_index,m) = Int1100(pp_index,m) &
                                    + RRb3*G(m)           + RRb4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5 :
! [2000]^(5) from [1000]^(6) and [0000]^(6)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                               + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 6:
! [0210]^(2) from [0200]^(3), [0100]^(3)
! c+1i
!
        do m=0,L_total-5
!
          dp_index=0
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              b_i=Lvec_d(d1,i)
!
              Int0210(dp_index,m) = RRc1(i)*Int0200(d1,m) + RRc2(i)*Int0200(d1,m+1)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int0210(dp_index,m) = Int0210(dp_index,m) &
                                    + dble(b_i)*RRc4*Int0100(p1,m+1)
              end if
            end do ! i 
          end do ! d1
!
        end do ! m
!
! Step 7:
! [1200]^(3) from [0200]^(4), [0100]^(4)
! a+1i
!
        do m=0,L_total-4
!
          do d1=1,6
            do i=1,3
              pd_index=6*(i-1)+d1
!
              b_i=Lvec_d(d1,i)
!
              Int1200(pd_index,m) = RRa1(i)*Int0200(d1,m) + RRa2(i)*Int0200(d1,m+1)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int1200(pd_index,m) = Int1200(pd_index,m) &
                                    + dble(b_i)*(RRa5*Int0100(p1,m) + RRa6*Int0100(p1,m+1))
              end if
            end do ! i 
          end do ! d1
!
        end do ! m
!
! Step 8:
! [1110]^(2) from [1100]^(3), [1000]^(3) and [0100]^(3)
! c+1i
!
        do m=0,L_total-5
!
          ppp_index=0
          pp_index=0
          do p1=1,3
            do p2=1,3
              pp_index=pp_index+1
              do i=1,3
                ppp_index=ppp_index+1
!                
                Int1110(ppp_index,m) = RRc1(i)*Int1100(pp_index,m) + RRc2(i)*Int1100(pp_index,m+1)
                if(i.eq.p1)then
                  Int1110(ppp_index,m) = Int1110(ppp_index,m) &
                                       + RRc3*Int0100(p2,m+1)
                end if
                if(i.eq.p2)then
                  Int1110(ppp_index,m) = Int1110(ppp_index,m) &
                                       + RRc4*Int1000(p1,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! p1
!
        end do ! m
!
! Step 9:
! [2010]^(2) from [2000]^(3) and [1000]^(3)
! c+1i
!
        do m=0,L_total-5
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2010(dp_index,m) = RRc1(i)*Int2000(d1,m) + RRc2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int2010(dp_index,m) = Int2010(dp_index,m) &
                                    + dble(a_i)*RRc3*Int1000(p1,m+1) 
              end if
            end do ! i
          end do ! d1 
!
        end do ! m
!
! Step 10:
! [2100]^(4) from [2000]^(5) and [1000]^(5)
! b+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2100(dp_index,m) = RRb1(i)*Int2000(d1,m) + RRb2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int2100(dp_index,m) = Int2100(dp_index,m) &
                                    + dble(a_i)*(RRb3*Int1000(p1,m) + RRb4*Int1000(p1,m+1))
              end if
            end do ! i
          end do ! d1 
!
        end do ! m
!
! Step 11:
! [1210]^(2) from [1200]^(3), [1100]^(3) and [0200]^(3)
! c+1i
!
        do m=0,L_total-5
!
          pd_index=0
          pdp_index=0
          do p1=1,3
            do d2=1,6
              pd_index=pd_index+1
              do i=1,3
                pdp_index=pdp_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1210(pdp_index,m) = RRc1(i)*Int1200(pd_index,m) + RRc2(i)*Int1200(pd_index,m+1)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int1210(pdp_index,m) = Int1210(pdp_index,m) &
                                       + dble(b_i)*RRc4*Int1100(pp_index,m+1)
                end if
                if(i.eq.p1)then
                  Int1210(pdp_index,m) = Int1210(pdp_index,m) &
                                       + RRc3*Int0200(d2,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m 
!
! Step 12:
! [2110]^(2) from [2100]^(3), [2000]^(3) and [1100]^(3)
! c+1i
!
        do m=0,L_total-5
!
          dp_index=0
          dpp_index=0
          do d1=1,6
            do p2=1,3
              dp_index=dp_index+1
              do i=1,3
                dpp_index=dpp_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2110(dpp_index,m) = RRc1(i)*Int2100(dp_index,m) + RRc2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + dble(a_i)*RRc3*Int1100(pp_index,m+1)
                end if
                if(i.eq.p2)then
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + RRc4*Int2000(d1,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m 
!
! Step 13:
! [2200]^(3) from [2100]^(4), [2000]^(4) and [1100]^(4)
! b+1i
!
        do m=0,L_total-4
!
          dp_index=0
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2
              dp_index=3*(d1-1)+p2
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1) &
                                  + RRb5*Int2000(d1,m)          + RRb6*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index,m) = Int2200(dd_index,m) &
                                     + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
              end if
            end do ! p2
!
            do p2=2,3
              dp_index=3*(d1-1)+p2
              do i=1,p2-1
                dd_index=dd_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2200(dd_index,m) = Int2200(dd_index,m) &
                                      + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m 
!
! Step 14:
! [1220]^(1) from [1210]^(2), [1200]^(2), [1110]^(2) and [0210]^(2)
! c+1i
!
        do m=0,L_total-6
!
          pdd_index=0
          pd_index=0
          do p1=1,3
            do d2=1,6
              pd_index=pd_index+1
              do p3=1,3
                i=p3
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                pdd_index=pdd_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1220(pdd_index,m) = RRc1(i)*Int1210(pdp_index,m) + RRc2(i)*Int1210(pdp_index,m+1) &
                                     + RRc5*Int1200(pd_index,m)     + RRc6*Int1200(pd_index,m+1)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int1220(pdd_index,m) = Int1220(pdd_index,m) &
                                       + dble(b_i)*RRc4*Int1110(ppp_index,m+1)
                end if
                if(i.eq.p1)then
                  dp_index=3*(d2-1)+p3 
                  Int1220(pdd_index,m) = Int1220(pdd_index,m) &
                                       + RRc3*Int0210(dp_index,m+1)
                end if
              end do ! p3
!
              do p3=2,3
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                do i=1,p3-1
                  pdd_index=pdd_index+1
!
                  b_i=Lvec_d(d2,i)
!
                  Int1220(pdd_index,m) = RRc1(i)*Int1210(pdp_index,m) + RRc2(i)*Int1210(pdp_index,m+1)
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1 ! b-1i
                    p2=dot_product(v_1i,xyz)
                    ppp_index=9*(p1-1)+3*(p2-1)+p3
                    Int1220(pdd_index,m) = Int1220(pdd_index,m) &
                                         + dble(b_i)*RRc4*Int1110(ppp_index,m+1)
                  end if
                  if(i.eq.p1)then
                    dp_index=3*(d2-1)+p3 
                    Int1220(pdd_index,m) = Int1220(pdd_index,m) &
                                         + RRc3*Int0210(dp_index,m+1)
                  end if
                end do ! i
              end do ! p3
            end do ! d2
          end do ! p1 
!
        end do ! m 
!
! Step 15:
! [2120]^(1) from [2110]^(2), [2100]^(2), [1110]^(2) and [2010]^(2)
! c+1i
!
        do m=0,L_total-6
!
          dpd_index=0
          dp2_index=0
          do d1=1,6
            do p2=1,3
              dp2_index=dp2_index+1
              do p3=1,3
                i=p3
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                dpd_index=dpd_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2120(dpd_index,m) = RRc1(i)*Int2110(dpp_index,m) + RRc2(i)*Int2110(dpp_index,m+1) &
                                     + RRc5*Int2100(dp2_index,m)     + RRc6*Int2100(dp2_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                       + dble(a_i)*RRc3*Int1110(ppp_index,m+1)
                end if
                if(i.eq.p2)then
                  dp3_index=3*(d1-1)+p3
                  Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                       + RRc4*Int2010(dp3_index,m+1)
                end if
              end do ! p3
!
              do p3=2,3
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                do i=1,p3-1
                  dpd_index=dpd_index+1
!
                  a_i=Lvec_d(d1,i)
!
                  Int2120(dpd_index,m) = RRc1(i)*Int2110(dpp_index,m) + RRc2(i)*Int2110(dpp_index,m+1)
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1 ! a-1i
                    p1=dot_product(v_1i,xyz)
                    ppp_index=9*(p1-1)+3*(p2-1)+p3
                    Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                         + dble(a_i)*RRc3*Int1110(ppp_index,m+1)
                  end if
                  if(i.eq.p2)then
                    dp3_index=3*(d1-1)+p3
                    Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                         + RRc4*Int2010(dp3_index,m+1)
                  end if
                end do ! i
              end do ! p3
            end do ! d2
          end do ! p1 
!
        end do ! m 
!
! Step 16:
! [2210]^(2) from [2200]^(3), [2100]^(3) and [1200]^(3)
! c+1i
!
        do m=0,L_total-5
!
          ddp_index=0
          dd_index=0
          do d1=1,6
            do d2=1,6
              dd_index=dd_index+1
              do i=1,3
                ddp_index=ddp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2210(ddp_index,m) = RRc1(i)*Int2200(dd_index,m) + RRc2(i)*Int2200(dd_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pd_index=6*(p1-1)+d2
                  Int2210(ddp_index,m) = Int2210(ddp_index,m) &
                                       + dble(a_i)*RRc3*Int1200(pd_index,m+1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dp_index=3*(d1-1)+p2
                  Int2210(ddp_index,m) = Int2210(ddp_index,m) &
                                       + dble(b_i)*RRc4*Int2100(dp_index,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! d1
!
        end do ! m 
!
! Step 17:
! [2220]^(1) from [2210]^(2), [2200]^(2), [2110]^(2), [1210]^(2)
! c+1i
!
        do m=0,L_total-6
!
          ddd_index=0
          dd_index=0
          do d1=1,6
            do d2=1,6
              dd_index=dd_index+1
              do p3=1,3
                i=p3
                ddd_index=ddd_index+1
                ddp_index=18*(d1-1)+3*(d2-1)+p3
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2220(ddd_index,m) = RRc1(i)*Int2210(ddp_index,m) + RRc2(i)*Int2210(ddp_index,m+1) &
                                     + RRc5*Int2200(dd_index,m)     + RRc6*Int2200(dd_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int2220(ddd_index,m) = Int2220(ddd_index,m) &
                                       + dble(a_i)*RRc3*Int1210(pdp_index,m+1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2220(ddd_index,m) = Int2220(ddd_index,m) &
                                       + dble(b_i)*RRc4*Int2110(dpp_index,m+1)
                end if
              end do ! p3
!
              do p3=2,3
                ddp_index=18*(d1-1)+3*(d2-1)+p3
                do i=1,p3-1
                  ddd_index=ddd_index+1
!
                  a_i=Lvec_d(d1,i)
                  b_i=Lvec_d(d2,i)
!
                  Int2220(ddd_index,m) = RRc1(i)*Int2210(ddp_index,m) + RRc2(i)*Int2210(ddp_index,m+1)
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1 ! a-1i
                    p1=dot_product(v_1i,xyz)
                    pdp_index=18*(p1-1)+3*(d2-1)+p3
                    Int2220(ddd_index,m) = Int2220(ddd_index,m) &
                                         + dble(a_i)*RRc3*Int1210(pdp_index,m+1)
                  end if
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1 ! b-1i
                    p2=dot_product(v_1i,xyz)
                    dpp_index=9*(d1-1)+3*(p2-1)+p3
                    Int2220(ddd_index,m) = Int2220(ddd_index,m) &
                                         + dble(b_i)*RRc4*Int2110(dpp_index,m+1)
                  end if
                end do ! i
              end do ! p3
            end do ! d2
          end do ! d1
!
        end do ! m 
!
! Step 18:
! [2221]^(0) from [2220]^(1), [2210]^(1), [2120]^(1), [1220]^(1)
! d+1i
!
        ddd_index=0
        dddp_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              do i=1,3
                dddp_index=dddp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                Pos_Int(dddp_index) = RRd1(i)*Int2220(ddd_index,0) + RRd2(i)*Int2220(ddd_index,1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdd_index=36*(p1-1)+6*(d2-1)+d3
                  Pos_Int(dddp_index) = Pos_Int(dddp_index) &
                                      + dble(a_i)*RRd3*Int1220(pdd_index,1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpd_index=18*(d1-1)+6*(p2-1)+d3
                  Pos_Int(dddp_index) = Pos_Int(dddp_index) &
                                        + dble(b_i)*RRd4*Int2120(dpd_index,1)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1 ! c-1i
                  p3=dot_product(v_1i,xyz)
                  ddp_index=18*(d1-1)+3*(d2-1)+p3
                  Pos_Int(dddp_index) = Pos_Int(dddp_index) &
                                        + dble(c_i)*(RRd5*Int2210(ddp_index,0) + RRd6*Int2210(ddp_index,1))
                end if
              end do ! i
            end do ! d3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dddp_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              do p4=1,3
                dddp_index=dddp_index+1
                if(d1.gt.3) Pos_Int(dddp_index) = sq3*Pos_Int(dddp_index)
                if(d2.gt.3) Pos_Int(dddp_index) = sq3*Pos_Int(dddp_index)
                if(d3.gt.3) Pos_Int(dddp_index) = sq3*Pos_Int(dddp_index)
              end do
            end do
          end do
        end do
!
! multiply through prefactor
!        Pos_Int(1:648) = prefactor*Pos_Int(1:648)
!
        return
      end if
!
!------------------------------------------------------------------
! [dddd]
!------------------------------------------------------------------
      if(Lcode.eq.2222)then
!
! Requires 27 steps
!
        allocate(Int1000(3,0:L_total-1), Int0100(3,0:L_total-3))
        allocate(Int0110(9,0:L_total-5), Int1010(9,0:L_total-5), Int0200(6,0:L_total-3))
        allocate(Int1100(9,0:L_total-3), Int2000(6,0:L_total-2), Int0210(18,0:L_total-5))
        allocate(Int1110(27,0:L_total-5), Int2010(18,0:L_total-5), Int1200(18,0:L_total-4))
        allocate(Int2100(18,0:L_total-3), Int0220(36,0:L_total-6), Int1120(54,0:L_total-6))
        allocate(Int2020(36,0:L_total-6), Int1210(54,0:L_total-5), Int2110(54,0:L_total-5))
        allocate(Int2200(36,0:L_total-4), Int1220(108,0:L_total-6), Int2120(108,0:L_total-6))
        allocate(Int2210(108,0:L_total-5), Int1221(324,0:L_total-7), Int2121(324,0:L_total-7))
        allocate(Int2211(324,0:L_total-7), Int2220(216,0:L_total-6), Int2221(648,0:L_total-7))
!

			if(verbose.eq.1) then
	
				print *, ""
				print *, "Case DDDD"
				print *, ""
		
			endif



! Step 1 :
! [1000]^(7) from [0000]^(8)
! a+1i
!
        do m=0,L_total-1
          do i=1,3 ! x,y,z
            Int1000(i,m) = RRa1(i)*G(m) + RRa2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 2 :
! [0100]^(5) from [0000]^(6)
! b+1i
!
        do m=0,L_total-3
          do i=1,3 ! x,y,z
            Int0100(i,m) = RRb1(i)*G(m) + RRb2(i)*G(m+1)
          end do ! i 
        end do ! m
!
! Step 3 :
! [0110]^(3) from [0100]^(4) and [0000]^(4)
! c+1i
!
        do m=0,L_total-5
!
          pp_index=0
          do p1=1,3
            do i=1,3
              pp_index=pp_index+1
!
              Int0110(pp_index,m) = RRc1(i)*Int0100(p1,m) + RRc2(i)*Int0100(p1,m+1)
              if(i.eq.p1)then
                Int0110(pp_index,m) = Int0110(pp_index,m) &
                                    + RRc4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 4 :
! [1010]^(3) from [1000]^(4) and [0000]^(4)
! c+1i
!
        do m=0,L_total-5
!
          pp_index=0
          do p1=1,3
            do i=1,3
              pp_index=pp_index+1
!
              Int1010(pp_index,m) = RRc1(i)*Int1000(p1,m) + RRc2(i)*Int1000(p1,m+1)
              if(i.eq.p1)then
                Int1010(pp_index,m) = Int1010(pp_index,m) &
                                    + RRc3*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 5 :
! [0200]^(4) from [0100]^(5) and [0000]^(5)
! b+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-4
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int0200(d_index,m) = RRb1(i)*Int0100(p1,m) + RRb2(i)*Int0100(p1,m+1) &
                                + RRb5*G(m) + RRb6*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int0200(d_index,m) = RRb1(i)*Int0100(p1,m) + RRb2(i)*Int0100(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 6:
! [1100]^(5) from [1000]^(6) and [0000]^(6)
! b+1i
!
        do m=0,L_total-3
!
          pp_index=0
          do p1=1,3 ! x, y, z
            do i=1,3 ! x, y, z
              pp_index=pp_index+1
!
              Int1100(pp_index,m) = RRb1(i)*Int1000(p1,m) + RRb2(i)*Int1000(p1,m+1)
              if(p1.eq.i)then
                Int1100(pp_index,m) = Int1100(pp_index,m) &
                                    + RRb3*G(m)           + RRb4*G(m+1)
              end if
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 7 :
! [2000]^(6) from [1000]^(7) and [0000]^(7)
! a+1i
!
! Note: d's are numbered xx-1, yy-2, zz-3, xy-4, xz-5, yz-6
!
        do m=0,L_total-2
! do first three d's
          d_index=0
          do p1=1,3 ! x,y,z
            i=p1
            d_index=d_index+1
            Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1) &
                                + RRa3*G(m) + RRa4*G(m+1)
          end do ! p1
! do last three d's
          do p1=2,3 ! y,z
            do i=1,p1-1
              d_index=d_index+1
              ! by design p1 ne i
              Int2000(d_index,m) = RRa1(i)*Int1000(p1,m) + RRa2(i)*Int1000(p1,m+1)
            end do ! i
          end do ! p1
!
        end do ! m
!
! Step 8:
! [0210]^(3) from [0200]^(4), [0100]^(4)
! c+1i
!
        do m=0,L_total-5
!
          dp_index=0
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              b_i=Lvec_d(d1,i)
!
              Int0210(dp_index,m) = RRc1(i)*Int0200(d1,m) + RRc2(i)*Int0200(d1,m+1)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int0210(dp_index,m) = Int0210(dp_index,m) &
                                    + dble(b_i)*RRc4*Int0100(p1,m+1)
              end if
            end do ! i 
          end do ! d1
!
        end do ! m
!
! Step 9:
! [1110]^(3) from [1100]^(4), [1000]^(4)
! c+1i
!
        do m=0,L_total-5
!
          ppp_index=0
          pp_index=0
          do p1=1,3
            do p2=1,3
              pp_index=pp_index+1
              do i=1,3
                ppp_index=ppp_index+1
!
                Int1110(ppp_index,m) = RRc1(i)*Int1100(pp_index,m) + RRc2(i)*Int1100(pp_index,m+1)
                if(i.eq.p1)then
                  Int1110(ppp_index,m) = Int1110(ppp_index,m) &
                                       + RRc3*Int0100(p2,m+1)
                end if
                if(i.eq.p2)then
                  Int1110(ppp_index,m) = Int1110(ppp_index,m) &
                                       + RRc4*Int1000(p1,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! p1
!
        end do ! m
!
! Step 10:
! [2010]^(3) from [2000]^(4) and [1000]^(4)
! c+1i
!
        do m=0,L_total-5
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2010(dp_index,m) = RRc1(i)*Int2000(d1,m) + RRc2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int2010(dp_index,m) = Int2010(dp_index,m) &
                                    + dble(a_i)*RRc3*Int1000(p1,m+1) 
              end if
            end do ! i
          end do ! d1 
!
        end do ! m
!
! Step 11:
! [1200]^(4) from [1100]^(5), [0100]^(5) and [1000]^(5)
! b+1i
!
        do m=0,L_total-4
!
          pd_index=0
          do p1=1,3
            do p2=1,3
              i=p2
              pd_index=pd_index+1
              pp_index=3*(p1-1)+p2
!
              Int1200(pd_index,m) = RRb1(i)*Int1100(pp_index,m) + RRb2(i)*Int1100(pp_index,m+1) &
                                  + RRb5*Int1000(p1,m)          + RRb6*Int1000(p1,m+1)
              if(i.eq.p1)then
                Int1200(pd_index,m) = Int1200(pd_index,m) &
                                    + RRb3*Int0100(p2,m) + RRb4*Int0100(p2,m+1)
              end if
            end do ! p2
!
            do p2=2,3
              pp_index=3*(p1-1)+p2
              do i=1,p2-1
                pd_index=pd_index+1
!
                Int1200(pd_index,m) = RRb1(i)*Int1100(pp_index,m) + RRb2(i)*Int1100(pp_index,m+1)
                if(i.eq.p1)then
                  Int1200(pd_index,m) = Int1200(pd_index,m) &
                                      + RRb3*Int0100(p2,m) + RRb4*Int0100(p2,m+1)
                end if
              end do ! i 
            end do ! p2
          end do ! p1
!
        end do ! m
!
! Step 12:
! [2100]^(5) from [2000]^(6) and [1000]^(6)
! b+1i
!
        do m=0,L_total-3
!
          dp_index=0 
          do d1=1,6
            do i=1,3
              dp_index=dp_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2100(dp_index,m) = RRb1(i)*Int2000(d1,m) + RRb2(i)*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                Int2100(dp_index,m) = Int2100(dp_index,m) &
                                    + dble(a_i)*(RRb3*Int1000(p1,m) + RRb4*Int1000(p1,m+1))
              end if
            end do ! i
          end do ! d1 
!
        end do ! m
!
! Step 13 : 
! [0220]^(2) from [0210]^(3), [0200]^(3) and [0110]^(3)
! c+1i
!
        do m=0,L_total-6
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2
              dd_index=dd_index+1
              dp_index=3*(d1-1)+p2
!
              b_i=Lvec_d(d1,i)
!
              Int0220(dd_index,m) = RRc1(i)*Int0210(dp_index,m) + RRc2(i)*Int0210(dp_index,m+1) &
                                  + RRc5*Int0200(d1,m)          + RRc6*Int0200(d1,m+1)
              if(b_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int0220(dd_index,m) = Int0220(dd_index,m) &
                                    + dble(b_i)*RRc4*Int0110(pp_index,m+1)
              end if
            end do ! p3
!
            do p2=2,3
              dp_index=3*(d1-1)+p2
              do i=1,p2-1
                dd_index=dd_index+1
!
                b_i=Lvec_d(d1,i)
!
                Int0220(dd_index,m) = RRc1(i)*Int0210(dp_index,m) + RRc2(i)*Int0210(dp_index,m+1)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int0220(dd_index,m) = Int0220(dd_index,m) &
                                      + dble(b_i)*RRc4*Int0110(pp_index,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m 
!
! Step 14:
! [1120]^(2) from [1110]^(3), [1100]^(3), [0110]^(3), [1010]^(3)
! c+1i
!
        do m=0,L_total-6
!
          ppd_index=0
          do p1=1,3
            do p2=1,3
              do p3=1,3
                i=p3
                ppd_index=ppd_index+1
                ppp_index=9*(p1-1)+3*(p2-1)+p3 
                pp_index=3*(p1-1)+p2
!
                Int1120(ppd_index,m) = RRc1(i)*Int1110(ppp_index,m) + RRc2(i)*Int1110(ppp_index,m+1) &
                                     + RRc5*Int1100(pp_index,m)   + RRc6*Int1100(pp_index,m+1)
                if(i.eq.p1)then
                  pp_index=3*(p2-1)+p3
                  Int1120(ppd_index,m) = Int1120(ppd_index,m) &
                                       + RRc3*Int0110(pp_index,m+1)
                end if
                if(i.eq.p2)then
                  pp_index=3*(p1-1)+p3
                  Int1120(ppd_index,m) = Int1120(ppd_index,m) &
                                       + RRc4*Int1010(pp_index,m+1)
                end if
              end do ! p3
!
              do p3=2,3
                ppp_index=9*(p1-1)+3*(p2-1)+p3 
                do i=1,p3-1
                  ppd_index=ppd_index+1
!
                  Int1120(ppd_index,m) = RRc1(i)*Int1110(ppp_index,m) + RRc2(i)*Int1110(ppp_index,m+1)
                  if(i.eq.p1)then
                    pp_index=3*(p2-1)+p3
                    Int1120(ppd_index,m) = Int1120(ppd_index,m) &
                                         + RRc3*Int0110(pp_index,m+1)
                  end if
                  if(i.eq.p2)then
                    pp_index=3*(p1-1)+p3
                    Int1120(ppd_index,m) = Int1120(ppd_index,m) &
                                         + RRc4*Int1010(pp_index,m+1)
                  end if
                end do ! i
              end do ! p3
            end do ! p2
          end do ! p1
!
        end do ! m 
!
! Step 15:
! [2020]^(2) from [2010]^(3), [2000]^(3) and [1010]^(3)
! c+1i
!
        do m=0,L_total-6
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2 ! do xx, yy, zz first
              dd_index=dd_index+1
              dp_index=3*(d1-1)+p2
! 
              a_i=Lvec_d(d1,i) ! angular momentum vector
!
              Int2020(dd_index,m) = RRc1(i)*Int2010(dp_index,m) + RRc2(i)*Int2010(dp_index,m+1) &
                                  + RRc5*Int2000(d1,m)          + RRc6*Int2000(d1,m+1)
              if(a_i.ne.0)then ! (a-1i) terms
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1_i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2020(dd_index,m) = Int2020(dd_index,m) &
                                    + dble(a_i)*RRc3*Int1010(pp_index,m+1)
              end if
            end do ! p2
!
            do p2=2,3 ! y, z
              dp_index=3*(d1-1)+p2
              do i=1,p2-1 ! x, y
                dd_index=dd_index+1
!
! i ne p2, therefore no (b-1i) terms
!
                a_i=Lvec_d(d1,i) ! angular momentum vector
!
                Int2020(dd_index,m) = RRc1(i)*Int2010(dp_index,m) + RRc2(i)*Int2010(dp_index,m+1)
                if(a_i.ne.0)then ! (a-1i) terms
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1_i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2020(dd_index,m) = Int2020(dd_index,m) &
                                      + dble(a_i)*RRc3*Int1010(pp_index,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m
!
! Step 16:
! [1210]^(3) from [1200]^(4), [1100]^(4) and [0200]^(4)
! c+1i
!
        do m=0,L_total-5
!
          pd_index=0
          pdp_index=0
          do p1=1,3
            do d2=1,6
              pd_index=pd_index+1
              do i=1,3
                pdp_index=pdp_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1210(pdp_index,m) = RRc1(i)*Int1200(pd_index,m) + RRc2(i)*Int1200(pd_index,m+1)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int1210(pdp_index,m) = Int1210(pdp_index,m) &
                                       + dble(b_i)*RRc4*Int1100(pp_index,m+1)
                end if
                if(i.eq.p1)then
                  Int1210(pdp_index,m) = Int1210(pdp_index,m) &
                                       + RRc3*Int0200(d2,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! p1
!
        end do ! m 
!
! Step 17:
! [2110]^(3) from [2100]^(4), [2000]^(4) and [1100]^(4)
! c+1i
!
        do m=0,L_total-5
!
          dp_index=0
          dpp_index=0
          do d1=1,6
            do p2=1,3
              dp_index=dp_index+1
              do i=1,3
                dpp_index=dpp_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2110(dpp_index,m) = RRc1(i)*Int2100(dp_index,m) + RRc2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + dble(a_i)*RRc3*Int1100(pp_index,m+1)
                end if
                if(i.eq.p2)then
                  Int2110(dpp_index,m) = Int2110(dpp_index,m) &
                                       + RRc4*Int2000(d1,m+1)
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m 
!
! Step 18:
! [2200]^(4) from [2100]^(5), [2000]^(5) and [1100]^(5)
! b+1i
!
        do m=0,L_total-4
!
          dd_index=0
          do d1=1,6
            do p2=1,3
              i=p2
              dp_index=3*(d1-1)+p2
              dd_index=dd_index+1
!
              a_i=Lvec_d(d1,i)
!
              Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1) &
                                  + RRb5*Int2000(d1,m)          + RRb6*Int2000(d1,m+1)
              if(a_i.ne.0)then
                v_1i(1:3)=Lvec_d(d1,1:3)
                v_1i(i)=v_1i(i)-1 ! a-1i
                p1=dot_product(v_1i,xyz)
                pp_index=3*(p1-1)+p2
                Int2200(dd_index,m) = Int2200(dd_index,m) &
                                     + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
              end if
            end do ! p2
!
            do p2=2,3
              dp_index=3*(d1-1)+p2
              do i=1,p2-1
                dd_index=dd_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2200(dd_index,m) = RRb1(i)*Int2100(dp_index,m) + RRb2(i)*Int2100(dp_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pp_index=3*(p1-1)+p2
                  Int2200(dd_index,m) = Int2200(dd_index,m) &
                                       + dble(a_i)*(RRb3*Int1100(pp_index,m) + RRb4*Int1100(pp_index,m+1))
                end if
              end do ! i
            end do ! p2
          end do ! d1
!
        end do ! m 
!
! Step 19:
! [1220]^(2) from [1210]^(3), [1200]^(3), [1110]^(3) and [0210]^(3)
! c+1i
!
        do m=0,L_total-6
!
          pdd_index=0
          pd_index=0
          do p1=1,3
            do d2=1,6
              pd_index=pd_index+1
              do p3=1,3
                i=p3
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                pdd_index=pdd_index+1
!
                b_i=Lvec_d(d2,i)
!
                Int1220(pdd_index,m) = RRc1(i)*Int1210(pdp_index,m) + RRc2(i)*Int1210(pdp_index,m+1) &
                                     + RRc5*Int1200(pd_index,m)     + RRc6*Int1200(pd_index,m+1)
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int1220(pdd_index,m) = Int1220(pdd_index,m) &
                                       + dble(b_i)*RRc4*Int1110(ppp_index,m+1)
                end if
                if(i.eq.p1)then
                  dp_index=3*(d2-1)+p3
                  Int1220(pdd_index,m) = Int1220(pdd_index,m) &
                                       + RRc3*Int0210(dp_index,m+1)
                end if 
              end do ! p3
!
              do p3=2,3
                pdp_index=18*(p1-1)+3*(d2-1)+p3
                do i=1,p3-1
                  pdd_index=pdd_index+1
!
                  b_i=Lvec_d(d2,i)
!
                  Int1220(pdd_index,m) = RRc1(i)*Int1210(pdp_index,m) + RRc2(i)*Int1210(pdp_index,m+1)
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1 ! b-1i
                    p2=dot_product(v_1i,xyz)
                    ppp_index=9*(p1-1)+3*(p2-1)+p3
                    Int1220(pdd_index,m) = Int1220(pdd_index,m) &
                                         + dble(b_i)*RRc4*Int1110(ppp_index,m+1)
                  end if
                  if(i.eq.p1)then
                    dp_index=3*(d2-1)+p3
                    Int1220(pdd_index,m) = Int1220(pdd_index,m) &
                                         + RRc3*Int0210(dp_index,m+1)
                  end if 
                end do ! i
              end do ! p3
            end do ! d2
          end do ! p1 
!
        end do ! m 
!
! Step 20:
! [2120]^(2) from [2110]^(3), [2100]^(3), [1110]^(3) and [2010]^(3)
! c+1i
!
        do m=0,L_total-6
!
          dpd_index=0
          dp2_index=0
          do d1=1,6
            do p2=1,3
              dp2_index=dp2_index+1
              do p3=1,3
                i=p3
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                dpd_index=dpd_index+1
!
                a_i=Lvec_d(d1,i)
!
                Int2120(dpd_index,m) = RRc1(i)*Int2110(dpp_index,m) + RRc2(i)*Int2110(dpp_index,m+1) &
                                     + RRc5*Int2100(dp2_index,m)     + RRc6*Int2100(dp2_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  ppp_index=9*(p1-1)+3*(p2-1)+p3
                  Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                       + dble(a_i)*RRc3*Int1110(ppp_index,m+1)
                end if
                if(i.eq.p2)then
                  dp3_index=3*(d1-1)+p3
                  Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                       + RRc4*Int2010(dp3_index,m+1)
                end if
              end do ! p3
!
              do p3=2,3
                dpp_index=9*(d1-1)+3*(p2-1)+p3
                do i=1,p3-1
                  dpd_index=dpd_index+1
!
                  a_i=Lvec_d(d1,i)
!
                  Int2120(dpd_index,m) = RRc1(i)*Int2110(dpp_index,m) + RRc2(i)*Int2110(dpp_index,m+1)
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1 ! a-1i
                    p1=dot_product(v_1i,xyz)
                    ppp_index=9*(p1-1)+3*(p2-1)+p3
                    Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                         + dble(a_i)*RRc3*Int1110(ppp_index,m+1)
                  end if
                  if(i.eq.p2)then
                    dp3_index=3*(d1-1)+p3
                    Int2120(dpd_index,m) = Int2120(dpd_index,m) &
                                         + RRc4*Int2010(dp3_index,m+1)
                  end if
                end do ! i
              end do ! p3
            end do ! d2
          end do ! p1 
!
        end do ! m 
!
! Step 21:
! [2210]^(3) from [2200]^(4), [2100]^(4) and [1200]^(4)
! c+1i
!
        do m=0,L_total-5
!
          ddp_index=0
          dd_index=0
          do d1=1,6
            do d2=1,6
              dd_index=dd_index+1
              do i=1,3
                ddp_index=ddp_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2210(ddp_index,m) = RRc1(i)*Int2200(dd_index,m) + RRc2(i)*Int2200(dd_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pd_index=6*(p1-1)+d2
                  Int2210(ddp_index,m) = Int2210(ddp_index,m) &
                                       + dble(a_i)*RRc3*Int1200(pd_index,m+1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dp_index=3*(d1-1)+p2
                  Int2210(ddp_index,m) = Int2210(ddp_index,m) &
                                       + dble(b_i)*RRc4*Int2100(dp_index,m+1)
                end if
              end do ! i
            end do ! d2
          end do ! d1
!
        end do ! m 
!
! Step 22 :
! [1221]^(1) from [1220]^(2), [1210]^(2), [1120]^(2) and [0220]^(2)
! d+1i
!
        do m=0,L_total-7
!
          pddp_index=0
          pdd_index=0
          do p1=1,3
            do d2=1,6
              do d3=1,6
                pdd_index=pdd_index+1
                dd_index=6*(d2-1)+d3
                do i=1,3
                  pddp_index=pddp_index+1
!
                  b_i=Lvec_d(d2,i)
                  c_i=Lvec_d(d3,i)
!
                  Int1221(pddp_index,m) = RRd1(i)*Int1220(pdd_index,m) + RRd2(i)*Int1220(pdd_index,m+1)
                  if(i.eq.p1)then
                    Int1221(pddp_index,m) = Int1221(pddp_index,m) &
                                          + RRd3*Int0220(dd_index,m+1)
                  end if
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1
                    p2=dot_product(v_1i,xyz)
                    ppd_index=18*(p1-1)+6*(p2-1)+d3
                    Int1221(pddp_index,m) = Int1221(pddp_index,m) &
                                          + dble(b_i)*RRd4*Int1120(ppd_index,m+1)
                  end if
                  if(c_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d3,1:3)
                    v_1i(i)=v_1i(i)-1
                    p3=dot_product(v_1i,xyz)
                    pdp_index=18*(p1-1)+3*(d2-1)+p3
                    Int1221(pddp_index,m) = Int1221(pddp_index,m) &
                                          + dble(c_i)*(RRd5*Int1210(pdp_index,m) + RRd6*Int1210(pdp_index,m+1))
                  end if
                end do ! i
              end do ! d3
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 23 :
! [2121]^(1) from [2120]^(2), [2110]^(2), [2020]^(2) and [1120]^(2)
! d+1i
!
        do m=0,L_total-7
!
          dpdp_index=0
          dpd_index=0
          do d1=1,6
            do p2=1,3
              do d3=1,6
                dpd_index=dpd_index+1
                dd_index=6*(d1-1)+d3
                do i=1,3
                  dpdp_index=dpdp_index+1
!
                  a_i=Lvec_d(d1,i)
                  c_i=Lvec_d(d3,i)
!
                  Int2121(dpdp_index,m) = RRd1(i)*Int2120(dpd_index,m) + RRd2(i)*Int2120(dpd_index,m+1)
                  if(i.eq.p2)then
                    Int2121(dpdp_index,m) = Int2121(dpdp_index,m) &
                                          + RRd4*Int2020(dd_index,m+1)
                  end if
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1
                    p1=dot_product(v_1i,xyz)
                    ppd_index=18*(p1-1)+6*(p2-1)+d3
                    Int2121(dpdp_index,m) = Int2121(dpdp_index,m) &
                                          + dble(a_i)*RRd3*Int1120(ppd_index,m+1)
                  end if
                  if(c_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d3,1:3)
                    v_1i(i)=v_1i(i)-1
                    p3=dot_product(v_1i,xyz)
                    dpp_index=9*(d1-1)+3*(p2-1)+p3
                    Int2121(dpdp_index,m) = Int2121(dpdp_index,m) &
                                          + dble(c_i)*(RRd5*Int2110(dpp_index,m) + RRd6*Int2110(dpp_index,m+1))
                  end if
                end do ! i
              end do ! d3
            end do ! d2
          end do ! p1
!
        end do ! m
!
! Step 24 :
! [2211]^(1) from [2210]^(2), [2110]^(2), [2200]^(2) and [1210]^(2)
! d+1i
!
        do m=0,L_total-7
!
          ddpp_index=0
          ddp_index=0
          dd_index=0
          do d1=1,6
            do d2=1,6
              dd_index=dd_index+1
              do p3=1,3
                ddp_index=ddp_index+1
                do i=1,3
                  ddpp_index=ddpp_index+1
!
                  a_i=Lvec_d(d1,i)
                  b_i=Lvec_d(d2,i)
!
                  Int2211(ddpp_index,m) = RRd1(i)*Int2210(ddp_index,m) + RRd2(i)*Int2210(ddp_index,m+1)
                  if(i.eq.p3)then
                    Int2211(ddpp_index,m) = Int2211(ddpp_index,m) &
                                          + RRd5*Int2200(dd_index,m) + RRd6*Int2200(dd_index,m+1)
                  end if
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1
                    p1=dot_product(v_1i,xyz)
                    pdp_index=18*(p1-1)+3*(d2-1)+p3
                    Int2211(ddpp_index,m) = Int2211(ddpp_index,m) &
                                          + dble(a_i)*RRd3*Int1210(pdp_index,m+1)
                  end if
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1
                    p2=dot_product(v_1i,xyz)
                    dpp_index=9*(d1-1)+3*(p2-1)+p3
                    Int2211(ddpp_index,m) = Int2211(ddpp_index,m) &
                                          + dble(b_i)*RRd4*Int2110(dpp_index,m+1)
                  end if
                end do ! i
              end do ! p3
            end do ! d2
          end do ! d1
!
        end do ! m
!
! Step 25:
! [2220]^(1) from [2210]^(2), [2200]^(2), [2110]^(2), [1210]^(2)
! c+1i
!
        do m=0,L_total-6
!
          ddd_index=0
          dd_index=0
          do d1=1,6
            do d2=1,6
              dd_index=dd_index+1
              do p3=1,3
                i=p3
                ddd_index=ddd_index+1
                ddp_index=18*(d1-1)+3*(d2-1)+p3
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
!
                Int2220(ddd_index,m) = RRc1(i)*Int2210(ddp_index,m) + RRc2(i)*Int2210(ddp_index,m+1) &
                                     + RRc5*Int2200(dd_index,m)     + RRc6*Int2200(dd_index,m+1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1 ! a-1i
                  p1=dot_product(v_1i,xyz)
                  pdp_index=18*(p1-1)+3*(d2-1)+p3
                  Int2220(ddd_index,m) = Int2220(ddd_index,m) &
                                       + dble(a_i)*RRc3*Int1210(pdp_index,m+1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1 ! b-1i
                  p2=dot_product(v_1i,xyz)
                  dpp_index=9*(d1-1)+3*(p2-1)+p3
                  Int2220(ddd_index,m) = Int2220(ddd_index,m) &
                                       + dble(b_i)*RRc4*Int2110(dpp_index,m+1)
                end if
              end do ! p3
!
              do p3=2,3
                ddp_index=18*(d1-1)+3*(d2-1)+p3
                do i=1,p3-1
                  ddd_index=ddd_index+1
!
                  a_i=Lvec_d(d1,i)
                  b_i=Lvec_d(d2,i)
!
                  Int2220(ddd_index,m) = RRc1(i)*Int2210(ddp_index,m) + RRc2(i)*Int2210(ddp_index,m+1)
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1 ! a-1i
                    p1=dot_product(v_1i,xyz)
                    pdp_index=18*(p1-1)+3*(d2-1)+p3
                    Int2220(ddd_index,m) = Int2220(ddd_index,m) &
                                         + dble(a_i)*RRc3*Int1210(pdp_index,m+1)
                  end if
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1 ! b-1i
                    p2=dot_product(v_1i,xyz)
                    dpp_index=9*(d1-1)+3*(p2-1)+p3
                    Int2220(ddd_index,m) = Int2220(ddd_index,m) &
                                         + dble(b_i)*RRc4*Int2110(dpp_index,m+1)
                  end if
                end do ! i
              end do ! p3
            end do ! d2
          end do ! d1
!
        end do ! m 
!
! Step 26 :
! [2221]^(1) from [2220]^(2), [2210]^(2), [2120]^(2) and [1220]^(2)
! d+1i
!
        do m=0,L_total-7
!
          dddp_index=0
          ddd_index=0
          do d1=1,6
            do d2=1,6
              do d3=1,6
                ddd_index=ddd_index+1
                do i=1,3
                  dddp_index=dddp_index+1
!
                  a_i=Lvec_d(d1,i)
                  b_i=Lvec_d(d2,i)
                  c_i=Lvec_d(d3,i)
!
                  Int2221(dddp_index,m) = RRd1(i)*Int2220(ddd_index,m) + RRd2(i)*Int2220(ddd_index,m+1)
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1
                    p1=dot_product(v_1i,xyz)
                    pdd_index=36*(p1-1)+6*(d2-1)+d3
                    Int2221(dddp_index,m) = Int2221(dddp_index,m) &
                                          + dble(a_i)*RRd3*Int1220(pdd_index,m+1)
                  end if
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1
                    p2=dot_product(v_1i,xyz)
                    dpd_index=18*(d1-1)+6*(p2-1)+d3
                    Int2221(dddp_index,m) = Int2221(dddp_index,m) &
                                          + dble(b_i)*RRd4*Int2120(dpd_index,m+1)
                  end if
                  if(c_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d3,1:3)
                    v_1i(i)=v_1i(i)-1
                    p3=dot_product(v_1i,xyz)
                    ddp_index=18*(d1-1)+3*(d2-1)+p3
                    Int2221(dddp_index,m) = Int2221(dddp_index,m) &
                                          + dble(c_i)*(RRd5*Int2210(ddp_index,m) + RRd6*Int2210(ddp_index,m+1))
                  end if
                end do ! i
              end do ! d3
            end do ! d2
          end do ! d1
!
        end do ! m
!
! Step 27 :
! [2222]^(0) from [2221]^(1), [2220]^(1), [2211]^(1), [2121]^(1) and [1221]^(1)
! d+1i
!
        dddd_index=0
        ddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              ddd_index=ddd_index+1
              do p4=1,3
                i=p4
                dddp_index=108*(d1-1)+18*(d2-1)+3*(d3-1)+p4
                dddd_index=dddd_index+1
!
                a_i=Lvec_d(d1,i)
                b_i=Lvec_d(d2,i)
                c_i=Lvec_d(d3,i)
!
                Pos_Int(dddd_index) = RRd1(i)*Int2221(dddp_index,0) + RRd2(i)*Int2221(dddp_index,1) &
                                    + RRd7*Int2220(ddd_index,0)     + RRd8*Int2220(ddd_index,1)
                if(a_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d1,1:3)
                  v_1i(i)=v_1i(i)-1
                  p1=dot_product(v_1i,xyz)
                  pddp_index=108*(p1-1)+18*(d2-1)+3*(d3-1)+p4
                  Pos_Int(dddd_index) = Pos_Int(dddd_index) &
                                      + dble(a_i)*RRd3*Int1221(pddp_index,1)
                end if
                if(b_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d2,1:3)
                  v_1i(i)=v_1i(i)-1
                  p2=dot_product(v_1i,xyz)
                  dpdp_index=54*(d1-1)+18*(p2-1)+3*(d3-1)+p4
                  Pos_Int(dddd_index) = Pos_Int(dddd_index) &
                                      + dble(b_i)*RRd4*Int2121(dpdp_index,1)
                end if
                if(c_i.ne.0)then
                  v_1i(1:3)=Lvec_d(d3,1:3)
                  v_1i(i)=v_1i(i)-1
                  p3=dot_product(v_1i,xyz)
                  ddpp_index=54*(d1-1)+9*(d2-1)+3*(p3-1)+p4
                  Pos_Int(dddd_index) = Pos_Int(dddd_index) &
                                      + dble(c_i)*(RRd5*Int2211(ddpp_index,0) + RRd6*Int2211(ddpp_index,1))
                end if
              end do ! p4
!
              do p4=2,3
                dddp_index=108*(d1-1)+18*(d2-1)+3*(d3-1)+p4
                do i=1,p4-1
                  dddd_index=dddd_index+1
!
                  a_i=Lvec_d(d1,i)
                  b_i=Lvec_d(d2,i)
                  c_i=Lvec_d(d3,i)
!
                  Pos_Int(dddd_index) = RRd1(i)*Int2221(dddp_index,0) + RRd2(i)*Int2221(dddp_index,1)
                  if(a_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d1,1:3)
                    v_1i(i)=v_1i(i)-1
                    p1=dot_product(v_1i,xyz)
                    pddp_index=108*(p1-1)+18*(d2-1)+3*(d3-1)+p4
                    Pos_Int(dddd_index) = Pos_Int(dddd_index) &
                                        + dble(a_i)*RRd3*Int1221(pddp_index,1)
                  end if
                  if(b_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d2,1:3)
                    v_1i(i)=v_1i(i)-1
                    p2=dot_product(v_1i,xyz)
                    dpdp_index=54*(d1-1)+18*(p2-1)+3*(d3-1)+p4
                    Pos_Int(dddd_index) = Pos_Int(dddd_index) &
                                        + dble(b_i)*RRd4*Int2121(dpdp_index,1)
                  end if
                  if(c_i.ne.0)then
                    v_1i(1:3)=Lvec_d(d3,1:3)
                    v_1i(i)=v_1i(i)-1
                    p3=dot_product(v_1i,xyz)
                    ddpp_index=54*(d1-1)+9*(d2-1)+3*(p3-1)+p4
                    Pos_Int(dddd_index) = Pos_Int(dddd_index) &
                                        + dble(c_i)*(RRd5*Int2211(ddpp_index,0) + RRd6*Int2211(ddpp_index,1))
                  end if
                end do ! i
              end do ! p4
            end do ! d3
          end do ! d2
        end do ! d1
!
! multiply by sqrt(3) for each d-function xy, xz, yz
!
        dddd_index=0
        do d1=1,6
          do d2=1,6
            do d3=1,6
              do d4=1,6
                dddd_index=dddd_index+1
                if(d1.gt.3) Pos_Int(dddd_index) = sq3*Pos_Int(dddd_index)
                if(d2.gt.3) Pos_Int(dddd_index) = sq3*Pos_Int(dddd_index)
                if(d3.gt.3) Pos_Int(dddd_index) = sq3*Pos_Int(dddd_index)
                if(d4.gt.3) Pos_Int(dddd_index) = sq3*Pos_Int(dddd_index)
              end do
            end do
          end do
        end do
!
! multiply through prefactor
!        Pos_Int(1:1296) = prefactor*Pos_Int(1:1296)
!
        return
      end if
!
!--------------------------------------------------------------------
! Shouldn't be able to get here, print out an error message
!--------------------------------------------------------------------
!
        write (*,*) 'Orbital angular momentum not recognised by subroutine PositionIntegral'
        write (*,*) 'Please check that your basis set contains only s and p functions'
!
      end subroutine PositionIntegral
