!*MODULE EXTRACULE  *DECK PositionIntegral
      subroutine PositionIntegral(Av,Bv,Cv,Dv,a,b,c,d,type_a,type_b,type_c,type_d,Rx,Ry,Rz,Pos_int)
! 
!   *****************************************************************
!   *                                                               *
!   *  Calculates Vectorized Position Extracule integrals usig RR   *
!   *                                                               *
!   *       --------------------- OUTPUT ---------------------      *
!   *       Pos_int = Position integrals                            *
!   *       --------------------------------------------------      *
!   *                                                               * 
!   *       --------------------- INPUT ----------------------      *
!   *       Av = cartesian coordinates of centre A                  *
!   *       Bv = cartesian coordinates of centre B                  *
!   *       Cv = cartesian coordinates of centre C                  *
!   *       Dv = cartesian coordinates of centre D                  *
!   *       a = exponent alpha (associated with centre A)           *
!   *       b = exponent beta (associated with centre B)            *
!   *       c = exponent gamma (associated with centre C)           *
!   *       d = exponent delta (associated with centre D)           *
!   *       A_ang = angular momentum of basis function on A         *
!   *       B_ang = angular momentum of basis function on B         *
!   *       C_ang = angular momentum of basis function on C         *
!   *       D_ang = angular momentum of basis function on D         * 
!   *       u = value of u (argument to Position intracule)         *
!   *       --------------------------------------------------      *
!   *                                                               *
!   *  For details of the equations contained in this section of    *
!   *  code, see Thakkar and Moore, Int. J. Quantum Chem., 1981     *
!   *                                                               *
!   *  code written by AJP (2014)                                   *
!   *                                                               *
!   *****************************************************************
!
!---------------------------------------------------------------------
! Input/Output:
!---------------------------------------------------------------------
      implicit none
      integer :: type_a, type_b, type_c, type_d ! verbose
      integer :: ang_ax, ang_ay, ang_az
      integer :: ang_bx, ang_by, ang_bz
      integer :: ang_cx, ang_cy, ang_cz
      integer :: ang_dx, ang_dy, ang_dz
      double precision, dimension(3) :: Av, Bv, Cv, Dv
      double precision :: a, b, c, d
      double precision, dimension(1296) :: Pos_int 
!
!--------------------------------------------------------------------
! Local: 
!--------------------------------------------------------------------
!
      double precision, dimension(3) :: Uv, Qv, Pv, Ts
      double precision :: prefac, Sab, U_length
      integer :: Lcode, i, j, k, n, l, m, L_total, a_i, b_i, c_i, d_i
      integer :: ang_a, ang_b, ang_c, ang_d, count
      double precision :: pi, toler, gam1, gam2, nu, tau, delta
      double precision :: lambda, sk
      double precision :: Rx, Ry, Rz
!
!--------------------------------------------------------------------
! Begin: 
!--------------------------------------------------------------------
!
      pi = 3.14159265358979323846264338327950288419716939937510d0

!--------------------------------------------------------------------
!  Calculate common intermediates, prefactor
!--------------------------------------------------------------------
!
      U_length = 0.0D0
      do i=1,3 
        Uv(i) = (a*Av(i)+b*Bv(i))/(a+b) + (c*Cv(i)+d*Dv(i))/(c+d)
        U_length = U_length + Uv(i)**2
      enddo
      U_length = dsqrt(U_length)

      do l=1,3
        Qv(l) = (c*Cv(l)+d*Dv(l))/(c+d)
      enddo

      do l=1,3
        Pv(l) = (a*Av(l)+b*Bv(l))/(a+b)
      enddo
!
      gam1 = a + b
      gam2 = c + d
      tau = gam1 + gam2
      delta = tau/(4.0D0*gam1*gam2)
      nu = (a+b)*(c+d)/(a+b+c+d)
!
      Sab = 0.0D0
! calculate exponent first
      do i=1,3
        Sab = Sab - a*b*(Av(i)-Bv(i))**2/gam1 - c*d*(Cv(i)-Dv(i))**2/gam2
      end do

      prefac=(4.0D0*pi/tau)**1.5D0*EXP(-nu*(4.0D0*(Rx**2+Ry**2+Rz**2)+U_length**2)+Sab+4.0D0*nu*(Uv(1)*Rx+Uv(2)*Ry+Uv(3)*Rz))

      if (type_a == 1) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 2) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 3) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 4) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "The orbital type for primitive A is not valid (i.e. > 10 or < 0)"
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 1) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      elseif (type_b == 5) then
        ang_bx = 2
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 6) then
        ang_bx = 0
        ang_by = 2
        ang_bz = 0
      elseif (type_b == 7) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 2
      elseif (type_b == 8) then
        ang_bx = 1
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 9) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 1
      elseif (type_b == 10) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 1
      else
        print *, "The orbital type for primitive B is not valid (i.e. > 10 or < 0)"
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      elseif (type_c == 5) then
        ang_cx = 2
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 6) then
        ang_cx = 0
        ang_cy = 2
        ang_cz = 0
      elseif (type_c == 7) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 2
      elseif (type_c == 8) then
        ang_cx = 1
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 9) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 1
      elseif (type_c == 10) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 1
      else
        print *, "The orbital type for primitive C is not valid (i.e. > 10 or < 0)"
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      elseif (type_d == 5) then
        ang_dx = 2
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 6) then
        ang_dx = 0
        ang_dy = 2
        ang_dz = 0
      elseif (type_d == 7) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 2
      elseif (type_d == 8) then
        ang_dx = 1
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 9) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 1
      elseif (type_d == 10) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 1
      else
        print *, "The orbital type for primitive D is not valid (i.e. > 10 or < 0)"
      endif
      ang_d = ang_dx + ang_dy + ang_dz

!      
!-------------------------------------------------------------------
! Calculate Lcode and L_total values
!-------------------------------------------------------------------
!
! Calculate total angular momentum, L_total
      Lcode = 1000*ang_a + 100*ang_b + 10*ang_c + 1*ang_d
!
!------------------------------------------------------------------
! [ssss]
!------------------------------------------------------------------
      if(Lcode.eq.0)then
!
        Pos_Int(1) = prefac

	!write (8,*) "I am an ssss"
!
        return
      endif
!
!------------------------------------------------------------------
! [psss]
!------------------------------------------------------------------
      if(Lcode.eq.1000)then
!
! Requires one step
!
! Step 1 :
! [1000]^(0) from [0000]^(1)
!

	count = 0

	type_a = 2
	type_b = 1
	type_c = 1
	type_d = 1
      do i=1,3 ! x,y,z

      if (type_a == 2) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 3) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 4) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 1
      else
        print *, "psss. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 1) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 0
      else
        print *, "psss. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "psss. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "psss. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
	   count = count + 1
	   Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
	   type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [ppss]
!------------------------------------------------------------------
      if(Lcode.eq.1100)then
!
! Requires 2 steps
!
        count = 0
        Ts = 0.0D0

        type_a = 2
        type_c=1
        type_d=1

        do i=1,3 ! x,y,z
	type_b = 2
	do j=1,3 ! x,y,z

      if (type_a == 2) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 3) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 4) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 1
      else
        print *, "ppss. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "ppss. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "ppss. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "ppss. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
	   count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
	   type_b = type_b + 1
	enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [psps]
!------------------------------------------------------------------
      if(Lcode.eq.1010)then

! Requires 2 steps

	count = 0
        Ts = 0.0D0

        type_a = 2
        type_b = 1
        type_d = 1

        do i=1,3 ! x,y,z
        type_c = 2
        do j=1,3 ! x,y,z

      if (type_a == 2) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 3) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 4) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 1
      else
        print *, "psps. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 1) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 0
      else
        print *, "psps. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "ppss. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "ppss. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_c = type_c + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif

!------------------------------------------------------------------
! [ppps]
!------------------------------------------------------------------
      if(Lcode.eq.1110)then
!
! Requires 4 steps
!
!

        count = 0
        Ts = 0.0D0

        type_a = 2
        type_d = 1

        do i=1,3 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
	type_c = 2
	do k=1,3 ! x,y,z

      if (type_a == 2) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 3) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 4) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 1
      else
        print *, "ppps. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "ppps. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "ppps. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "ppps. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
	   type_c = type_c + 1
	enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [ppsp]
!------------------------------------------------------------------
      if(Lcode.eq.1101)then
!
! Requires 4 steps
!
!
	print *, "Shouldn't reach this class for position space"
        count = 0
        Ts = 0.0D0

        type_a = 2
        type_c = 1

        do i=1,3 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
        type_d = 2
        do k=1,3 ! x,y,z

      if (type_a == 2) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 3) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 4) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 1
      else
        print *, "ppsp. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "ppsp. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "ppsp. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      else
        print *, "ppsp. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo
	
        return
      endif
!
!------------------------------------------------------------------
! [pppp]
!------------------------------------------------------------------
      if(Lcode.eq.1111)then
!
! Requires 7 steps
!
!

        count = 0
        Ts = 0.0D0

        type_a = 2
        do i=1,3 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
        type_c = 2
        do k=1,3 ! x,y,z
	type_d = 2
	do n=1,3

      if (type_a == 2) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 3) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 4) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 1
      else
        print *, "pppp. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "pppp. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "pppp. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      else
        print *, "pppp. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
	   type_d = type_d + 1
	enddo
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dsss]
! Note: d integrals required some modification of code notation
!------------------------------------------------------------------
      if(Lcode.eq.2000)then
!
! Requires 2 steps
!
!

        count = 0
        Ts = 0.0D0

        type_a = 5
        type_b = 1
        type_c = 1
        type_d = 1

        do i=1,6 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dsss. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 1) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 0
      else
        print *, "dsss. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "dsss. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "dsss. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dpss]
!------------------------------------------------------------------
      if(Lcode.eq.2100)then
!
! Requires 3 steps
!

        count = 0
        Ts = 0.0D0

        type_a = 5
        type_c = 1
        type_d = 1

        do i=1,6 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dpss. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "dpss. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "dpss. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "dpss. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dsps]
!------------------------------------------------------------------
      if(Lcode.eq.2010)then
!
! Requires 3 steps
!
!
        count = 0
        Ts = 0.0D0

        type_a = 5
        type_b = 1
        type_d = 1

        do i=1,6 ! x,y,z
        type_c = 2
        do j=1,3 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dsps. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 1) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 0
      else
        print *, "dsps. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "dsps. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "dsps. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_c = type_c + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dssp]
!------------------------------------------------------------------
      if(Lcode.eq.2001)then
!
! Requires 3 steps
!
	print *, "Shouldn't have entered this class for position space"
        count = 0
        Ts = 0.0D0

        type_a = 5
        type_b = 1
        type_c = 1

        do i=1,6 ! x,y,z
        type_d = 2
        do j=1,3 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dssp. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 1) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 0
      else
        print *, "dssp. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "dssp. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      else
        print *, "dssp. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d +1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [ddss]
!------------------------------------------------------------------
      if(Lcode.eq.2200)then
!
! Requires 5 steps
!
        count = 0
        Ts = 0.0D0

        type_a = 5
        type_c = 1
        type_d = 1

        do i=1,6 ! x,y,z
        type_b = 5
        do j=1,6 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "ddss. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 5) then
        ang_bx = 2
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 6) then
        ang_bx = 0
        ang_by = 2
        ang_bz = 0
      elseif (type_b == 7) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 2
      elseif (type_b == 8) then
        ang_bx = 1
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 9) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 1
      elseif (type_b == 10) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 1
      else
        print *, "ddss. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "ddss. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "ddss. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dsds]
!------------------------------------------------------------------
      if(Lcode.eq.2020)then
!
! Requires 5 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        type_b = 1
        type_d = 1

        do i=1,6 ! x,y,z
        type_c = 5
        do j=1,6 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dsds. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 1) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 0
      else
        print *, "dsds. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 5) then
        ang_cx = 2
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 6) then
        ang_cx = 0
        ang_cy = 2
        ang_cz = 0
      elseif (type_c == 7) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 2
      elseif (type_c == 8) then
        ang_cx = 1
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 9) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 1
      elseif (type_c == 10) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 1
      else
        print *, "dsds. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "dsds. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_c = type_c + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dssd]
!------------------------------------------------------------------
      if(Lcode.eq.2002)then
!
! Requires 5 steps

	print *, "Shouldn't have entered this class for position space"
        count = 0
        Ts = 0.0D0

        type_a = 5
        type_b = 1
        type_c = 1

        do i=1,6 ! x,y,z
        type_d = 5
        do j=1,6

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dssd. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 1) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 0
      else
        print *, "dssd. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "dssd. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 5) then
        ang_dx = 2
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 6) then
        ang_dx = 0
        ang_dy = 2
        ang_dz = 0
      elseif (type_d == 7) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 2
      elseif (type_d == 8) then
        ang_dx = 1
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 9) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 1
      elseif (type_d == 10) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 1
      else
        print *, "dssd. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d +1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dpps]
!------------------------------------------------------------------
      if(Lcode.eq.2110)then
!
! Requires 5 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        type_d = 1

        do i=1,6 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
        type_c = 2
        do k=1,3 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dpps. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "dpps. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "dpps. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "dpps. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dpsp]
!------------------------------------------------------------------
      if(Lcode.eq.2101)then
!
! Requires 5 steps

	print *, "Shouldn't have entered this class for position space"
        count = 0
        Ts = 0.0D0

        type_a = 5
        type_c = 1

        do i=1,6 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
        type_d = 2
        do k=1,3

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dpsp. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "dpsp. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "dpsp. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      else
        print *, "dpsp. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dspp]
!------------------------------------------------------------------
      if(Lcode.eq.2011)then
!
! Requires 5 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        type_b = 1

        do i=1,6 ! x,y,z
        type_c = 2
        do j=1,3 ! x,y,z
        type_d = 2
        do k=1,3

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dspp. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 1) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 0
      else
        print *, "dspp. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "dspp. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      else
        print *, "dspp. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_c = type_c + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [ddps]
!------------------------------------------------------------------
      if(Lcode.eq.2210)then
!
! Requires 8 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        type_d = 1

        do i=1,6 ! x,y,z
        type_b = 5
        do j=1,6 ! x,y,z
        type_c = 2
        do k=1,3 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "ddps. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 5) then
        ang_bx = 2
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 6) then
        ang_bx = 0
        ang_by = 2
        ang_bz = 0
      elseif (type_b == 7) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 2
      elseif (type_b == 8) then
        ang_bx = 1
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 9) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 1
      elseif (type_b == 10) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 1
      else
        print *, "ddps. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "ddps. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "ddps. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dpds]
!------------------------------------------------------------------
      if(Lcode.eq.2120)then
!
! Requires 8 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        type_d = 1

        do i=1,6 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
        type_c = 5
        do k=1,6 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dpds. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "dpds. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 5) then
        ang_cx = 2
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 6) then
        ang_cx = 0
        ang_cy = 2
        ang_cz = 0
      elseif (type_c == 7) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 2
      elseif (type_c == 8) then
        ang_cx = 1
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 9) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 1
      elseif (type_c == 10) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 1
      else
        print *, "dpds. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "dpds. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dpsd]
!------------------------------------------------------------------
      if(Lcode.eq.2102)then
!
! Requires 8 steps

	print *, "Shouldn't have entered this class for position space"
        count = 0
        Ts = 0.0D0

        type_a = 5
        type_c = 1

        do i=1,6 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
        type_d = 5
        do k=1,6 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dpsd. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "dpsd. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 1) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 0
      else
        print *, "dpsd. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 5) then
        ang_dx = 2
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 6) then
        ang_dx = 0
        ang_dy = 2
        ang_dz = 0
      elseif (type_d == 7) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 2
      elseif (type_d == 8) then
        ang_dx = 1
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 9) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 1
      elseif (type_d == 10) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 1
      else
        print *, "dpsd. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

	return
      endif
!
!------------------------------------------------------------------
! [ddds]
!------------------------------------------------------------------
      if(Lcode.eq.2220)then
!
! Requires 12 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        type_d = 1

        do i=1,6 ! x,y,z
        type_b = 5
        do j=1,6 ! x,y,z
        type_c = 5
        do k=1,6 ! x,y,z

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "ddds. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 5) then
        ang_bx = 2
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 6) then
        ang_bx = 0
        ang_by = 2
        ang_bz = 0
      elseif (type_b == 7) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 2
      elseif (type_b == 8) then
        ang_bx = 1
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 9) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 1
      elseif (type_b == 10) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 1
      else
        print *, "ddds. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 5) then
        ang_cx = 2
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 6) then
        ang_cx = 0
        ang_cy = 2
        ang_cz = 0
      elseif (type_c == 7) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 2
      elseif (type_c == 8) then
        ang_cx = 1
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 9) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 1
      elseif (type_c == 10) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 1
      else
        print *, "ddds. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 1) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 0
      else
        print *, "ddds. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dppp]
!------------------------------------------------------------------
      if(Lcode.eq.2111)then
!
! Requires 9 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        do i=1,6 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
        type_c = 2
        do k=1,3 ! x,y,z
        type_d = 2
        do n=1,3

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dppp. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "dppp. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "dppp. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      else
        print *, "dppp. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [ddpp]
!------------------------------------------------------------------
      if(Lcode.eq.2211)then
!
! Requires 12 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        do i=1,6 ! x,y,z
        type_b = 5
        do j=1,6 ! x,y,z
        type_c = 2
        do k=1,3 ! x,y,z
        type_d = 2
        do n=1,3

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "ddpp. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 5) then
        ang_bx = 2
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 6) then
        ang_bx = 0
        ang_by = 2
        ang_bz = 0
      elseif (type_b == 7) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 2
      elseif (type_b == 8) then
        ang_bx = 1
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 9) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 1
      elseif (type_b == 10) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 1
      else
        print *, "ddpp. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "ddpp. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      else
        print *, "ddpp. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dpdp]
!------------------------------------------------------------------
      if(Lcode.eq.2121)then
!
! Requires 12 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        do i=1,6 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
        type_c = 5
        do k=1,6 ! x,y,z
        type_d = 2
        do n=1,3

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dpdp. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "dpdp. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 5) then
        ang_cx = 2
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 6) then
        ang_cx = 0
        ang_cy = 2
        ang_cz = 0
      elseif (type_c == 7) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 2
      elseif (type_c == 8) then
        ang_cx = 1
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 9) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 1
      elseif (type_c == 10) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 1
      else
        print *, "dpdp. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      else
        print *, "dpdp. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dppd]
!------------------------------------------------------------------
      if(Lcode.eq.2112)then
!
! Requires 12 steps

	print *, "Shouldn't have entered this clas for position space"
        count = 0
        Ts = 0.0D0

        type_a = 5
        do i=1,6 ! x,y,z
        type_b = 2
        do j=1,3 ! x,y,z
        type_c = 2
        do k=1,3 ! x,y,z
        type_d = 5
        do n=1,6

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dppd. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 2) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 3) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 4) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 1
      else
        print *, "dppd. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 2) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 3) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 4) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 1
      else
        print *, "dppd. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 5) then
        ang_dx = 2
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 6) then
        ang_dx = 0
        ang_dy = 2
        ang_dz = 0
      elseif (type_d == 7) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 2
      elseif (type_d == 8) then
        ang_dx = 1
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 9) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 1
      elseif (type_d == 10) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 1
      else
        print *, "dppd. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

	return
      endif
!
!------------------------------------------------------------------
! [dddp]
!------------------------------------------------------------------
      if(Lcode.eq.2221)then
!
! Requires 18 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        do i=1,6 ! x,y,z
        type_b = 5
        do j=1,6 ! x,y,z
        type_c = 5
        do k=1,6 ! x,y,z
        type_d = 2
        do n=1,3

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dddp. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 5) then
        ang_bx = 2
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 6) then
        ang_bx = 0
        ang_by = 2
        ang_bz = 0
      elseif (type_b == 7) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 2
      elseif (type_b == 8) then
        ang_bx = 1
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 9) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 1
      elseif (type_b == 10) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 1
      else
        print *, "dddp. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 5) then
        ang_cx = 2
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 6) then
        ang_cx = 0
        ang_cy = 2
        ang_cz = 0
      elseif (type_c == 7) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 2
      elseif (type_c == 8) then
        ang_cx = 1
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 9) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 1
      elseif (type_c == 10) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 1
      else
        print *, "dddp. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 2) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 3) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 4) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 1
      else
        print *, "dddp. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!------------------------------------------------------------------
! [dddd]
!------------------------------------------------------------------
      if(Lcode.eq.2222)then
!
! Requires 27 steps

        count = 0
        Ts = 0.0D0

        type_a = 5
        do i=1,6 ! x,y,z
        type_b = 5
        do j=1,6 ! x,y,z
        type_c = 5
        do k=1,6 ! x,y,z
        type_d = 5
        do n=1,6

      if (type_a == 5) then
        ang_ax = 2
        ang_ay = 0
        ang_az = 0
      elseif (type_a == 6) then
        ang_ax = 0
        ang_ay = 2
        ang_az = 0
      elseif (type_a == 7) then
        ang_ax = 0
        ang_ay = 0
        ang_az = 2
      elseif (type_a == 8) then
        ang_ax = 1
        ang_ay = 1
        ang_az = 0
      elseif (type_a == 9) then
        ang_ax = 1
        ang_ay = 0
        ang_az = 1
      elseif (type_a == 10) then
        ang_ax = 0
        ang_ay = 1
        ang_az = 1
      else
        print *, "dddd. A. You messed up somewhere. Try again, boss."
      endif
      ang_a = ang_ax + ang_ay + ang_az

      if (type_b == 5) then
        ang_bx = 2
        ang_by = 0
        ang_bz = 0
      elseif (type_b == 6) then
        ang_bx = 0
        ang_by = 2
        ang_bz = 0
      elseif (type_b == 7) then
        ang_bx = 0
        ang_by = 0
        ang_bz = 2
      elseif (type_b == 8) then
        ang_bx = 1
        ang_by = 1
        ang_bz = 0
      elseif (type_b == 9) then
        ang_bx = 1
        ang_by = 0
        ang_bz = 1
      elseif (type_b == 10) then
        ang_bx = 0
        ang_by = 1
        ang_bz = 1
      else
        print *, "dddd. B. You messed up somewhere. Try again, boss."
      endif
      ang_b = ang_bx + ang_by + ang_bz

      if (type_c == 5) then
        ang_cx = 2
        ang_cy = 0
        ang_cz = 0
      elseif (type_c == 6) then
        ang_cx = 0
        ang_cy = 2
        ang_cz = 0
      elseif (type_c == 7) then
        ang_cx = 0
        ang_cy = 0
        ang_cz = 2
      elseif (type_c == 8) then
        ang_cx = 1
        ang_cy = 1
        ang_cz = 0
      elseif (type_c == 9) then
        ang_cx = 1
        ang_cy = 0
        ang_cz = 1
      elseif (type_c == 10) then
        ang_cx = 0
        ang_cy = 1
        ang_cz = 1
      else
        print *, "dddd. C. You messed up somewhere. Try again, boss."
      endif
      ang_c = ang_cx + ang_cy + ang_cz

      if (type_d == 5) then
        ang_dx = 2
        ang_dy = 0
        ang_dz = 0
      elseif (type_d == 6) then
        ang_dx = 0
        ang_dy = 2
        ang_dz = 0
      elseif (type_d == 7) then
        ang_dx = 0
        ang_dy = 0
        ang_dz = 2
      elseif (type_d == 8) then
        ang_dx = 1
        ang_dy = 1
        ang_dz = 0
      elseif (type_d == 9) then
        ang_dx = 1
        ang_dy = 0
        ang_dz = 1
      elseif (type_d == 10) then
        ang_dx = 0
        ang_dy = 1
        ang_dz = 1
      else
        print *, "dddd. D. You messed up somewhere. Try again, boss."
      endif
      ang_d = ang_dx + ang_dy + ang_dz

      Ts(1) = 0.0D0
      Ts(2) = 0.0D0
      Ts(3) = 0.0D0

           do l = 0, ang_ax + ang_bx
              do m = 0, ang_cx + ang_dx
                Ts(1) = Ts(1) + lambda(l,ang_ax,ang_bx,Pv(1)-Av(1),Pv(1)-Bv(1),gam1)*sk(l+m,Pv(1),Qv(1),Rx,delta)*&
                  lambda(m,ang_cx,ang_dx,Qv(1)-Cv(1),Qv(1)-Dv(1),gam2)
              enddo
           enddo
           do l = 0, ang_ay + ang_by
              do m = 0, ang_cy + ang_dy
                Ts(2) = Ts(2) + lambda(l,ang_ay,ang_by,Pv(2)-Av(2),Pv(2)-Bv(2),gam1)*sk(l+m,Pv(2),Qv(2),Ry,delta)*&
                  lambda(m,ang_cy,ang_dy,Qv(2)-Cv(2),Qv(2)-Dv(2),gam2)
              enddo
           enddo
           do l = 0, ang_az + ang_bz
              do m = 0, ang_cz + ang_dz
                Ts(3) = Ts(3) + lambda(l,ang_az,ang_bz,Pv(3)-Av(3),Pv(3)-Bv(3),gam1)*sk(l+m,Pv(3),Qv(3),Rz,delta)*&
                  lambda(m,ang_cz,ang_dz,Qv(3)-Cv(3),Qv(3)-Dv(3),gam2)
              enddo
           enddo
           count = count + 1
           Pos_Int(count) = prefac*Ts(1)*Ts(2)*Ts(3)
	   !write (8,*) count, prefac, Pos_Int(count)
           type_d = type_d + 1
        enddo
           type_c = type_c + 1
        enddo
           type_b = type_b + 1
        enddo
           type_a = type_a + 1
        enddo

        return
      endif
!
!--------------------------------------------------------------------
! Shouldn't be able to get here, print out an error message
!--------------------------------------------------------------------
!
        write (*,*) 'Orbital angular momentum not recognised by subroutine PositionIntegral'
        write (*,*) 'Please check that your basis set contains only s and p functions'


      end subroutine PositionIntegral


! This function calculates the factorial of an integer, n
        recursive function factorial(n) result(res)
                integer res, n
                if(n == 0) then
                        res=1
                else
                        res = n*factorial(n-1)
                endif
        end

! This function calculates the sk multiplier from the angular components (Tx, Ty, and Tz) of the integrals
        double precision function sk(k,a,b,c,x)

        implicit none
        integer k, w, j, factorial
        double precision a, b, c, x

        sk = 0.0D0
        do j = 0, k/2
                sk = sk + (-x)**j/factorial(j)*(a+b-2*c)**(k-2*j)/factorial(k-2*j)
        enddo
        sk = sk*factorial(k)/(-x)**k

        return
        end

! This function calculates the lambda portion from the angular comopnents (Tx, Ty, and Tz) of the integrals
        double precision function lambda(j,l,m,a,b,gam)

        implicit none
        integer j, l, m, i, factorial
        double precision a, b, gam
        double precision, dimension(0:4,0:2,0:2) :: fs

!
        fs = 0.0D0
        fs(0,0,0) = 1
        fs(0,1,0) = a
        fs(1,1,0) = 1
        fs(0,0,1) = b
        fs(1,0,1) = 1
        fs(0,1,1) = a*b
        fs(1,1,1) = a + b
        fs(2,1,1) = 1
        fs(0,2,0) = a**2
        fs(1,2,0) = 2*a
        fs(2,2,0) = 1
        fs(0,0,2) = b**2
        fs(1,0,2) = 2*b
        fs(2,0,2) = 1
        fs(0,2,1) = a**2*b
        fs(1,2,1) = a**2 + 2*a*b
        fs(2,2,1) = 2*a + b
        fs(3,2,1) = 1
        fs(0,1,2) = b**2*a
        fs(1,1,2) = b**2 + 2*a*b
        fs(2,1,2) = 2*b + a
        fs(3,1,2) = 1
        fs(0,2,2) = a**2*b**2
        fs(1,2,2) = 2*a**2*b + 2*a*b**2
        fs(2,2,2) = a**2 + 4*a*b + b**2
        fs(3,2,2) = 2*a + 2*b
        fs(4,2,2) = 1

        lambda = 0.0D0
        do i = 0, (l+m-j)/2
          lambda = lambda + (fs(2*i+j,l,m)*(factorial(2*i+j)))/(factorial(i)*factorial(j)*(4*gam)**(i+j))
        enddo

        return
        end
