BASIS="Hay-Wadt VDZ (n+1) ECP"
 K   0
 S   3  1.00
       3.07200000        -0.308306700    
      0.675200000         0.782071100    
      0.254500000         0.414288300    
 S   4  1.00
       3.07200000         0.189408700    
      0.675200000        -0.547233200    
      0.254500000        -0.757645800    
      0.529000000E-01     0.995215700    
 S   1  1.00
      0.209000000E-01      1.00000000    
 P   3  1.00
       8.23300000        -0.419919000E-01
      0.952600000         0.577681900    
      0.301300000         0.523461100    
 P   1  1.00
      0.376000000E-01      1.00000000    
 P   1  1.00
      0.140000000E-01      1.00000000    
 ZZZZ
 CA  0
 S   3  1.00
       3.48400000        -0.333737000    
      0.855100000         0.758983100    
      0.319200000         0.460610900    
 S   4  1.00
       3.48400000         0.260603000    
      0.855100000        -0.683915000    
      0.319200000         -1.19878070    
      0.144700000          1.02512210    
 S   1  1.00
      0.350000000E-01      1.00000000    
 P   3  1.00
       9.42000000        -0.436943000E-01
       1.15000000         0.603474700    
      0.368800000         0.496110700    
 P   1  1.00
      0.705000000E-01      1.00000000    
 P   1  1.00
      0.263000000E-01      1.00000000    
 ZZZZ
 SC  0
 S   3  1.00
       3.71700000        -0.392629200    
       1.09700000         0.714733900    
      0.416400000         0.550940000    
 S   4  1.00
       3.71700000         0.219744600    
       1.09700000        -0.454606700    
      0.416400000        -0.693818100    
      0.761000000E-01      1.12601720    
 S   1  1.00
      0.284000000E-01      1.00000000    
 P   3  1.00
       10.4000000        -0.492615000E-01
       1.31100000         0.607196500    
      0.426600000         0.491758200    
 P   1  1.00
      0.470000000E-01      1.00000000    
 P   1  1.00
      0.140000000E-01      1.00000000    
 D   4  1.00
       15.1300000         0.379291000E-01
       4.20500000         0.173836000    
       1.30300000         0.426805700    
      0.368000000         0.623853900    
 D   1  1.00
      0.812000000E-01      1.00000000    
 ZZZZ
 TI  0
 S   3  1.00
       4.37200000        -0.363709800    
       1.09800000         0.818453300    
      0.417800000         0.418452600    
 S   4  1.00
       4.37200000         0.204902700    
       1.09800000        -0.557541300    
      0.417800000        -0.589365200    
      0.872000000E-01      1.14516610    
 S   1  1.00
      0.314000000E-01      1.00000000    
 P   3  1.00
       12.5200000        -0.456908000E-01
       1.49100000         0.620331300    
      0.485900000         0.476532900    
 P   1  1.00
      0.530000000E-01      1.00000000    
 P   1  1.00
      0.160000000E-01      1.00000000    
 D   4  1.00
       20.2100000         0.341682000E-01
       5.49500000         0.171000600    
       1.69900000         0.440584900    
      0.484000000         0.611424600    
 D   1  1.00
      0.115700000          1.00000000    
 ZZZZ
 V   0
 S   3  1.00
       4.59000000        -0.427730200    
       1.49300000         0.706969100    
      0.557000000         0.589581300    
 S   4  1.00
       4.59000000         0.250108100    
       1.49300000        -0.469777700    
      0.557000000        -0.712941500    
      0.975000000E-01      1.10065690    
 S   1  1.00
      0.342000000E-01      1.00000000    
 P   3  1.00
       13.7600000        -0.482312000E-01
       1.71200000         0.614116100    
      0.558700000         0.483834200    
 P   1  1.00
      0.590000000E-01      1.00000000    
 P   1  1.00
      0.180000000E-01      1.00000000    
 D   4  1.00
       25.7000000         0.331033000E-01
       6.53000000         0.179575300    
       2.07800000         0.437306200    
      0.624300000         0.598486000    
 D   1  1.00
      0.154200000          1.00000000    
 ZZZZ
 CR  0
 S   3  1.00
       5.36100000        -0.380568900    
       1.44900000         0.779562500    
      0.549600000         0.473077700    
 S   4  1.00
       5.36100000         0.220311100    
       1.44900000        -0.542339100    
      0.549600000        -0.604771600    
      0.105200000          1.11366570    
 S   1  1.00
      0.364000000E-01      1.00000000    
 P   3  1.00
       16.4200000        -0.461397000E-01
       1.91400000         0.610996500    
      0.624100000         0.485963500    
 P   1  1.00
      0.630000000E-01      1.00000000    
 P   1  1.00
      0.190000000E-01      1.00000000    
 D   4  1.00
       28.9500000         0.337787000E-01
       7.70800000         0.178034500    
       2.49500000         0.437000800    
      0.765500000         0.594179500    
 D   1  1.00
      0.188900000          1.00000000    
 ZZZZ
 MN  0
 S   3  1.00
       5.91400000        -0.376450800    
       1.60500000         0.772478900    
      0.626000000         0.476934600    
 S   4  1.00
       5.91400000         0.211996600    
       1.60500000        -0.519947200    
      0.626000000        -0.585768100    
      0.111500000          1.10039640    
 S   1  1.00
      0.380000000E-01      1.00000000    
 P   3  1.00
       18.2000000        -0.447901000E-01
       2.14100000         0.626031100    
      0.700900000         0.469632900    
 P   1  1.00
      0.690000000E-01      1.00000000    
 P   1  1.00
      0.210000000E-01      1.00000000    
 D   4  1.00
       32.2700000         0.341580000E-01
       8.87500000         0.176110500    
       2.89000000         0.439429800    
      0.876100000         0.594327100    
 D   1  1.00
      0.212000000          1.00000000    
 ZZZZ
 FE  0
 S   3  1.00
       6.42200000        -0.392788200    
       1.82600000         0.771264300    
      0.713500000         0.492022800    
 S   4  1.00
       6.42200000         0.178687700    
       1.82600000        -0.419403200    
      0.713500000        -0.456818500    
      0.102100000          1.10350480    
 S   1  1.00
      0.363000000E-01      1.00000000    
 P   3  1.00
       19.4800000        -0.470282000E-01
       2.38900000         0.624884100    
      0.779500000         0.472254200    
 P   1  1.00
      0.740000000E-01      1.00000000    
 P   1  1.00
      0.220000000E-01      1.00000000    
 D   4  1.00
       37.0800000         0.329000000E-01
       10.1000000         0.178741800    
       3.22000000         0.448765700    
      0.962800000         0.587636100    
 D   1  1.00
      0.226200000          1.00000000    
 ZZZZ
 CO  0
 S   3  1.00
       7.17600000        -0.385673400    
       2.00900000         0.745311600    
      0.805500000         0.509181900    
 S   4  1.00
       7.17600000         0.173616100    
       2.00900000        -0.397044200    
      0.805500000        -0.463062200    
      0.107000000          1.08996540    
 S   1  1.00
      0.375000000E-01      1.00000000    
 P   3  1.00
       21.3900000        -0.480413000E-01
       2.65000000         0.622233700    
      0.861900000         0.475804200    
 P   1  1.00
      0.800000000E-01      1.00000000    
 P   1  1.00
      0.230000000E-01      1.00000000    
 D   4  1.00
       39.2500000         0.361541000E-01
       10.7800000         0.189674400    
       3.49600000         0.452498100    
       1.06600000         0.571042700    
 D   1  1.00
      0.260600000          1.00000000    
 ZZZZ
 NI  0
 S   3  1.00
       7.62000000        -0.408255000    
       2.29400000         0.745530800    
      0.876000000         0.532572100    
 S   4  1.00
       7.62000000         0.187259100    
       2.29400000        -0.396696400    
      0.876000000        -0.495400300    
      0.115300000          1.08443430    
 S   1  1.00
      0.396000000E-01      1.00000000    
 P   3  1.00
       23.6600000        -0.481558000E-01
       2.89300000         0.625847300    
      0.943500000         0.471515800    
 P   1  1.00
      0.840000000E-01      1.00000000    
 P   1  1.00
      0.240000000E-01      1.00000000    
 D   4  1.00
       42.7200000         0.372699000E-01
       11.7600000         0.195610300    
       3.81700000         0.456127300    
       1.16900000         0.562158700    
 D   1  1.00
      0.283600000          1.00000000    
 ZZZZ
 CU  0
 S   3  1.00
       8.17600000        -0.421026000    
       2.56800000         0.738592400    
      0.958700000         0.552569200    
 S   4  1.00
       8.17600000         0.178766500    
       2.56800000        -0.359227300    
      0.958700000        -0.470482500    
      0.115300000          1.08074070    
 S   1  1.00
      0.396000000E-01      1.00000000    
 P   3  1.00
       25.6300000        -0.489173000E-01
       3.16600000         0.627285400    
       1.02300000         0.471618800    
 P   1  1.00
      0.860000000E-01      1.00000000    
 P   1  1.00
      0.240000000E-01      1.00000000    
 D   4  1.00
       41.3400000         0.465424000E-01
       11.4200000         0.222782400    
       3.83900000         0.453905900    
       1.23000000         0.531476900    
 D   1  1.00
      0.310200000          1.00000000    
 ZZZZ
 RB  0
 S   3  1.00
       1.45700000         -1.06174060    
      0.940000000          1.17221250    
      0.270600000         0.743700800    
 S   4  1.00
       1.45700000         0.505217400    
      0.940000000        -0.571840400    
      0.270600000        -0.683530800    
      0.366000000E-01      1.11364770    
 S   1  1.00
      0.155000000E-01      1.00000000    
 P   3  1.00
       3.30000000        -0.729417000E-01
      0.598500000         0.632178200    
      0.206700000         0.470742600    
 P   2  1.00
      0.194700000        -0.125055200    
      0.318000000E-01      1.04382060    
 P   1  1.00
      0.124000000E-01      1.00000000    
 ZZZZ
 SR  0
 S   3  1.00
       1.64800000        -0.951987900    
       1.00300000          1.08698570    
      0.310600000         0.720316000    
 S   4  1.00
       1.64800000         0.791433700    
       1.00300000        -0.943404600    
      0.310600000         -1.30430630    
      0.109900000         0.904686900    
 S   1  1.00
      0.292000000E-01      1.00000000    
 P   3  1.00
       3.55200000        -0.886461000E-01
      0.697500000         0.666185700    
      0.248000000         0.439070500    
 P   2  1.00
      0.273500000        -0.178747000    
      0.570000000E-01      1.07661340    
 P   1  1.00
      0.222000000E-01      1.00000000    
 ZZZZ
 Y   0
 S   3  1.00
       1.75100000         -1.16179050    
       1.14300000          1.29588770    
      0.358100000         0.711512500    
 S   4  1.00
       1.75100000         0.968312300    
       1.14300000         -1.13463670    
      0.358100000         -1.19140540    
      0.105800000         0.928106300    
 S   1  1.00
      0.318000000E-01      1.00000000    
 P   3  1.00
       3.88400000        -0.820601000E-01
      0.766000000         0.675641300    
      0.289000000         0.419548200    
 P   2  1.00
      0.289600000        -0.150108000    
      0.629000000E-01      1.06869260    
 P   1  1.00
      0.223000000E-01      1.00000000    
 D   3  1.00
       1.52300000         0.107484300    
      0.563400000         0.456395400    
      0.183400000         0.603985500    
 D   1  1.00
      0.569000000E-01      1.00000000    
 ZZZZ
 ZR  0
 S   3  1.00
       1.97600000        -0.920674500    
       1.15400000          1.09290330    
      0.391000000         0.679562100    
 S   4  1.00
       1.97600000         0.750606000    
       1.15400000        -0.981082600    
      0.391000000         -1.02251510    
      0.100100000          1.08600380    
 S   1  1.00
      0.334000000E-01      1.00000000    
 P   3  1.00
       4.19200000        -0.942554000E-01
      0.876400000         0.678367500    
      0.326300000         0.424661800    
 P   2  1.00
      0.297200000        -0.152740100    
      0.724000000E-01      1.07771850    
 P   1  1.00
      0.243000000E-01      1.00000000    
 D   3  1.00
       2.26900000         0.599741000E-01
      0.785500000         0.473415800    
      0.261500000         0.611717800    
 D   1  1.00
      0.802000000E-01      1.00000000    
 ZZZZ
 NB  0
 S   3  1.00
       2.18200000        -0.884614400    
       1.20900000          1.10337750    
      0.416500000         0.629877600    
 S   4  1.00
       2.18200000         0.779028700    
       1.20900000         -1.07527880    
      0.416500000         -1.15060140    
      0.145400000         0.996915500    
 S   1  1.00
      0.392000000E-01      1.00000000    
 P   3  1.00
       4.51900000        -0.817303000E-01
      0.940600000         0.699511500    
      0.349200000         0.398099600    
 P   2  1.00
      0.410600000        -0.121217600    
      0.752000000E-01      1.04804770    
 P   1  1.00
      0.247000000E-01      1.00000000    
 D   3  1.00
       3.46600000         0.315983000E-01
      0.993800000         0.483430600    
      0.335000000         0.616489300    
 D   1  1.00
      0.102400000          1.00000000    
 ZZZZ
 MO  0
 S   3  1.00
       2.36100000        -0.912176000    
       1.30900000          1.14774530    
      0.450000000         0.609710900    
 S   4  1.00
       2.36100000         0.813925900    
       1.30900000         -1.13600840    
      0.450000000         -1.16115920    
      0.168100000          1.00647860    
 S   1  1.00
      0.423000000E-01      1.00000000    
 P   3  1.00
       4.89500000        -0.908258000E-01
       1.04400000         0.704289900    
      0.387700000         0.397317900    
 P   2  1.00
      0.499500000        -0.108194500    
      0.780000000E-01      1.03680930    
 P   1  1.00
      0.247000000E-01      1.00000000    
 D   3  1.00
       2.99300000         0.527063000E-01
       1.06300000         0.500390700    
      0.372100000         0.579402400    
 D   1  1.00
      0.117800000          1.00000000    
 ZZZZ
 TC  0
 S   3  1.00
       2.34200000         -1.49117820    
       1.63400000          1.67490430    
      0.509400000         0.657300600    
 S   4  1.00
       2.34200000          1.35239970    
       1.63400000         -1.62163010    
      0.509400000         -1.14637700    
      0.170600000         0.985919000    
 S   1  1.00
      0.435000000E-01      1.00000000    
 P   3  1.00
       5.27800000        -0.995419000E-01
       1.15600000         0.708154400    
      0.430200000         0.397357100    
 P   2  1.00
      0.476700000        -0.973127000E-01
      0.895000000E-01      1.04048620    
 P   1  1.00
      0.246000000E-01      1.00000000    
 D   3  1.00
       4.63200000         0.268724000E-01
       1.27900000         0.507308900    
      0.442500000         0.591138100    
 D   1  1.00
      0.136400000          1.00000000    
 ZZZZ
 RU  0
 S   3  1.00
       2.56500000         -1.04310560    
       1.50800000          1.33147860    
      0.512900000         0.561306500    
 S   4  1.00
       2.56500000         0.877012800    
       1.50800000         -1.26346600    
      0.512900000        -0.838498700    
      0.136200000          1.06377730    
 S   1  1.00
      0.417000000E-01      1.00000000    
 P   3  1.00
       4.85900000        -0.945755000E-01
       1.21900000         0.743479800    
      0.441300000         0.366814400    
 P   2  1.00
      0.572500000        -0.880864000E-01
      0.830000000E-01      1.02839700    
 P   1  1.00
      0.250000000E-01      1.00000000    
 D   3  1.00
       4.19500000         0.485729000E-01
       1.37700000         0.510522300    
      0.482800000         0.573002800    
 D   1  1.00
      0.150100000          1.00000000    
 ZZZZ
 RH  0
 S   3  1.00
       2.64600000         -1.35540840    
       1.75100000          1.61122330    
      0.571300000         0.589381400    
 S   4  1.00
       2.64600000          1.14721370    
       1.75100000         -1.49435250    
      0.571300000        -0.858970400    
      0.143800000          1.02972410    
 S   1  1.00
      0.428000000E-01      1.00000000    
 P   3  1.00
       5.44000000        -0.987699000E-01
       1.32900000         0.743359500    
      0.484500000         0.366846200    
 P   2  1.00
      0.659500000        -0.838056000E-01
      0.869000000E-01      1.02448410    
 P   1  1.00
      0.257000000E-01      1.00000000    
 D   3  1.00
       3.66900000         0.760059000E-01
       1.42300000         0.515885200    
      0.509100000         0.543658500    
 D   1  1.00
      0.161000000          1.00000000    
 ZZZZ
 PD  0
 S   3  1.00
       2.78700000         -1.61023930    
       1.96500000          1.84898420    
      0.624300000         0.603749200    
 S   4  1.00
       2.78700000          1.35407750    
       1.96500000         -1.67808480    
      0.624300000        -0.855938100    
      0.149600000          1.02002990    
 S   1  1.00
      0.436000000E-01      1.00000000    
 P   3  1.00
       5.99900000        -0.103491000    
       1.44300000         0.745695200    
      0.526400000         0.365649400    
 P   2  1.00
      0.736800000         0.763285000E-01
      0.899000000E-01     0.974006500    
 P   1  1.00
      0.262000000E-01      1.00000000    
 D   3  1.00
       6.09100000         0.376146000E-01
       1.71900000         0.520047900    
      0.605600000         0.570607100    
 D   1  1.00
      0.188300000          1.00000000    
 ZZZZ
 AG  0
 S   3  1.00
       2.95000000         -1.79105640    
       2.14900000          2.02445700    
      0.668400000         0.607283900    
 S   4  1.00
       2.95000000          1.01411250    
       2.14900000         -1.24139710    
      0.668400000        -0.490142700    
      0.997000000E-01      1.11283750    
 S   1  1.00
      0.347000000E-01      1.00000000    
 P   3  1.00
       6.55300000        -0.107911700    
       1.56500000         0.740364500    
      0.574800000         0.372100800    
 P   2  1.00
      0.908500000        -0.418371000E-01
      0.833000000E-01      1.00875860    
 P   1  1.00
      0.252000000E-01      1.00000000    
 D   3  1.00
       3.39100000         0.139693800    
       1.59900000         0.474442100    
      0.628200000         0.515631100    
 D   1  1.00
      0.210800000          1.00000000    
 ZZZZ
 CS  0
 S   3  1.00
      0.870900000         -1.16047980    
      0.539300000          1.41985150    
      0.172400000         0.587290500    
 S   4  1.00
      0.870900000         0.823772700    
      0.539300000         -1.07091810    
      0.172400000        -0.908512200    
      0.393000000E-01      1.07740490    
 S   1  1.00
      0.151000000E-01      1.00000000    
 P   3  1.00
       1.46800000        -0.132103300    
      0.413400000         0.657241000    
      0.153600000         0.475645600    
 P   2  1.00
      0.145800000        -0.168055600    
      0.279000000E-01      1.06657460    
 P   1  1.00
      0.113000000E-01      1.00000000    
 ZZZZ
 BA  0
 S   3  1.00
      0.869900000         -2.25497470    
      0.667600000          2.51457860    
      0.198200000         0.577518400    
 S   4  1.00
      0.869900000          2.03913830    
      0.667600000         -2.37177120    
      0.198200000         -1.27580060    
      0.823000000E-01      1.17033460    
 S   1  1.00
      0.231000000E-01      1.00000000    
 P   3  1.00
       1.60500000        -0.162640300    
      0.479000000         0.697128900    
      0.181800000         0.450510700    
 P   2  1.00
      0.180400000        -0.264253700    
      0.476000000E-01      1.13472120    
 P   1  1.00
      0.192000000E-01      1.00000000    
 ZZZZ
 LA  0
 S   3  1.00
      0.916700000         -3.02407510    
      0.742700000          3.29714760    
      0.223700000         0.551354200    
 S   4  1.00
      0.916700000          2.69102430    
      0.742700000         -3.04743630    
      0.223700000         -1.10302110    
      0.792000000E-01      1.19418630    
 S   1  1.00
      0.239000000E-01      1.00000000    
 P   3  1.00
       1.55400000        -0.181716800    
      0.562200000         0.663222600    
      0.223900000         0.502978200    
 P   2  1.00
      0.212500000        -0.196881000    
      0.483000000E-01      1.09075420    
 P   1  1.00
      0.179000000E-01      1.00000000    
 D   2  1.00
      0.452400000         0.446805100    
      0.160200000         0.654349300    
 D   1  1.00
      0.531000000E-01      1.00000000    
 ZZZZ

