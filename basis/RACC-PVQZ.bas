
BASIS="aug-cc-pVQZ"
 H   0
 S   3  1.00
       82.6400000         0.200600000E-02
       12.4100000         0.153430000E-01
       2.82400000         0.755790000E-01
 S   1  1.00
      0.797700000          1.00000000    
 S   1  1.00
      0.258100000          1.00000000    
 S   1  1.00
      0.898900000E-01      1.00000000    
 P   1  1.00
       2.29200000          1.00000000    
 P   1  1.00
      0.838000000          1.00000000    
 P   1  1.00
      0.292000000          1.00000000    
 S   1  1.00
      0.236300000E-01      1.00000000    
 ZZZZ
 HE  0
 S   4  1.00
       528.500000         0.940000000E-03
       79.3100000         0.721400000E-02
       18.0500000         0.359750000E-01
       5.08500000         0.127782000    
 S   1  1.00
       1.60900000          1.00000000    
 S   1  1.00
      0.536300000          1.00000000    
 S   1  1.00
      0.183300000          1.00000000    
 P   1  1.00
       5.99400000          1.00000000    
 P   1  1.00
       1.74500000          1.00000000    
 P   1  1.00
      0.560000000          1.00000000    
 S   1  1.00
      0.481900000E-01      1.00000000    
 ZZZZ
 B   0
 S   9  1.00
       23870.0000         0.880000000E-04
       3575.00000         0.687000000E-03
       812.800000         0.360000000E-02
       229.700000         0.149490000E-01
       74.6900000         0.514350000E-01
       26.8100000         0.143302000    
       10.3200000         0.300935000    
       4.17800000         0.403526000    
       1.72700000         0.225340000    
 S   9  1.00
       23870.0000        -0.180000000E-04
       3575.00000        -0.139000000E-03
       812.800000        -0.725000000E-03
       229.700000        -0.306300000E-02
       74.6900000        -0.105810000E-01
       26.8100000        -0.313650000E-01
       10.3200000        -0.710120000E-01
       4.17800000        -0.132103000    
       1.72700000        -0.123072000    
 S   1  1.00
      0.470400000          1.00000000    
 S   1  1.00
      0.189600000          1.00000000    
 S   1  1.00
      0.739400000E-01      1.00000000    
 P   3  1.00
       22.2600000         0.509500000E-02
       5.05800000         0.332060000E-01
       1.48700000         0.132314000    
 P   1  1.00
      0.507100000          1.00000000    
 P   1  1.00
      0.181200000          1.00000000    
 P   1  1.00
      0.646300000E-01      1.00000000    
 D   1  1.00
       1.11000000          1.00000000    
 D   1  1.00
      0.402000000          1.00000000    
 D   1  1.00
      0.145000000          1.00000000    
 S   1  1.00
      0.272100000E-01      1.00000000    
 P   1  1.00
      0.187800000E-01      1.00000000    
 ZZZZ
 C   0
 S   9  1.00
       33980.0000         0.910000000E-04
       5089.00000         0.704000000E-03
       1157.00000         0.369300000E-02
       326.600000         0.153600000E-01
       106.100000         0.529290000E-01
       38.1100000         0.147043000    
       14.7500000         0.305631000    
       6.03500000         0.399345000    
       2.53000000         0.217051000    
 S   9  1.00
       33980.0000        -0.190000000E-04
       5089.00000        -0.151000000E-03
       1157.00000        -0.785000000E-03
       326.600000        -0.332400000E-02
       106.100000        -0.115120000E-01
       38.1100000        -0.341600000E-01
       14.7500000        -0.771730000E-01
       6.03500000        -0.141493000    
       2.53000000        -0.118019000    
 S   1  1.00
      0.735500000          1.00000000    
 S   1  1.00
      0.290500000          1.00000000    
 S   1  1.00
      0.111100000          1.00000000    
 P   3  1.00
       34.5100000         0.537800000E-02
       7.91500000         0.361320000E-01
       2.36800000         0.142493000    
 P   1  1.00
      0.813200000          1.00000000    
 P   1  1.00
      0.289000000          1.00000000    
 P   1  1.00
      0.100700000          1.00000000    
 D   1  1.00
       1.84800000          1.00000000    
 D   1  1.00
      0.649000000          1.00000000    
 D   1  1.00
      0.228000000          1.00000000    
 S   1  1.00
      0.414500000E-01      1.00000000    
 P   1  1.00
      0.321800000E-01      1.00000000    
 ZZZZ
 N   0
 S   9  1.00
       45840.0000         0.920000000E-04
       6868.00000         0.717000000E-03
       1563.00000         0.374900000E-02
       442.400000         0.155320000E-01
       144.300000         0.531460000E-01
       52.1800000         0.146787000    
       20.3400000         0.304663000    
       8.38100000         0.397684000    
       3.52900000         0.217641000    
 S   9  1.00
       45840.0000        -0.200000000E-04
       6868.00000        -0.159000000E-03
       1563.00000        -0.824000000E-03
       442.400000        -0.347800000E-02
       144.300000        -0.119660000E-01
       52.1800000        -0.353880000E-01
       20.3400000        -0.800770000E-01
       8.38100000        -0.146722000    
       3.52900000        -0.116360000    
 S   1  1.00
       1.05400000          1.00000000    
 S   1  1.00
      0.411800000          1.00000000    
 S   1  1.00
      0.155200000          1.00000000    
 P   3  1.00
       49.3300000         0.553300000E-02
       11.3700000         0.379620000E-01
       3.43500000         0.149028000    
 P   1  1.00
       1.18200000          1.00000000    
 P   1  1.00
      0.417300000          1.00000000    
 P   1  1.00
      0.142800000          1.00000000    
 D   1  1.00
       2.83700000          1.00000000    
 D   1  1.00
      0.968000000          1.00000000    
 D   1  1.00
      0.335000000          1.00000000    
 S   1  1.00
      0.546400000E-01      1.00000000    
 P   1  1.00
      0.440200000E-01      1.00000000    
 ZZZZ
 O   0
 S   9  1.00
       61420.0000         0.900000000E-04
       9199.00000         0.698000000E-03
       2091.00000         0.366400000E-02
       590.900000         0.152180000E-01
       192.300000         0.524230000E-01
       69.3200000         0.145921000    
       26.9700000         0.305258000    
       11.1000000         0.398508000    
       4.68200000         0.216980000    
 S   9  1.00
       61420.0000        -0.200000000E-04
       9199.00000        -0.159000000E-03
       2091.00000        -0.829000000E-03
       590.900000        -0.350800000E-02
       192.300000        -0.121560000E-01
       69.3200000        -0.362610000E-01
       26.9700000        -0.829920000E-01
       11.1000000        -0.152090000    
       4.68200000        -0.115331000    
 S   1  1.00
       1.42800000          1.00000000    
 S   1  1.00
      0.554700000          1.00000000    
 S   1  1.00
      0.206700000          1.00000000    
 P   3  1.00
       63.4200000         0.604400000E-02
       14.6600000         0.417990000E-01
       4.45900000         0.161143000    
 P   1  1.00
       1.53100000          1.00000000    
 P   1  1.00
      0.530200000          1.00000000    
 P   1  1.00
      0.175000000          1.00000000    
 D   1  1.00
       3.77500000          1.00000000    
 D   1  1.00
       1.30000000          1.00000000    
 D   1  1.00
      0.444000000          1.00000000    
 S   1  1.00
      0.695900000E-01      1.00000000    
 P   1  1.00
      0.534800000E-01      1.00000000    
 ZZZZ
 F   0
 S   9  1.00
       74530.0000         0.950000000E-04
       11170.0000         0.738000000E-03
       2543.00000         0.385800000E-02
       721.000000         0.159260000E-01
       235.900000         0.542890000E-01
       85.6000000         0.149513000    
       33.5500000         0.308252000    
       13.9300000         0.394853000    
       5.91500000         0.211031000    
 S   9  1.00
       74530.0000        -0.220000000E-04
       11170.0000        -0.172000000E-03
       2543.00000        -0.891000000E-03
       721.000000        -0.374800000E-02
       235.900000        -0.128620000E-01
       85.6000000        -0.380610000E-01
       33.5500000        -0.862390000E-01
       13.9300000        -0.155865000    
       5.91500000        -0.110914000    
 S   1  1.00
       1.84300000          1.00000000    
 S   1  1.00
      0.712400000          1.00000000    
 S   1  1.00
      0.263700000          1.00000000    
 P   3  1.00
       80.3900000         0.634700000E-02
       18.6300000         0.442040000E-01
       5.69400000         0.168514000    
 P   1  1.00
       1.95300000          1.00000000    
 P   1  1.00
      0.670200000          1.00000000    
 P   1  1.00
      0.216600000          1.00000000    
 D   1  1.00
       5.01400000          1.00000000    
 D   1  1.00
       1.72500000          1.00000000    
 D   1  1.00
      0.586000000          1.00000000    
 S   1  1.00
      0.859400000E-01      1.00000000    
 P   1  1.00
      0.656800000E-01      1.00000000    
 ZZZZ
 NE  0
 S   9  1.00
       99920.0000         0.860000000E-04
       14960.0000         0.669000000E-03
       3399.00000         0.351800000E-02
       958.900000         0.146670000E-01
       311.200000         0.509620000E-01
       111.700000         0.143744000    
       43.3200000         0.304562000    
       17.8000000         0.400105000    
       7.50300000         0.218644000    
 S   9  1.00
       99920.0000        -0.200000000E-04
       14960.0000        -0.158000000E-03
       3399.00000        -0.824000000E-03
       958.900000        -0.350000000E-02
       311.200000        -0.122330000E-01
       111.700000        -0.370170000E-01
       43.3200000        -0.861130000E-01
       17.8000000        -0.158381000    
       7.50300000        -0.114288000    
 S   1  1.00
       2.33700000          1.00000000    
 S   1  1.00
      0.900100000          1.00000000    
 S   1  1.00
      0.330100000          1.00000000    
 P   3  1.00
       99.6800000         0.656600000E-02
       23.1500000         0.459790000E-01
       7.10800000         0.173419000    
 P   1  1.00
       2.44100000          1.00000000    
 P   1  1.00
      0.833900000          1.00000000    
 P   1  1.00
      0.266200000          1.00000000    
 D   1  1.00
       6.47100000          1.00000000    
 D   1  1.00
       2.21300000          1.00000000    
 D   1  1.00
      0.747000000          1.00000000    
 S   1  1.00
      0.105400000          1.00000000    
 P   1  1.00
      0.817800000E-01      1.00000000    
 ZZZZ
NA     0 
S     16     1.00 
        1224000.0000000             0.478894D-05     
        183200.0000000              0.372395D-04     
         41700.0000000              0.195831D-03     
         11810.0000000              0.826698D-03     
          3853.0000000              0.300251D-02     
          1391.0000000              0.970310D-02     
           542.5000000              0.282337D-01     
           224.9000000              0.732058D-01     
            97.9300000              0.162897D+00     
            44.3100000              0.288708D+00     
            20.6500000              0.346829D+00     
             9.7290000              0.206865D+00     
             4.2280000              0.328009D-01     
             1.9690000             -0.647736D-03     
             0.8890000              0.145878D-02     
             0.3964000             -0.178346D-03     
S     16     1.00 
        1224000.0000000            -0.116958D-05     
        183200.0000000             -0.909110D-05     
         41700.0000000             -0.478499D-04     
         11810.0000000             -0.201962D-03     
          3853.0000000             -0.735837D-03     
          1391.0000000             -0.238746D-02     
           542.5000000             -0.704969D-02     
           224.9000000             -0.187856D-01     
            97.9300000             -0.446153D-01     
            44.3100000             -0.897741D-01     
            20.6500000             -0.142940D+00     
             9.7290000             -0.124315D+00     
             4.2280000              0.999648D-01     
             1.9690000              0.417080D+00     
             0.8890000              0.475123D+00     
             0.3964000              0.163268D+00     
S     16     1.00 
        1224000.0000000             0.175871D-06     
        183200.0000000              0.136594D-05     
         41700.0000000              0.719795D-05     
         11810.0000000              0.303349D-04     
          3853.0000000              0.110752D-03     
          1391.0000000              0.358596D-03     
           542.5000000              0.106272D-02     
           224.9000000              0.282687D-02     
            97.9300000              0.676742D-02     
            44.3100000              0.136480D-01     
            20.6500000              0.222814D-01     
             9.7290000              0.196011D-01     
             4.2280000             -0.167708D-01     
             1.9690000             -0.773734D-01     
             0.8890000             -0.113501D+00     
             0.3964000             -0.139130D+00     
S     1     1.00 
             0.0699300              1.0000000        
S     1     1.00 
             0.0328900              1.0000000        
S     1     1.00 
             0.0161200              1.0000000        
P     9     1.00 
           413.4000000              0.908196D-03     
            97.9800000              0.741773D-02     
            31.3700000              0.357464D-01     
            11.6200000              0.118520D+00     
             4.6710000              0.261403D+00     
             1.9180000              0.378395D+00     
             0.7775000              0.334632D+00     
             0.3013000              0.126844D+00     
             0.2275000             -0.147117D-01     
P     9     1.00 
           413.4000000             -0.901741D-04     
            97.9800000             -0.739342D-03     
            31.3700000             -0.357309D-02     
            11.6200000             -0.120142D-01     
             4.6710000             -0.267178D-01     
             1.9180000             -0.392753D-01     
             0.7775000             -0.376083D-01     
             0.3013000             -0.433228D-01     
             0.2275000              0.518003D-01     
P     1     1.00 
             0.0752700              1.0000000        
P     1     1.00 
             0.0312600              1.0000000        
P     1     1.00 
             0.0134200              1.0000000        
D     1     1.00 
             0.1538000              1.0000000        
D     1     1.00 
             0.0865000              1.0000000        
D     1     1.00 
             0.0487000              1.0000000        
S     1     1.00 
             0.0050300              1.0000000        
P     1     1.00 
             0.0077200              1.0000000        
ZZZZ
 AL  0
 S  13  1.00
       419600.000         0.278219000E-04
       62830.0000         0.216330000E-03
       14290.0000         0.113754000E-02
       4038.00000         0.479635000E-02
       1312.00000         0.172389000E-01
       470.500000         0.538066000E-01
       181.800000         0.141326000    
       74.4600000         0.289268000    
       31.9000000         0.384825000    
       13.9600000         0.232852000    
       5.18000000         0.293330000E-01
       2.26500000        -0.300574000E-02
      0.966400000         0.166673000E-02
 S  13  1.00
       419600.000        -0.723754000E-05
       62830.0000        -0.561733000E-04
       14290.0000        -0.296528000E-03
       4038.00000        -0.124913000E-02
       1312.00000        -0.455101000E-02
       470.500000        -0.144393000E-01
       181.800000        -0.403464000E-01
       74.4600000        -0.922618000E-01
       31.9000000        -0.164510000    
       13.9600000        -0.141296000    
       5.18000000         0.195365000    
       2.26500000         0.572475000    
      0.966400000         0.374041000    
 S  13  1.00
       419600.000         0.167150000E-05
       62830.0000         0.129641000E-04
       14290.0000         0.685101000E-04
       4038.00000         0.288274000E-03
       1312.00000         0.105276000E-02
       470.500000         0.333878000E-02
       181.800000         0.939217000E-02
       74.4600000         0.216047000E-01
       31.9000000         0.395873000E-01
       13.9600000         0.349180000E-01
       5.18000000        -0.528415000E-01
       2.26500000        -0.191878000    
      0.966400000        -0.254115000    
 S   1  1.00
      0.244700000          1.00000000    
 S   1  1.00
      0.118400000          1.00000000    
 S   1  1.00
      0.502100000E-01      1.00000000    
 P   8  1.00
       891.300000         0.491755000E-03
       211.300000         0.415843000E-02
       68.2800000         0.212538000E-01
       25.7000000         0.764058000E-01
       10.6300000         0.194277000    
       4.60200000         0.334428000    
       2.01500000         0.375026000    
      0.870600000         0.204041000    
 P   8  1.00
       891.300000        -0.888695000E-04
       211.300000        -0.745823000E-03
       68.2800000        -0.387025000E-02
       25.7000000        -0.139350000E-01
       10.6300000        -0.366860000E-01
       4.60200000        -0.627797000E-01
       2.01500000        -0.789602000E-01
      0.870600000        -0.288589000E-01
 P   1  1.00
      0.297200000          1.00000000    
 P   1  1.00
      0.110000000          1.00000000    
 P   1  1.00
      0.398900000E-01      1.00000000    
 D   1  1.00
      0.804000000E-01      1.00000000    
 D   1  1.00
      0.199000000          1.00000000    
 D   1  1.00
      0.494000000          1.00000000    
 S   1  1.00
      0.183000000E-01      1.00000000    
 P   1  1.00
      0.121000000E-01      1.00000000    
 ZZZZ
 SI  0
 S  13  1.00
       513000.000         0.260920000E-04
       76820.0000         0.202905000E-03
       17470.0000         0.106715000E-02
       4935.00000         0.450597000E-02
       1602.00000         0.162359000E-01
       574.100000         0.508913000E-01
       221.500000         0.135155000    
       90.5400000         0.281292000    
       38.7400000         0.385336000    
       16.9500000         0.245651000    
       6.45200000         0.343145000E-01
       2.87400000        -0.334884000E-02
       1.25000000         0.187625000E-02
 S  13  1.00
       513000.000        -0.694880000E-05
       76820.0000        -0.539641000E-04
       17470.0000        -0.284716000E-03
       4935.00000        -0.120203000E-02
       1602.00000        -0.438397000E-02
       574.100000        -0.139776000E-01
       221.500000        -0.393516000E-01
       90.5400000        -0.914283000E-01
       38.7400000        -0.165609000    
       16.9500000        -0.152505000    
       6.45200000         0.168524000    
       2.87400000         0.569284000    
       1.25000000         0.398056000    
 S  13  1.00
       513000.000         0.178068000E-05
       76820.0000         0.138148000E-04
       17470.0000         0.730005000E-04
       4935.00000         0.307666000E-03
       1602.00000         0.112563000E-02
       574.100000         0.358435000E-02
       221.500000         0.101728000E-01
       90.5400000         0.237520000E-01
       38.7400000         0.443483000E-01
       16.9500000         0.419041000E-01
       6.45200000        -0.502504000E-01
       2.87400000        -0.216578000    
       1.25000000        -0.286448000    
 S   1  1.00
      0.359900000          1.00000000    
 S   1  1.00
      0.169900000          1.00000000    
 S   1  1.00
      0.706600000E-01      1.00000000    
 P   8  1.00
       1122.00000         0.448143000E-03
       266.000000         0.381639000E-02
       85.9200000         0.198105000E-01
       32.3300000         0.727017000E-01
       13.3700000         0.189839000    
       5.80000000         0.335672000    
       2.55900000         0.379365000    
       1.12400000         0.201193000    
 P   8  1.00
       1122.00000        -0.964883000E-04
       266.000000        -0.811971000E-03
       85.9200000        -0.430087000E-02
       32.3300000        -0.157502000E-01
       13.3700000        -0.429541000E-01
       5.80000000        -0.752574000E-01
       2.55900000        -0.971446000E-01
       1.12400000        -0.227507000E-01
 P   1  1.00
      0.398800000          1.00000000    
 P   1  1.00
      0.153300000          1.00000000    
 P   1  1.00
      0.572800000E-01      1.00000000    
 D   1  1.00
      0.120000000          1.00000000    
 D   1  1.00
      0.302000000          1.00000000    
 D   1  1.00
      0.760000000          1.00000000    
 S   1  1.00
      0.275000000E-01      1.00000000    
 P   1  1.00
      0.200000000E-01      1.00000000    
 ZZZZ
 P   0
 S  13  1.00
       615200.000         0.247450000E-04
       92120.0000         0.192465000E-03
       20950.0000         0.101202000E-02
       5920.00000         0.427261000E-02
       1922.00000         0.154161000E-01
       688.000000         0.485976000E-01
       265.000000         0.130060000    
       108.200000         0.274514000    
       46.2200000         0.385402000    
       20.2300000         0.255934000    
       7.85900000         0.391237000E-01
       3.54700000        -0.368010000E-02
       1.56400000         0.208211000E-02
 S  13  1.00
       615200.000        -0.672205000E-05
       92120.0000        -0.522311000E-04
       20950.0000        -0.275361000E-03
       5920.00000        -0.116307000E-02
       1922.00000        -0.424281000E-02
       688.000000        -0.136114000E-01
       265.000000        -0.385114000E-01
       108.200000        -0.906643000E-01
       46.2200000        -0.166584000    
       20.2300000        -0.161447000    
       7.85900000         0.146781000    
       3.54700000         0.566682000    
       1.56400000         0.416433000    
 S  13  1.00
       615200.000         0.184740000E-05
       92120.0000         0.143380000E-04
       20950.0000         0.757228000E-04
       5920.00000         0.319205000E-03
       1922.00000         0.116851000E-02
       688.000000         0.374267000E-02
       265.000000         0.106817000E-01
       108.200000         0.252657000E-01
       46.2200000         0.479283000E-01
       20.2300000         0.477096000E-01
       7.85900000        -0.466525000E-01
       3.54700000        -0.234968000    
       1.56400000        -0.311337000    
 S   1  1.00
      0.488800000          1.00000000    
 S   1  1.00
      0.226600000          1.00000000    
 S   1  1.00
      0.933100000E-01      1.00000000    
 P   8  1.00
       1367.00000         0.421015000E-03
       324.000000         0.360985000E-02
       104.600000         0.189217000E-01
       39.3700000         0.705560000E-01
       16.2600000         0.188157000    
       7.05600000         0.338709000    
       3.13000000         0.381943000    
       1.39400000         0.195261000    
 P   8  1.00
       1367.00000        -0.100827000E-03
       324.000000        -0.854499000E-03
       104.600000        -0.457116000E-02
       39.3700000        -0.170327000E-01
       16.2600000        -0.475204000E-01
       7.05600000        -0.852786000E-01
       3.13000000        -0.109676000    
       1.39400000        -0.161181000E-01
 P   1  1.00
      0.517900000          1.00000000    
 P   1  1.00
      0.203200000          1.00000000    
 P   1  1.00
      0.769800000E-01      1.00000000    
 D   1  1.00
      0.165000000          1.00000000    
 D   1  1.00
      0.413000000          1.00000000    
 D   1  1.00
       1.03600000          1.00000000    
 S   1  1.00
      0.354000000E-01      1.00000000    
 P   1  1.00
      0.272000000E-01      1.00000000    
 ZZZZ
 S   0
 S  13  1.00
       727800.000         0.236025000E-04
       109000.000         0.183482000E-03
       24800.0000         0.964278000E-03
       7014.00000         0.406537000E-02
       2278.00000         0.146973000E-01
       814.700000         0.465081000E-01
       313.400000         0.125508000    
       127.700000         0.268433000    
       54.4800000         0.384809000    
       23.8500000         0.265372000    
       9.42800000         0.437326000E-01
       4.29000000        -0.378807000E-02
       1.90900000         0.218083000E-02
 S  13  1.00
       727800.000        -0.652179000E-05
       109000.000        -0.506631000E-04
       24800.0000        -0.266833000E-03
       7014.00000        -0.112601000E-02
       2278.00000        -0.411186000E-02
       814.700000        -0.132454000E-01
       313.400000        -0.377004000E-01
       127.700000        -0.898554000E-01
       54.4800000        -0.167098000    
       23.8500000        -0.169354000    
       9.42800000         0.127824000    
       4.29000000         0.564862000    
       1.90900000         0.431767000    
 S  13  1.00
       727800.000         0.189406000E-05
       109000.000         0.146948000E-04
       24800.0000         0.775460000E-04
       7014.00000         0.326509000E-03
       2278.00000         0.119686000E-02
       814.700000         0.384799000E-02
       313.400000         0.110539000E-01
       127.700000         0.264645000E-01
       54.4800000         0.508771000E-01
       23.8500000         0.530030000E-01
       9.42800000        -0.425518000E-01
       4.29000000        -0.250853000    
       1.90900000        -0.333152000    
 S   1  1.00
      0.627000000          1.00000000    
 S   1  1.00
      0.287300000          1.00000000    
 S   1  1.00
      0.117200000          1.00000000    
 P   8  1.00
       1546.00000         0.441183000E-03
       366.400000         0.377571000E-02
       118.400000         0.198360000E-01
       44.5300000         0.742063000E-01
       18.3800000         0.197327000    
       7.96500000         0.351851000    
       3.54100000         0.378687000    
       1.59100000         0.170931000    
 P   8  1.00
       1546.00000        -0.113110000E-03
       366.400000        -0.958581000E-03
       118.400000        -0.513471000E-02
       44.5300000        -0.192641000E-01
       18.3800000        -0.535980000E-01
       7.96500000        -0.960333000E-01
       3.54100000        -0.118183000    
       1.59100000         0.923194000E-02
 P   1  1.00
      0.620500000          1.00000000    
 P   1  1.00
      0.242000000          1.00000000    
 P   1  1.00
      0.901400000E-01      1.00000000    
 D   1  1.00
      0.203000000          1.00000000    
 D   1  1.00
      0.504000000          1.00000000    
 D   1  1.00
       1.25000000          1.00000000    
 S   1  1.00
      0.428000000E-01      1.00000000    
 P   1  1.00
      0.317000000E-01      1.00000000    
 ZZZZ
 CL  0
 S  13  1.00
       834900.000         0.231688000E-04
       125000.000         0.180154000E-03
       28430.0000         0.947782000E-03
       8033.00000         0.400139000E-02
       2608.00000         0.144629000E-01
       933.900000         0.456586000E-01
       360.000000         0.123248000    
       147.000000         0.264369000    
       62.8800000         0.382989000    
       27.6000000         0.270934000    
       11.0800000         0.471404000E-01
       5.07500000        -0.371766000E-02
       2.27800000         0.219158000E-02
 S  13  1.00
       834900.000        -0.649649000E-05
       125000.000        -0.504895000E-04
       28430.0000        -0.266113000E-03
       8033.00000        -0.112499000E-02
       2608.00000        -0.410497000E-02
       933.900000        -0.131987000E-01
       360.000000        -0.375342000E-01
       147.000000        -0.897233000E-01
       62.8800000        -0.167671000    
       27.6000000        -0.174763000    
       11.0800000         0.114909000    
       5.07500000         0.563618000    
       2.27800000         0.441606000    
 S  13  1.00
       834900.000         0.196645000E-05
       125000.000         0.152620000E-04
       28430.0000         0.806086000E-04
       8033.00000         0.339960000E-03
       2608.00000         0.124551000E-02
       933.900000         0.399612000E-02
       360.000000         0.114751000E-01
       147.000000         0.275504000E-01
       62.8800000         0.532917000E-01
       27.6000000         0.571246000E-01
       11.0800000        -0.395201000E-01
       5.07500000        -0.264343000    
       2.27800000        -0.349291000    
 S   1  1.00
      0.777500000          1.00000000    
 S   1  1.00
      0.352700000          1.00000000    
 S   1  1.00
      0.143100000          1.00000000    
 P   8  1.00
       1703.00000         0.474039000E-03
       403.600000         0.406412000E-02
       130.300000         0.213355000E-01
       49.0500000         0.794611000E-01
       20.2600000         0.208927000    
       8.78700000         0.364945000    
       3.91900000         0.371725000    
       1.76500000         0.146292000    
 P   8  1.00
       1703.00000        -0.128266000E-03
       403.600000        -0.109356000E-02
       130.300000        -0.583429000E-02
       49.0500000        -0.219258000E-01
       20.2600000        -0.601385000E-01
       8.78700000        -0.106929000    
       3.91900000        -0.122454000    
       1.76500000         0.383619000E-01
 P   1  1.00
      0.720700000          1.00000000    
 P   1  1.00
      0.283900000          1.00000000    
 P   1  1.00
      0.106000000          1.00000000    
 D   1  1.00
      0.254000000          1.00000000    
 D   1  1.00
      0.628000000          1.00000000    
 D   1  1.00
       1.55100000          1.00000000    
 S   1  1.00
      0.519000000E-01      1.00000000    
 P   1  1.00
      0.376000000E-01      1.00000000    
 ZZZZ
 AR  0
 S  13  1.00
       950600.000         0.227545000E-04
       142300.000         0.176945000E-03
       32360.0000         0.931282000E-03
       9145.00000         0.392860000E-02
       2970.00000         0.142064000E-01
       1064.00000         0.448114000E-01
       410.800000         0.121001000    
       168.000000         0.260579000    
       71.9900000         0.381364000    
       31.6700000         0.276058000    
       12.8900000         0.505179000E-01
       5.92900000        -0.359866000E-02
       2.67800000         0.218798000E-02
 S  13  1.00
       950600.000        -0.646201000E-05
       142300.000        -0.502346000E-04
       32360.0000        -0.264804000E-03
       9145.00000        -0.111895000E-02
       2970.00000        -0.408276000E-02
       1064.00000        -0.131216000E-01
       410.800000        -0.372855000E-01
       168.000000        -0.894709000E-01
       71.9900000        -0.168054000    
       31.6700000        -0.179594000    
       12.8900000         0.102953000    
       5.92900000         0.562630000    
       2.67800000         0.450355000    
 S  13  1.00
       950600.000         0.202056000E-05
       142300.000         0.156851000E-04
       32360.0000         0.828617000E-04
       9145.00000         0.349264000E-03
       2970.00000         0.127976000E-02
       1064.00000         0.410365000E-02
       410.800000         0.117789000E-01
       168.000000         0.283868000E-01
       71.9900000         0.552406000E-01
       31.6700000         0.607492000E-01
       12.8900000        -0.362012000E-01
       5.92900000        -0.275398000    
       2.67800000        -0.362845000    
 S   1  1.00
      0.941600000          1.00000000    
 S   1  1.00
      0.423900000          1.00000000    
 S   1  1.00
      0.171400000          1.00000000    
 P   8  1.00
       1890.00000         0.495752000E-03
       447.800000         0.425172000E-02
       144.600000         0.223277000E-01
       54.4600000         0.830878000E-01
       22.5100000         0.217110000    
       9.77400000         0.374507000    
       4.36800000         0.366445000    
       1.95900000         0.129245000    
 P   8  1.00
       1890.00000        -0.138863000E-03
       447.800000        -0.118870000E-02
       144.600000        -0.632553000E-02
       54.4600000        -0.238813000E-01
       22.5100000        -0.649238000E-01
       9.77400000        -0.115444000    
       4.36800000        -0.123651000    
       1.95900000         0.649055000E-01
 P   1  1.00
      0.826000000          1.00000000    
 P   1  1.00
      0.329700000          1.00000000    
 P   1  1.00
      0.124200000          1.00000000    
 D   1  1.00
      0.311000000          1.00000000    
 D   1  1.00
      0.763000000          1.00000000    
 D   1  1.00
       1.87300000          1.00000000    
 S   1  1.00
      0.610000000E-01      1.00000000    
 P   1  1.00
      0.435000000E-01      1.00000000    
 ZZZZ

----------------------------------------------------------------------------
Elements supported

H He B C N O F Ne Al Si P S Cl Ar Ga Ge As Se Br Kr

----------------------------------------------------------------------------

