if ( `grep 'SEPDADIR' ~/.bashrc | wc -l` != "0") then
	sed -n -i".bak" '/SEPDADIR/{n;x;d};x;1d;p;${x;p;}' ~/.bashrc
	sed -i".bak" '/SEPDABASIS/d' ~/.bashrc
	sed -i".bak" '/SEPDABIN/d' ~/.bashrc
	rm ~/.bashrc.bak
endif

echo "export SEPDADIR=`pwd`" >> ~/.bashrc
echo 'export SEPDABASIS=$SEPDADIR/basis' >> ~/.bashrc
echo 'export SEPDABIN=$SEPDADIR/bin' >> ~/.bashrc
echo 'alias sepda="csh $SEPDADIR/sepda.csh"' >> ~/.bashrc
echo '' >> ~/.bashrc

if ( `grep 'SEPDADIR' ~/.cshrc | wc -l` != "0") then
	sed -n -i".bak" '/SEPDADIR/{n;x;d};x;1d;p;${x;p;}' ~/.cshrc
	sed -i".bak" '/SEPDADIR/d' ~/.cshrc
	sed -i".bak" '/SEPDABASIS/d' ~/.cshrc
	sed -i".bak" '/SEPDABIN/d' ~/.cshrc
	rm ~/.cshrc.bak
endif

echo "setenv SEPDADIR `pwd`" >> ~/.cshrc
echo 'setenv SEPDABASIS $SEPDADIR/basis' >> ~/.cshrc
echo 'setenv SEPDABIN $SEPDADIR/bin' >> ~/.cshrc
echo 'alias sepda "csh $SEPDADIR/sepda.csh"' >> ~/.cshrc
echo '' >> ~/.cshrc

ifort -o bin/ipos.out -warn nousage f90/int/posInt.f90 f90/int/iposRR.f90
ifort -o bin/imom.out -warn nousage f90/int/momInt.f90 f90/int/imomRR.f90
ifort -o bin/ipm.out -warn nousage f90/int/posmomInt.f90 f90/int/iposmomRR.f90
ifort -o bin/epos.out -warn nousage f90/ext/posExt.f90 f90/ext/eposRR.f90
ifort -o bin/emom.out -warn nousage f90/ext/momExt.f90 f90/ext/emomRR.f90
ifort -o bin/evpos.out -warn nousage f90/ext/posvExt.f90 f90/ext/evposRR.f90

