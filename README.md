# README for SEPDA v 1.0 #

Please refer to the User's manual for more detailed instructions on installation
and program use. First and foremost, the user should clone the repository to their
local machine.

After cloning the repository and prior to use, SEPDA must be installed using the
provided installation script as follows:

        csh install.csh

Requirements for the use of the program include:

        An operating system with a UNIX-based command line
        Python version 2.x
        Fortran 90 compiler

Following installation, jobs can be run using the following command

        sepda infile.inp outfile.out

OR

        sepda infile.inp

This will read input variables from infile.inp and in the first instance, output
to outfile.out, while in the second instance, output will be directed to
infile.dat. For detailed information regarding the content of the input file,
please refer to Chapter 3 of the User's manual.
